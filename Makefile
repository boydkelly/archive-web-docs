french:
	po4a posts-fr2en.cfg
	po4a slides-dyu.cfg
	po4a slides-en.cfg
	git add -f l10n/pot/*/*.pot
	git add -f l10n/po/*/*.po
	git diff --quiet --exit-code || git commit --quiet -a -m "update on: $(date)"
	poedit&

english:
	po4a posts-en2fr.cfg
	git add -f l10n/pot/*/*.pot
	git add -f l10n/po/*/*.po
	git diff --quiet --exit-code || git commit --quiet -a -m "update on: $(date)"
	poedit&


date := $(shell date -Ih)
publish:
	echo $(date)
	git diff --quiet --exit-code || git commit --quiet -a -m "published on: $(date)"
	git push origin main --quiet

