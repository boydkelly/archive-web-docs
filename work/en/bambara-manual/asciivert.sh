#!/usr/bin/bash
#[ -z "$1" ] && { echo "You need to specify a pdf file "; exit 1; }
from=${1%.*}

#for x in "$from.pdf"; do
for x in `ls *.pdf` ; do
  echo $x
  #echo ${x%.*}.txt
  #pdftotext -eol unix -layout -nopgbrk "$x" "${x%.*}.txt"
  pdftotext -eol unix -layout -nopgbrk "$x" "${x%.*}.adoc"
  # insert a title at the first row
  sed -i "1 i\= ${x%.*}" "${x%.*}.adoc" 
  # I think this is already numbered lists to a .
  sed -i 's/^ *[0-9]\{1,2\}\. */\. /g' "${x%.*}.adoc"
  # any roman numerals replace with ==
sed -i 's/^ *[IVXLC]\{1,\}\. */=== /g' "${x%.*}.adoc"
sed -i 's/^ *[ABC]\.[0-9]\. */== /g' "${x%.*}.adoc"
sed -i 's/^ *[ABC]\. */== /g' "${x%.*}.adoc"
#sed -i 's/^\. */\. /g' "${x%.*}.adoc"
#replace weird unicode with *
sed -i 's/^ *• */* /g' "${x%.*}.adoc"
#replace hyphen with * 
sed -i 's/^ *- */* /g' "${x%.*}.adoc"
done;

#pandoc -f html -t asciidoctor -o $from.html  
#pandoc -f text -t asciidoctor -o $from.txt.adoc  
#asciidoctor -b docbook -a leveloffset=+1 -o - foo.adoc | pandoc --atx-headers --wrap=preserve -t gfm -f docbook - > foo.md
