#!/usr/bin/bash
set -o errexit
set -o nounset
set -o pipefail

function validate {
  csvclean -n -v -t $1
  awk 'BEGIN{FS=","} !n{n=NF}n!=NF{rc=1;print "Error line " NR " " NF " fields"; exit rc} END{print NF " fields" }' $1
  #delete missing field 1
  sed -i /^,/d $1 
  #remove any defs with discontined verbs for now
  sed -i /[...]/d $1
}

files=files
pages=pages
all="/home/bkelly/dev/jula/dyu-xdxf/dyu-refs.csv"
stop="stop.dyu.txt"

validate $all

#for x in "*.adoc" ; do
#  echo $x
##cat ${$x} | sed -e "$(sed 's:.*:s/^&$//ig:' $input)"
#cat $input | awk -F, '{print $2}{sed -e "s/$1/test/p" $x}'
#echo
#done

cp $files/* ./$pages/
cat ${all} | sed -e "$(sed 's:.*:s/^&,.*$//i:' $stop)" | sed 's/[ \t]*$//' | sed /^$/d > input.csv

#| sort -u | shuf | awk -v f=$udhrstats 'BEGIN {OFS=","} NR==1, NR==10 {print ". " $1  >> f}'
while IFS= read -r -d '' filepath
do
  pagepath=./pages/${filepath#./files/}
  while IFS="," read -r word id ;
  do
    sed -i -r -e  "/^image:|^:.*/! s/( $word )/ xref:fr@ROOT:lexique-dyu.adoc#$id\[$word] /i" $pagepath;
  done < input.csv
done <   <(find ./$files/ -maxdepth 1 -name "*.adoc" -print0)
rm input.csv
git commit -a -m $0 && git push

