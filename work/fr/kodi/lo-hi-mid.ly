\version "2.21.6"
{
    \omit Staff.TimeSignature
    \omit Staff.Clef
    \omit Stem
  f' e'' c''
}
%{  \override Staff.Clef.color = #white
\override Staff.Clef.layer = #-1
%}
