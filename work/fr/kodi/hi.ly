\version "2.21.6"
{
    \omit Staff.TimeSignature
    \omit Staff.Clef
    \omit Stem
   \hideNotes
   f' 
   \unHideNotes
   e''
   \hideNotes
   c''
}
%{  \override Staff.Clef.color = #white
\override Staff.Clef.layer = #-1
%}
