#/usr/bin/bash
#This may be outdated by the script in Obsidian, but may need the tsv here
#check this later
project=proverbs
for x in tmp tsv md; do export "${x}=${project}.$x"; done

rm -f $tsv $tmp 

while IFS= read -r -d '' file
do
asciidoctor -s -e -a skip-front-matter -b docbook $file -o - \
  | pandoc -f docbook -t org | sed -r '/:.*:|^\*/d' \
  | sed -e 's/ \+/ /g' \
  | sed -e '/^$/d' \
  | sed -e '/^#+B/,/^#+E/d' >> $tmp

done <   <(find ./files -name "*.adoc"  -print0)

cat $tmp | sort -t '|' -k 2 \
         | sed -r -e 's/^\| | \|$//g' \
         | sed -e 's/ | /\t/' > $tsv

echo -e "---" > $md
echo -e "date: $(date -I)" >> $md
echo -e "\n\n---\n" >> $md
echo -e "Jula|French"  >> $md
echo -e "------------ | ------------" >> $md

cat $tmp | sed -r -e 's/^\| | \|$//g' \
         | sort -t '|' -k 1 >> $md && rm $tmp

cp $md ~/Documents/Jula/Obsidian/

 #awk -F "\t" 'BEGIN{OFS=|} '
 #remove headers
 #remove double spacing
 #remove blank lines
 #remove doc with #+BEGIN etc
 #fix ra/la
 #vix Ny > Ɲ
