#!/usr/bin/bash
#This may be outdated by the script in Obsidian, but may need the tsv here
#check this later
partial="/home/bkelly/dev/web-docs/docs/fr/modules/proverbs/partials/"
static_dev="/home/bkelly/dev/dev-build/supp_ui/"
static_main="/home/bkelly/dev/dyu-build/supp_ui/"
slides="/home/bkelly/dev/jula/slides-dyu/"
lexicon="/home/bkelly/dev/jula/dyu-xdxf/"
project=proverbs
for x in tmp tsv md; do export "${x}=${project}.$x"; done

while IFS= read -r -d '' file
do
cat $file \
  | sed -e '/^|/!d' | sed -e '/^|=/d' \
  | sed -r -e 's/^\||\|$//' \
  | sed -e 's/|/\t/' \
  | sed -e '/^$/d' >> $tsv.~

cat $file \
  | sed -e '/^|/!d' | sed -e '/^|=/d' \
  | sed -r -e 's/^\||\|$//' \
  | sed -e '/^$/d' >> $md.~

done <   <(find ./files -name "*.adoc"  -print0)

sort -t $'\t' -k 2  $tsv.~ > $tsv
echo -e "------------ | ------------" > $md
sort -t '|' -k 1  $md.~ >> $md

rm *.~

echo $(wc -l $tsv) proverbs

for x in $partial $static_dev $static_main $slides $lexicon ; do cp -v $tsv $x$tsv ; done

cp $md ~/Documents/Jula/Obsidian/
