------------ | ------------
A bɛ fɛntigiw ɲana ko fangantanw bɛ tugura le.|Le riches pensent que les pauvres font exprès.
« A bla ! » « N’ta bla ! » Tasuma tɛ.|« Laisse! » « Je ne laisse pas! » Ce n'est pas du feu!
A flɛ, a flɛ, n’i ma ye, n’a turura i ɲa ra i bɛna a ye cɔ.|Regarde! Regarde! Si tu ne vois pas, quand l'objet va te piquer dans l'oeil tu le verras.
« Alahu Akbar, » bɛɛ tɛ moriya ye.|Tous ceux qui disent « Alahu Akbar, » ne sont pas de grands religieux.
Ala ka gbolo o gbolo dan, a gbanlon bɛ yi, a sumalon bɛ yi.|Chaque peau que Dieu a créée a son jour où il est froid, et son jour où il est chaud.
Ala ka sula lɔn kojugukɛbaga ye, k’a to a kana yeri kɛ su fɛ.|Dieu sait que le singe est mauvais, c'est pourquoi il n'a pas permis de voir la nuit.
Ala le bɛ ji di bagabaga ma, k’a ta so lɔ.|C'est Dieu qui donne l'eau aux termites pour construire leur maison.
Ala ma borokandenw dan ka o kaɲa.|Dieu n'a pas créé les doigts d'égale longueur.
Ala ma fɛn dan min bɛ kanuya bɔ.|Dans tout ce que Dieu a créé rien ne vaut l'amour.
An ye taga, an ye taga, a laban ye bori ye.|Marchons vite! Marchons vite! C'est qu'on finira par courir.
A to ! A to ! N’i ma to, n’a tonna i kɔrɔ, i kelen b’a cɛ.|Arrête! Arrête! Si tu n'arrêtes pas, Quand tu en auras une montagne devant toi, tu le ramasseras seul.
Aw ye Julaw mina ! aw ye Julaw bla ! Nin bɛɛ bɛ nankɔgɔ le ɲinina.| « Saisissez les comerçants! Laissez les commerçants! » Tous ceux là cherchent du sel à acheter.
« A’ y’a mina, a b’a da ra », o ye tiɲɛ ye, nka « a’ y’a mina a b’a kɔnɔ », o ye faninya ye.|« Saisissez le, il a ça dans sa bouche! » Cela peut être vrai. Mais « Saisissez-le, il a ça dans son ventre! » C'est pas fondé.
 « Baara wɛrɛ tɛ ne ra ni nin tɛ », o kuma ka jugu wokɔnɔfɛn ma dɛ!|Quand le creuseur dit: « Je n'ai rien d'autre à faire que ça! » Cela fait peur au rat qui est dans le trou.
Ba degunin bɛ kinin kɛ.|Le cabri coincé peut mordre.
Badenyakɛlɛ bɛ sisi bɔ, nka a tɛ mana.|Palabre des frères fait de la fumée seulement il ne s'enflamme pas.
Bagama kɛlɛtigɛbaga mako tɛ jo sɔrɔri ra.|Celui qui fait palabre juste pour provoquer ne cherche pas de raison.
Bakɔrɔnin ka sibon sɔrɔ, a ko "lukɛnɛ!"|Le bouc a trouvé une chambre, il demande la cour.
Bala sara, ka bala wɛrɛ sa, ni jugunin tɛ ɲamɔgɔden ye, dugutigiya sera a yɛrɛ ma sa dɛ !|Un porc-épic est mort, un autre porc-épic est mort, si le hérisson n'est pas un bâtard, c'est lui qui doit être le chef de village maintenant.
Ba min bɛ kasi, minlɔgɔ tɛ o ra.|Le cabri qui bêle n'a pas soif.
Basa bɛ ji min ni sisɛw ye.|Le margouillat boit avec les poules.
Bɛɛ ka farin i facɛ ta so da ra.|Chacun est brave devant la porte de son père.
Bɛɛ k’a lɔn ko galama tɛ kongosɔgɔw minyɔrɔ ra.|Tout le monde sait que là où les animaux boivent il n'y a de louche.
Bɛɛ n’i diyanyamɔgɔ lo.|A chacun a son partenaire aimé.
Bɛɛ n’i karanmɔgɔ.|A chacun son maître.
Bɛɛ sobɛ ye i sobɛ le ye.|Le sérieux de chacun est son sérieux.
Bɛɛ y’a ye, o ka fisa mɔgɔ kelen y’a ye.|Si tous voient, c'est mieux qu'une seule personne voit.
Bilakoro facɛ bɛ se a ra, bilakoro bɛ se basa ra, basa bɛ se liden na, liden bɛ se bilakoro n’a facɛ ra.|Le père du garçon est plus fort que lui, le garçon est plus fort que le margouillat, le margouillat est plus fort que l'abeille, l'abeille est plus fort que le garçon et son père.
Bobo bɛ siko, ni dugu gbɛra a ɲafɔko b’a kɔnɔgban.|Le muet fait un rêve la nuit, le matin il ne sait comment le raconter.
Bonbosi tɛ kɔrɔya sarati ye, bakɔrɔnin bɛ woro n’a ta ye.|La barbe n'est pas une preuve qu'on est vieux, le bouc en a à sa naissance.
Bori min tɛ dabla k’a ye, n’i sen ka di cogo o cogo i bɛ mina o ra.|Tu as beau être bon coureur, dans une course qui n'en finit pas, tu seras un jour rattrappé.
Boro fla le bɛ ɲɔgɔn ko.|Les deux mains se lavent mutuellement
Borokanden tɛ so ci, nka a b’a yira a cibagaw ra.|Un doigt ne peut pas détruire la maison, mais il peut la montrer à ceux qui détruisent.
Bɔrɔ lakolon tɛ se ka lɔ.|Le sac vide ne peut pas tenir debout.
Bulonkɔnɔsi tɛ se ka den n’a fa kɛ flan ye.|Dormir dans le même vestibule ne fait pas que l'enfant et son père soient des camarades d'âge.
Canfɔbaga ma man di.|L'homme qui dit la vérité n'est pas aimé.
Cankuma bɛ mɔgɔ dɔ kɔnɔ, nka a fɔda t’a fɛ.|Quelqu'un peut avoir une parole de vérité mais il n'a pas la bouche pour le dire.
Cɛ dɔ ka surun, nka a ta derege kan tɛ mina dɛ!|Certains hommes sont courts, mais on ne peut pas attraper les colles de leur chemise
Cɛ fin bɛɛ tɛ numu ye, a gbɛman bɛɛ tɛ fla ye fana.|Tous les hommes noirs ne sont pas forgerons, tous les hommes clairs ne sont pas peulh.
Cɛkɔrɔba jamu ye ko "kogbɛlɛnkɛ."|Le nom du vieux est "celui-qui-brave-difficulté"
Cɛkɔrɔba siginin le bɛ siyɛnta ɲalɔn, n’a wurira ka lɔ a bɛ bɔ a kɔnɔ.|Le vieux sait bien lutter quand il reste assis, dès qu'il se lève, il ne sait plus rien.
Cɛnin min ka so lɔn, ani so ka cɛnin min faga, o bɛɛ ye o cɛnin kelen le ye.|Le garçon qui savait bien monter à cheval, et le garçon que le cheval a tué, C'est bien le même garçon.
Cɛnnikɛbaga ta fanga ka bon ni ɲanikɛbaga ta fanga ye.|Celui qui détruit a plus de force que celui construit.
Cɛ siranbagatɔ borikan ye ko : “Mugu banna, kisɛ bɛɛ banna!|Le guerrier peureux court en disant: " Il n'y a plus de poudre, plus de plomb!"
Cɛ tan bɛ fani min karan, cɛ tan b’a bɔrɔtɔ, o fani tɛ karan ka ban ka ye.|Un habit cousu par dix hommes, et à nouveau déchiré par dix autres, on aura jamais fini de coudre cet habit.
Cɛya ye gundo ye, musoya ye gundo ye.|Être homme, est un secret, être une femme, est un secret.
Da dɔ bɛ foroto tɔgɔ fɔ, nka a t'a domu|La bouche peut dire le nom du piment mais sans le manger
Daha kumunin lo kabini a fitinin.|L'oseil est acide depuis qu'il n'est qu'une petite plante.
Da ka surun, nka a fɔɲɔ bɛ taga yɔrɔ jan.|La bouche est courte, mais le vent qui en sort va loin.
Danninfɛn ye wurifɛn ye, yiriden ye mɔfɛn ye, mɔgɔya ye hakiri ye.|Le grain qu'on sème doit pousser, le fruit de l'arbre doit mûrir, dans la vie l'homme doit réfléchir.
Da tɛ balimaya sira bɔ, sen le b’a bɔ.|Le chemin qui relie les frères n'est pas tracé avec la bouche, mais avec les pieds
Degiri le bɛ kɔnɔnin se pan na.|C'est en apprenant que l'oiseau saura voler.
Den bɛ ben a minabaga le boro.|L'enfant ne tombe que de la main de celui qui le prend.
Den bɛ i ko bɔgɔ kɛnɛ, n’i ka lalaga cogo min na, a bɛ ja ten.|L'enfant est comme de la boue humide, la forme qu'on lui donne, il sèche en gardant cette forme.
Den bɛ kasi sin le nɔ fɛ, n'o dira a ma a bɛ je.|C'est à cause du sein que l'enfant pleure, quand on le lui donne il se tait.
Den bɛ se ka wolo a fa kɔ, nka a tɛ wolo a ba kɔ.|L'enfant peut naître en l'absence de son père, mais pas en l'absence de sa mère.
Dencɛ bɛɛ tɛ cɛ ye.|Tous les fils ne sont pas des garçons.
Dencɛ wolonfla bɛ musokɔrɔba min fɛ, o ta banna tɔgɔ tɛ se ka fɔ, a bɛ fɔ ko "musokɔrɔba ta bana."|On ne peut pas dire le nom de la maladie, de la vielle femme qui a sept fils; on dit seulement: "la maladie de la vielle."
Denmisɛn lɔnnin bɛ min ye, cɛkɔrɔba siginin bɛ o ye.|Ce que l'enfant voit étant débout, le vieux le voit étant assis.
Denmisɛnso sen ka teri, nka a tɛ taga yɔrɔ jan.|le cheval de jeunesse est rapide, mais il ne va pas loin.
Denmisɛn tɛgɛkokonyuman bɛ mɔgɔkɔrɔba dɛgɛ nɔɔni.|L'enfant dont la main est bien lavée peut préparer le "dégué" d'une vielle personne.
Denmisɛnwuru sen ka di, nka a tɛ sira lɔn.|Le chien de jeunesse sait courir, mais il ne sait pas la route.
Denmisɛnwuru sen ka teri, nka a tɛ kongo lɔn.|Le chien de jeunesse est rapide, mais il ne connait pas la brousse.
Derege min tɛ i kanna,  i boro bɛ mun ɲini o jufa ra?|Qu'est ce que ta main cherche dans la poche du boubou de quelqu'un d'autre?
Deregenin kan ba|Le boubou est petit et l'ouverture pour la tête trop large.
Dɛgɛminbaga da kurunin lo, nka a diminin tɛ.|Le buveur de dégué a la bouche bien fermée, mais il n'est fâché
Digiri dan ye dɛndɛn ye.|Quand on pousse jusqu'au mur, c'est la fin.
Dimi le be dimi sa|C'est le mal qui guérit le mal.
Diya ma fɛn min ɲa, kɛlɛ tɛ o ɲa.|La querelle ne peut pas arranger ce que la douceur n'a pas pu arranger.
Diyanya le b’a to an b'an boro la ɲɔgɔn kan na, n’o tɛ an si tagamatɔ tɛ ben.|C'est par amour qu'on se met la main sur l'épaule en marchant, sinon chacun peut marcher seul san tomber.
Domuni diman diya tɛ lɔn, fɔ n'a banna minan kɔnɔ.|On ne connait le bon goût de la nourriture, que lorsqu'il finit dans la marmite.
Dondokɔrɔ kunkolo tɛ dogo na na.|La tête du vieux coq est toujours visible dans la sauce.
Dɔ bɛ so sanna, dɔ bɛ soku tɛrɛmɛna.|Certains achètent le cheval, alors que d'autres discutent le prix d'une queue de cheval.
Dɔn diyatuma le ye mɔgɔ bɔtuma ye dɔn na.|C'est lorsque la danse est agréable qu'il faut sortir de l'arène.
Dɔnkiri ka di ɲanibagatɔ ye, nka a b’a siɲanakun bɔ.|Le malheureux aime entendre la chanson, mais à force d'y penser ça va l'empêcher de dormir la nuit.
Dɔn min ka teri ni marakadɔn ye, kirikirisiyɛn sen b’o ra.|Si une danse est plus rapide que la danse des maraka c'est que les danseurs doivent avoir un peu d'épilepsie.
Dɔ ta kasiko ye dɔ ta yɛrɛko ko ye.|Ce qui fait pleurer l'un, fait rire l'autre.
Dɔ ta sonzani ye dɔ ta sama ye.|Un lapin chez certains vaut un éléphant chez d'autres.
Dɔw tɛ gbɛɲɛ bɔrɔlaman lɔn, n’a ma bɔ k’a yira o ra.|Certains ne reconnaissent pas un fouet dans un sac, il faut l'enlever pour le leur montrer.
Dudunkan bɛnna, balankan bɛnna, filenkan bɛnna, mɔgɔya bɛnnin ka fisa ni nin bɛɛ ye.|Les tam-tam s'accordent bien, les balafonds s'accordent bien, les flûtes s'accordent bien,  mais il est encore mieux quand les hommes s'accordent bien.
Dudunkan o, balankan o, susurikan ka di lonancɛ toro ra ka tɛmɛ nin bɛɛ kan.|Le son des tam-tam c'est bien, le son des balafonds c'est bien, mais le meilleur son dans l'oreille de l'étranger c'est le bruit des pilons.
Dugukolo ɲuman bɛɛ tɛ sɛnɛkɛyɔrɔ ye.|Toutes les bonnes terres ne sont pas cultivables
Dugumɛnɛnin sɔnkɔrɔ ye kinin ye.|Piquer, est une vieille habitude de la fourmi.
Dugu min mɔgɔw bɛ surugukasi kɛ, ni ele ka bakasi kɛ yi, o b’i domu.|Un village où les habitants imitent le cri de l'hyène, si toi tu imites le cri d'un cabri dans ce village, tu seras mangé.
Dugu mɔgɔkɔrɔbaw ye yiribaw le ye, ni o ka ben, o ɲɔgɔn sɔrɔ man di.|Les vieux du village sont des grands arbres, Quand ils tombent, on les remplace difficilement.
Dugu o dugu tɔgɔ ye ko bɛndugu, kɛlɛ tɛ o dugu ci.|Des querelles ne peuvent pas casser un village qui s'appelle " village entente"
Dugutaraman kasi bɛnna dondokɔrɔnin ma. |Chanter tard la nuit convient bien au vieux coq.
Dugutigi den ta sira tɛ kogokɔfɛkuma ra.|Le fils du chef de village n'a rien à voir dans ce qu'on dit derrière le mur.
Dugutigi ta ye a ta dugu ye ; "n’tɛ si" ta ye a nin ye.|Le chef de village est responsable de son village, celui qui refuse de dormir au village est responsable de son âme.
Dunuɲalatigɛ ye batigɛ le ye.|La vie est une traversée de fleuve.
Dunuɲa ye jigijigi caaman, ani yɛlɛyɛlɛ caaman ye.|La vie c'est beaucoup de montées et beaucoup de descentes
Dunuɲa ye sɔgɔmada caaman ye.|La vie est faite de beaucoup de matins
Ele ka kɔnɔnin min fan cɛ, dɔ ka o bamuso domu.|L'oiseau dont (tu te vantes d'avoir) ramassé les oeufs, quelqu'un d'autre a mangé sa mère.
Ele ka wɔlɔni min faga, dɔ ka o fan cɛ.|La perdrix que tu as tuée, quelqu'un d'autre en a ramassé les oeufs.
Fagamadenbugɔ ni sigi tɛ bɛn.|Frapper le fils du chef et vouloir rester dans le village ne sont pas choses compatibles.
Fagama jo n’a jalaki bɛɛ ye a jo ye.|Qu'il ait tort ou raison, le chef a toujours raison.
Fagama ta kumalɔn tɛ kumalɔn ye, fangantanw ta bɛ lakari a ye, mɔgɔbaw ta bɛ lakari a ye.|Le chef sait parler, mais lui même ne sait pas parler, on lui rapporte les paroles des pauvres on lui rapporte les paroles des grands, (et il les répète).
Fagantan nana dunuɲa sen mina a bosobagaw ye le.|Le pauvre est né pour saisir les pieds du monde, et aider les riches à le dépecer.
Fagantan ta karafe ye a kanaderege ye.|Le mors du pauvre c'est la chemise qu'il porte.
Faninyatigɛbaga le b’a fɔ ko a seere bɛ ba kɔ.|C'est le menteur qui dit que son témoin est de l'autre côte du fleuve.
Farata ɲininkari kojugu b’a lakasi.|Poser trop de questions à l'orphelin finit par le faire pleurer.
Fa tɛ ɲɛbɛrɛ ra ko a bɛ taga dondokɔrɔnin fo sɔgɔma.|Le cafard n'est pas fou pour se permettre d'aller saluer le coq le matin.
Fatɔ bɛ yaala n’a den fiyereta ye tuma min na, o le y'a santuma ye.|Lorsque le fou promène son fils pour le vendre c'est à ce moment qu'il faut l'acheter.
Fatɔ laganviyara, nka a man kɛnɛ.|le fou est libre, mais il est malade.
Fɛnbanbari diya tɛ lɔn tuun.|Une affaire sans fin, finit par ne plus avoir de goût.
Fɛn bɛɛ bɛ taga ka segi mɔgɔ munyuribaga ma.|Tout part et reviens vers l'homme patient.
Fɛn bɛɛ kura cɛ ka ɲi, fɔ kaburu kura.|Tout nouveau tout beau, sauf la tombe.
Fɛn fɔlɔ le ye fɛn fɔlɔ ye, ni min bɔra o kɔ, a bɛ fɔ ko dɔwɛrɛ bɔra.|la première chose est vraiment la première, Ce qui vient après, on dit: " un autre".
Fɛn kelen tigi caaman ma ɲi, fɛn kelen tigintan ma ɲi.|Une chose avec beaucoup de propriétaires, c'est pas bien, une chose sans propriétaire, c'est pas bien.
Fɛn min bɛ banabagatɔ kɛnɛya, o bɛ fa bla kɛnɛbagatɔ ra.|Ce qui guérit un malade, rend fou un bien portant.
Fɛn min bɛ mɔgɔ mina, i tɛ o senkan mɛn.|Ce qui va t'attraper, tu ne l'entends pas venir.
Fɛn min bɛ to misɛnya ra, o dan ye foni ye.|Il n'y a que le fonio qui reste petit.
Fɛn min firibaga tɛ nimisa, o tɔmɔbaga tɛ ɲagari.|Ce que quelqu'un a jeté sans regret, celui qui le ramasse n'aura pas de joie.
Fɛn min ka di kogoro ye, kaganan tɛ o tana.|Ce que le varan terrestre aime, ne peut pas être le totem du varan d'eau.
Fɛn min ka di mɔgɔ ye i ka kan ka o bla i ɲa fɛ, n’o tɛ a kɔflɛ bɛ caya.|Ce que tu aimes, mets le devant toi, sinon les coups d'oeil par derrière seront nombreux.
Fɛn min ka mɔgɔ sennateriya, o ka kan ka i darateriya.|Ce qui t'a fait courir vite, doit te faire parler vite.
Fɛn min ka ɲi ɲɔ ma o ye bɔrɔ ye.|Ce qui est mieux pour le mil, c'est le sac.
Fɛn min nana i kama o tɛ i jɛn.|Ce qui est venu rien que pour toi, ne te manquera jamais.
Fɛn ɲanin, tigi caaman.|Chose réussie, (revendiquée par) plusieurs propriétaires!
Fɛn saba furura fɛn saba ma, ka fɛn saba woro: sigi ka fagantanya woro, tagama ka sɛgɛ woro, wɔsiji ka sɔrɔ woro.|Trois se sont mariés à trois, pour mettre au monde trois: l'oisiveté a mis au monde la pauvreté, la marche a mis au monde la fatigue, la sueur a mis au monde le gain.
Fɛnw fincogo ye kelen ye, nka o manamanacogo tɛ kelen ye.|Les choses noircissent de la même manière, mais ils ne brillent pas de la même manière.
Fiyentɔ bɛ dɔnkɛ n’a ta dɔnkɛsara ye a boro.|L'aveugle danse avec son salaire de danse en main.
Fiyentɔ jusuba, i ɲa yɛlɛlon le fɔra i ye cɔ.|Un aveugle qui a un gros coeur? on t'a certainement dit le jour où tes yeux s'ouvriront.
Fiyentɔ ta kɔgɔ bɔnna bɛrɛkisɛ ra, takanɛnɛ dabɔra.|Le sel de l'aveugle est versé dans le gravier, Il va goûter un à un tout ce que sa main va trouver.
Fiyentɔya ladegi man gbo.|Faire l'aveugle n'est pas difficile.
Fiyentɔya o, nanbaraya o, wurudennin ka nin bɛɛ kɛ ka tɛmɛn.|Vivre sans voir, sans marcher, le petit chiot a passé par toutes ces étapes.
“Flamuso, i kɔnɔman lo wa ?” “Ɔn hɔn.” “Den ye cɛ le ye wa ?” “N’ ma o lɔn !”|« Femme peulh, tu es enceinte? » «  Oui! » « Ton enfant est-il un garçon? » « Ça, je ne sais pas ! »
Foro damina ye wagabɔn ye.|Pour faire un champ, on commence par enlever les herbes.
Forokonin tora a logisara ra.|Le sac a été vendu à son prix de revient, (sans bénéfice).
Foyi tɛ to kuraya ra.|Rien ne reste toujours à l'état neuf.
Foyi tɛ yi ni kɔrɔ tɛ min na.|Rien ne se fait sans cause.
Fɔnɲɔba bɛ yiribaw ben, nka a bɛ tɛmɛ yirimisɛnw ni binw kunna.|Le grand vent fait tomber les gros arbres, mais il passe au dessus des arbustes et des herbes.
Fɔɲɔ bɛ ɲɔfiyɛbaga boro kɔrɔtanin le sɔrɔ.|Le vent vient trouver les mains de la vanneuse en haut.
Gbabugu ka kɔrɔ ni misiri ye.|La cuisine est plus âgée que la mosquée.
Gban tɛ se ka janya a karibaga ma.|Le pied de gombo ne peut pas être trop long pour celui qui coupe le gombo.
Gbɛrɛgbɛrɛko sɔrɔ man gbo.|Un malheur arrive facilement.
Girindi tɛ fa ye, nka kɔngɔtɔ t’a kɛ.|Roter ne veut pas dire qu'on est rassasié, mais celui qui a faim ne le fait pas.
Gundojugu bɛ i ko sogo kɛnɛ, n’a torira, a kasa bɛ bɔ.|Le mauvais secret c'est comme de la viande fraîche, Quand il pourrit, il sent mauvais.
Hakɛ bɛ dunuɲa ko bɛɛ ra. |Il y a une offense en toutes choses dans ce monde.
Hakɛ bɛɛ tɛ bla ka lahara kɔnɔ.|Toutes les offenses n'attendent pas le dernier jugement.
« Hakɛ to ! » O tɛ wurukinda suma.|Dire: « Pardon! » ne guérit pas une morsure de chien.
Hakiri le ye bɔrɔba tajuru ye.|La sagesse est la corde par laquelle le gros sac lourd se prend.
Hakiri le ye mɔgɔya ye.|C'est la réflexion qui fait l'homme.
Hakiri tɛ dan, nka a bɛ kɔrɔsiyɛn.|On ne sème pas la sagesse, mais on en enlève les mauvaise herbes.
Hakiritigi fla le bɛ sisɛfan firi ɲɔgɔn fɛ.|C'est deux hommes sages qui peuvent se lancer un oeuf.
Hali ni faninya ka san tan kɛ tagama ra, tiɲɛn ta sɔgɔmada kelen tagama bɛ kun a ra.|Même si le mensonge fait dix ans de marche, une seule matinée de marche de la vérité le rattrape
Hali n’i jugu ye sonsan ye, a lɔn ko a bɛ bori.|Même si ton ennemi est un lièvre, reconnais au moins qu'il sait courir.
Hali n’i ka fagobagatɔ bla turubara le kɔnɔ, a koyokoyonin bɛ bɔ.|Même si tu mets le fainéant dans une boite de pommade, il en sortira le corps sec.
Hali n’i ka i kinbiri gban ka bagabagabɔbaga fo, o tɛ dɔ bɔ sisɛ sɔngɔ ra.|Même si tu te mets à genoux pour saluer l'éleveur de poule qui cherche les termites pour ses poules, cela ne va pas diminuer le prix des poulets.
Hali ni lonancɛ ɲakiri bonya ka borokuru bɔ, dugulen fiyentɔ ka fisa n’a ye.|Même si l'oeil de l'étranger est gros comme un poing, l'autochtone aveugle voit mieux que lui.
Hali n’i ma fa to ra, n’i ka miiri i sigituma ma, i bɛ wuri.|Même si tu n'es pas rasasié, quand tu penses depuis le moment où tu manges, tu dois te lever.
Hali ni wari tɛ i fɛ, sanin tɛ i fɛ, ni i ni mɔgɔw bɛ ɲɔgɔn fɛ, i jigimafara.|Même si tu n'as pas d'argent, tu n'as pas d'or, si tu vis avec les hommes, tu es heureux!
I bɛ cɛ dɔ ta k’a ben, nka a dugumatereke tɛ kun i ra.|Tu peux terrasser un homme, mais perdre la vie en voulant le maintenir à terre.
I bɛ mɔgɔ dɔ jɛn muru, ka na muru n’a kala bɛɛ di dɔwɛrɛ ma.|Tu refuses de donner le couteau à quelqu'un mais tu donnes et le couteau, et sa manche à un autre.
I fa yɛlɛnna yiri min na, hali n’i ma se ka yɛlɛ o yiri ra, i ka kan ka se ka i boro la a jura.|L'arbre dans lequel ton père montait, même si tu ne peux pas monter à cet arbre, tu dois pouvoir au moins poser ta main sur son tronc.
I gbolo tɛ se yɔrɔ min na n’i ko i b’a sama k’a se yi, a bɛ faran.|Là où ta peau n'arive pas, si tu veux la tirer pour y arriver à tous prix, elle va se déchirer.
I ka kɔ tigɛ, kɔwɛrɛ b’i ɲa.|Tu as traversé une rivière, mais il y en a une autre devant toi.
I kɛra dɛgɛsusukolonkalan ye, i kun gbɛra, i ma mugu domu.|Tu es devenu comme le pilon qui a pilé le dégué, ta tête est blanche, mais tu n'as mangé de farine.
I ma min fɔ i talonyɔrɔ ra, ka na o fɔ i benyɔrɔ ra.|Ce que tu n'as pas dit là où tu as trébuché, il ne faut pas le dire là où tu es tombé!
I sɔnkɔrɔ bla, o ka fisa ni jatigiya yɛlɛma ye.|Laisser ton mauvais caractère, vaut mieux que changer de tuteur.
I ta ye jɔncɛnin dɔ ye, nka a kɔnɔnɔkirisi tɛ i ta ye.|Un petit esclave peut t'appartenir, mais la formule magique qu'il connait ne t'appartient pas.
I tɛna ɲa bo min cɛ kɔ, i kana to sisɛw y’a yɛrɛgɛ.|Les excréments que tu devras ramasser à tout prix, ne laisse les poules l'éparpiller.
I wuri ka bɔ n'ta siginan kan, o ye tiɲɛ ye, nka wuri ka bɔ n'nakan kan, o ye faninya ye.| « Tu occupes ma chaise, lève toi! » Cela est vrai. Mais « tu occupes mon destin, lève toi! », c'est faux.
Jagofɛn ɲuman le bɛ a yɛrɛ fiyeere.|La bonne marchandise se vend d'elle même.
Jalayiri ka bon ni jalayiri ye, nka jalayiri man kuna ni jalayiri ye.|Un cailcédrat peut être plus gros qu'un autre cailcédrat, mais un cailcédrat n'est pas plus amère qu'un autre cailcédrat.
Jama jɛnna ka dugawu kɛ mɔgɔ min ye, mɔgɔ kelen ta danga tɛ se ka foyi kɛ o ra.|La malédiction d'une seule personne ne va rien lui faire à l'homme que toute la foule a béni ensemble, 
“Janto n’yɛrɛ ra ” kɔrɔko ka ɲi.|Celui qui prend soin de lui même, avance bien en âge.
Jatigiɲininka ka fisa lonanya ra.|Se renseigner auprès d'un autochtone est bon pour l'étranger.
Jeli tɛ malo, nka a tenda bɛ wɔsi.|Le griot n'a pas honte, mais son front transpire.
Jenbe tɛ a yɛrɛ fɔ, mɔgɔ le bɛ jenbe fɔ|Ce n'est pas le tam-tam qui parle mais celui qui joue le tam-tam.
Jɛgɛ bɔra Zan ta jɔ ra, ka don N’Golo ta jɔ ra, a bɛɛ ye kelen ye.|Le poisson a quitté le filet de Pierre, pour entrer dans celui de Paul, tout est pareil.
Jɛgɛ jigi je ji ye, sogo jigi ye kongo ye, mɔgɔ jigi ye mɔgɔ ye.|L'espoir du poisson c'est l'eau, l'espoir du gibier c'est la forêt, l'espoir de l'homme, c'est l'homme.
Jɛgɛ ka ji lɔn, nka nanji ko tɛ.|Le poisson sait nager dans l'eau, mais pas dans l'eau de la sauce.
Jɛgɛ tɛ ji ra, i kana tɔgɔ la jɔ kolon na.|Dans ce marigot il n'y a pas de poisson, n'accuse pas le pauvre vieux filet.
Jɛn o jɛn, faranlon b’a ra.|Toute union a son jour de séparation.
Jɛn ta nɔgɔ tɛ nɔgɔjugu ye.|Les taches de l'union se sont pas de mauvaises taches.
Ji bɛ don kɔ ra dɔɔni dɔɔni le.|C'est peu à peu que l'eau emplit la rivière.
Ji bɛɛ n’a tɛmɛsira lo.|Chaque eau a son passage.
Ji bɛ jaba ɲa, a ma fɔ ko i ye i tunun k’a turu dɛ.|L'oignon aime l'eau, mais on ne dit pas de plonger sous l'eau pour le cultiver.
Ji bɔnin tɛ se ka cɛ.|On ne peut pas ramasser l'eau versée.
Ji gbannin fla tɛ se ka ɲɔgɔn suma.|Deux eaux chaudes ne peuvent pas se refroidir.
Jigi ka fisa ni fa ye.|Espoir vaut mieux que être rassasié.
Jigi tɛ mɔgɔ min fɛ i man kɛnɛ.|Celui qui n'a pas d'espoir est malade.
Jugunin n’a kɔnɔfɛn bɛɛ ye duga ta ye.|Le hérisson appartient au vautour, avec tout ce qu'il contient
Juru le ka misidennin n’a bamuso faran.|C'est la corde qui a séparé le veau de sa mère.
Jusu le bɛ kɛlɛ kɛ.|C'est avec le coeur qu'on se bat.
Ka Ala to san fɛ, duga tɛ bin domu.|Tant que Dieu reste au ciel, le vautour ne mangera pas de l'herbe.
Ka bakɔrɔnin dɔ falen dɔ ra, i b’a sɔrɔ kelen jogo le ma ɲi.|Si on échange un bouc contre un autre bouc, c'est que l'un d'eux a un mauvais caractère.
Kabini Ala ka dunuɲa dan k’a bla a yɛrɛ kɔ fɛ, a m’a kɔ flɛ tuun.|Depuis que Dieu a créé le monde et la mis derrière lui, il ne s'est plus retourné pour le regarder.
K’a fɔ fagantan ma ko: “I ni sɔgɔma ”, o bɛ bɛn, nka k’a fɔ a ma ko “ hɛra sira wa ”, o ye kɛlɛtigɛkan ye.|Dire à un pauvre "Bonjour", c'est bien, mais lui dire: "tu as bien dormi?" c'est une provocation.
K’a fɔ ko “tasuma !” o tɛ mɔgɔ da jɛnin.|Dire: "du feu!" ça ne brûle pas la bouche.
Ka i borituma lɔn o ka fisa, ni “n'bɛ se bori ra.”|Bien choisir le moment de courir, vaut mieux que de dire: " Je sais courir!"
Ka i sen don ji ra, ni o bɛ mɔgɔ kɛ bozo ye, ni ne ka n'ta fla sama k’a bɔ, n'tɛ kɛ i yɛrɛ ta ye wa?|Si mettre mon pied dans l'eau fera de moi un pêcheur, j'enlève don mes deux pieds pour être libre.
Ka kɔrɔtɔ Ala ma, o le bɛ na ni gbata lɔri ye, n’o tɛ ni su kora yɔrɔ bɛɛ bɛ kɛ suma ye.|C'est l'impatience à l'égard de Dieu qui fait qu'on construit un hangar, sinon la nuit il y a de l'ombre partout.
Kami ta ɲɛgɛnw b'a kɔ kan, adamaden ta b'a kɔnɔ.|Les points de beauté de la pintade sont sur son dos, les points de beauté de l'homme intelligent sont dans son coeur.
Ka mɔgɔ ye, ani k'a lɔn, o tɛ kelen ye.|Voir l'homme et connaître l'homme sont deux choses différentes.
Kanbeleba le bɛ sunguruba siyɔrɔ lɔn.|C'est le jeune homme frivole qui sait où dort la fille frivole.
Kanbele cɛsirikonyuman bɛɛ tɛ kɛrɛkɛcɛ ye.|Tous les jeune hommes aux ceintures bien attachées ne sont pas guerriers.
Ka ɲɔgɔn siraratagama, sula ni wuru ta teriya ma o bɔ.|L'amitié du singe et du chien n'arrive pas au point de se rendre visite souvent.
Karan ni kolɔn tɛ kelen ye.|L'instruction est différente de la sagesse.
Karifa ka gbɛlɛ Ala ma.|Chose confiée coûte chère à Dieu
Karisa tagara lɔgɔ ra, ne fana bɛ taga, nka karisa tagara ni min ye, ni ele ma taga n’o ye, karisa bɛna segi ni min ye, ele tɛna segi n’o ye dɛ!|Untel est allé au marché, moi aussi j'y vais, mais si tu n'as pas apporté au marché ce qu'un tel a apporté, tu ne pourras pas rapporter du marché ce qu'un tel va rapporter.
Ka san tan kɛ faligbɛn na, hali n’i ma falikan mɛn, i bɛ sirafaran caaman lɔn.|Si tu fais dix ans derrière les ânes, même si tu ne connais la langue des ânes, tu sauras beaucoup de chemins.
Ka segi i cɛkɔrɔ ma o tɛ kojugu ye, nka a bɛ mɔgɔ lɔnbaga caya.|Retourner chez ton ancien mari, n'est pas grave, mais ça fait que tu seras connue de beaucoup trop de gens.
Ka segi kumakɔrɔ kan, o ye dalamaga le ye.|Retourner à la même parole déjà dites, revient à remuer simplement les lèvres.
Kasi ni ɲaji tɛ lɔn sanji jukɔrɔ.|On ne reconnait pas des pleurs et des larmes sous la pluie.
Ka tiga wɔrɔ ɲɔgɔn kɔ, a fara ka ca.|Décortiquer les arachides les uns après les autres les coques s'éparpillent et deviennent trop nombreuses.
Kɛlɛ kɔ ye diya ye.|Après la querelle l'entente!
“Kɛ n’ yɛrɛ ra” dan ye kɔlɔnkɔnɔboci ye.|Peter dans dans un puits, c'est le plus grand mal que quelqu'un puisse se faire à lui même.
Kɛrɛkɛcɛ kelen, burufiyɛbaga kɔnɔntɔn, ɲagari bɛ o kɛrɛ sa.|Neuf joueurs de cor pour un seul guerrier, trop de joie va annuler cette guerre.
Kirisi min bɛ mɔgɔ bɔ kɔlɔn kɔnɔ, o bɛ kɔrɔbɔ ni jurufiyɛ le ye.|La formule magique qui peut faire sortir quelqu'un du puits, se vérifie avec une puisette d'abord.
Ko baranin bɛ ko sulanin sa.|Une choses imprévue qui arrive, nulle une chose qui était prévue.
Ko bɛɛ n’a tuma, ko bɛ n’a wagati.|Chaque chose a son temps, chaque chose a son moment.
Ko caaman bɛ mɔgɔya ra, bɛɛ tɛ o lɔn.|Il y a beaucoup de choses dans les relations humaines, que plusieurs ne sauvent pas.
Kofitininkɛ bɛ mɔgɔ fadencɛ ɲadon i ra. Kobakɛ bɛ mɔgɔ fadencɛ ɲabɔ i ra.|Faire des choses médiocres fait que tes frères te méprisent. Faire de grandes choses fait que tes frères te respectent
Kogoro ɲa wulenna, nka a ta ko tɛ mɔgɔ fɛ.|La varan terrestre a les yeux rouges mais il n'est pas fâché contre quelqu'un.
Kogo surunin bɛɛ ta surunya tɛ bɔgɔdɛsɛ ye.|Tous les mûr courts ne sont pas courts parce que les briques ont manqué.
Kojugu le ye mɔgɔ karanmɔgɔ ye.|C'est le malheur qui enseigne l'homme.
Kojugu senkan tɛ bɔ.|Les pas du malheur ne font pas de bruit.
Kolo tɛ nɛn na ko a tɛ yɛlɛma.|La langue n'a pas d'os, pour qu'elle ne tourne pas.
Kongotaga bɛɛ tɛ donsoya ye.|Tous les "allés en brousse" ne sont pas pour la chasse.
Kononin kiri lamaga lamaga kojugu b’a ci.|A force de trop bouger l'oeuf de l'oiseau, il finit par se casser.
Ko sonzanin ta ye lo ye, ni lo ka bɔ ko sonzanin y’a ɲa datugu!|On dit que le masque appartient au lièvre, mais quand le masque sort, on dit au lièvre de se fermer les yeux!
Ko tɛ ko sa, nka fiyentɔya bɛ donsoya sa.|On dit que rien ne peut rien empêcher, mais la cecité empêche d'être chasseur.
Ko to ɲɔgɔn ta ra, o bɛ karatabugu mɛɛn si ra.|Quand les habitants de la case se soumettent les uns aux autres, ça donne longue vie à la case.
Kɔgɔ bɛ fali kɔ ra, a lɔgɔ b’a ra.|L'âne porte le sel sur son dos, et il en a envie.
Kɔlɔnkɔnɔ banba, n'i benna a kan, kara, n'a benna i kan, kara.|Crocodile dans un puits: tu tombes sur lui, c'est grave! Il tombe sur toi c'est grave!
Kɔ mɔna ni kan min ye, jɛgɛ bɛ taran n’o kan le ye.|Les closes sur lesquelles on a fait la pêche, c'est sur les mêmes qu'on partage les poissons.
Kɔnɔ bɛɛ bɛ ɲɔ domu, nka min bɛ si ɲɔtu ra, a bɛ fɔ o ma ko ɲɔdomukɔnɔ.|Tous les oiseaux mangent le mil, mais celui qui dort dans le champ de mil, lui, on l'appelle mange-mil.
Kɔnɔ bɛ se ka pan ni ji min ye, a bɛ o le min.|La quantité d'eau qui peut permettre à l'oiseau de voler, c'est ce qu'il boit.
Kɔnɔ dugu tɛ san ye.|Le village de l'oiseau n'est pas dans le ciel.
Kɔnɔnin kelen tɛ denminsɛnnin fla ɲanagbɛ.|Un seul oiseau ne peut pas amuser deux enfants.
Kɔnɔnin min k’a ta fan la lɔgɔfɛ ra, mankan tɛ se ka o lawuri tuun.|Un oiseau qui a fait son nid dans un marché, quel bruit peut le faire fuir?
Kɔɲɔmuso nakun ye cɛko ye.|La mariée vient pour son mari.
Kɔrɔ kun ye hakirisigi ye.|Le but de l'âge mûr c'est avoir un esprit calme.
Kɔrɔ ni kɛnɛya, a fla sɔrɔ man di.|Viellesse et bonne santé, il n'est pas facile d'avoir les deux.
Kɔrɔ tɛ fago sa.|L'âge n'empêche pas d'être sot.
Kɔrɔ tɛ mɔgɔ sigi n’a kɛbaga tɛ i boro.|L'âge avancé n'oblige pas quelqu'un à s'asseoir, à moins que tu n'aies quelqu'un d'autres qui travaille à ta place.
Kukala tɛ tori fɛ, nka Ala b’a fifa.|Le crapeau n'a pas de queue, mais Dieu le souffle.
Kuma bɛɛ ma na fɔri kama.|Toutes les paroles ne sont pas venues pour être dites.
Kuma bɛɛ n’a fɔbaga lo.|Pour chaque parole il y a la personne pour la dire.
Kuma fɔkonyuman ni tiɲɛn tɛ kelen ye.|Parole bien dite, n'est pas égale à vérité
Kuma ko ale tigɛra mɔgɔ fla ra: min b’a da don a ra k’a sɔrɔ mɔgɔ m’a sara a ra, ani min b'a daje, k’a sɔrɔ kuma dira a ma.|La parole dit qu'elle a perdu confiance en 2 personnes: celui qui parle sans qu'on ne lui passe la parole, celui qui se tait alors qu'on lui a passé la parole.
Kuma le bɛ na ni kuma ye.|C'est de la parole que nait la parole.
Kuma man di, kumabariya man di.|Parler n'est pas bon, ne pas parler n'est pas bon!
Kuma man kan ka kɛ gbɛsɛ ye k’a bɛn mɔgɔ kelen da ma.|La prole ne doit pas être un cure-dent pour la laisser dans la bouche d'une seule personne.
“Kuma, mun le ka i ɲa ?” “N’fɔcogo !” “Kuma, mun le ka i tiɲɛ ?” “N’fɔcogo!”|Pourquoi la parole t'a rendue bonne? -- La manière de dire!, pourquoi la parole t'a rendue mauvaise? -- La manière de dire!
Kuma tɛ ban, a bɛ lalɔ le.|La parole ne finit pas, on l'arrête.
Kuma tɛ mɔgɔ bon ka i jɛn.|La (flèche de la) parole ne manque jamais sa cible.
Kun bɛ li nka kunnadiya tɛ li.|On peut raser la tête, mais on ne peut pas raser le chance (qui est dans la tête)
Kunkolo min bɛ yaala o bɛ bɛn n’a cibaga ye lon dɔ.|Tête qui se promène trop, rencontrera un jour son casseur.
Kun tɛ ci kuntigi yɛrɛ kɔ!|On ne peut pas caser la tête en l'absence de son propriétaire.
Kun tɛ li a tigi kɔ.|On ne peut pas raser la tête sans son propriétaire.
Kurunkɔnɔmɔgɔ bɛɛ ŋaninya ye kelen ye, Ala y’an seko ɲa.|Tous les passagers d'une pirogue ont la même pensée, Que Dieu permette une bonne traversée.
Lakajan ka di ɲɔgɔmɛ ye, nka a kɔkuru tɛ sɔn.|Le chameau voudrait bien se coucher sur le dos, mais sa bosse refuse.
« La n’na »  man jugu ; « kɛ n’sɔn ye » le ka jugu.|On peut t'accuser, ce n'est pas grave, mais quand ce qu'on dit devient ton caractère, c'est ce qui est grave.
Layiritabariya ka fisa ni layiridafabariya ye.|Ne pas faire de promesse vaut mieux que d'en faire sans la tenir.
Lonancɛ jiminbari tɛ tiɲɛn fɔ.|L'étranger qui n'a pas bu ne dit pas la vérité.
Lonan jatigibugɔ, i tagamatuma sera dɛ!|Etranger qui frappe autochtone, ton départ est proche!
Lon bɛɛ n’a dugugbɛkan.|Chaque jour a son lendemain.
Lon ka jan, nka a sebari tɛ.|Le jour qu'on attend semble loin, mais il arrivera sans doute.
Lon o lon cɛfari si man ca.|Brave chaque jour n'a pas longue vie.
Lɔfɛn bɛɛ ye lafɛn ye.|Tout ce qui est débout est appelé a se coucher un jour.
“Lɔgɔ girinna”, o ye tiɲɛ ye, nka "ne tɛ lɔ ni n’ma se so”, walifɛn le b’i kun.|Le marché s'est terminé dans le désordre, mais celui qui dit qu'il ne s'arrêtera plus jusqu'à la maison, a certainement pris des choses d'autrui.
Lɔnnin ye kɔlɔn le ye, dan tɛ min na.|L'instruction est un puits qui n'a pas de fond.
Makantaga tɛ mɔgɔ sɔnkɔrɔ bɔ i ra.|Aller à la Mecque n'enlève pas un ancien caractère.
Masacɛ fla tɛ kun gbolo kelen kan.|Deux rois ne peuvent pas s'asseoir sur la même peau.
Masa kelen tere tɛ dunuɲa ban.|La fin du règne d'un roi n'est pas la fin du monde.
Mɛɛnɲanakɔnɔnin tɛ se ka siɲanakɔnɔnin den ta.|L'oiseau qui dort tard ne peut pas prendre le petit de l'oiseau qui veille toute la nuit.
Minan dɔ bɛ jɛngɛ nka a kɔnɔfɛn tɛ bɔn.|Un panier peut se pencher sans que son contenu ne verse.
Min bɛ fɔ waraba su kunna, o tɛ fɔ a ɲanaman kunna.|C'est qu'on dit près du cadavre du lion on ne peut pas dire cela près de son vivant.
Misi bɛ basi to a fari ra ka nɔnɔ bɔ.|La vache a du sang dans son corps, mais elle donne du lait.
Misiden tɛ jara lɔn, nka a facɛ k’a lɔn.|Le veau ne connait pas le lion, mais son père le connait.
Misikuru gbɛnbere ye kelen ye, nka mɔgɔ kelen kelen bɛɛ n’a ta gbɛnbere lo.|Un seul bâton suffit pour conduire un troupeau, pour conduire des hommes il faut un bâton pour chacun.
Mori bɛɛ n’a lɔyɔrɔ lo misiri ra.|Chaque maître religieux a sa place dans la mosquée.
Moriya ye cɛn ye, nka “n’tɛ bɔ misiri kɔnɔ ,” i b’a sɔrɔ jurunantigi dɔ bɛ i kɔnɔna so.|Être un grand religieux c'est d'accord, mais ne plus vouloir quitter la mosquée, c'est qu'un créancier t'attend à la maison.
Mɔgɔ be mɔgɔ kɛ mɔgɔ ye.|C'est l'homme qui fait l'homme.
Mɔgɔ bennin fla tɛ se ka ɲɔgɔ lawuri.|Deux hommes tombés ne peuvent pas se relever l'un l'autre.
Mɔgɔ bɛ Ala ye hali n’i ma jan.|On peut voir Dieu, sans même se coucher sur le dos.
Mɔgɔ bɛ ban ko dɔ ma, k’a sɔrɔ i m’a fɔ “n’tɛ”.|On peut refuser une chose, sans dire "non!"
Mɔgɔ bɛ baroda dɔ ra, nka ɲanafin bɛ i ra.|On peut être dans une compagnie, et s'ennuyer.
Mɔgɔ bɛ bɔ jɔ fɛ, nka a ɲamisɛn ko tɛ.|Un homme peut passer à travers un filet, mais ce n'est pas le filet aux petites mailles.
Mɔgɔ bɛ fasa k’a sɔrɔ bana tɛ i ra.|L'homme peut maigrir sans être malade.
Mɔgɔ bɛ i diyanyako lɔn, nka Ala le bɛ i nafako lɔn.|L'homme sait ce qui lui plaît, mais c'est Dieu qui sait ce qui est pour son bien.
Mɔgɔ bɛ jɔn tu dɔ ra, nka i sirifu bɛ bɔ a ra.|Tu peux mépriser une forêt, mais il y aura suffisamment de lianes dedans pour t'attacher.
Mɔgɔ bɛ kɔ min lɔn, i bɛ to ji le ra.|La rivière qu'on connait, c'est dans son eau qu'on se noie.
Mɔgɔ bɛ lon dɔ kɔlɔnnin sogi, lon dɔ minlɔgɔnin kosɔn.|On creuse un petit puits, pour la petite soif d'un jour.
Mɔgɔ bɛ se ka kɛ kolɔnbaga ye, nka i tɛ se ka kɛ ko bɛɛ lɔnbaga ye.|On peut être un savant, mais on ne peut pas tout savoir.
Mɔgɔ bɛ tagama bi, sini i bɛ ŋunuma, i bɛ ŋunuma bi, sini i bɛ tagama.|L'homme marche aujourd'hui sur deux pieds, demain à quatre pieds ; il va à quatre pattes aujourd'hui, demain il marche sur deux pieds.
Mɔgɔ bɛ tugu ka ŋuna, nka i tɛ se tugu ka wɔsi.|On peut gémir par exprès, mais on ne peut pas transpirer par exprès.
Mɔgɔ b'i ɲa ko, hali n’a cinnin lo.|On se lave les yeux, même si on est aveugle.
Mɔgɔ boritɔ bɛ pan ka fla min tigɛ, dagasigi tɛ masɔrɔ o ra tuun.|Un médicament qu'on coupe en courant, (s'agissant des feuilles) on n'a pas le temps de le préparer dans un canari.
Mɔgɔdarabɔsanji tɛ fin terebɔ fɛ.|Pluie de provocation ne commence pas à l'est.
Mɔgɔ denmisɛnman bɛ lɔgɔkurun min ɲini, i cɛkɔrɔbaraman bɛ ja o le ra.|Le morceau de bois que tu as cherché pendant dans ta jeunesse, c'est avec ça que tu te chauffes dans ta viellesse.
Mɔgɔ dɔ bɛ faninya tigɛ i ye, sabu a bɛ malo i ma, walama a bɛ siran.|Quelqu'un te ment, simplement parce qu'il a honte de toi, ou qu'il a peur.
Mɔgɔ fla le bɛ kɛlɛ kɛ.|C'est deux personnes qui se battent.
Mɔgɔ, i bɛ na mɔgɔw le boro, i bɛ taga mɔgɔw le boro.|Homme, tu viens (au monde) entre les mains des hommes, tu repars entre les mains des hommes.
Mɔgɔ ka kan ka i pan ko dɔw kunna, janko i ye dunuɲa diyabɔ.|On doit sauter par dessus certaines choses afin de pouvoir jouir de la vie.
Mɔgɔ ka kan ka i senkɔrɔyɔrɔ flɛ sani i ye cun.|On doit regarder par terre avant de sauter en bas.
Mɔgɔ kelen hakiri kelen, mɔgɔ fla hakiri fla.|Un homme, une idée, deux hommes, deux idées.
Mɔgɔ kelen ɲa wɔɔrɔ, o tuma fiyen tɛ dugu ra wa?|Toi seul tu as six yeux! Est ce qu'il n'y a pas d'aveugle dans le village?
Mɔgɔ kelen saya tɛ dugu diman ci, nka a b’a suma.|La mort d'un seul homme ne détruit pas le village, mais elle refroidit le village.
Mɔgɔ koji bɛɛ tɛ bɔn i kan.|Quand on se lave, toutes les goûtes d'eau ne touche pas le corps.
Mɔgɔkɔrɔbakuma ye surugubo le ye, n’a kɔrɔra a bɛ gbɛ.|Les paroles des anciens c'est les excréments de l'hyène, avec le temps, elles blanchissent.
Mɔgɔ kɔrɔtɔnin tɛ fa to gbannin na, sani to ye suma o b’a sɔrɔ a wurira.|L'homme pressé ne mange pas à sa faim quand le plat est chaud, avant que le plat ne se refroidisse il se lève.
Mɔgɔ kumabari ta kuma b’a kɔnɔ.|L'homme qui ne parle pas, a sa parole dans son coeur.
Mɔgɔ lɔnnin ci ka di.|Il est plus facile d'envoyer en mission un homme qui est débout.
Mɔgɔ m'a fɔ ko lo bɔra, lo ko: "N'bɔra!".|Personne n'a dit que le masque est sorti et le masque lui même dit: "Je suis sorti!"
Mɔgɔ man kan ka bere to i boro, k'a to wuru ye i kin.|Il ne faut pas laisser le chien te mordre alors que tu as un bâton en main.
Mɔgɔ man kan k’a borokɔnɔ jɛgɛ firi a senkɔrɔta kosɔn.|Il ne faut pas jeter le poisson que tu as en main au profit de celui qui est à tes pieds.
Mɔgɔ man kan ka don kɛrɛ ra ko jo bɛ i fɛ, i bɛ don kɛrɛ ra le ko mɔgɔw b’i kɔ.|N'entre pas dans une querelle parce que tu as raison, entre parce que tu as des gens derrière toi.
Mɔgɔ min b’a fɔ i diminin ma ko i ye sabari, ni o ma kɛ i ɲin ye, i jugu tɛ.|Celui qui te dit de te calmer quand tu es fâché, même s'il n'est pas ton ami, il n'est pas ton ennemi.
Mɔgɔ min bɛ na i ta so o ka fisa n’i ye.|Celui qui vient chez toi est mieux que toi.
Mɔgɔ min bɛna to ji ra, hali ni o tigi ka nɛgɛ wulen le sɔrɔ a b’a mina.|Celui qui va se noyer saisira même un fer rougi au feu pour se sauver
Mɔgɔ, n’i worora, i bɛ neni, i bɛ kɔrɔfɔ.|Homme, si tu nais, tu sera insulté, tu seras frappé.
Mɔgɔ ɲumankɛtɔ bɛ juguman kɛ.|En faisant le bien, parfois il arrive du mal.
Mɔgɔ saba bɛ dunuɲa ra minw tɛ fɛn sɔrɔ: jeli bobo, donso sɔgɔsɔgɔtɔ, karanmɔgɔ aladaribari.|Il ya trois hommes dans le monde qui ne seront jamais riches: le griot muet, le chasseur qui tousse, le religieux qui ne prie pas.
Mɔgɔsaba teriya, kelen b’a kun fɛ.|Amitié de 3 personnes, l'un restera seul.
Mɔgɔ si kelen fa tɛ Ala ye.|Personne n'a Dieu pour père à lui tout seul.
Mɔgɔ si t’a fɔ ko: “Ala ye baraka don sama ra !”|Personne ne dira: "Que Dieu donne de la force à l'éléphant!"
Mɔgɔ si t'a ta dugusira yira n'a numanboro ye|Personne ne montre le chemin de son village avec la main gauche.
Mɔgɔ sɔnkɔrɔ bɛ kiti gboya i ra.|Ton ancien caractère peut te faire perdre le procès.
Mɔgɔ tɛ bakɔrɔnin karifa surugu ma.|On ne confie pas le bouc à l'hyène.
Mɔgɔ tɛ bɔ dundunfɔyɔrɔ ra ka na i kɔnɔbara fɔ so kɔnɔ.|On ne quitte pas auprès du tam-tam, pour aller taper son ventre à la maison
Mɔgɔ tɛ den dɔn, k’a kɔsin i yɛrɛ ma.|On ne fait pas danser un bébé (sur les genoux) en ayant son dos vers soi même.
Mɔgɔ tɛ don sulaw ta toron na, ko dɔ kukala kana maga i ra.|On ne peut pas entrer dans le jeu des singes, sans que la queue de l'un ne te touche.
Mɔgɔ tɛ gban mɔgɔ kɔ gbansan, mɔgɔ fana tɛ faran mɔgɔ ra gbansan.|On ne suit pas quelqu'un san cause, on ne se sépare pas de quelqu'un sans cause.
Mɔgɔ tɛ gbɔncɛkɔrɔba degi tigawɔrɔ ra.|On n'apprend pas au vieux singe à décortiquer les arachides.
Mɔgɔ tɛ hɛra sɔrɔ n’i ma ɲani.|On n'a pas le bonheur sans souffrance.
Mɔgɔ tɛ i ɲaw di i buranw ma ka flɛri kɛ n’i tɔn ye.|On ne donne pas ses yeux à ses beaux parents, pour regarder avec sa nuque.
Mɔgɔ tɛ i ta samafagamugu cɛn sonzannin na.|On ne gaspille pas sa cartouche pour éléphant sur un lapin.
Mɔgɔ tɛ i tere kɛ, ka mɔgɔ wɛrɛ tere kɛ.|On ne peut pas faire son temps, et faire le temps de quelqu'un d'autre.
Mɔgɔ tɛ jakuma lɔgɔ girinwagati lɔn.|On ne sait pas à quel moment un marché de chats sera troublé.
Mɔgɔ tɛ kala ye, ka i ɲa turu a ra.|On ne voit pas la paille pour aller se piquer les yeux dessus.
Mɔgɔ tɛ kɛ i yɛrɛ jugu ye.|On ne peut pas être son propre ennemi.
Mɔgɔ tɛ kɛ kɔgɔ ye ka bɔ nan bɛɛ ra.|On ne peut pas être du sel pour saler toutes les sauces.
Mɔgɔ tɛ ko diman dabla gbansan.|Quand on arrête de faire quelque chose qu'on aimait, il y a une raison.
Mɔgɔ tɛ kɔ jan tigɛ n’i ma i kɔ flɛ.|On ne peut pas traverser une longue rivière sans regarder en arrière.
Mɔgɔ tɛ mɔgɔ lɔn ni aw ma jɛn ko ra.|On ne peut pas connaître quelqu'un si vous n'avez rien fait ensemble.
Mɔgɔ tɛ mɔgɔ lɔn n’i ma se o ta so.|On ne connait pas quelqu'un sans arriver chez eux.
Mɔgɔ tɛ ɲɛgɛnɛ kɛ, banba ye bɔ o ra ka i mina.|On ne peut pas uriner et qu'un crocodile en sort pour vous attraper.
Mɔgɔ tɛ se ka to bagamin na ka yɛrɛko.|On ne peut pas boire la bouillie et rire en même temps.
Mɔgɔ tɛ so san a senkan fɛ.|On n'achète un cheval en se bansant sur le seul bruit de ses pas.
Mɔgɔ tɛ surugulɔgɔ wurituma lɔn.|Personne ne sait à quel moment le marché des hyènes ferme.
Mɔgɔ tɛ to ko ra k’a diya lɔn.|On ne sait pas le goût d'une chose tant qu'on est dedans.
Mɔgɔ tɛ to kɔlɔn kɔnɔ ka i bencogo ɲa fɔ.|On ne peut pas rester au fond d'un puits et expliquer comment on est tombé dedans.
Mɔgɔ tɛ to yiri ra ka yiri sɔrɔ, nka mɔgɔ bɛ to mɔgɔ ra ka mɔgɔ sɔrɔ.|On ne peut pas rester sur un arbre et atteindre un autre arbre, mais on peut rester avec une personne et atteindre une autre.
Mɔgɔtigi nakan tɛ firi.|Quand l'homme populaire arrive on le sait.
Mɔgɔw k'i lɔn ni turunin min ye n'i ka o li, mɔgɔw bɛ firi i ma.|La coupe de cheveux avec laquelle on te connaît, si tu l'enlèves, les gens ne te reconnaîtront plus.
Mɔgɔ worora ka jori min to i facɛ sen na, i ka kan ka deri o kasa ra.|La plaie que tu as trouvé sur le pied de ton père à ta naissance, tu dois être habitué à l'odeur de cette plaie.
Mɔgɔ yecogo bɛ i ta batigɛsara nɔgɔya.|Ta physionomie peut diminuer le prix de ta traversée du fleuve (en pirogue)
Mugu min bɛ fagama faga, jamanaden bɛɛ bɛ o cikan mɛn.|Le coup de fusil qui tue le chef, tout le peuple va l'entendre tirer.
Mun bɛ sen ni kun se ɲɔgɔn ma, ni lakojuguya tɛ ?|Un pied et une tête ne se toucheront jamais, si ce n'est à cause d'une mauvaise manière de se coucher.
Mun le ka di nanbara ye, “na n'bi ta” kɔ ?|Qu'est ce que le paralysé aime, si ce n'est " viens que je te prenne!"
Munyu le bɛ si bɔ sisɛfan na.|C'est la patience qui donne des plumes à l'oeuf.
Murunin da diyakojugu bɛ a la faran.|Couteau trop tranchant déchire son foureau.
Muso dentan, muso saraman.|Femme sans enfants, femme coquette.
Muso ɲanin den le bɛ cɛn.|C'est l'enfant de la bonne femme qui est mal éduqué.
Na an ye jɛn, o kɔrɔ le ye na an ye kɛrɛ.|« Viens qu'on vive ensemble! » est égal à « viens qu'on fasse palabre! »
Na diman tɛ mɛn daga kɔnɔ.|Bonne sauce ne dure pas dans la marmite.
N’a fɔra ko mɔgɔ ka di, a sɔgɔ tɛ, a kɛwale lo.|Quand on dit que les hommes c'est bon, il ne s'agit pas de leur chair, c’est leurs actes
Naloman ye dugu fagama dɔ le ye.|Le sot est lui aussi un personnage important du village.
Nanforotigiya tɛ kelennamiiri sa.|La richesse n'empêche pas d'être solitaire et pensif.
« Na n’nyu ! na n’jigi ! » Kunnankolon tagama le ɲɔgɔn tɛ.|« Venez me charger! Venez me décharger! » Le mieux c'est de voyager sans bagages.
Nataba ni lɔgɔlamini.|Qui veut beaucoup en payant moins, fera plusieurs tours dans le marché
N’bɛ nin fɛ, n’tɛ nin fɛ, fla le yirara i ra.|Je veux ça, pas ça, c'est qu'on t'a montré deux choses.
« N’donna, nka n’ma foyi ta! » I donbariya le tun ka fisa.|« Je suis entré, mais je n'ai rien pris! » le mieux serait de ne même pas entrer.
Nɛgɛ tigɛyɔrɔ ye numuntogo ye.|C'est dans la forge qu'on coupe le fer.
Ni Ala ka sen kari a b’a tagamacogo yira.|Si Dieu casse le pied, il montre comment marcher avec.
Ni baara tɛ i ra, kɛ mɔgɔ fɛ min tɛ i fɛ.|Si tu n'as vraiment rien à faire, cherche la compagnie de quelqu'un qui ne t'aime pas
N’i bamuso n’a sinamuso bɛ kɛrɛ ra, i sago ye ko i bamuso y’a ben.|Si ta mère et sa rivale se battent, ton souhait est que ta mère la fasse tomber.
Ni ba nɔnɔ ka diya i da cogo o cogo, i tɛ sɔn ka bakɔrɔnin wele ko i belencɛ.|Tu as beau aimer le lait de cabri, tu ne vas pas quand même appeler le bouc ton oncle
Ni basa ku ma tigɛ a tɛ dinga yɔrɔ lɔn.|Tant qu'on ne coupe la queue du margouillat, il ne saura pas où est son trou.
Ni basi ka denmisɛn saran, o b’a fɔ ko a domukojuguya lo, nka n’a ka mɔgɔkɔrɔba saran, o b’a fɔ ko ji le m’a labɔ.|Quand le couscous prend l'enfant à la gorge, on dit c'est parce qu'il est gourmand; mais quand il prend un adultte à la gorge, on dit c'est parce qu'il n'y a pas assez d'eau dans le couscous.
Ni ba tɛ mɔgɔ min fɛ, i b’i mamamuso sin min.|Celui qui n'as plus de mère, tête le sein de sa grand-mère.
N’i bɛ borira ko su bɛna ko i ra, n’i benna ka i woto kari, dugu bɛna gbɛ i ra.|Si tu cours pour arriver avant la nuit, si tu tombes et te casse la jambe,tu arriveras après la nuit.
N’i bɛ kurusi karan sula ye, i b’a kukala bɔyɔrɔ kɛ a ra.|Si tu veux coudre un culotte pour un singe, il faut prévoir une place pour sa queue.
N’i bɛ ɲa, ɲa ye kelen ye, kana ɲa ni juguya jɛn.|Si tu veux être bon, il y a une seule façon de l'être, ne sois pas bon et mauvais à la fois.
N’i bɛ suma feere, ni fitiri ka se i ye dɔ bɔ a sɔngɔ ra, n’o tɛ a bɛ gbala.|Si tu vends l'ombre, si le crépuscule arrive, diminue le prix, sinon plus personne n'achètera.
Ni biɲɛ ɲinibaga yɛrɛ sen lanin bɛ biɲɛnkala kan, o biɲɛ tɛ se ka ye.|Si celui qui cherche l'aiguille a lui même son pied dessus, on ne verra jamais cetta aiguille
N’i boro ka i don ko ra i bɛ bɔ, n’i sen ka i don i bɛ bɔ, nka n’i da ka i don i tɛ bɔ.|Si ta main te met dans une affaire, tu peux t'en sortir, Si ton pied te met dans une affaire, tu peux t'en sortir, mais si ta bouche te met dans une affaire, tu ne peux pas t'en sortir.
Ni dalateriya ka kungo min lase mɔgɔ ma, sennateriya tɛ se ka o bɔ i kunna.|Un problème que tu as créé en parlant trop vite, tu ne peux pas t'en défaire même en courant vite.
Ni dɔrɔ kunna a daga kɔnɔ, a ka kan ka kun a minbaga kɔnɔ.|Si la marmite a pu contenir le dolo, le ventre du buveur aussi doit pouvoir contenir le dolo.
Ni dundun ka faran, a bɛ kɛ duduntigi kelen ta ye.|Quand le tam-tam se déchire, ça devient l'affaire du batteur seul.
Ni fɛn ka dɔgɔya yɔrɔ min a bɛ kari o yɔrɔ le ra. |Là où c'est mince, c'est là que ça se casse.
Ni fɛn ɲuman tɔmɔbaga ko a tɛ ɲa a kɔ, o burunna min fɛ o bɛna a yɛrɛ faga.|Si celui qui a ramassé l'objet ne veut plus s'en passer, et que dire de celui qui l'a égaré? il va se tuer!
Ni fiyentɔ ko san finna, i b'a sɔrɔ a bɛ dɔɔni yera.|Si l'aveugle dit que le ciel est sombre, c'est qu'il voit un peu.
Ni fiyentɔ sen ka juru tigɛ, mɔgɔ tɛ jigi a ra, nka n’a ɲi ka tigɛ, sariya b’a mina.|Si le pied de l'aveugle coupe la corde, on ne peut lui en vouloir, mais si ses dents coupent la corde, il est condamnable.
Ni folo bɛ i kan na, n’i bɛ ko, i b’a ko, n’i bɛ mun i b’a mun, nka a man di i ye.|Si tu as le goitre, quand tu te laves, tu le laves, quand tu t'embaumes, tu l'embaumes, mais tu ne l'aimes pas.
Ni forotobɔrɔ ka kɔrɔ cogo o cogo, tisota bɛ sɔrɔ a ra.|Le sac de piment a beau être vieux, il y en aura suffisamment pour faire éternuer.
Ni fugula fiyeerera k’a wari don kɔgɔ ra, o bɛɛ ye kunkolo ta nafa le ye.|Quand on vend le chapeau pour acheter du sel, Tout est à l'avantage de la tête.
Ni hakɛ mɛɛnna, i b’a sɔrɔ a tagara bere kɛnɛ le tigɛ.|Si l'offense tarde à être punie, c'est qu'elle est allée chercher un bâton frais.
Ni jeliya diyara jeli min na, o b’ a yɛrɛ kɛ Kuyate ye.|Le Griot qui a bien reussi, se fait appeler Kouyaté.
Ni jende ka yiri tigɛ, a kala b’a kɔrɔfɔ.|Quand la hache coupe l'arbre, la manche de la hache s'en plaint.
Ni jɛgɛdenin ka fɛn o fɛn fɔ banba ta ko ra, a fɔbaga le k’a fɔ.|Tout ce que le petit poisson dit à propos du crocodile, il le dit en tant qu'un vrai témoin.
Ni jidaga ni jifiyɛ bɛɛ sera kɔlɔnda ra, foyi wɛrɛ ma to so tuun.|Si la jarre d'eau et le gobelet à boire sont tous deux au bord du puits, Il n'y a plus rien qui reste à la maison.
Ni ji tun bɛ mɔgɔ gbɛ, kɔlɔnkɔnɔtɔri tun bɛ arabu ye.|Si l'eau pouvait rendre blanc, le crapaud qui vit dans le puits serait un arabe depuis.
Ni juru bɛ so sen na, fali b’a dan bori ra.|Si le cheval a la corde au pied, même l'âne peut courir plus vite que lui.
N’i ka basa jannin ye, a su lo.|Dès que tu vois le margouillat sur le dos, c'est qu'il est mort.
N’i ka bon dɔ tuba wuri ka taga a biri dɔwɛrɛ kunna, n’a ma bonya a ma, a bɛ dɔgɔya a ma.|Si tu enlèves le toit d'une maison pour aller le mettre sur une autre, il sera ou trop large, ou trop étroit.
N’i ka daga ta k’a lase i kinbirikun na, Ala b’a lase i kun na.|Si tu prends la marmite et la mets sur les genoux, Dieu t'aidera à la mettre sur la tête.
N’i ka da ta kumu bɔ a ra, a kɛra flaburu gbansan ye.|Si tu enlèves l'acidité de l'oseille, ça devient une simple feuille.
N’i ka dingakɔnɔfɛn bɔtɔ bugɔ, a bɛ sekɔ dinga kɔnɔ.|Si tu tapes l'animal qui en train de sortir du trou il y retourne.
N’i ka dugulen ta filen sonya, i bɛna taga a fiyɛ yɔrɔ wɛrɛ le.|Si tu voles la flûte d'un autochtone, c'est que tu iras la jouer ailleurs.
N’i ka dunuɲa sobagirin, i na ban k’a nɔnzitagama.|Si tu promènes le monde au grand galop tel un cheval tu finiras par le promener à pas hésitant tel un caméléon.
N’i ka dunuɲa yaala, n’i ma fɛn sɔrɔ, i n’a fɛn lɔn.|Si tu promènes le monde, même si tu ne trouves rien, tu sauras bien des choses.
N’i ka fɛn min ye a bɛ tagama surugu kɔ fɛ su fɛ, a den lo.|Ce que tu vois marcher derrière l'hyène la nuit, c'est son petit.
N’i k’a fɔ i sɔn ma ko “ n’kɔnɔ yan, ” n’i mɛnna a b’i sɛgɛrɛ.|Si tu dis à ton caractère: "Attends moi ici!" si tu dures trop il va te rejoindre.
N’i ka i den tɔgɔla ko “baba kumandan”, i bɛna lɛnpo sara a ye.|Si tu nommes ton fils "Monsieur le commandant" tu vas toi même lui payer l'impôt.
N’i ka i den wele ko sama, a bɛ yiri fɔlɔ min kari, o bɛ ben ele yɛrɛ le kan.|Si tu appelles ton fils " Éléphant", le premier arbre qu'il va faire tomber, tombera sur toi même.
N’i ka i ta wurujugu faga, dɔ ta b’i kin.|Si tu tues ton chien méchant, celui d'un autre va te mordre.
N’i ka i tericɛ ta derege ye i jugu kan na, aw farantuma le sera.|Dès que tu vois ton ennemi s'habiller avec la chemise de ton ami, c'est que votre amitié est arrivée a son terme.
N’i ka i yɛrɛ kɛ fiyentɔ ta dɔnkirilaminabaga ye, a bɛ gbara i ra.|Si tu te fais répondeur du chant de l'aveugle, c'est de toi qu'il va s'approcher.
N’i ka jebagatɔ ta nanji min, n’i ma fɔ den bɔra a fa fɛ, i n’a fɔ a bɔra a ba fɛ.|Si tu bois la sauce de la femme qui vient d'accoucher, ou tu diras que l'enfant ressemble à son père, ou tu diras qu'il ressemble à sa mère.
N’i ka ji kɛ naloman kun kolon kɔnɔ, a b’a susu.|Si tu mets de l'eau dans un mortier pour le sot, il va piler.
N’i ka ko kɛ sabaga ra, a bɛ sa n’a ye a kɔnɔ, n’i ka ko kɛ mɔbaga ra, a bɛ mɔ n’a ye a kɔnɔ.|Si tu fais quelque chose au mourant, il mourras avec ça dans son coeur, si tu fais quelque chose à l'enfant, il grandira avec ça dans son coeur.
N’i ka lagbɛrikɛbaga ye a bɛ kasira, a ma konyuman le ye lagbɛri ra.|Quand tu vois que le charlatan se met à pleurer, c'est qu'il n'a pas vu quelque chose de bon dans la divination.
N’i k’a mɛn ko: “An ye taga kunu ta yɔrɔ ra”, kunu ta tun diyara.|Quand quelqu'un dit: "Allons à l'endroit d'hier, c'est que cela avait été bon hier.
N’i ka mɛn ko dunuɲa ka di, i b'a dimanyɔrɔ le ra.|Quand quelqu'un dit que le monde c'est bon, c'est qu'il est au bon endroit.
N’i ka mɛn ko "n’bɛ n’ta dɛgɛ bla a ye kumu," i bɛ dɔ ta kɛnɛ le kan.|Si tu dis que tu vas laisser ton dégué se fermenter bien, c'est que tu es en train de boire le dégué frais d'un autre.
N’i k’a mɛn ko: "n’bonbonsi ka jan tasumafiyɛ ma”, a fiyɛbaga le bɛ i boro.|Quand tu dis que ta barbe est trop longue pour souffler sur le feu, c'est que tu as quelqu'un d'autre pour le souffler.
N’i k'a mɛn ko ne ɲɔgɔn siyɛntabaga tɛ, i sen fla bɛ dugu ma le.|Pour être le meilleur lutteur, il faut d'abord avoir ses deux pieds à terre.
N’i ka mɛn ko: "n’ta juru bɛ banbandennin na", i k’a layɔrɔ kelen le lɔn.|Si tu dis que le petit caïman te doit, c'est que tu sais bien où il se couche .
N’i ka mɛn ko n’tɛ faninyatigɛbaga lamɛn, a ɲajitɔ le m’a se i ma.|Si tu dis que tu n'écoutes jamais un menteur, c'est qu'il n'est pas venu chez toi en larmes.
N’i k'a mɛn ko :“N’tɛ muso kan mɛn,” i ni muso le ma si.|Si tu dis que tu n'écoutes pas les femmes, c'est que tu n'as pas passé la nuit avec une femme.
N'i ka nɛn ye borokandenw cɛ ra, ni li tɛ yi, kɔgɔ le bɛ yi.|Quand tu vois la langue entre les doigts, C'est qu'il y a du miel, ou du sel.
N'i ka ɲinan jalaki, i ye sunbara fana jalaki.|Si tu accuse la souris, il faut aussi accuser le soumbala.
N’i ka paraw ni bɔrɔkɔnɔbla bɛɛ kɛ kelen ye, i bɛna fɛn ɲanaman dɔ bla i yɛrɛ kun.|Si dès le premier coup de bâton tu mets l'animal dans le sac, tu finiras par mettre dans ton sac un animal encore vivant.
N’i ka sa ye ka bere min to i boro i b’a faga ni o ye.|Le bâton que tu as en main au moment où tu vois le serpent, c'est avec celui là que tu le tues.
N’i ka simanjugu dan, n’a ma falen i ɲana, a bɛ falen i den ɲana.|Si tu sèmes le mauvais grain, s'il ne germe pas en ta présence, il germera en présence de ton fils.
N’i ka tere kuru bɛɛ kɛ ka musokɔrɔnin banba, wula fɛ n’i ka fofo, o fofori le bɛ to a kɔnɔ.|Si tu mets la vieille femme au dos pendant toute la journée, et que dans la soirée tu la traînes à terre, c'est le fait de l'avoir traînée qu'elle va retenir.
N’i ka tɔgɔ min la i ta wuru ra, bɛɛ b’a wele o le ra.|Le nom que tu donnes à ton chien, c'est par ce nom que tous vont l'appeler.
N’i ka tuganin ye a ka duga fa neni, a jatigicɛ ye kɔnɔsogolon le ye.|Si tu vois que la tourterelle insulte le vautour, c'est qu'elle a l'autruche comme tuteur.
N’i k’a ye ko dugu diyara, dɔ le ye naloman ye.|Si tu vois que le village est intéressant, c'est que quelqu'un est sot.
N’i k’a ye ko tasuma bɛ ji gban, fɛn le bɛ o fla ni ɲɔgɔn cɛ.|Si le feu chauffe l'eau, c'est parce qu'il y a quelque chose entre les deux.
N’i k’a ye ko tɛgɛcira sangafobaga ɲa kan, i b’a sɔrɔ a k’a da don cɛntakokuma ra le.|Quand tu vois qu'on gifle celui qui est venu saluer aux obsèques, c'est qu'il s'est mêlé aux problèmes d'héritage.
Ni kɛnɛsigiri ma bɔ, kɛnɛsigiblan ye sekɔ a tigi ma.|Si la circoncision n'a plus eu lieu, qu'on retourne le caleçon de circoncision à son propriétaire.
N’i ko i cɛkaɲi su fɛ, ni dugu gbɛra i bɛ ye.|Si la nuit tu dis "je suis beau" Quand il fera jour, on te verra.
N’i ko i ma fitirimawaliya lɔn, i ma ɲunan caaman le kɛ.|Si tu ne connais pas l'ingratitude, c'est que tu n'a pas fait beaucoup de bonnes actions.
Ni ko ka gban, ka gban, a bantɔ lo.|Quand une chose chauffe, chauffe, c'est qu'il arrive à sa fin.
N’i ko mɔgɔ ma ko :“Ala ye i sara !” N’a digira a ra, i b’a sɔrɔ a ka min kɛ o ma ɲi le.|Si tu dis à quelqu'un " que Dieu te le rende!" Si ça lui fait mal, c'est que ce qu'il a fait est mauvais.
N’i ko sigi ye diya, fɔ dɔ y’i la.|Pour que la cohabitation soit agréable, il faut que l'un se couche (en signe d'humilité).
Ni kɔnɔboritɔ ko dugu taranna, i b’a sɔrɔ kɔnɔbori tɛ tiɲɛ ye le.|Si celui qui a la diarrhée dit que la nuit est trop avancée pour sortir, c'est qu'il n'a pas vraiment la diarrhée.
Ni kɔnɔdimi kirisitigi yɛrɛ kɔnɔ k’a dimi, dɔwɛrɛ tɛ o ra, a ye daji kunu.|Si le maître des formules magiques a lui même mal au ventre, il n'y a rien d'autres que d'avaller sa salive.
Ni kuma ma gboya mɔgɔ min ɲana, a tɛ diya o tigi ɲana.|Il faut que la parole soit dure en ta présence, pour qu'elle devienne douce en ta présence.
Ni kuma o kuma ɲagaminna, cɛkɔrɔbaw le b'a ɲanabɔ.|Quand la parole est mélangée, c'est les vieux qui l'arrangent.
Ni kuma tɛ i da, a fɔ :“ jɛgɛ bɛ ji ra !”|Si tu ne sais pas quoi dire, dis que le poisson vit dans l'eau.
N’i ma ba tigɛ ban, i kana banba neni.|Si tu n'as pas encore traversé le fleuve, n'insulte pas le crocodile.
Ni miiri bɔra mɔgɔya ra, mɔgɔya banna.|Si la réflexion manque dans les relations humaines, Il n'y a plus de relations humaines.
Ni misi panna a biribaga kunna, ele sigikun ye mun ye tuun?|La vache a sauté par dessus la tête de celui qui la trait, et toi tu es assis pour quoi encore?
Ni mɔgɔ ma ben, a kunnaminan suguya tɛ lɔn.|Tant que l'homme ne tombe pas, on ne sait pas ce qu'il ya dans le panier qu'il porte.
Ni mɔgɔ ma sa, ko bɛɛ juru b’i ra.|Tant qu'on n'est pas mort, tout peut arriver.
Ni mɔgɔ saba ka jɛn kurusi kelen na, dɔ ta lebu le b’a ra.|Si trois personnes partagent une culotte, l'un court le danger de rester tout nu un jour.
Ni mɔgɔw ka i wele ko “Sama !” i kana a to i wele tuun ko “sonzan !”|Si les gens t'appellent " Éléphant!" ne leur donne pas l'occasion de t'appeler " Lapin!"
Ni muru da ka diya cogo o cogo, a tɛ se k’a kala tigɛ.|Le couteau a beau être tranchant, il ne peut pas couper sa manche.
Ni nafigi ka don mɔgɔ n’i ta kurusi cɛ, i b’a bɔ k’a firi.|Si le calomniateur se met entre toi et ton pantalon, tu va enlever ton pantalon et le jeter.
Ni nin yiriboro tun tɛ ne tun bɛna kɔnɔnin nin faga, ni nin yiriboro tun tɛ fana kɔnɔnin tun tɛ sigi yan.|Sans cette branche, j'aurai tué cet oiseau, mais sans cette branche, l'oiseau ne s'aseyerait pas là non plus.
Nin ka di, nin ka gbo, totasa fla le bɛ mɔgɔ bla o fɔ ra.|C'est quand tu as deux plats devant toi que tu peux dire: "Ceci est bon! Ceci est mauvais!"
Nin muso cɛ kaɲi! A jogo ka ɲi, a cɛ ɲininka fɔlɔ dɛ!|Ah! Cette femme est belle! Elle a un bon caractère! Pose la question à son mari d'abord.
Ni sa kunkolo tigɛra, a tɔ kɛra jurukisɛ ye.|Quand la tête du serpent est coupé, il n'est plus qu'un corde.
Ni sanji ka i sɔrɔ togo kolon kɔnɔ, a tɛ lɔ tuun.|Si la pluie te trouve sous une cabane délabrée, elle ne s'arrête plus de tomber.
Ni san ka pɛrɛn bɛɛ b'a kun le mina|Quand le tonnerre gronde, c'est sa propre tête que chacun protège
Ni san ɲagamina, kun tɛ karojate ra tuun.|Quand on a perdu le fil des années, il ne sert plus à rien de compter les mois.
Ni sisɛnin ma “Kus !” mɛn, a na “Paraw ” mɛn.|Si la poule n'entend pas la voix qui lui demande de quitter, elle entendra le coup de bâton qui lui demande de quitter
Ni so bɛ i ben, i tɛ a toro ye.|Quand le cheval va te faire tomber, tu ne vois pas ses oreilles.
Ni sosonin bɛ dɔnkɛ, a b’a senkalaninw kɔrɔsi.|Quand le moustique danse, il fait attention à ses petites jambes.
Ni sula boro dɛsɛra zaban min ma, a b’a fɔ ko o kumunin lo.|Quand le singe ne peut pas atteindre la pomme sauvage il dit que ce fruit là est acide.
Ni sula ka yiri lɔn cogo o cogo, a bɛ kɔnɔ kɔ.|Le singe a beau savoir grimper à l'arbre, il vient après l'oiseau.
Ni sunbara ka diya, nɛrɛbɔbaga tɔgɔ tɛ fɔ.|Quand le sounbara est bon, on ne parle pas de celle qui a récolté le néré.
Ni surugu ka i muso ɲamɔgɔcɛ mina, mɔgɔ ma surugu ci, nka surugu ma kojugu kɛ.|Si l'hyène attrape l'amant de ta femme, personne n'a demandé ça à l'hyène, mais ce que l'hyène a fait là n'est pas mauvais.
N’i ta fɛ ka mɔgɔ ye, i bɛ yɛlɛma kongo kɔnɔ.|Si tu ne veux plus voir des hommes, tu déménages en brousse.
N’i tagara dugu min na ka taga a sɔrɔ bɛɛ lɔnin bɛ a sen kelen kan, n’i tɛ koɲinibaga ye, i fana ka kan ka lɔ i sen kelen kan.|Si tu vas dans un village, et tu trouves que tous habitants se tiennent sur un seul pied, si tu ne cherches pas un problème, tu dois aussi te tenir sur un pied.
Ni tasuma ka ba tigɛ a yɛrɛ ma, a bɛ kɛ a fagabaga boro kɔnɔgban ye.|Si le feu a traversé le fleuve tout seul, celui qui va l'éteindre aura des problème.
N’i tɛ jɛgɛ fɛ i bɛ mun kɛ kɔda ra ?|Tu n'aimes pas le poisson, et qu'est ce que tu fais au bord du marigot?
N'i tɔgɔjugu kɔnna i ɲa, i bɛ ko nka i tɛ gbɛ.|Si ton mauvais nom te devance, Tu vas beau te laver, tu ne seras jamais propre.
Ni tuganin kɔnna ɲɔfiyɛbagaw ɲa, a bɛna dɔ kɛ sigi ra.|Si la tourterelle arrive avant les vanneuses il attendra longtemps.
Ni tu ma gbasi a kɔnɔnɔfɛn tɛ lɔn.|Tant qu'on ne frappe dans le buisson on ne sait pas ce qu'il y a dedans.
N’i woroden kɛra sa ye, i b’a siri i yɛrɛ cɛ ra le.|Si ton fils est un serpent, il faut en faire une ceinture autour de toi.
Ni yiriba ka ben, kɔnɔw bɛ yɛrɛgɛ.|Dès que le gros arbre tombe, les oiseaux se dispersent.
Ni yirimɔgɔnin ko: “lɔgɔ tɛ tasuma ra !” Ale yɛrɛ tɛ lɔgɔ dɔ ye wa ?|Si la statue de bois dit qu'il n'ya pas de bois dans le feu, lui même n'est-il pas du bois?
N’ka so lɔn, n’ka ji lɔn, yɛrɛlɔn le ɲɔgɔn tɛ.|« Je sais aller à cheval, je sais nager, » le mieux c'est: « je sais me prendre. »
« N’kun gbanana », o ma se kariri ma, n’i ka banfla bɔ a bɛ lɔn.|Tu n'as besoin de jurer que tu es rasé, si tu enlèves le bonnet on saura.
Nɔnɔ tɛ dɛgɛ tiɲɛ.|Le lait ne gâte pas dégué.
« N’ta turujuru b’i ra, » o ye tiɲɛ ye, nka « n't’a fɛ ka i da turuman ye », o ye ɲadonya ye.|« J'ai un crédit d'huile sur toi »,c'est vrai. « Je ne veux plus te voir la bouche huileuse! » Ça c'est du mépris.
N’tɛ bla ɲa, n’tɛ bla kɔ, aw mɔgɔ fla lo.|Il faut que vous soyez trois en chemin pour dire: "Je ne veux ni être devant, ni être derrière!"
Ɲa bɛ ko dɔ ye, da tɛ o fɔ, tulo bɛ ko dɔ mɛn, da tɛ o fɔ.|Les yeux voient certaines choses que la bouche ne dit pas, l'oreille entend certaines choses que la bouche ne dit pas.
Ɲafɛn tiɲɛ, tiɲɛfɛn fana tɛ ɲa.|Ce qui est destiné à reussir ne se gâte pas, ce qui est destiné à être gâté, ne réussit pas.
Ɲa jiginta tɛ sunɔgɔ.|Des yeux sans espoir ne dorment point.
Ɲamanfirisununkun bɛ yi, nka balemafirisununkun tɛ yi.|Il y des endroits pour jeter des ordures, mais il y en a pas pour jeter des frères.
Ɲanafin bɛ fali faga.|L'ennui tue l'âne.
Ɲa tɛ doni ta, nka a bɛ doni gbiriman lɔn.|L'oeil ne porte pas de bagage, mais il sait reconnaître un bagage lourd.
Ɲinan bɛ tokalama ben, nka tɛ se k’a lawuri.|La souris fait tomber l'ustensile mais il ne peut pa le relever.
Ɲin gbɛra, nka basi b’a jukɔrɔ.|La dent est blanche, mais il y a du sang (rouge) en bas.
Ɲininkarikɛbaga tɛ firi.|Celui qui se renseigne ne peut pas se perdre.
Ɲɔ bɛ don ji ra lon min, a tɛ kɛ dɔrɔ ye o lon.|Le mil ne devient pas du dolo, le même jour où on le met dans l'eau.
Ɲɔ bɛ ɲɔbugu, nka takan le b’a ra.|Il ya du mil au village du mil, mais il ya une condition pour le prendre.
Ɲɔgɔn bamusodomu le bɛ subagaya diya.|Se manger les mères les uns des autres rend la sorcellerie intéressante.
Ɲɔ sannin, ani i yɛrɛ ta ɲɔ sɛnɛnin, o ɲɔ fla diya tɛ kelen ye.|Le mil acheté et le mil cultivé soi même n'ont pas le même goût.
Ŋanin firibaga bɛ ɲina, nka min k’a dɔn o tɛ ɲina.|Celui qui jette les épines oublie, mais celui qui les piétine n'oublie pas.
O b’a fɔ tɔri ma yɔrɔ min ko “Bagayɔgɔ”, o ye a buguyɔrɔ ye.|Le moment où on peut (remercier ) le crapaud et lui dire:"Bakayoko" c'est le moment où il se couche.
O bɛ den min bugɔ jɛgɛjalanɲimin kosɔn, ni o kasitɔ b’a da sin baji ma, o tɛ jɛgɛjalanɲimin dablakan ye.|L'enfant qu'on frappe parce qu'il mange du poisson séché, si ce dernier pleure en dirigeant sa bouche vers le fleuve, ce n'est signe qu'il veut arrêter de manger le poisson séché.
Sababu ko i kana to di ale ma, i kana ji di ale ma, nka i kana ɲina ale kɔ.|La cause dit ne pas lui donner à manger, ni a à boire, mais de ne pas l'oublier.
Sabari le bɛ si bɔ sisɛfan na.|C'est la douceur qui donne des plumes à l'oeuf.
Sa bɛ siran, a fagaba bɛ siran.|Le serpent a peur, celui qui le tue a aussi peur.
Safinaminan cira, wuru ta mun bɛ o ra?|La savonnière est cassée, en quoi cela regarde le chien?
Sagafaga tɛ wurufaga sa, sogo bɛɛ n’a domubaga lo.|Tuer le mouton, n'empêche pas de tuer le chien; chaque viande a son consommateur.
Sagajigi ta kunbiri bɛɛ tɛ siralɔnbariya ye.|Toutes les fois que le bélier baisse la tête ne veut pas dire qu'il ne sait pas la route.
Sama tɛ bonya kongo ma.|L'éléphant n'est jamais trop grand pour la forêt.
Sama tɛ dɛsɛ a ɲi kɔrɔ.|Les ivoires de l'éléphant ne peuvent pas être trop lourdes pour lui.
Sandigi ta foyi tɛ sisɛdenin ta manyumankokan na.|L'épervier s'en fout des cris de détresse du poussin.
Sanfinin bɛɛ ji tɛ na.|Tous les ciels nuageux ne donnent pas de la pluie.
Sani i ye fitinan mana fiyentɔ ye, o turu kɛ a ta sɔsɔ ra, o bɛna a mako ɲa.|Au lieu d'allumer une lampe à huile pour un aveugle, mets cette huile dans son haricot, c'est mieux pour lui.
Sani i ye jɛgɛjalan kɛ ka su fifa, i tun ye o kɛ nanji ye k’a di a ɲanaman ma.|Au lieu de souffler le mort avec le poisson séché, il aurait fallu lui en faire un bouillon de son vivant.
Sani i ye kɔgɔ kɛ i nɛnkun kan, a kɛ i kɛwale ra.|Au lieu de mettre du sel sur ta langue, mets le dans tes actes.
Saninfugula min bɛ kɛnɛbagatɔ kun na, banabagatɔ le bɛ o ye.|Les bien portants ont des couronnes en or, que seuls les malades voient.
Sa ɲa ka misɛn, nka boro tɛ su a ra.|Le serpent a des petits yeux, mais on ne peut y mettre le doigt. (en signe de mépris)
Saraka bɛ bɔ lon min a tɛ mina o lon.|Le sacrifice n'est pas exaucé le même jour où on l'offre.
Se kelen tɛ sira bɔ.|Un seul pied ne peut tracer un chemin.
Sibankɔnɔ tɛ laadikan mɛn.|Oiseau qui cherche la mort n'écoute pas de conseils.
Sigi ka di banba ye, nka a kukala tɛ sɔn.|le crocodile aimerait bien s'asseoir, mais sa queue refuse.
Sigi tɛ mɔgɔ sɔn, n’a kɛbaga t’i boro.|Rester assis ne récompense personne a moins que tu n'aies quelqu'un qui travaille pour toi.
Sin tɛ kɔnɔ fɛ, nka Ala b’a den baro.|L'oiseau n'a pas de mamelles, mais Dieu nourrit son petit.
Siradarakɔlɔn, n’i tagatɔ ma min a ra, i sekɔtɔ bɛ min a ra.|Puits au bord du chemin, si tu n'y bois pas en allant, tu y boiras au retour.
Sirakɔgɔmaw le bɛ ɲɔgɔn kinyɔrɔ lɔn.|C'est les tortues elles même qui savent où se mordre.
Siranbagatɔ kungo man dɔgɔ.|Un peureux ne manque de problèmes.
Sisɛba tɛ fɛnjugu yɛrɛgɛ k’a kɛ a den kɔrɔ.|La mère poule n'éparpille rien de mauvais devant ses poussins.
Sisɛ da ka dɔgɔ burufiyɛ ma.|La bouche de la poule est trop petite pour jouer du cor.
Sisɛ da ma ɲi murusankokuma ra.|Le poulet ne doit pas parler dans une affaire d'achat de couteau.
Sisɛ tɛ se ka ban dugutaga ma.|Le poulet ne peut pas refuser d'aller en voyage.
Sisɔrɔ le ka gbɛlɛn ka tɛmɛ siyɔrɔsɔrɔ kan.|Avoir la vie est plus difficile que d'avoir un endroit pour vivre.
Sogomuso kɔnɔman, donsocɛ muso kɔnɔman, dɔ na kɛ dɔ ta nanji ye.|La femme de l'antilope enceinte, la femme du chasseur enceinte, l'un sera sauce pour l'autre.
Sogotigi le bɛ tasumatigi yɔrɔ ɲini.|C'est celui qui a la viande qui cherche celui qui a le feu.
Solikabɔ fla tɛ se ka kɛ sɔgɔmada kelen na.|On ne peut se lever tôt deux fois dans la même matinée.
Sonzannin bɔra fali fɛ, nka a den tɛ.|Le lièvre ressemble à l'âne, mais il n'est pas son fils.
So ɲuman bɛɛ tɛ mɔgɔ siyɔrɔ ye.|Toutes les belles maisons ne sont pas pour y passer la nuit.
Sɔn tɛ pan, a den ye ŋunuman.|L'antilope saute, son petit ne va pas ramper.
Sɔrɔ bɛ fɛnɲini juguya.|Plus on trouve plus on cherche.
Subaga bɛ ɲina, nka a ka min den domu o tɛ ɲina.|Le sorcier oublie, mais celui dont il a mangé l'enfant n'oublie pas.
Sufɛkunli lawurira torotigɛ le ma.|La vraie raison pour raser la tête la nuit c'est de couper une oreille.
Sufɛmisi bɛɛ ye misifin.|Toutes les vaches sont noires la nuit.
Su fɛ yirikurun finman, a tɛ mɔgɔ mina, nka a b’i lasiran.|Morceau de bois noir la nuit, ça n'attrape pas l'homme, mais ça fait peur.
Sula tɛ wuru kɔdimitɔ lɔn.|Le singe ne cherche pas à savoir si le chien a mal au dos ou non.
Sulaw tɛ ɲɔgɔn kɔnɔ kabakariyɔrɔ ra.|Les singes ne s'attendent pas pour commencer à manger le mais.
Su min bɛ diya, o bɛ lɔn kabini a wulada.|Quand la nuit va être bonne on le reconnait dans la soirée
Sunɔgɔbagatɔ kunun ka di ni tugubaganci ye.|Le vrai dormeur est plus facile à réveiller que celui qui fait semblant.
Surugu dɛsɛra koro min na, wuru tɛ se ka o ta.|L'os que l'hyène n'a pas pu croquer, le chien ne peut pas le prendre.
Surugu fanin bɛ banaji fiyeere.|L'hyène rassasiée vend le bouillon de cabri.
Surugu fanin bɛ banogo kɛ kannakɔnɔn ye.|L'hyène rassasiée se sert des intestins de cabri comme collier.
Surugu fanin bɛ basen kɛ tagamabere ye.|L'hyène rassasiée, se sert de la patte de cabri comme simple canne.
Surugu kelen le bɛ surugu tɔ bɛɛ tɔgɔ cɛn.|C'est une seule hyène qui gâte le nom de toutes les autres hyènes.
Surugu ko ale tɛ mɔgɔnafin da domu|L’hyène dit qu’elle ne mange la bouche de l’homme
Surugu ko, ko mɔgɔ ka kan k’i yɛrɛ degi sensabatagama ra, lon dɔ kosɔn.|L'hyène dit qu'il est bon d'apprendre à marcher à trois pattes, en prévision de certains jours.
Surugu ko : “serilon na muso bɛɛ cɛ kaɲi.”|L'hyène dit: "le jour de la fête toutes les femmes sont belles.
Surugu senna ka teri tabadagamin ma.|L'hyène n'a pas le temps de fumer une pipe.
Surugu ta sogoflagbɛn, dɔ bɛ taga k’a dan.|Quand l'hyène court après deux proies, une lui échappe.
Surugu tɛ forotomugu lɔn domufɛn ye.|L'hyène ne reconnait pas la poudre de piment comme nourriture.
Ta bɛ ele bonbosi ra, ta bɛ i brancɛ bonbosi ra, fɔ hakiri le ye sɔrɔ o ko ra.|Ta barbe est en feu, la barbe de ton beau père est en feu, Il faut beaucoup de sagesse ici.
Tagamabari tɛ yirikurun lɔn fɛnjugu ye.|Celui qui ne marche jamais ne sait pas que la souche de bois est dangereuse.
Tagayɔrɔjan tɛ se ka mɔgɔ firi i facɛ ta bulonda ma.|On a beau voyager pour aller loin, on ne peut pas ne pas reconnaître la porte de la maison paternelle.
Tasuma minana, sogo tɛ n'boro, ne marora.|Le feu est allumé, et je n'ai pas de viande, quelle honte!
Tiga bɛɛ ye kelen ye, nka minw bɔra fara kelen kɔnɔ, olugu ta bɔna le ɲɔgɔn tɛ.|Tous les grains d'arachide se ressemblent mais ceux qui viennent de la même coque, se ressemblent le plus.
Tiga torinin kelen bɛ a tɔ bɛɛ gboya mɔgɔ da ra.|Un seul grain d'arachide pouri rend tous les autres mauvais dans la bouche.
Tinba b’a yɛrɛ sɛgɛ bala ye.|L'oryctérope se fatigue (à creuser les trous) pour le porc-épic.
Tiɲɛn bɛ mɔgɔ ɲa wulen, nka a t’a ci.|La vérité rougit les yeux, mais elle ne les casse pas.
Tiɲɛ ye naloman boda flɛ ye, nka "n’ bɛ taga n’somɔgɔw wele ka na", i bɛna a sɔrɔ ko naloman tagara dɛ.|Regarder l'anus du sot, c'est vrai, mais dire: " Je vais chercher mes parents pour venir le regarder!" vous viendrez trouver que le sot est parti!
Tolen ko : “ne bɛ n'dulonyɔrɔ le ɲini, ne tɛ kotigiya ɲinina.|La chauve souris dit:"je cherche tout juste un coin pour m'accrocher, je ne cherche pas un grand nom."
Torogberen tɛ sankurukan mɛn, nka ni sanji bɛ ben a bɛ o lɔn.|Le sourd n'entend pas le ciel gronder, mais quand il pleut il le sait.
Toron ni yɛrɛko le bɛ dugu diya.|C'est les jeux et le rire qui rendent le village intéressant.
Toron tɛ sɔbɛ sa.|Le jeux n'empêche pas d'être sérieux.
Totodinga sogibagajugu, lon dɔ i bɛ bɔ i bɛmacɛ dɔ sukoro kan.|Trop grand creuseur de trou de rat, un jour tu risques de déterrer les os d'un de tes ancêtres!
Tɔgɔ ye sɔrɔfɛn ye, tɔgɔ tɛ karabara.|Un grand nom, on vous l'attribue, un grand nom, on ne le force pas.
Tɔnɔ fla tɛ sɔrɔ sisɛfan kelen na.|On ne peut pas avoir deux bénéfices dans le même oeuf.
Tɔri firikojugu b’a bla suma ra.|A force de trop jeter le crapaud, on le met à l'ombre.
Tɔri n’a kɔnɔfɛn bɛɛ ye duga ta ye.|Le crapaud appartient au vautour, avec tout ce qu'il contient .
Wagati sebari tɛ karaba.|On ne force pas un temps qui n'est pas encore arrivé.
Wara kɔngɔtɔ ma ɲi, a fanin ma ɲi.|Quand le fauve est affamé il est dangereux, quand il est rassasié il est dangereux.
Wara min ko : “a ye n’kun ci !”, ɲama tɛ o ra.|L'animal qui dit lui même:"Fracassez moi le crâne!" n'a pas de force vengeresse!
Warasogo gbaniman le ka di.|La viande d'un fauve est bonne quand c'est chaud.
Woroden ye dugalen le ye, a tɛ flɛ ka ban.|L'enfant est un miroir pour les parents, on n'en a jamais assez de le regarder.
Wɔlɔnin ma kuru ban, kiri kɔnɔntɔn!|La perdrix ne couve pas encore, et déjà neuf oeufs!
Wɔrɔsɔ bɔrɔraman, mɔgɔ tɛ o si lɔn, nka fɛn dɔ kurunin bɛ bɔrɔ kɔnɔ, i b’o lɔn.|On ne peut pas reconnaître une faucille dans un sac, mais on peut au moins savoir qu'il y a un objet courbe dans le sac.
Wɔsi tɛ lɔn sanji jukɔrɔ.|Si tu transpires sous la pluie, on ne le saura pas.
Wurufa fla ye jendekala ye.|Le remède de la râge c'est un coup de gourdin.
Wuruwele le bɛ a kongotaga diya.|C'est quand on appelle le chien qu'il est facile de l'amener à la chasse.
Wuru welenin le bugɔ ka di.|C'est le chien appelé qui est facile à frapper.
Yɛlɛmayɛlɛma le bɛ ŋɔmi ɲa.|C'est à force de tourner et de retourner que la galette cuit bien.
Yɛrɛɲini ka fisa ni tɔgɔɲini ye.|Chercher sa vie est mieux que de chercher un nom.
Yiriden gberen bɛ se ka ben yiriden mɔnin ɲa.|Le fruit vert peu tomber avant le fruit mûr.
Yirinin min b’a kelen na, suma ɲanaman tɛ sɔrɔ o ra.|Un arbre solitaire, ne donne pas beaucoup d'ombre.
Yirinin min bɛ kuru kan, a bɛ ale ɲana ko a ka jan ni yiri tɔw bɛɛ ye.|L'arbre qui est sur la montagne pense qu'il est le plus grand de tous les arbres.
