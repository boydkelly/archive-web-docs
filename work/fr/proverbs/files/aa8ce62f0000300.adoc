---
title: Miiri
author: Inconnu
date: 2020-12-27
type: doc
draft: false
categories: []
tags: []
---

= Miiri
:author: Inconnu
:date: 2020-12-27
:description: Paroles qui incitent à la reflexion
:sectnums:
:sectnumlevels: 2
:lang: fr
include:julakan:ROOT:partial$locale/attributes.adoc[]

== Miiri / La reflexion

[width="80%",cols="2",frame="none",options="none",stripes="even"]
|====
|Ni miiri bɔra mɔgɔya ra, mɔgɔya banna.|Si la réflexion manque dans les relations humaines, Il n'y a plus de relations humaines.
|Sɔrɔ bɛ fɛnɲini juguya.|Plus on trouve plus on cherche.
|Ni sosonin bɛ dɔnkɛ, a b’a senkalaninw kɔrɔsi.|Quand le moustique danse, il fait attention à ses petites jambes.
|I sɔnkɔrɔ bla, o ka fisa ni jatigiya yɛlɛma ye.|Laisser ton mauvais caractère, vaut mieux que changer de tuteur.
|Subaga bɛ ɲina, nka a ka min den domu o tɛ ɲina.|Le sorcier oublie, mais celui dont il a mangé l'enfant n'oublie pas.
|Ŋanin firibaga bɛ ɲina, nka min k’a dɔn o tɛ ɲina.|Celui qui jette les épines oublie, mais celui qui les piétine n'oublie pas.
|“Lɔgɔ girinna”, o ye tiɲɛ ye, nka "ne tɛ lɔ ni n’ma se so”, walifɛn le b’i kun.|Le marché s'est terminé dans le désordre, mais celui qui dit qu'il ne s'arrêtera plus jusqu'à la maison, a certainement pris des choses d'autrui.
|Sula tɛ wuru kɔdimitɔ lɔn.|Le singe ne cherche pas à savoir si le chien a mal au dos ou non.
|Sunɔgɔbagatɔ kunun ka di ni tugubaganci ye.|Le vrai dormeur est plus facile à réveiller que celui qui fait semblant.
|Su fɛ yirikurun finman, a tɛ mɔgɔ mina, nka a b’i lasiran.|Morceau de bois noir la nuit, ça n'attrape pas l'homme, mais ça fait peur.
|Sulaw tɛ ɲɔgɔn kɔnɔ kabakariyɔrɔ ra.|Les singes ne s'attendent pas pour commencer à manger le mais.
|I wuri ka bɔ n'ta siginan kan, o ye tiɲɛ ye, nka wuri ka bɔ n'nakan kan, o ye faninya ye.| « Tu occupes ma chaise, lève toi! » Cela est vrai. Mais « tu occupes mon destin, lève toi! », c'est faux.
|Ka ɲɔgɔn siraratagama, sula ni wuru ta teriya ma o bɔ.|L'amitié du singe et du chien n'arrive pas au point de se rendre visite souvent.
|Tagayɔrɔjan tɛ se ka mɔgɔ firi i facɛ ta bulonda ma.|On a beau voyager pour aller loin, on ne peut pas ne pas reconnaître la porte de la maison paternelle.
|Tasuma minana, sogo tɛ n'boro, ne marora.|Le feu est allumé, et je n'ai pas de viande, quelle honte!
|Tiga bɛɛ ye kelen ye, nka minw bɔra fara kelen kɔnɔ, olugu ta bɔna le ɲɔgɔn tɛ.|Tous les grains d'arachide se ressemblent mais ceux qui viennent de la même coque, se ressemblent le plus.
|Tiga torinin kelen bɛ a tɔ bɛɛ gboya mɔgɔ da ra.|Un seul grain d'arachide pouri rend tous les autres mauvais dans la bouche.
|Ka tiga wɔrɔ ɲɔgɔn kɔ, a fara ka ca.|Décortiquer les arachides les uns après les autres les coques s'éparpillent et deviennent trop nombreuses.
|Tinba b’a yɛrɛ sɛgɛ bala ye.|L'oryctérope se fatigue (à creuser les trous) pour le porc-épic.
|Tiɲɛ ye naloman boda flɛ ye, nka "n’ bɛ taga n’somɔgɔw wele ka na", i bɛna a sɔrɔ ko naloman tagara dɛ.|Regarder l'anus du sot, c'est vrai, mais dire: " Je vais chercher mes parents pour venir le regarder!" vous viendrez trouver que le sot est parti!
|====
