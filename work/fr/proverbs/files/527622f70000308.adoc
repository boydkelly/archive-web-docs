---
title: Fɛn kelen
author: Inconnu
date: 2020-12-27
type: doc
draft: false
categories: []
tags: []
---
= Fɛn kelen
:author: Inconnu
:date: 2020-12-27
:description: Une chose
:sectnums:
:sectnumlevels: 2
:lang: fr
include:julakan:ROOT:partial$locale/attributes.adoc[]

== Fɛn kelen / Une chose

[width="80%",cols="2",frame="none",options="none",stripes="even"]
|====
|Fɛn kelen tigi caaman ma ɲi, fɛn kelen tigintan ma ɲi.|Une chose avec beaucoup de propriétaires, c'est pas bien, une chose sans propriétaire, c'est pas bien.
|Fɛn min bɛ banabagatɔ kɛnɛya, o bɛ fa bla kɛnɛbagatɔ ra.|Ce qui guérit un malade, rend fou un bien portant.
|Fɛn min bɛ mɔgɔ mina, i tɛ o senkan mɛn.|Ce qui va t'attraper, tu ne l'entends pas venir.
|Fɛn min bɛ to misɛnya ra, o dan ye foni ye.|Il n'y a que le fonio qui reste petit.
|Fɛn min ka di mɔgɔ ye i ka kan ka o bla i ɲa fɛ, n’o tɛ a kɔflɛ bɛ caya.|Ce que tu aimes, mets le devant toi, sinon les coups d'oeil par derrière seront nombreux.
|Fɛn min ka ɲi ɲɔ ma o ye bɔrɔ ye.|Ce qui est mieux pour le mil, c'est le sac.
|Fɛn min nana i kama o tɛ i jɛn.|Ce qui est venu rien que pour toi, ne te manquera jamais.
|Fɛn min ka mɔgɔ sennateriya, o ka kan ka i darateriya.|Ce qui t'a fait courir vite, doit te faire parler vite.
|Fɛn ɲanin, tigi caaman.|Chose réussie, (revendiquée par) plusieurs propriétaires!
|Ni fɛn ɲuman tɔmɔbaga ko a tɛ ɲa a kɔ, o burunna min fɛ o bɛna a yɛrɛ faga.|Si celui qui a ramassé l'objet ne veut plus s'en passer, et que dire de celui qui l'a égaré? il va se tuer!
|Fɛnw fincogo ye kelen ye, nka o manamanacogo tɛ kelen ye.|Les choses noircissent de la même manière, mais ils ne brillent pas de la même manière.
|Fɔnɲɔba bɛ yiribaw ben, nka a bɛ tɛmɛ yirimisɛnw ni binw kunna.|Le grand vent fait tomber les gros arbres, mais il passe au dessus des arbustes et des herbes.
|Fɔɲɔ bɛ ɲɔfiyɛbaga boro kɔrɔtanin le sɔrɔ.|Le vent vient trouver les mains de la vanneuse en haut.
|Fiyentɔ jusuba, i ɲa yɛlɛlon le fɔra i ye cɔ.|Un aveugle qui a un gros coeur? on t'a certainement dit le jour où tes yeux s'ouvriront.
|Fiyentɔ bɛ dɔnkɛ n’a ta dɔnkɛsara ye a boro.|L'aveugle danse avec son salaire de danse en main.
|Fiyentɔya ladegi man gbo.|Faire l'aveugle n'est pas difficile.
|Fiyentɔya o, nanbaraya o, wurudennin ka nin bɛɛ kɛ ka tɛmɛn.|Vivre sans voir, sans marcher, le petit chiot a passé par toutes ces étapes.
|Ni folo bɛ i kan na, n’i bɛ ko, i b’a ko, n’i bɛ mun i b’a mun, nka a man di i ye.|Si tu as le goitre, quand tu te laves, tu le laves, quand tu t'embaumes, tu l'embaumes, mais tu ne l'aimes pas.
|Foro damina ye wagabɔn ye.|Pour faire un champ, on commence par enlever les herbes.
|Forokonin tora a logisara ra.|Le sac a été vendu à son prix de revient, (sans bénéfice).
|Ni forotobɔrɔ ka kɔrɔ cogo o cogo, tisota bɛ sɔrɔ a ra.|Le sac de piment a beau être vieux, il y en aura suffisamment pour faire éternuer.
|Foyi tɛ yi ni kɔrɔ tɛ min na.|Rien ne se fait sans cause.
|Foyi tɛ to kuraya ra.|Rien ne reste toujours à l'état neuf.
|“Flamuso, i kɔnɔman lo wa ?” “Ɔn hɔn.” “Den ye cɛ le ye wa ?” “N’ ma o lɔn !”|« Femme peulh, tu es enceinte? » «  Oui! » « Ton enfant est-il un garçon? » « Ça, je ne sais pas ! »
|Gbabugu ka kɔrɔ ni misiri ye.|La cuisine est plus âgée que la mosquée.
|Gbɛrɛgbɛrɛko sɔrɔ man gbo.|Un malheur arrive facilement.
|Girindi tɛ fa ye, nka kɔngɔtɔ t’a kɛ.|Roter ne veut pas dire qu'on est rassasié, mais celui qui a faim ne le fait pas.
|Gundojugu bɛ i ko sogo kɛnɛ, n’a torira, a kasa bɛ bɔ.|Le mauvais secret c'est comme de la viande fraîche, Quand il pourrit, il sent mauvais.
|Gban tɛ se ka janya a karibaga ma.|Le pied de gombo ne peut pas être trop long pour celui qui coupe le gombo.
|Hakɛ bɛ dunuɲa ko bɛɛ ra. |Il y a une offense en toutes choses dans ce monde.
|Hakɛ bɛɛ tɛ bla ka lahara kɔnɔ.|Toutes les offenses n'attendent pas le dernier jugement.
|« Hakɛ to ! » O tɛ wurukinda suma.|Dire: « Pardon! » ne guérit pas une morsure de chien.
|Hakiri le ye mɔgɔya ye.|C'est la réflexion qui fait l'homme.
|==== 
