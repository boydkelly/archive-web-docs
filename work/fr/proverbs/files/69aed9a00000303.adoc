---
title: Kuma laban
author: Inconnu
date: 2020-12-27
type: doc
draft: false
categories: []
tags: []
---
= Kuma laban
:author: Inconnu
:date: 2020-12-27
:type: article
:sectnums:
:sectnumlevels: 2
:lang: fr
include:julakan:ROOT:partial$locale/attributes.adoc[]

[width="80%",cols="2",frame="none",options="none",stripes="even"]
|====
|
Masacɛ dɔ ko a ta jɔncɛ ma ko a ye na ni fɛn dɔ ye min ka jugu ka tɛmɛ danfɛn bɛɛ kan; 
jɔncɛ nana ni nɛnkun ye minan dɔ kɔnɔ.
Masacɛ ko a ma ko a ye na ni fɛn dɔ ye min ka ɲi ka tɛmɛ danfɛn bɛɛ kan;
jɔncɛ nana ni nɛnkun dɔwɛrɛ ye tuun.
Ni aw ye hakiritigi ye, aw ye nin kuma kɔrɔ fɔ!
|
Un roi dit a son serviteur de lui apporter ce qu'il ya de plus mauvais;
le serviteur lui apporta une langue dans un plat.
Le roi lui demanda de lui apporter ce qu'il ya de plus beau,
Le serviteur lui apporta une autre langue.
Dites moi ce que cela veut dire, si vous êtes intelligent!
|====
