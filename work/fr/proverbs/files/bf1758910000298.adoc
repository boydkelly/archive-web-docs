---
title: Tiɲɛ tɛ fla ye
author: Inconnu
date: 2020-12-27
type: doc
draft: false
categories: []
tags: []
---

= Tiɲɛ tɛ fla ye
:author: Inconnu
:date: 2020-12-27
:description: Proverbes de la vérité qui n'est pas deux
:sectnums:
:sectnumlevels: 2
:lang: fr
include:julakan:ROOT:partial$locale/attributes.adoc[]

== Tiɲɛn tɛ fla ye / La vérité n'est pas deux.

[width="80%",cols="2",frame="none",options="none",stripes="even"]
|====
|Canfɔbaga ma man di.|L'homme qui dit la vérité n'est pas aimé.
|Kuma fɔkonyuman ni tiɲɛn tɛ kelen ye.|Parole bien dite, n'est pas égale à vérité
|Hali ni faninya ka san tan kɛ tagama ra, tiɲɛn ta sɔgɔmada kelen tagama bɛ kun a ra.|Même si le mensonge fait dix ans de marche, une seule matinée de marche de la vérité le rattrape
|Cankuma bɛ mɔgɔ dɔ kɔnɔ, nka a fɔda t’a fɛ.|Quelqu'un peut avoir une parole de vérité mais il n'a pas la bouche pour le dire.
|Tiɲɛn bɛ mɔgɔ ɲa wulen, nka a t’a ci.|La vérité rougit les yeux, mais elle ne les casse pas.
|O b’a fɔ tɔri ma yɔrɔ min ko “Bagayɔgɔ”, o ye a buguyɔrɔ ye.|Le moment où on peut (remercier ) le crapaud et lui dire:"Bakayoko" c'est le moment où il se couche.
|Tɔri firikojugu b’a bla suma ra.|A force de trop jeter le crapaud, on le met à l'ombre.
|Ni mɔgɔ ma sa, ko bɛɛ juru b’i ra.|Tant qu'on n'est pas mort, tout peut arriver.
|Ni nin yiriboro tun tɛ ne tun bɛna kɔnɔnin nin faga, ni nin yiriboro tun tɛ fana kɔnɔnin tun tɛ sigi yan.|Sans cette branche, j'aurai tué cet oiseau, mais sans cette branche, l'oiseau ne s'aseyerait pas là non plus.
|Ni sanji ka i sɔrɔ togo kolon kɔnɔ, a tɛ lɔ tuun.|Si la pluie te trouve sous une cabane délabrée, elle ne s'arrête plus de tomber.
|Ni sisɛnin ma “Kus !” mɛn, a na “Paraw ” mɛn.|Si la poule n'entend pas la voix qui lui demande de quitter, elle entendra le coup de bâton qui lui demande de quitter
|Ni sunbara ka diya, nɛrɛbɔbaga tɔgɔ tɛ fɔ.|Quand le sounbara est bon, on ne parle pas de celle qui a récolté le néré.
|Ni tasuma ka ba tigɛ a yɛrɛ ma, a bɛ kɛ a fagabaga boro kɔnɔgban ye.|Si le feu a traversé le fleuve tout seul, celui qui va l'éteindre aura des problème.
|Faninyatigɛbaga le b’a fɔ ko a seere bɛ ba kɔ.|C'est le menteur qui dit que son témoin est de l'autre côte du fleuve.
|Nɔnɔ tɛ dɛgɛ tiɲɛ.|Le lait ne gâte pas dégué.
|Ni jɛgɛdenin ka fɛn o fɛn fɔ banba ta ko ra, a fɔbaga le k’a fɔ.|Tout ce que le petit poisson dit à propos du crocodile, il le dit en tant qu'un vrai témoin.
|Ni tuganin kɔnna ɲɔfiyɛbagaw ɲa, a bɛna dɔ kɛ sigi ra.|Si la tourterelle arrive avant les vanneuses il attendra longtemps.
|Ɲafɛn tiɲɛ, tiɲɛfɛn fana tɛ ɲa.|Ce qui est destiné à reussir ne se gâte pas, ce qui est destiné à être gâté, ne réussit pas.
|Ɲamanfirisununkun bɛ yi, nka balemafirisununkun tɛ yi.|Il y des endroits pour jeter des ordures, mais il y en a pas pour jeter des frères.
|Ɲa bɛ ko dɔ ye, da tɛ o fɔ, tulo bɛ ko dɔ mɛn, da tɛ o fɔ.|Les yeux voient certaines choses que la bouche ne dit pas, l'oreille entend certaines choses que la bouche ne dit pas.
|Ɲa jiginta tɛ sunɔgɔ.|Des yeux sans espoir ne dorment point.
|Ɲa tɛ doni ta, nka a bɛ doni gbiriman lɔn.|L'oeil ne porte pas de bagage, mais il sait reconnaître un bagage lourd.
|Ɲanafin bɛ fali faga.|L'ennui tue l'âne.
|Ɲin gbɛra, nka basi b’a jukɔrɔ.|La dent est blanche, mais il y a du sang (rouge) en bas.
|Ɲinan bɛ tokalama ben, nka tɛ se k’a lawuri.|La souris fait tomber l'ustensile mais il ne peut pa le relever.
|N'i ka ɲinan jalaki, i ye sunbara fana jalaki.|Si tu accuse la souris, il faut aussi accuser le soumbala.
|Fɛn fɔlɔ le ye fɛn fɔlɔ ye, ni min bɔra o kɔ, a bɛ fɔ ko dɔwɛrɛ bɔra.|la première chose est vraiment la première, Ce qui vient après, on dit: " un autre".
|Ɲɔ bɛ don ji ra lon min, a tɛ kɛ dɔrɔ ye o lon.|Le mil ne devient pas du dolo, le même jour où on le met dans l'eau.
|Ɲɔ bɛ ɲɔbugu, nka takan le b’a ra.|Il ya du mil au village du mil, mais il ya une condition pour le prendre.
|Ɲɔgɔn bamusodomu le bɛ subagaya diya.|Se manger les mères les uns des autres rend la sorcellerie intéressante.
|Warasogo gbaniman le ka di.|La viande d'un fauve est bonne quand c'est chaud.
|Sa bɛ siran, a fagaba bɛ siran.|Le serpent a peur, celui qui le tue a aussi peur.
|Ni sa kunkolo tigɛra, a tɔ kɛra jurukisɛ ye.|Quand la tête du serpent est coupé, il n'est plus qu'un corde.
|N’i ka sa ye ka bere min to i boro i b’a faga ni o ye.|Le bâton que tu as en main au moment où tu vois le serpent, c'est avec celui là que tu le tues.
|Sa ɲa ka misɛn, nka boro tɛ su a ra.|Le serpent a des petits yeux, mais on ne peut y mettre le doigt. (en signe de mépris)
|====
