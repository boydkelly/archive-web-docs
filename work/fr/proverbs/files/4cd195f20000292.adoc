---
title: Kuma ɲuman nafa
author: Inconnu
date: 2020-12-27
type: doc
draft: false
categories: []
tags: []
---
= Kuma ɲuman nafa
:author: Inconnu
:date: 2020-12-27
:description Avantage d'une bonne parole
:sectnums:
:sectnumlevels: 2
:lang: fr
include:julakan:ROOT:partial$locale/attributes.adoc[]

== Kuma ɲuman nafa | Avantage d'une bonne parole

[width="80%",cols="2",frame="none",options="none",stripes="even"]
|====
|Kanbele cɛsirikonyuman bɛɛ tɛ kɛrɛkɛcɛ ye.|Tous les jeune hommes aux ceintures bien attachées ne sont pas guerriers.
|Kanbeleba le bɛ sunguruba siyɔrɔ lɔn.|C'est le jeune homme frivole qui sait où dort la fille frivole.
|Kami ta ɲɛgɛnw b'a kɔ kan, adamaden ta b'a kɔnɔ.|Les points de beauté de la pintade sont sur son dos, les points de beauté de l'homme intelligent sont dans son coeur.
|Mɔgɔ man kan ka don kɛrɛ ra ko jo bɛ i fɛ, i bɛ don kɛrɛ ra le ko mɔgɔw b’i kɔ.|N'entre pas dans une querelle parce que tu as raison, entre parce que tu as des gens derrière toi.
|Karisa tagara lɔgɔ ra, ne fana bɛ taga, nka karisa tagara ni min ye, ni ele ma taga n’o ye, karisa bɛna segi ni min ye, ele tɛna segi n’o ye dɛ!|Untel est allé au marché, moi aussi j'y vais, mais si tu n'as pas apporté au marché ce qu'un tel a apporté, tu ne pourras pas rapporter du marché ce qu'un tel va rapporter.
|Kɛlɛ kɔ ye diya ye.|Après la querelle l'entente!
|Mɔgɔ fla le bɛ kɛlɛ kɛ.|C'est deux personnes qui se battent.
|Kɛrɛkɛcɛ kelen, burufiyɛbaga kɔnɔntɔn, ɲagari bɛ o kɛrɛ sa.|Neuf joueurs de cor pour un seul guerrier, trop de joie va annuler cette guerre.
|“Kɛ n’ yɛrɛ ra” dan ye kɔlɔnkɔnɔboci ye.|Peter dans dans un puits, c'est le plus grand mal que quelqu'un puisse se faire à lui même.
|Kirisi min bɛ mɔgɔ bɔ kɔlɔn kɔnɔ, o bɛ kɔrɔbɔ ni jurufiyɛ le ye.|La formule magique qui peut faire sortir quelqu'un du puits, se vérifie avec une puisette d'abord.
|Kofitininkɛ bɛ mɔgɔ fadencɛ ɲadon i ra. Kobakɛ bɛ mɔgɔ fadencɛ ɲabɔ i ra.|Faire des choses médiocres fait que tes frères te méprisent. Faire de grandes choses fait que tes frères te respectent
|Ko baranin bɛ ko sulanin sa.|Une choses imprévue qui arrive, nulle une chose qui était prévue.
|Ko bɛɛ n’a tuma, ko bɛ n’a wagati.|Chaque chose a son temps, chaque chose a son moment.
|Ko caaman bɛ mɔgɔya ra, bɛɛ tɛ o lɔn.|Il y a beaucoup de choses dans les relations humaines, que plusieurs ne sauvent pas.
|Ni ko ka gban, ka gban, a bantɔ lo.|Quand une chose chauffe, chauffe, c'est qu'il arrive à sa fin.
|Ko sonzanin ta ye lo ye, ni lo ka bɔ ko sonzanin y’a ɲa datugu!|On dit que le masque appartient au lièvre, mais quand le masque sort, on dit au lièvre de se fermer les yeux!
|Ko to ɲɔgɔn ta ra, o bɛ karatabugu mɛɛn si ra.|Quand les habitants de la case se soumettent les uns aux autres, ça donne longue vie à la case.
|Kogo surunin bɛɛ ta surunya tɛ bɔgɔdɛsɛ ye.|Tous les mûr courts ne sont pas courts parce que les briques ont manqué.
|Kojugu senkan tɛ bɔ.|Les pas du malheur ne font pas de bruit.
|Kolo tɛ nɛn na ko a tɛ yɛlɛma.|La langue n'a pas d'os, pour qu'elle ne tourne pas.
|Kogoro ɲa wulenna, nka a ta ko tɛ mɔgɔ fɛ.|La varan terrestre a les yeux rouges mais il n'est pas fâché contre quelqu'un.
|Sirakɔgɔmaw le bɛ ɲɔgɔn kinyɔrɔ lɔn.|C'est les tortues elles même qui savent où se mordre.
|I ka kɔ tigɛ, kɔwɛrɛ b’i ɲa.|Tu as traversé une rivière, mais il y en a une autre devant toi.
|Kɔ mɔna ni kan min ye, jɛgɛ bɛ taran n’o kan le ye.|Les closes sur lesquelles on a fait la pêche, c'est sur les mêmes qu'on partage les poissons.
|Kɔgɔ bɛ fali kɔ ra, a lɔgɔ b’a ra.|L'âne porte le sel sur son dos, et il en a envie.
|Kɔlɔnkɔnɔ banba, n'i benna a kan, kara, n'a benna i kan, kara.|Crocodile dans un puits: tu tombes sur lui, c'est grave! Il tombe sur toi c'est grave!
|Kɔnɔ bɛ se ka pan ni ji min ye, a bɛ o le min.|La quantité d'eau qui peut permettre à l'oiseau de voler, c'est ce qu'il boit.
|Kɔnɔnin min k’a ta fan la lɔgɔfɛ ra, mankan tɛ se ka o lawuri tuun.|Un oiseau qui a fait son nid dans un marché, quel bruit peut le faire fuir?
|Kɔɲɔmuso nakun ye cɛko ye.|La mariée vient pour son mari.
|Ka kɔrɔtɔ Ala ma, o le bɛ na ni gbata lɔri ye, n’o tɛ ni su kora yɔrɔ bɛɛ bɛ kɛ suma ye.|C'est l'impatience à l'égard de Dieu qui fait qu'on construit un hangar, sinon la nuit il y a de l'ombre partout.
|====
