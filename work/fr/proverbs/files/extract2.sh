#!/usr/bin/bash
# keep words with contractions
# replace all characters that are not alphanumeric and apostrophe wih space; replace spaces wih line return; replace upper with lower; delete plurals, sort, remove dups
while IFS= read -r -d '' file
do
  echo $file
#  epub2txt $file > ${file%.*}.txt
## replace all characters that are not alphanumeric and apostrophe wih space; replace spaces wih line return; replace upper with lower; delete plurals, delete single letter words except aeinou, sort, remove dups
#  epub2txt --raw $file > ${file%.*}.upper.tmp
  awk -F"|" '{ print $2 }' $file | sed -e 's/====//g' > ${file%.*}.upper.tmp
## remove words with contractions
# replace all characters that are not alphanumeric wih space; replace spaces wih line return; replace upper with lower; delete plurals, sort, remove dups
# epub2txt  --raw $file | sed -e "s/[^[:alpha:]]/ /g; s/[[:space:]]/\n/g" | tr [Ɔ,Ɛ,Ɲ,Ŋ] [ɔ,ɛ,ɲ,ŋ] | tr [:upper:] [:lower:] | sed -e '/.*w$/d' | sort | uniq > ${file%.*}.no-contractions.dict.tmp
done <   <(find . -name "*.adoc" -print0)

# make this one word per line and then nuke thousands of french greek and hebrew words
# words that contain apostrophe after second character, apostropy at begining of word
# This is stuff where there is no real need to check.  It can't be jula.
# Remove all words that end in w.  Jula plurals not needed.
cat *.upper.tmp | sed -e "s/[^[:alpha:]’]/ /g; s/[[:space:]]/\n/g" \
  | sort | uniq > dyu.upper-list.tmp
 # && 
 # rm *.upper.tmp

# create lists of french, english latin words in file
#for x in fr_FR; do
#  echo $x
#  hunspell -G -d $x dyu.upper-list.tmp | sort | uniq > remove.$x.word-list.tmp
#done 

# remove french english and latin words
#cat remove.*.word-list.tmp | sort |uniq > all.word-list.txt && rm remove.*.word-list.tmp
comm -23 dyu.upper-list.tmp french.txt > dyu.no-french.tmp

# make all lower case, remove jula plurals, remove single letter words with exceptions; remove doubles and sort
#cat dyu.no-foreign.tmp  | tr [Ɔ,Ɛ,Ɲ,Ŋ] [ɔ,ɛ,ɲ,ŋ] | tr [:upper:] [:lower:] | sed -e "/^[^aeinou]$/d" | sort | uniq > dyu.lower.tmp
cat dyu.no-french.tmp  | tr [Ɔ,Ɛ,Ɲ,Ŋ] [ɔ,ɛ,ɲ,ŋ] | tr [:upper:] [:lower:] | sort | uniq > dyu.lower.tmp
# && rm dyu.no-foreign.tmp

#comm -23 dyu.lower.tmp <(cat dyu.names.txt dyu.bible.txt | sort | uniq) > dyu.no-names.tmp

# delete any french accented words and anything with a q or an x
# delete manuscript and other common consonant comboos
# delete double vowel combos
# any greek or hebrew
# latin hebrew names with apostrophes
# names that start with apostrophes
# names that end with apostrophes
#cat dyu.no-names.tmp  \
#  | sed -e '/כ\|lxx\|[rd]em\|us$\|ite\|[abcdeijklnoprstu]h\|^i/d' \
#  | sed -e '/[^aeinou]$/d' \
#  | sed -e '/bb\|c[ct]\|d[dnp]\|ff\|g[gn]\|kk\|ll\|mm\|pp\|s[clst]\|tt\|vv/d' \
#  | sed -e '/[aiu]e\|[aeu]i\|[aeiu]o\|[eiou]a\|[aeio]u/d' \
#  | sed -e "/^[yhdlvw].*[ʼʽ’].*/d" \
#  | sort | uniq > dyu.clean.tmp
#
#cat dyu.clean.tmp dyu.addback.txt | sort | uniq > dyu.word-list.txt
# # | sed -e '/.*[ʽʼ]$/d' \
cat dyu.lower.tmp | sort | uniq > dyu.proverbes.txt
cat dyu.proverbes.txt | wc -l && rm *.tmp
#cat remove.*.*.tmp remove.manual.txt | sort |uniq > remove.word-list.txt
  #| sed -e "/[כwαάβγΔΕεέζηΘθΙιἵίΚκλμνοόπρσςτυῦΧχωῶῳ]/d" \
  #| sed -e "/[גההַוזטָךללֶמנןעעִעֶרששָׂשָּׂת]" \
  #| sed -n -e '/lxx\|[rd]em\|us$\|ite\|[abcdeijklnoprstu]h\|^i/p' > deleted-words.txt \
  #| sed -n -e '/[^aeinou]$/p' >> deleted-words.txt \

#rm *.tmp
  #| sed -e '/l.*’/d' \
  #| sed -e '/d’/d' \
  #| sed -e '/w.*[ʼʽ]/d' \
  #| sed -e "/^h.*ʼ.*/d" \
#hunspell -G -d he_IL dyu.word-list.txt > remove.hebrew.word-list.tmp
#hunspell -G -d grc dyu.word-list.txt > remove.greek.word-list.tmp
# fold used to create single line with uniq letters from file (used with sed above to remove greek and hebrew words)
#fold -w1 remove.manual.txt |sort |uniq | tr -d '\n' >> extract.sh
#^i removes all words that begin with i.   There are only about 3 or so, re-add these
#ah removes farahuna, but its a proper name.  
#eh remove eeh jehova
#nh removes ɔnhɔn
#rem removes zeremi
