#!/usr/bin/bash
# keep words with contractions
# replace all characters that are not alphanumeric and apostrophe wih space; replace spaces wih line return; replace upper with lower; delete plurals, sort, remove dups
while IFS= read -r -d '' file
do
  echo "$file"
#  epub2txt $file > ${file%.*}.txt
## replace all characters that are not alphanumeric and apostrophe wih space; replace spaces wih line return; replace upper with lower; delete plurals, delete single letter words except aeinou, sort, remove dups
#  epub2txt --raw $file > ${file%.*}.upper.tmp
  awk -F"|" '{ print $2 }' "$file" | sed -e 's/====//g' > ./"${file%.*}".~.tmp
## remove words with contractions
# replace all characters that are not alphanumeric wih space; replace spaces wih line return; replace upper with lower; delete plurals, sort, remove dups
# epub2txt  --raw $file | sed -e "s/[^[:alpha:]]/ /g; s/[[:space:]]/\n/g" | tr [Ɔ,Ɛ,Ɲ,Ŋ] [ɔ,ɛ,ɲ,ŋ] | tr [:upper:] [:lower:] | sed -e '/.*w$/d' | sort | uniq > ${file%.*}.no-contractions.dict.tmp
done <   <(find ./files -maxdepth 1 -name "*.adoc" -print0)

# make this one word per line and then nuke thousands of french greek and hebrew words
# words that contain apostrophe after second character, apostropy at begining of word
# This is stuff where there is no real need to check.  It can't be jula.
# translate to lower;
# Remove plurals (all words that end in w.)
cat ./files/*.~.tmp | sed -e "s/[^[:alpha:]’]/ /g; s/[[:space:]]/\n/g;" | sed -e '/^$/d' \
  | tr [Ɔ,Ɛ,Ɲ,Ŋ] [ɔ,ɛ,ɲ,ŋ] | tr [:upper:] [:lower:] | sed -r 's/(^.*)(w$)/\1/' | sort > dyu.lower-list.tmp  
comm -23 dyu.lower-list.tmp french.txt > dyu.no-french.tmp

cat dyu.no-french.tmp | sort | uniq > dyu.proverbs-word-list.txt
cat dyu.no-french.tmp | sort | uniq -c | sort -n > dyu.proverbs-word-frequency.txt && rm dyu.no-french.tmp
wc -l dyu.proverbs-word-list.txt && rm *.tmp
rm ./files/*.~.tmp
