---
title: SÉANCE 35 COURS DE DIOULA 
author: Youssouf DIARRASSOUBA
date: 2020-11-07
type: doc
---

= SÉANCE 35 COURS DE DIOULA 
:author: Youssouf DIARRASSOUBA
:date: 2020-11-07
:type: post
:experimental:
:sectnums:
:sectnumlevels: 2
:lang: fr
:page-lang: {lang}
include::ROOT:partial$locale/attributes.adoc[]

== LÉÇON 35 : KALANSEN BI SABA NI LOORUNAN

=== ÉCHANGES ET COMPRÉHENSION DE TEXTE DIOULA

[abstract]
Dans ce texte, vous allez repérer des termes de bénédiction à utiliser dans des causeries évoquant les cas de décès, de commémoration de la date anniversaire de la mort de quelqu’un, etc.…

NOTE: La communauté mandingue est particulièrement attachée aux civilités et autres marques de salutation, notamment dans les contextes de tristesse.

==== BI BARO : MUSO FILA YA BAARO

[width="100%",cols="2*a",frame="none",options="none",stripes="none"]
|====
|* Umu : I ni wula Fanta.
|Bonsoir Fanta

|* Fanta : Nse n teri Umu, hɛrɛ telenna wa ?
|Bonsoir mon amie, la journée paisible ?

|* Umu : Tɔɔrɔ si tɛ n na, i ka kɛnɛ wa ?
|Rien de mal. Comment va ta santé?

|* Fanta : Tɔɔrɔ si tɛ n na, i somɔgɔw do ?
|Rien de mal chez moi, et ta famille?

|* Umu : Tɔɔrɔ si t’o la, i cɛ ka kɛnɛ wa ?
|Rien de mal avec eux, et comment va ton mari ?

|* Fanta : Tɔɔrɔ si t’a la, i bɔ la min ten (tan) ?
|Rien de mal chez lui, il est sorti ou comme ça ?

|* Umu : Kabini sɔgɔma an tun bɛ sarakabɔyɔɔrɔ la, su ko la mɔgɔ tɛ n kɔ so kɔnɔ.
|Depuis le matin nous étions à la cérémonie funébre du 7ie jour. La nuit venue, personne derrière dans la maison. 

|* Fanta : Jɔn ya sarakabɔ tun bɛ yen ?
|C'était le funébre de qui ?

|* Umu : An ya cɛkɔrɔba dɔ tun lo, a tele wolonfla sarakabɔ le tun bɛ bi ye. 
yɔrɔw ka jan ɲɔgɔn na. 
Mɔbiliko ka gbɛlɛn o le kosɔn an mɛn na tan.
|C'était un de nos vieux, c'était aujourd’hui le 7ie jour du funèbre. 
Les endroits sont éloignés.
Le transport est difficile.  À cause de cela nous avons duré comme ça.

|* Fanta : Eee, n teri Umu á ni sɛgɛn. 
Saya yɛrɛ kɔnin tɛ mɔgɔ kelenko ye. 
A bɛ bɛɛ ya da la.
Ala ye an ni a cɛ janya.
Sanni an yɛrɛw ta lon ye se, an ye ɲuman kɛ.
|Eee mon amie Umu, vous êtes fatiqués. 
Quant à la mort c'est pas l'affaire d'une seule personne. 
c'est devant la porte de tous. 
Que dieu mette une grande distance entre nous et la mort.
Avant que notre jour n'arrive... faisons le bien

|* Umu : O ye cɛn (tuɲan) ye dɛ, nimisa tɛ min na ole ye ɲuman kɛ ye.
|C'est la vérité, ou il n'y a pas de regret, c'est faire le bien.

|* Fanta : Ala ye hinɛ a la, ka yaafa kɛ a ma/ye.
Ala ye a jigiyɔrɔ ɲɛ (ɲan). 
A ka an kɔn kiti min na, Ala ye a nɔgɔya a ye.

|Que Dieu ai pitié de lui, qu'il le pardone. 
Que sa demeure(tombe) soit rendu agréable  
il nous devance dans le jugement, que dieu le lui facilite.

|* Umu : Amiina, Ala ye dugawu mina. 
|Que Dieu accepte (attraper) les bénédictions.
Ayiwa ka an bɛn (An bɛ) oh.

|* Fanta : k’an bɛn (An bɛ) Umu, n b’i somɔgɔw fo.
|Au revoir, je salut la famille.

|* Umu : O bena a mɛn. Ala ye tele/tere ɲuman ban.
|Bien sûr. Que Dieu fasse que la fin de la journée soit bonne. 
|* Fanta : Amiina
|Amen
|====
ɲan=rendre bien

.VOCABULAIRE : 
[width="100%",cols="3",frame="none",options="none",stripes="even"]
|====
a|
Ala:: Dieu
Umu:: Oumou, prénom féminin
à:: il, elle, lui
á:: vous
amiina:: amen
an:: nous
ban:: finir
bi:: aujourd’hui
bɔla:: sortir+continuité
bɛ:: aux
bɛ:: être
bɛɛ:: tous
bɛ… la:: forme progressive
cɛ:: distance
cɛkɔrɔba:: vieil homme
cɛn:: vérité
da:: porte
dugawu:: vœux, bénédiction
dɔ:: un
dɛ:: assertion
gbɛlɛn:: difficile
hinɛ:: avoir pitié
i:: tu
jan:: long, grand
janya:: devenir grand
janya:: distance, trajet
jigiyɔrɔ:: destination, escale
jɔn:: qui ?
a|
ka:: aux. devant adjectif
ka:: être
kabini:: depuis
kelen:: une
kiti:: jugement
ko:: affaire
kola:: tomber+acc
kosɔn:: à cause
kɔ:: derrière
kɔn:: devancer
kɔnin:: quant à
kɔnɔ:: dans
kɛ:: faire
la:: à
le:: présentatif : c’est
lo:: présentatif
lon:: date, jour
ma:: à
min:: où ce que, lequel
mina:: attraper, exhausser
mɔbiliko:: affaire de voiture
mɔgɔ:: personne
mɛnna:: durer+acc
n:: moi
na:: dans
na:: postposition
ni:: et
nimisa:: regret
nɔgɔya:: faciliter
a|
o:: cela, ça
sanni:: avant que
sarakabɔ:: cérémonie funèbre
sarakabɔyɔrɔ:: cérémonie funèbre du 7e jour du décès
saya:: mort
se:: arriver
so:: maison
su:: nuit
sɔgɔma:: matin
sɛgɛn:: fatigue
ta:: pour, de
tan:: ainsi, comme ça
tele:: jour, journée
teri:: ami
tun:: marque imparfait
tɛ:: négation
wolonfla:: sept
ya:: possessif
ye:: aux. impératif
ye:: identité
ye:: postposition
yen:: là
yɔrɔw:: lieux
yɛrɛ:: même
yɛrɛw:: mêmes
ɲuman:: bon, bien
ɲɔgɔn:: l’un, l’autre
ɲɛ:: rendre meilleur
|====

== POINT DE GRAMMAIRE : LA POSSESSION ET LES COMPLEMENTS DU NOM

=== Construire cinq phrases pour chaque structure syntaxique en vous appuyant sur les structures suivantes :

. « …ye…ta ye » = appartenir
- Gafe nin ye ne ta ye= ce livre m’appartient.
- « …tun ye…ta ye »= appartenait.
- « …tɛ…ta ye » =
- « …tun tɛ…ta ye »
- « …kɛ… ta ye »
- « …kɛla… ta ye »

[%collapsible]
====
. mobili min tɛmɛna, a ye i ta ye wa?
. nɛgɛso nin y'i ta ye wa?.
. ɔ̀n ɔ́n nɛgɛso nin tɛ ne ta ye. 
. tunyan tɛ.  a y'i ta ye 
. a tun ye ne ta ye. n k'a feere n sigiɲɔgɔn ma.
. a kɛla n sigiɲɔgɔn dencɛ ta ye.
====

. *” kɛ...ye”* = devenir
- Nin bɛ kɛ fɛn ye
- Nin kɔnin tɛ kɛ foyi ye.
- ”kɛla… ye”
- ”ma kɛ… ye”

[%collapsible]
====
- a kɛla kuntigi ye.
- a ma kɛ mɔgɔba ye.
- a kɛla karamɔgɔ ɲuman ye.
- a ma kɛ mobilitigi ye. a ma peremi sɔrɔ.
- a kɛla tere fila ye.
====

. « ka ban »
- A ka baara kɛ ka ban
- …domuni kɛ…
- …kuman fɔ… (parlé de lui à)
- A ma mɔgɔw sara ka ban

[%collapsible]
====
- an ka sogo domu ka ban
- mobili bɔla ka ban
- an ka a facɛ kuman fɔ ka ban
- denmisenw ka tolonkɛ ka tere ban
- a tagala abidjan ka ban
====
. « tuma dɔ la » = souvent, parfois
- A ka ɲin tuma dɔ la, a ka jugu tuma dɔ la.

[%collapsible]
====
- tuma dɔ la, toto be don bon na ka baranda domu.
- tuma ɗɔ la, mɔgɔw be wuya fɔ. 
- tuma dɔ la, n bɛ julakan degi.
- a ka ɲi tuma dɔ la, mɔgɔ be sport kɛ;
- tuma caaman na, n bɛ baro kɛ ni musa ye a ya cɔsi la 
====

nimisa b'a la:: la ou il y du regret
