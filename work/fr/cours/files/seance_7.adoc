---
title:  SÉANCE 7 COURS DE DIOULA 
author: Youssouf DIARRASSOUBA
date:  2020-05-24
type: doc
---

= SÉANCE 7 COURS DE DIOULA 
:author: Youssouf DIARRASSOUBA
:date: 2020-05-24
:type: post
:sectnums:
:sectnumlevels: 2
:lang: fr
:page-lang: {lang}
include::ROOT:partial$locale/attributes.adoc[]

== LÉÇON 7 : KALANSEN WORONFILANAN

== REVISION DU COURS SUR L’HABITUEL

=== Exercices de compréhension

Traduire les phrases suivantes :

. Je ne suis pas musulman, je bois l’alcool
. L’ami de Siriki ne mange pas la viande de porc
. Le petit frère de ma femme ne travaille pas samedi et dimanche
. Kelly apprend le dioula tous les samedis
. Ali cultive du mil chaque année
. La mère de mon ami cuisine très souvent
. Les enfants jouent au foot tous les jours
. Youssouf ne mange pas du riz les matins


.Réponses
[%collapsible]
====
. n tɛ silama ye; n bɛ dɔrɔ min
. Siriki tericɛ tɛ lɛsɔgɔ domu
. n ya musuodɔgɔcɛ tɛ bara kɛ samedilon ne dimansilon
. Kelly bɛ julakan karan samedi bɛɛ
. Ali bɛ ɲɔ sɛnɛ saan bɛɛ
. n tericɛ bamuso bɛ tobali kɛ tuma dɔw
. denminsenw bɛ balɔn tan lon bɛɛ
. Yousouf tɛ malo domu sɔgɔma bɛɛ
. Moussa baarakɛlaw tɛ domuni kɛ sɔgɔma bɛɛ
. Jɔn bɛ baara kɛ tɛnɛlon bɛɛ
====

=== Exercise

Proposez des réponses à ses questions

. Bagan jumɛn tɛ sogo domun ?
. Mɔgɔ jumɛn tɛ dɔrɔ min ?
. I bɛ sogo san jɔn fɛ/bolo lɔgɔfɛ la ?
. Coulibaly bɛ zagamɛn domun wa ?
. Adamanden bɛ mun kɛ lon bɛɛ?
. Ile bɛ mun domun lon bɛɛ sɔgɔma ?
. Sanji bɛ ben tuman caman Buake wa ?
. I bɛ kunun lɛri jumɛn tuman caman?

== Quelques précisions sur le cours précédent

Si les structures étudiées précédemment ne posent pas de problèmes à priori, au niveau des formes grammaticales où l’objet n’est pas formellement marqué, le locuteur est appelé à « nominaliser » le verbe et le faire suivre du verbe « Kɛ » (faire).

En quoi consiste la nominalisation ?

La nominalisation est un processus grammatical qui consiste à transformer un verbe en nom.
Pour nominaliser un verbe, on ajoute à celui-ci un suffixe (li ou ni) selon la terminaison du verbe.
Si le verbe a une terminaison orale (c'est-à-dire lorsque la dernière syllabe du verbe est orale), on lui ajoute « li ».
Mais lorsque ce verbe a une terminaison nasale, on lui ajoute « ni ».

[width="100%",cols="2",frame="none",options="none",stripes="even"]
|===
|Verbe + ni|dernière syllabe du verbe est nasalisée.
|Verbe + li|dernière syllabe du verbe est oral
|===

Lire les exemples suivants

* N bɛ zagamɛn domu
* N bɛ domuni kɛ (Sans object)
* Ali bɛ misi feere
* Ali bɛ feere kɛ (Sans objet)
* Cɛ nin bɛ sogo san Mariam ka butiki la
* Cɛ nin bɛ sanni kɛ Mariam ka butiki la (Sans objet)
* Musa bɛ zouglou dɔn
* Ali bɛ dɔn kɛ lon bɛɛ (Sans objet)

=== Exercice de nominalisation

Donnez les formes nominalisées de ces verbes

. karan (coudre) 
. dalabɔ (provoquer)
. domun (manger)
. filɛ (regarder)
. fo (saluer)
. kɔrɔfɔ (critiquer, médire)
. ɲiniga (demander)
. san (acheter)
. soɲan (voler) 
. sɔsɔ (discuter)
. tobi (cuisiner)
. tɛrɛmɛn (négocier, marchander) 

[NOTE]
====
Il existe des mots en Dioula de Côte d’Ivoire qui sont employés en tant que noms et verbe selon le contexte dans lesquels ils sont employés : il s’agit des verbo- nominaux.
Ils sont en stock limité.
====

[width="100%",cols="2a",frame="none",options="none",stripes="even"]
|===
|
baara:: travailler/travail
bori:: courir/course
dimin:: faire
dɔn:: danser/danse
feere:: vendre/vente
|
karan:: étudier/étude
kɛlɛ:: combattre/combat
yafa:: pardonner/pardon
dimi:: mal/douleur
sɛnɛ:: cultiver/culture
|===

NOTE: Cette liste est loin d’être exhaustive, elle vous permet cependant de voir ce qu’il est convenu d’appeler « verbo-nominaux ».
La pratique quotidienne de la langue et/ou l’usage d’un dictionnaire permettra de les répertorier.

== SÉANCE D’APPRENTISSAGE DU JOUR

== Le progressif

[abstract]
Le cours de ce jour porte sur le progressif.
Les phrases au progressif expriment les actions en cours de réalisation.
Il s’agit du « véritable présent » en Dioula de Côte d’Ivoire.

[width="100%",cols="2",frame="none",options="none",stripes="even"]
|===
|Ali bɛ maro feere la|Ali est en train de cultiver du riz
|Musa tɛ lemuru min na|Moussa n’est pas en train de boire de l’orange
|Penda bɛ samara san na|Penda est en train d’acheter des chaussures
|Alimata bɛ sogo tobi la|Alimata est en train de préparer de la viande
|===

La structure qui se dégage de ces phrases exprimant les actions en cours de réalisation est la suivante : Sujet + auxiliaire (bɛ ou tɛ) + objet + verbe + la/na.

Le choix des postpositions la et na est fonction de la terminaison du verbe :

✓ Lorsque le verbe a une terminaison orale, on le fait suivre de la postposition « la ».

. Diarra bɛ samara feere la
. Amidu bɛ jenbe fɔ la
. Penda tɛ a ya mobili ko la
. I bamuso bɛ i wele la

✓ Cependant, lorsque la terminaison (la dernière syllabe) du verbe est nasale (an, en, ɛn, in, un, on), on le fait suivre de la postposition na.

. Diarra bɛ samara san na
. Amidu bɛ maro sɛnɛ na
. filɛ ! Siriki bɛ nan na
. denminsɛnw bɛ maro domun na

=== Exercice

Construire des phrases au progressif avec les verbes suivants :

[width="100%",cols="2",frame="none",options="none",stripes="even"]
|====
a|
karan:: coudre
domu:: manger
san:: acheter
fo:: saluer
wele:: appeler
a|
ɲini:: chercher
woroman:: trier
dalabɔ:: provoquer
saman:: tirer
firi:: jeter
|====

