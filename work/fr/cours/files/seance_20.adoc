---
title:  SÉANCE 20 COURS DE DIOULA
author: Youssouf DIARRASSOUBA
date:  2020-08-08
type: doc
---

= SÉANCE 20 COURS DE DIOULA
:author: Youssouf DIARRASSOUBA
:date: 2020-08-08
:type: post
:experimental:
:sectnums:
:sectnumlevels: 2
:lang: fr
:page-lang: {lang}
include::ROOT:partial$locale/attributes.adoc[]

== LÉÇON 20 : KALANSEN MUGANNAN

=== BARO TANNAN/THEME 10 : Premier jour de  à Abidjan

====
Contexte de communication:: se la Abidjan.

A ma mɔgɔ lɔn nka a kan ka a a yɛrɛ….
A jigi la lotɛlibon na yani a bɛ bon sɔrɔ ka sigi.
A tericɛ JAMES bɛa a dɛmɛn ka bon nyini Abidjan.
Bon sɔrɔ ma di Abidjan nka ni wari bɛ i bolo, i bɛ bonnyuman sɔrɔ jonan.
====

****
* I danse
* N’ba n tericɛ, I bɛ mini ?
* N bɛ so kɔnɔ. Ile don ?
* N bɛ Abijan lotɛlibon dɔ la fɔlɔ
* I bɛ lotɛlibon jumɛn na ?
* Lotɛlibon min tɔgɔ ye TIAMA. A bɛ Plato la
* N ka a yɔrɔ lɔn. N bena n labɛn ka i sɔrɔ yen
* N bɛ I kɔnɔ na. I bena nan wagati jumɛn?
* 10 h wagati la ni Ala sɔn na
* K’an bɛ JAMES
****

Quelques heures plus tard (lɛri daman daman timinin kɔfɛ)

****
* N ya foli bɛ i ye
* N’ba, i nanni diya la n ye kosɔbɛ. N ya bonko bɛ di ?
* N ka bon dɔ ye Riviera, Cocody kin kɔnɔ. An bena taga a filɛ nyɔgɔn fɛ
* Lɔ n bɛ n labɛn an bɛ taga
* I kana mɛn dɛh, ni an ma taga jonan mɔgɔ gbɛrɛ bɛ se ka bon tɔgɔ ta
* N ka a famu. N bena n sennateliya!
****

Près d’une heure plus tard (lɛri kelen nyɔgɔn timinin kɔfɛ)

****
* Bon filɛ nin ye
* Aaaah, bon nin cɛ ka nyi kojugu. Bonsara ye joli ye ?
* Waga bi naani
* Eeeeh n tericɛ, o ka ca dɛh. N tun bɛ bon min na Bouake, n tun b’a sara waga mugan
* O ye can ye nka Abidjan, bonw sɔngɔ ka ca
* N bena tere fila kɛ lotɛlibon yani n bɛ wari dafanin sɔrɔ.
* Yani o wagati cɛ, n bena bon gbɛrɛ nyini i ye ni Ala sɔn na
* O bena diya n ye kosɔbɛ. I ni ce JAMES
* Folikun tɛ. Teriya kun ye nyɔgɔndɛmɛn fana ye
****
