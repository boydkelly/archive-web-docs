---
title: SÉANCE 40 COURS DE DIOULA
author: Youssouf DIARRASSOUBA
date: 2020-12-12
type: doc
draft: false
categories: []
tags: []
---

= SÉANCE 40 COURS DE DIOULA 
:author: Youssouf DIARRASSOUBA
:date: 2020-12-12
:type: post
:experimental:
:sectnums:
:sectnumlevels: 2
:lang: fr
:page-lang: {lang}
include::ROOT:partial$locale/attributes.adoc[]

== LÉÇON 40 : KALANSEN BI NAANI

=== LECTURE ET COMPREHENSION DE TEXTE

Kabini sɔgɔma joona mɔgɔw bɛ wili joona.
bɛɛ b’i kunsin gari ma.
mɔgɔw ka ca gari la, mɔbili caman bɛ yen.
bɛɛ bɛ taga i ta fan. (chacun va de son cote)
bɛɛ kɔrɔtɔnin lo.
Biye feerelaw bɛ yen, o siginin bɛ o ya biro kɔnɔ ka mɔgɔw kɔnɔ.
mɔgɔw bɛ nan ka biye ta.
Ni minan b’o bolo, o ka kan ka o fana sara.
Hali ni fen wɛrɛ t’i bolo valizi kelen kɔ, i ka kan k’o sara.
(mème si tu n'a qu'un seule valize)

Tagamaden dɔw bɛ nan, dɔw bɛ taga.
Mankan caman bɛ gari la.
I bɛ sunguruninw tɛmɛtɔ ye.
Dɔw bɛ woso belekenin ani ku beleke feere.
dɔw bɛ ji sumanin, tomiji ani ɲamakuji feere.
Feerekɛbagaw ka ca gari la.
mɔgɔw bɛ kɛlɛ, o bɛ dimi ɲɔgɔn kɔrɔ.
Sonw bɛ yen, n’i m’i yɛrɛ kɔrɔsi, o b’i sonya.
voleur
baaraɲiniw ani wotorotigiw bɛ yen, o bɛ doni ta mɔgɔw ye ka taga yɔrɔw la.

.Ɲiningaliw
====

====

.Vocabulaire
[width="100%",cols="3",frame="none",options="none",stripes="even"]
|====
a|
ani:: et
beleke:: bouillie
belekenin:: bouillie
biro:: bureau
biye:: billet
bolo:: main
bɛ:: aux.
bɛ:: aux. présent
bɛ:: être
bɛɛ:: tous, tout le monde
ca:: nombreux
caman:: beaucoup
dow:: les autres, d’autres
fan:: côté
fana:: aussi
feere:: vendre
feerelaw:: vendeurs
fɛn:: chose
gari:: gare
hali:: même
i kunsin…ma:: se diriger vers
i:: toi, tu, se
ji:: eau
joona:: très tôt
a|
ka kan ka:: doivent
ka:: aux
ka:: pour
kabini:: depuis
kelen:: un, une
ku:: igname
kunsin:: diriger
kɔ:: que
kɔnɔ:: attendre
kɔnɔ:: dans
kɔrɔtɔnin:: pressé
k’an ka:: dois
la:: à
la:: là
lo:: c’est
ma:: vers, à
mankan:: bruit
minan:: bagage
mɔbili:: voiture
mɔgɔw:: gens
na:: venir
ni:: si
a|
o:: cela, ca
sara:: payer
siginin:: assis
sumanin:: glacée
sunguruninw:: jeunes filles
sɔgɔma:: matin
ta:: de
ta:: prendre
taa:: partir
taamaden:: voyageur
tomiji:: jus de tamarin
tɛ:: nég.
tɛmɛtɔ:: passant, en train de passer
u ya:: leur
u:: ils
valizi:: valise
wili:: se lever
woso:: patate douce
wɛrɛ:: autre
ye:: voir
yen:: là-bas
ŋamakuji:: jus de gengembre
|====

=== EXERCICE

. Ni i bɛ gari la, i bɛ to ka mun san feerekelaw fɛ?
. Gari bɛ i ya so kɛrɛfɛ Bouake wa?
. Ni i bɛ bɔ Bouaké, san Pedro mobilisara ye joli ye?
. Muna mankan caman bɛ gari la?
. Bouake ni san pedro cɛ ye kilo joli ye?
. Tubabu bɛ se ka don forobamobilikɔnɔ ten wa?
. N’i be taga wayasi la, i ka kan ka mun labɛn ?
. i dege la ka sira jan tagaman moto la wa?
. A bɛ bɛn polisiw bɛ wari minɛn sofɛriw la sira la wa?
. I tagatɔ wayasi la, i bɛ sanni kɛ sira la tuman caman wa?

[%collapsible]
====
. Ni n bɛ gari la, n bɛ to ka sisɛ fan ni baranda san feerekelaw fɛ.
. Ɔn-ɔn.  Gari tɛ n ya so kɛrɛfɛ. A ka jan.  A ka kilomɛtɛri naani ɲogon bɔ.
. Ni n bɛ bɔ Bouake...
. Mankan caaman bɛ gari la sabu mɔgɔ caaman bɛ yen, mobili caaman bɛ yen, feerekɛlaw caaman be yen. 
. Bouake ni San Pedro cɛ ye kilo kɛmɛ naani ni bi wɔlɔnfla ni kelen ye.
. Ɔn-hɔn.  Tubabu be se.
. N bena fɛ caaman labɛn.  Fani, fura, domu, dji, wari, unite, ordinateri.  Fɛɛn caaman wɛrɛ .
. Ɔn-ɔn. N ma dege ka sira jan tagaman moto la.  Farati b'a la.
. Ɔn-hɔn.  Sinɛ kelen.
. N bɛ sanni kɛ sira la tuman dɔ.
====

=== POINT DE GRAMMAIRE

Révision du segment « ni » en jula. Il introduit la condition dans l’énonciation

[%horizontal]
====
N’i bɛ bɔ, i bɛ da tugu.:: Si tu sors, tu fermes la porte.

N’i bɛ domuni kɛ, i kana ɲinan denmisɛnw kɔ.:: Si tu manges n’oublies pas les enfants.

Fɔlɔlɔ, ni lo tun bɔ la, musow tun bɛ boli ka dogo.:: Quand le masque apparaissait les femmes couraient se cacher.

Ni su ko la, denmisɛnw ma kan ka bɔ.:: Quand il fait nuit les enfants ne doivent pas sortir.
====

=== EXERCICE : Construire au moins 10 phrases sur la base de ces structures

N'i bɛ sinɔgɔ, soɲalikɛla bena na.
N'i be taa kɔgɔjida la, kana do kɔgɔji.
N'i be domuni kɛ, i ka kan ka i tɛgɛ ko.
N'i facɛ be nan, i ka kan k' a fo.
N'i be taga lɔgɔfiya la, i be voca ni tomati saan.
Ni courant tigɛla, an tɛ se ka kalan kɛ.
Ni ji tigɛla, mɔgɔ tɛ se ka ko, mɔgo tɛ se ka toblili kɛ. 
Ni dugu gwɛ la, foyi tɛ.
Ni patɔrɔn na na, a bena baarakɛlaw sara.
Ni n ka mobili saan, n bena taga abidjan ka sani kɛ.

ne be siran nɔnɔ kumu ɲa

