---
title: SÉANCE 41 COURS DE DIOULA
author: Youssouf DIARRASSOUBA
date: 2020-12-19
type: doc
draft: false
categories: []
tags: []
---

= SÉANCE 41 COURS DE DIOULA
:author: Youssouf DIARRASSOUBA
:date: 2020-12-19
:type: post
:experimental:
:sectnums:
:sectnumlevels: 2
:lang: fr
:page-lang: {lang}
include::ROOT:partial$locale/attributes.adoc[]

== LÉÇON 41 : KALANSEN BI NAANI NI KELENNAN

=== BARO : SOKƆNƆBAGANW

Sokɔnɔbaganw ye: misi, so, fali, saga, baa, wulu ani jakuma.
O ɲaci ka bon mɔgɔw ye.
Misimuso bɛ nɔnɔ di misiden ma.
Nɔnɔ ka di mɔgɔ caman ye nka a man di dɔw ye.
Tora ye misi cɛman ye.
Tora fla bɛ siri misidaba la ka sɛnɛ kɛ ka suman caman sɔrɔ.
Misidaba sɛnɛkɛ ka teli ni bololadaba ta ye.
Misiw kulɛri (ɲa) ka ca : ɲa wulenman bɛ yen, ɲa finman, ɲa gbɛman, ɲɛgɛɲɛgɛlen ani tɔmitɔmilen fana bɛ yen.
Mɔgɔ bɛ yɛlɛn misi kan, a sogo bɛ domun fana.
Misibaara ka gbɛlɛn, bɛɛ tɛ se ka misi mara.
Misibaara ka di flaw ye kosɔbɛ.
Mɔgɔ caman bɛ misiw bla k’o balo k’a masɔrɔ nafa caman bɛ sɔrɔ o la.
So ni fali olugu fana ye bagan ye minw bɛ mɔgɔw dɛmɛ. yɛlɛnfɛnw lo ka taga yɔrɔw la, dugu ni dugu.
O bɛ soo walima fali don wotoro la k’a saman.
Soo ni fali bɛ doniba ta.
Baganmaralaw bɛ saga ni baa fana balo k’o feere.
Sagasogo ka di ni baasogo ye.
Saga cɛman tɔgɔ bɛ ko sagajigi.
Baa cɛman tɔgɔ bɛ ko bakɔrɔnin.
Saga gbolo, baa gbolo ani misi gbolo tɛ fili.
O bɛ kɛ ka baara caman kɛ.

Samaraw, bololabɔɔrɔw, kunkololabɔɔrɔw, gbolo bɛ kɛ ka nin bɛɛ ladinɛ.

Wulu ka di mɔgɔ caman ye ka bla so kɔnɔ walasa ka se ka yɔrɔ kɔlɔsi mɔgɔ juguw kanma.
Wulu b’a tigi lɔn kosɔbɛ.
A tɛ mɔgɔ min lɔn, a b’o tigi bali ka don a tigi ya so kɔnɔ.
Mɔgɔ caman bɛ siran wuluw ɲɛ.
Ni jakuma b’i fɛ so, ɲinanw tɛ caya bon kɔnɔ.
Jakuma ka jugu ɲinanw ma kojugu.
-- chat est dangéreux pour les souris.

ɲinan bɛ boli tuman bɛɛ jakuma ɲɛ.
Jakuma bɛ girin ka la o kan k’o mina.

premier ti celui dont on a parle.  2i tigi proprietaire.


== NYININGALI NINUGU JAABI

. Tora ye mun ye?
. Ile nyana, sagasogo ni baasogo jumɛn ne ka di?
. Muna mɔgɔ caman bɛ misi, saga ni baa balo?
. Nafa jumɛn bɛ baganw gbolo la?
. Muna mɔgɔ caman bɛ siran wulu ɲɛ?
. Sokonɔbagan bɛ i ya so kɔnɔ Bouaké wa?
. Nɔnɔ ka di ile ye wa?
. Misi ɲa bulalaman bɛ sɔrɔ dunuɲa kɔnɔ wa?
. I be se ka tukɔnɔbagan looru tɔgɔ fɔ n ye wa?
. Adamadenw bɛ siran tukɔnɔbagan jumɛn ɲɛ?
. I dege la ka malobaga ni misi nɔnɔ min wa?
. Gbolosamara ka di ile ye wa?
. Mɔgɔ dɔw bɛ misisen domun ni pilakali ye. Ile dege la ka o kɛ wa?
. Sokɔnɔbagan bɛ domun wa ?
. Baa cɛman tɔgɔ bɛ di?
. Basa min kun wulenin bɛ, a tɔgɔ bɛ di? basa kerengbe 
. Saga ni misi bɛ balo ni mun ye? ni binkene ye.
. Jakuma bɛ sogo domun wa?
. farafinsisɛ kelen sɔngɔ ye joli Bouaké?
. Ile bɛ lɛɛsogo domun wa. N’i ko «ɔn-hɔn», i b’a domun ni mun ye tuman caman?

[%collapsible]
====
. Tora ye misi cɛman ye
. Ne ɲana sagasogo ka di ni baasogo ye
. Sabu o bɛ sogo ni gbolo di mɔgɔ ma
. Babanw glolo nafa ka bon. o bɛ kɛ ka samaraw ni bololabɔɔrɔw, ni kunkoloabɔɔrɔw ladinɛ.
. Ni a tɛ mɔgɔ lɔn, a be se ka kin.
. jakuma tun bɛ n ya so kɔnɔ Bouaké.  Nka, n k'a di n sigiɲogon ma.
. Nɔnɔ ka di kɔsɔbe.  Degué ka di kojugu.
. Misi ɲa bulaman tɛ sɔrɔ.
. Toto, ɲinan, ɲɛmbɛrɛ, silandrɛ, basa
. Wulu, Misi, Baa, Saga, Toto.
. Adamadenw bɛ siran wulu ɲɛ.
. Ɔn-hɔn, ne ka malobaga domu ni misi nɔnɔ tuma caaman.
. Ɔn-hon gbolosamara ka di kojugu.
. n dega la.  nka pilakali man di n ye
. Ɔn⁻ɔn.  Sokɔnɔbagan tɛ domun.
. Baa cɛman tɔgɔ be bakɔrɔnin
. a tɔgɔ bɛ (n ma faamu)
. n ka dege la fɔlɔ.  nka a ka gwɛlɛ.
. n bɛ lɛsogo domu tuma dama dama. ni fɔrɔnto. 
====

.Vocabulaire 
[width="100%",cols="3",frame="none",options="none",stripes="even"]
|====
a|
ani:: et
ba:: chèvre
baa:: chèvre
baara:: travail
baganmaralaw:: éléveurs
baganw:: animaux
bagbolo:: peau de chèvre
bakɔrɔnin:: bouc
bali:: empêcher
balo:: nourrir, élever
bamuso:: chèvre
basogo:: viande de chèvre
bla:: laisser
bla:: laisser, garder
boli:: courir
bololabɔɔrɔw:: sacs à main
bololadaba:: daba à la main
bon:: chambre
bon:: grand
bɛ:: aux.
bɛɛ:: tous
ca:: nombreux, beaucoup
caman:: beaucoup
caya:: être beaucoup
cɛman:: mâle, viril
daabaw:: animaux domestiques
di:: bon, bonne
di:: donner
domu:: manger
don:: entrer
don:: mettre
doniba:: fardeaux
dugu:: village, ville
dɔw:: d’autres
dɛmɛ:: aider
fali:: âne
fana:: aussi, également
fili:: jeter
finman:: noire
fla:: deux
flaw:: peusl
fɛ:: postposition
gbolo:: peau
a|
gbɛlɛn:: difficile
gbɛman:: blanche
girin:: précipiter
jakuma:: chat
juguw:: méchantes
ka:: être
ka:: pour
kan:: sur
kanma:: contre
kosɔbɛ:: très bien
kulɛri:: couleur
kɔlɔsi:: garder
kɔnɔ:: dans
kɛ:: faire
kɛrɛkɛw:: selles
k’a masɔrɔ:: parce que
la:: dans
la:: mettre
ladinɛ:: fabriquer
lo:: c’est
lɔn:: connaitre
ma:: au
mara:: garder
min:: qui
mina:: attraper
minw:: lesquels, qui
misi:: bovidé, vache
misibaara:: travail de bovidé
misidaba:: charrue
misiden:: veau
misigbolo:: peau de bœuf
misimuso:: vache
mɔgɔ:: personne
mɔgɔw:: gens
nafa:: interêt, avantage
ni:: et, que, si
nin:: ceci, ça
ni…ye:: que, plus…que
nɔnɔ:: lait
o:: eux, ilsaa
olugu:: ceux-ci
saga:: mouton
sagagbolo:: peau de mouton
a|
sagajigi:: bélier
sagasogo:: viande de mouton
sama:: tirer
samaraw:: chaussures
se:: pouvoir
siran:: avoir peur
siri:: attacher, atteler
so:: cheval
so:: maison
sogo:: viande
sokɔnɔbaganw:: animaux domestiques
suman:: recolte
sɔrɔ:: trouver
sɛnɛ:: culture
sɛnɛkɛ:: labour
ta:: pour, connectif
ta:: prendre
taa:: aller
teli:: rapide
tigi:: maître, chef
tora:: taureau
tuma:: temps, moment
tɔgɔ:: nom
tɔmitɔmilen:: tachetée
tɛ:: nég.
u:: ils
walasa:: afin que
walima:: ou bien
wotoro:: charrette
wulenman:: rouge
wulu:: chien
ye:: être
ye:: postposition
ye:: pour
yen:: là-bas
yɔrɔ:: endroit
yɛlɛfɛnw:: montures
yɛlɛn:: monter
ɲa:: couleur
ɲaci:: utilité
ɲinanw:: souris
ɲɛ:: de, devant
ɲɛgɛɲɛgɛlen:: bariolée
|====

=== POINT DE GRAMMAIRE

. Révision du segment « di...ma » en jula.
* Donne-le moi:: a di n ma
* Donne-le lui:: a di a ma
* Donne-le nous:: a di an ma
* Donne-lui le sein:: sin di a ma
* Donne-lui le lait:: Nɔnɔ di a ma
* Donne-lui la cola et le tabac:: woro ni saramugu d’a ma
* La vache donne le lait à son veau:: Misimuso bɛ nɔnɔ di a den ma.

. EXERCICE : Construire 10 phrases avec la formule « di...ma »

[%collapsible]
====
. Wari di n ma!
. Misi muso bɛ nɔnɔ di mɔgɔw ma.
. Mɔgɔw bɛ nɔnɔ kumu di an ma
. n ka jo di i ma
. i be se ka ladili di n ma
. minji di an ma
. muso ka domuni di a denw ma
. coca cola bɛ sucaro bana di mɔgɔ ma
. jakuma be kun dimi di ɲinaw ma
. dɔtɔrcɛ ka furasansɛbɛ di banabagatɔ ma   
====

