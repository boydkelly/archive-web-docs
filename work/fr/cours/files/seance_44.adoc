---
title: SÉANCE 44 COURS DE DIOULA
author: Youssouf DIARRASSOUBA
date: 2021-01-02
type: doc
draft: false
categories: []
tags: []
---

= SÉANCE 44 COURS DE DIOULA
:author: Youssouf DIARRASSOUBA
:date: 2021-01-02
:type: post
:toc: true
:experimental:
:sectnums:
:sectnumlevels: 2
:lang: fr
:page-lang: {lang}
include::ROOT:partial$locale/attributes.adoc[]

== LÉÇON 44 : KALANSEN BI NAANI NI SABA

=== BARO : GWƐLƐA

Gbɛlɛya min tun kofɔ la o sera bɛɛ ɲa na.
Nin tɛ yɔrɔ kelen koo ye, yɔrɔ bɛɛ lo.
bɛɛ bɛ kule la, k’a masɔrɔ bɛɛ sɛgɛn na, bɛɛ ko wari.
Wari tɛ sɔrɔ la ka caya ka makow ɲɛ.
(pour satisfaire les besoins)
Wari banna, gbɛlɛya bɛ bɛɛ kan.
Kunkow ka ca, bɛɛ n’a ya kunko.
Baarakɛbaliw ka ca.
(les sans emplois)
Sabu baara tɛ sɔrɔla.
N’i ma baara kɛ i tɛ wari sɔrɔ i tɛ fen sɔrɔ.
Gbɛlɛya donna yɔrɔw la.
fɛnw sɔngɔ ka gbɛlɛ, lɔgɔfiyɛ ka gbɛlɛ, can lo, nka mɔgɔ dɔw bɛ yen foyi man di olugu ye gbɛlɛya kɔ.
(pour certaines personnes n'aime pas autre chose que les difficultés)
O bɛ dɔ fara fɛnw sɔngɔ kan.
N’i k’o ɲininga, o b’a fɔ ko olugu nɔ tɛ.
(si tu leur demande ils disent ce n'est pas leur faute)
O b’a fɔ ko minanw sɔngɔ yɛlɛn na.
jigi=décendre yɛlɛn=augmenter
O b’a fɔ ko dɔ fara la mɔbilisara kan.
fara=ajouter
(fɛnw sɔngɔ yɛlɛna; dɔ fara la fɛn sɔngɔ kan)
An bɛ wele bla ɲɛmɔgɔw ma o ye janto jamana na.
(lance un appel)
O ye hinɛ tɔw la: fangantanw, banabagatɔw ani sentanw. (pas de pieds)
O ye sabali ka baarada caman kɛ fasodenw ye jango baarakɛbaliw ye dɔgɔya.
O ye jija ka faso wari lakanda, k’a lamara ka ɲa k’a kɛ ka hɛrɛ lase jamanadenw ma.

foyi man di adamadenw dɔw ye wari kɔ. 
foyi man di ne ye misi sɔgɔ kɔ

.Vocabulaire
[width="100%",cols="3",frame="none",options="none",stripes="even"]
|====
a|
amana:: pays
baara:: travail
baarada:: travail, emploi
baarakɛbaliw:: ceux qui ne trvaillent pas
banabagatɔw:: malades
banna:: finir+acc
bɛ:: aux.
bɛ:: être
bɛɛ:: tous
ca:: nombreux
caman:: beaucoup
can:: vérité
caya:: multiplier, être beaucoup
di:: bon, agréable
donna:: entrer+acc
dɔ:: quelque chose, en
dɔgɔya:: dimunier
dɔw:: certains
entanw:: ceux qui sont sans pouvoir
fangatanw:: pauvres
fara:: ajouter
farala:: ajouter+acc
faso:: patrie
fasodenw:: peuple, patriotes
foyi:: rien
fɔ:: dire
fɛn:: chose
fɛnw:: choses
gbɛlɛ:: dur, cher
gbɛlɛya:: diffuculté, cherté
gbɛlɛyaba:: grande cherté
hɛrɛ:: paix, bonheur
i ta:: le tien
a|
i:: toi
in:: relatif, qui
inɛ:: avoir pitié
jamanadenw:: peuple, population
jango:: afin que
janto:: veiller
jija:: s’efforcer
ka:: pour
kan:: sur
kelen:: un, une
ko:: affaire
ko:: dire
ko:: que
kofɔra:: annoncer+acc
kulela:: crier+acc
kunkow:: problèmes
kɔ:: que
kɛ:: faire, utiliser
k’a masɔrɔ:: parce que
la:: dans
la:: laisser
lakanda:: protéger
lamara:: garder
lase:: adresser, faire parvenir
lo:: c’est
lɔgɔfyɛ:: marché
ma:: à
makow:: besoins
man:: nég. Devant adjectif
minanw:: marchandises
mɔbilisara:: transport
mɔgɔ:: personne
a|
na:: dans, devant
na:: dans, sur
ni:: et
nin:: ceci, ça
nka:: mais
nɔ:: trace, fute
ɲa:: bien
ɲa:: œil, devant
ɲininka:: demander
ɲɛ:: satisfaire
ɲɛmɔgɔ:: gens de devant, dirigeants
o:: ça
olugu:: eux
sabali:: pardonner
sabu:: parce que
sera:: arriver+acc.
sɔngɔ:: prix
sɔrɔla:: trouver+forme progressive
sɛgɛnna:: fatiguer+acc
ta:: pour
tun:: marque imparfait
tɔw:: les autres
tɛ:: neg., ce n’est pas
u:: ils, leur, elles
wari:: argent
wele:: appel
ye:: postposition
ye:: pour
yen:: là
yɔrɔ:: endroit, lieu
yɛlɛnna:: monter+acc
|====
                                                        
=== POINT DE GRAMMAIRE : LA POSSESSION, LES COMPLEMENTS DU NOM

. …ye…ta ye » = Appartenir
** Gafe nin ye ne ta ye:: ce livre m’appartient.
** « …tun ye…ta ye » = appartenait.
** « …tɛ…ta ye » =
** « …tun tɛ…ta ye »
** « …kɛ…ta ye »
** « …kɛla…ta ye »

. « kɛ... ye » = devenir
** Nin bɛ kɛ fɛn ye
** Nin kɔnin tɛ kɛ foyi ye.
** « kɛla... ye »
** « ma kɛ... ye »

. « ka ban »
*  A ka baara kɛ ka ban
*  …domuni kɛ….
*  …kuman fɔ…
*  A ma mɔgɔw sara ka ban

=== EXERCICE : Donnez cinq (05) exemples pour chaque structure

=== NYININGALI NINUGU JAABI

. Muna an ma se ka kalan kɛ taratalon?
. I ka mun kɛ kunu sufɛ ?
. I bɛ domuni jumɛn kɛ fɛtilonw la?
. I be se ka dugawu kɛ julakan na wa?
. Bi ye san kulan tere filanan ye. I ya lanyini ye mun ye san kula nin kɔnɔ?

.Réponses
[%collapsible]
====
. Lɔn o lɔn, n be baara kɛ su fɛ.  N tun sɛgɛla taratalon tɛmɛni.
. N tun ka baara kɛ ni n ya site web ye.
. N ka sisɛ domu. 
. Dɔɔni.  Ala ya wula nɔgɔya.
. 



====

