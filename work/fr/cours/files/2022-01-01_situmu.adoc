---
title: Situmu
---

= Situmu
:date: 2022-01-01
:lang: fr
include::ROOT:partial$locale/attributes.adoc[]

.Situmu
[width="100%",cols="a,a,a,a",frame="none",options="header",stripes="even"]
|====
|Contexte|Français|Jula|Alternative


| (Tu es étranger)

|
[unordered]
As-tu jamais mangé des chenilles?::
oui j'ai déjà mangé des chenilles
+
non je n'ai jamais mangé des cheniiles

|
[unordered]
i delila ka situmu domu wa?::
* ɔh hɔn n delila ka situmu domu
+
* ɔn ɔn n ma deli ka situmu domu
+
* ɔn ɔn n ma deli ka situmu domu fɔlɔ

|
|
|
|
[unordered]
i ka situmu domu k'a ye wa?::
* ɔn hɔn n ka situmu domu k'a ye
+
* ɔn ɔn n ma situmu domu k'a ye


|
|
|
|
[unordered]
i deli la ka situmu domu wa?::
* ɔh hɔn n be deli ka situmu domu.
+
* ɔn ɔn n tɛ deli ka situmu domu.

|
|
|
[unordered]
Es-tu jamais allé à Tingrela?::
* Oui je suis déjà allé à Tingrela
+
* Non je ne suis jamais allé à Tingrela


|
[unordered]
i delila ka taga Tingrela wa?::
* ɔn hɔn, n tagala Tingrela siyɛn kelen
+
* ɔn ɔn n ma ta Tingrela fɔlɔ

|
|(les chenilles sont dans la cuisine)

|
[unordered]
As-tu déjà mangé les chenilles ?:: 
* Non je n'ai pas encore mangé les chenilles
+
* Oui j'ai déjà mangé les chenilles


|
[unordered]
i ka situmu domu ka ban wa?::
* ɔn ɔn n ma situmu domu ka ban.
+ 
* ɔh hon n ka situmu domu fɔlɔ. 

|
|(Il faut manger tes chenilles chaque jour.) 

|
[unordered]
As-tu fini de manger les chenilles?::
* Oui j'ai fini de manger les chenilles.
+
* Non je n'ai pas fini de manger les chenilles.

|
[unordered]
i ka situmu domu ka ban wa?::
* ɔh  hɔn, n ka situmu domu ka ban.
+
* ɔn ɔn n ma situmu domu fɔlɔ.
|


| Le vendredi soir

|
[unordered]
As-tu l'habitude de manger des chenilles?::
* Oui J'ai l'habitude de manger des chenilles.
+
* Non Je n'ai pas l'habitude de manger des chenilles.


|
[unordered]
i be to ka situmu domu wa?::
* ɔh hon ne be to ka situmu domu.
+
* n be situmu domun sinyɛn kelen kelen.
+
* ɔn ɔn, n tɛ to ka situmu domu.
+
* ɔn ɔn, n tɛ situmu domu siyɛn caman 

|
[unordered]
i be deli ka situmu domu wa?::
* ɔh hɔn n be deli ka situmu domu.
+
* ɔn ɔn n tɛ deli ka situmu domu.


| Avec les amis 

|
[unordered]
N'as-tu pas l'habitude de manger des chenilles?::
* Oui je n'ai pas l'habitude de manger des chenilles.
+
* Non J'ai l'habitude de manger des chenilles.


|
[unordered]
i tɛ deli ka situmu domu wa?::
* ɔh hɔn n degila ka situmu domu.
+
* ɔn ɔn n ma deli ka situmu doumu.
|

|
[unordered]
Est-il jamais allé à Abidjan?::
* Oui il est déjà allé à Abidjan.
+
* Non il n'est jamais allé à Abidjan.

|
|
[unordered]
a degila ka taga Abidan wa?::
* ɔh hɔn, a tagala Abidjan tuma caman.
+
* ɔh ɔn, a ma taga Abidjan fɔlɔ.

|====


