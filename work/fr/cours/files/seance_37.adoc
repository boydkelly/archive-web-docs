---
title: SÉANCE 37 COURS DE DIOULA
author: Youssouf DIARRASSOUBA
date: 2020-11-21
type: doc
---

= SÉANCE 37 COURS DE DIOULA
:author: Youssouf DIARRASSOUBA
:date: 2020-11-21
:type: post
:experimental:
:sectnums:
:sectnumlevels: 2
:lang: fr
:page-lang: {lang}
include::ROOT:partial$locale/attributes.adoc[]

== LÉÇON 37 : KALANSEN BI SABA NI WORONFILANAN

=== REVISION : PETIT EXERCICE DE TRADUCTION

* Traduire ses phrases et donnez, le cas échéant, les deux sens possibles de la même phrase.
. Baarakɛ ka di ni baarakɛbaliya ye
. Wari man di ni adamadenya ye
. Nɔgɔya ka di bɛɛ ye ni gbɛlɛya ye
. Flabuluna ka di ni malo ye
. Sumali ɲuman ka ɲin ni sumalijugu ye
. Sigiyɔrɔ saniyanin ka ɲin ni sigiyɔrɔ nɔgɔnin ye
. Kuman ɲuman fɔ ka fisa ni kuman jugu fɔ ye
. A ka baara kɛ ni Musa ye
. Cɛ nin bɛ tolon kɛ nin bɛɛ ye yan
. Domuni bɛɛ ka di Youssouf ye fɔ bananku belekenin


[%collapsible]
====
. Le travail est bien sans chommage
 * Le travail est bien quand il y a du travail.
. Les gens ont besoin de l'argent
. ?
. La sauce feuille est bon avec le riz.
. Récolte??
. L'endroit propre pour s'assoir  est bon que l'endroit sale  (ivoirien ! !)
. Dire une bonne parole est mieux que dire un mauvaise parole.
. Il travail avec Musa.
. Cet homme s'amuse avec tous ici.
====

== LA NOTION DE COULEUR EN DIOULA DE COTE D’IVOIRE

* Sɛbɛlikɛlan joli b’i kun ? (Combien de Stylo tu as sur toi ?)
* Sɛbɛlikɛlan naani dɔrɔn le b’a kun (Il a seulement quatre stylos sur lui)

. Exercice 1 : Construire des phrases en utilisant ces six (06) couleurs

. Sɛblɛlikɛlan bulalaman kelen dɔrɔn bɛ n kun.
. moblili beleble wulenman ka fisa mobilili fintini gbɛman ye wa ?
. Cɛ nin bɛ dɛrɛke bulalaman don tuma dɔ
. Flaburu janin tɛ binkɛnlaman ye tugun
. I be siran jakuma finman ɲa wa? 

[width="100%",cols="3*",frame="none",options="none",stripes="even"]
|====
a|bulalaman:: le bleu
finman:: le noir
a|wulenman:: le rouge
binkɛnɛlaman:: le vert
a|gbɛman:: le blanc
nɛrɛmugulaman:: jaune 
|====
                        
== BARO : Mɔgɔ ɲuman ni mɔgɔ jugu

* Kumanden ninugu kalan ka o kɔrɔ fɔ an ye

Mɔgɔ ɲuman ye jogoɲumantigi ye, min majiginin bɛ, a t’a yɛrɛ bonya, a bɛ mɔgɔw bonya.
Nyumankɛbaga ni jugumankɛbaga tɛ kelen ye.
N’i ka ɲuman kɛ, ɲuman (bena) na fɔ i ye.
N’i ka juguman kɛ, juguman fana (bena) na fɔ i ye.
I kana ɲuman ni juguman lajɛn ka kɛ kelen ye
Mɔgɔ ma kan ka ɲuman kɛ ka nata a tɔnɔ fɛ dununya nin kɔnɔ

Ɲumankɛbaga ma kan ka kɔrɔtɔ.
A tɛ ben, hali n’a ben na dununya kɔnɔ yan, a (bena) na tɔnɔ belebele sɔrɔ lahara lon.
Mɔgɔ ɲuman sara bɛ Ala le bolo.
Mɔgɔ jugu fana sara bɛ Ala le bolo.
Ala ka teli ka sara ɲuman belebele di mɔgɔ ɲuman ma, ka jugumankɛbaga halaki.
Jugumankɛbaga tɛ foyi sɔrɔ nimisa kɔ.

* N tericɛ, ɲiningali ninugu jabi 

. Ɲumankɛbaga ye jɔn ye ?
. Jugumankɛbaga bɛ lɔn mun man ?
. Ɲumankɛbaga sara ye mun ye ?
. Ile ɲana, Adamandenw bɛ ɲumankɛ nata le kosɔn wa ?
. Tɔnɔn jumɛn bɛ jugumankɛ la ?
. Bi, mɔgɔ juguw ka ca ni mɔgɔ ɲumanw ye dununya kɔnɔ wa ?
. Dɔrɔ min ye ko ɲuman ye wa ?
. Jogo daman daman yira min ye mɔgɔjugu tagamasere ye ?

[%collapsible]
====
. Ɲumankɛbaga ye mɔgɔ min bɛ mɛnni kɛ Ala fɛ. Ɲumankɛbaga ye mɔgɔ min majiginin ye. 
. Ala bena jugumankɛbaga halaki.
. A bena kɛ ala le bolo.
. a be fɛ ka Ala bena tɔnɔ di a ma. 
. A bena tɔnɔ sɔrɔ ni ala ye
. Bi, mɔgɔ juguw ca ca ni mɔgɔ ɲumanw ka ca fana.
. duvɛ ka fisa kutuku ye
. wuya, mɔgɔ fagala, sɔɲalikɛla 
====
.Vocabulaire 
[width="100%",cols="3",frame="none",options="none",stripes="even"]
|====
a|
Ala:: Dieu
a:: il, lui, le
ani:: et
baro:: dialogue
belebele:: gros, énorme
ben:: tomber
benna:: tomber+acc.
bolo:: main
bonya:: être fier
bonya:: respecter
bɛ:: aux.
di:: donner
dununya:: monde
fana:: aussi
fɔ:: dire
fɛ:: postposition
foyi:: rien
halaki:: détruire
hali o:: cependant
hali:: même
i:: tu, toi
jogo:: caractère
juguman:: mal, méchant
a|jugumankɛbaga:: celui qui fait le mal
ka:: aux., être, pour
kalansen:: leçon
kan:: interdiction, ne…pas
kana:: injonctif
kelen:: un, pareil
kɔ:: que
kɔnɔ:: dans
kɔnɔntɔ:: neuf
kɔnɔntɔnan:: neuvième
kɔrɔtɔ:: presser, s’impatienter
kɛ:: faire
laharalon:: jour du jugement
lajɛn:: unir, regrouper
lɔn:: connaitre
majiginin:: courtois, soumis
min:: ce que, qui
mɔgɔ:: personne
na:: dans, pour
na:: futur
na:: va
a|nata:: prétendre
ni:: et, si
nimisa:: regret
nin:: ce, cet, cette
nyumankɛbaga:: celui qui fait le bien
o:: ça
sara:: rémunértion, paye
sɔrɔ:: trouver
teli:: rapide, prompt
tigi:: propriétaire
tɔnɔ:: bénéfice
tɛ:: négation
yan:: ici
ye:: à
ye:: être
ye:: postposition
yɛrɛ:: même
ɲanamaya:: vie
ɲuman:: bonne
|====

