---
title:  SÉANCE 25 COURS DE DIOULA 
author: Youssouf DIARRASSOUBA
date:  2020-09-02
type: doc
---

= SÉANCE 25 COURS DE DIOULA 
:author: Youssouf DIARRASSOUBA
:date: 2020-09-02
:type: post
:experimental:
:sectnums:
:sectnumlevels: 2
:lang: fr
:page-lang: {lang}
include::ROOT:partial$locale/attributes.adoc[]

== LÉÇON 25 : KALANSEN MUGAN NI LOORUNAN

=== RUBRIQUE DIALOGUE

* Baro tan ni naaninan/Thème 14 :  n’a ya denbaya bɛ maki la

Lɔgɔfɛ kɔfɛ, Coulibaly n’a ya denbaya taga la maki la. Coulibaly, Alexia ni Markus bena domuni ni minfen kula nɛnɛn.

(Arrivés au maki : O senin maki la)

* A ni sɔgɔma
* N b’a, a danse. Sigiyɔrɔ be yi
* Hakɛto, kɔngɔ bɛ an na. Domuni bɛ feere yan wa?
* ɔn-hɔn, domuni bɛ yan. Nka a b’a fɛ ka mun domun?
* Malo ni tigadɛgɛnan
* Heeee, n balenmancɛ. Ile fana, malo ni tigadɛgɛnan tɛ sɔrɔ yan dɛh ! Yan ye maki le ye. Nanmanfenw tɛ feere yan
* O tuman na, alugu bɛ domunisugu jumɛn le feere yan ?
* Yan ye maki ye, sisɛsogo jɛninin ni cɛkɛ bɛ sɔrɔ walima jɛgɛ jɛninin
* Ayiwa, an bena o sisɛsogo ni jɛgɛ domun
* Ka nan ni jɛgɛ joli ye?
* Alexia, Markus, alugu bɛ mun fɛ ?
* (Alexia) Ne bena sisɛsogo ta nka Markus b’ɛ jɛgɛ le fɛ
* N tericɛ, nan ni sisɛsogo yiranni ni jɛgɛ kelen ye
* Sisɛ kelen ye kɛmɛ seegi nka jɛgɛ ye kɛmɛ naani
* A ka nyi, i be se ka nan n’o ye
* Cɛkɛ do, a bɛ siri joli fɛ?
* Cɛkɛ ye mun ye?
* A kuman tɛ fɔ, a ye an nɛnɛn ka a filɛ dɔrɔn. A ka nyi ni sogosugu bɛɛ ye
* Siri saba bena an bɔ
* A ya domuni filɛ. Ala ye a suman a kɔnɔ
* Amina

(Après avoir pris le repas : o tlanin domunin na)

* N balenmancɛ, cɛkɛ ka di dɛh. An tun ma degi (deli) ka a domun fɔlɔ. An bena dɔ san ka taga ni a ye Canada
* N’i b’a fɛ ka cɛkɛ san, n bena cɛkɛ feereyɔɔrɔ nyuman dɔ yira i la
* O bena diya n ye kosɔbɛ. Dɔrɔ jumɛn bɛ sɔrɔ yan?
* Ginɛsi, bofor, dorogba…Dɔrɔ sugu caman bɛ yan
* Dorogba wa? Balɔntanna dorogba ya dɔrɔ fana bɛ feere wa ?
* A ya dɔrɔ tɛ dɛh. Kɔdiwarikaw le ka a tɔgɔ di dɔɔrɔ tɔgɔ ma sabu a k’a pibilisite kɛ telebizɔn na
* N ka a famu sisan. Ayiwa dorogba saba di an man
* Dorogba saba bɛ bɛn kɛmɛ naani ma

Après avoir bu: o tlanin dɔrɔmin na

* A bɛɛ kɛla joli ?
* Waga kelen ni kɛmɛ wɔɔrɔ
* Waga fila minan. Wariminsɛn ye i ta ye
* A ni ce. An bena segi yan lon jumɛn?
* N ma la a la ni an bena segi yan. An tagayɔrɔ ka ca. Nka bena i ya telefɔni nimero ta
* I bɛ se ka n wele 08892439 kan. An bɛ ni Ala sɔn na
* An bɛ n balenmancɛ

== LA FORMULE GRAMMATICALE DU JOUR

Des phrases construites avec « bɛ/tɛ » qui expriment un constat, une remarque ou une interdiction.
En espérant que ces quelques exemples permettront de mieux appréhender les éventuelles nuances.

* Ji bɛ feere yan ne
* Jabibi bɛ sɔrɔ Bouake lɔgɔfɛba la
* Nanmanfen tɛ feere maki la
* Dɔrɔ bɛ min lu nin kɔnɔ lon bɛɛ
* Mobili tɛ timin sira nin kan
* Samara tɛ don misiri kɔnɔ abada
* Ba nin jɛgɛ tɛ domun
* Cɛkɛ tɛ sɔrɔ Canada
* Sirikɛrɛti tɛ feere yan
* Nyɛgɛnɛn tɛ kɛ yan dɛh

== DIVERS

Discussions autour des éventuelles préoccupations de l’apprenant à propos des précédents cours ou de situations de communication qu’il souhaite mieux comprendre.
Toutes les questions sont les bienvenues.

