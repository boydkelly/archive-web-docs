---
title: DENKUNLI
author: Youssouf DIARRASSOUBA
date: 2021-10-18
draft: false
categories: []
tags: []
---

= DENKUNLI
:author: Youssouf DIARRASSOUBA
:date: 2021-10-18
:sectnums:
:sectnumlevels: 2
:lang: fr
:page-lang: {lang}
include::ROOT:partial$locale/attributes.adoc[]

== BARO: DENKUNLI

An bɛ baro min bɔ a ye sisan, o ye denkunli ta waleya ye.

Denkunli ye bɛɛ ta ko ye.
Ni den bange la a kun bɛ li, a tɔgɔ bɛ don a toro la.
Kabini sɔgɔma joonan, mɔgɔw bɛ nan ka caya denkunliyɔrɔ la, karamɔgɔw, cɛkɔrɔbaw, somɔgɔw, faw ni baw ni manmamusow ni teriw ni blanw, yɔrɔ bɛ fa dewu.
Cɛw bɛ sigi luda la walima o bɛ taga misiri la.
Musow bɛ taga so kɔnɔ k’a masɔrɔ denɲɛnin bɛ o yɔrɔ la a n’a bamuso.
Jeliw, numuw, wolosow, olugu bɛɛ ye ɲamakalaw ye, denkunli tɛ kɛ o kɔ.
Ala kana jeli to gintan na, Ala kana gintan fɔ jeli kɔ.
N’i ka mɔgɔw wele, n’i ma fɛn d’o ma i na fɛn fɔ o ye.
Denfaw bɛ yamariya di karamɔgɔw ma k’o ye tɔgɔ la den na.

Denkunli minan ye woro, dɛgɛ, bɔmɔn, bisiki, wari bɛ la o kan.
Ni o minanw se la karamɔgɔw kɔrɔ, baara bɛ damina.
Bɛɛ b’i daje sewu kalankɛ wagati la.
Karamɔgɔw bɛ dugawu don, o bɛ kalan kɛ, o bɛ den tɔgɔ fɔ ka bɔ kɛnɛ kan, jeliba b’a lase, bɛɛ b’a mɛn.

O bɛ dugawu don den ye.

"Ala ye den balo, ka a nakan diya, ka a baman balo k’a faman balo, ka sinji diya a da"

Ala ye dugawu mina.

Amiina.

N’o tila la denkunli la, yani mɔgɔw bɛ taga so, musow bɛ nan ni suman tobininw ye, zagamɛn, sɔsɔ ani dɛgɛ.

Denkunlijama bɛ domuni kɛ ka fa.

Bɛɛ bɛ nisɔndiya. O bɛ dugawu don ɲɔgɔn ye ka sɔrɔ ka yɛrɛkɛ.

=== VOCABULAIRE :

.Vocabulaire
[width="100%",cols="3",frame="none",opts="none",stripes="even"]
|====
a|
denkunli:: baptème
an:: nous
bɛ:: aux.
baro:: conversation, dialogue
min:: lequel, laquelle, que
bɔ:: sortir, dire
a:: vous
ye:: postposition
bɔ… ye:: raconter quelque chose à quelqu’un
sisan:: maintenant, tout de suite
o:: ça
ye:: être
a:: il
kun:: tête
li:: raser
tɔgɔ:: nom
don:: mettre
toro:: oreille
la:: dans
kabini:: depuis
sɔgɔma:: matin
joona:: tôt
mɔgɔw:: les gens
na:: venir
ka:: aux.
caya:: être nombreux
denkunliyɔrɔ:: lieu de baptème
karamɔgɔw:: marabouts
cɛkɔrɔbaw:: vieux
somɔgɔw:: parents
faw:: les pères
ni:: et
baw:: les mères
manmamusow:: les grands-mères
manmacɛw:: grands-pères
teriw:: les amis
blanw:: les beaux-parents
yɔrɔ:: endroit
bɛ:: aux.
fa:: remplir, bourrer
dewu:: totalement, complètement
cɛw:: hommes
sigi:: s’asseoir
luda:: devanture de la cour
fɔ:: dérouler, se passer
kɔ:: derrière, après
mɔgɔw:: gens
wele:: appeler
ma:: nég. Passé
fɛn:: chose
di:: donner
fɔ:: dire
O:: leur
a|
ye:: aux.
tɔgɔ:: nom
la:: poser, mettre, porter
den:: enfan
na:: à, dans
minan:: accessoire
ye:: être
woro:: cola
dɛgɛ:: dêguê
bɔmɔn:: bonbon
bisiki:: biscuit
lase:: adresser
mɛn:: entendre
balo:: faire vivre, nourrir
nakan:: destin
diya:: rendre agréable
baman:: avec une mère
faman:: avec un père
sinji:: le sein
da:: bouche
bugɔ:: frapper
dɔgɔ:: petit frère
don:: mettre
ɲɔgɔn:: les uns, les autres
ta:: de
waleya:: acte, fait
bɛɛ:: tous
ko:: affaire
den:: enfant
bange la:: naître+acc
la:: à, dans
walima:: ou bien
O:: ils 
taga:: partir
sigi:: s’asseoir, s’installer
misiri:: mosquée
la:: à, dans
musow:: femmes
so:: maison
kɔnɔ:: dans
k’a masɔrɔ:: parce que
denɲɛnin:: bébé
yɔrɔ:: endroit
bamuso:: mère
jeliw:: griots
numuw:: forgerons
wolosow:: esclave de 2e génération
olu, olugu:: eux
bɛɛ:: tous
ɲamakalaw:: hommes de caste
kɔ:: derrière
kana:: injonctif
a|
to:: rester
gintan:: divertissement
na:: dans
Ala:: Dieu
wari:: argent
la:: mettre, ajouter
o:: ça
kan:: sur
tumana:: moment, période
min:: où
o:: ces
minanw:: affaires, accessoires
sela:: arriver+acc
karamɔgɔw:: marabouts
kɔrɔ:: auprès de
baara:: travail
damina:: commencer
bɛɛ:: tous
bɛ:: aux
i:: toi, forme pronominale
daje:: se taire
sewu:: totalement
kalankɛwagati:: moment de lire le coran
dugawu:: bénédictions
don:: mettre
kalan:: lecture
kɛ:: faire
fɔ:: dire
bɔ:: sortir
kɛnɛ:: place
silamɛ:: musulman
mina:: attraper
amiina:: amen
tumana:: moment
tila la:: finir+acc
yani:: avant que
taga:: partir
na:: venir
suman:: repas
tobininw:: préparés
zagamɛn:: riz gras
tulɔnnin:: gras
sɔsɔ:: haricot
jaman:: foule, gens, monde
domuni:: repas
fa:: être rassasié
nisɔndiya::  être content, de bonnne humeur
kasɔrɔ ka:: avant de, procéder à, et après
yɛrɛkɛ:: s’éparpiller
|====

=== NYININGALIW :

. Denkuliminanw ye jumɛn ye?
. Muna o b’a fɔ ko denkunli tɛ mɔgɔkelenko ye?
. Denkunli tɛ kɛ jɔn kɔ?
. Denkunlibaara bɛ damina wagati jumɛn?
. jɔn bɛ den tɔgɔ lase bɛɛ b’a mɛn?
. Denkunlijaman bɛ mun kɛ kasɔrɔ ka yɛrɛkɛ ?
. Muna bɛɛ b’i daje sewu kalankɛ wagati la?
. I dege (deli) la ka taga denkunlikɛyɔrɔ dɔ la wa?
. Denkunli bɛ kɛ Kanada wa?
. I ka nyamakala dɔ lɔn Buake wa?

.Jaabiliw
[%collapsible]
====

====

