---
title: SÉANCE 42 COURS DE DIOULA 
author: Youssouf DIARRASSOUBA
date: 2020-12-24
type: doc
draft: false
categories: []
tags: []
---

= SÉANCE 42 COURS DE DIOULA 
:author: Youssouf DIARRASSOUBA
:date: 2020-12-24
:type: post
:experimental:
:sectnums:
:sectnumlevels: 2
:lang: fr
:page-lang: {lang}
include::ROOT:partial$locale/attributes.adoc[]

== LÉÇON 42 : KALANSEN BI NAANI NI FILANAN

=== BARO : DONSOYA

Bamori tun ye donso farin ye.
A bɛ taga kongo kɔnɔ ka taga donsoya kɛ ka sogo faga.
Donsoya ka gbɛlɛ.
N’i bɛ siran, i tɛ se ka taga kongo kɔnɔ.
Kongosogow, waraw, jaraw, jinajuguw ani waraninkalanw bɛ wulaba kɔnɔ.
Waraw bɛ mɔgɔ domun kongo kɔnɔ ; sajuguw bɛ yiriw la ani binw na.
Olugu bɛ mɔgɔw kin.
N’i ma kɔgɔ, i tɛ se ka donsoya kɛ.
Donsoncɛ Bamori tun b’a yɛrɛ labɛn k’a yɛrɛ cɛsiri ŋanyi... ka marfa dulon a kaman na, ka sɛmɛn ta, ka jende ta, ka flenin ta, ka wulu wele ka kongosira ta.
A tɛgɛ tun ka di kosɔbɛ.
O le kosɔn, tuman bɛɛ a bɛ nan ka bɔ kongo la ni sogo dɔ ye.
Tere fila o tele fila, a bɛ sogo faga.

Lon dɔ, a bɛ na ni sɔn ye, lon dɔ a bɛ nan ni mangalanin ye.
A tun bɛ dagbɛ fana faga. N’a tun ka dagbɛ faga lon min na, a tun bɛ nan a tɔɲɔgɔnw wele.
O tun bɛ taga ɲɔgɔn fɛ ka taga a ta ka nan n’a ye so kɔnɔ.

Bɛɛ ko Bamori ye donsocɛ ba ye. A tun bɛ se ko caman na.

.Vocabulaire
[width="100%",cols="3",frame="none",options="none",stripes="even"]
|====
a|
a:: il, lui
ani:: et
bamori:: Bamory
binw:: herbes
bɔ:: quitter, sortir
bɛ:: aux. présent
bɛɛ:: tous
caman:: beaucoup
cɛsiri:: attacher la taille/ la ceinture, se
dagbɛ:: hypotrague
di:: donner
domun:: manger, dévorer
donso:: chasseur
donsoya:: chasse
dulon:: pendre
dɔ:: un
faga:: tuer
fana:: aussi
farin:: célèbre
filenin:: petit sifflet
fɛ:: postposition
gbɛlɛ:: difficile
i:: tu
jaraw:: lions
jende:: hache
jinajuguw:: mauvais génies
ka:: aux., pour, être
kaman:: épaule
kin:: mordre
a|
koo:: affaire, chose
ko:: dire
kongo sira:: route pour la brousse
kongo sogow:: animaux sauvages
kongo:: brousse, forêt
kosɔbɛ:: très bien
kosɔn:: à cause de
kɔgɔ:: mûr
kɔnɔ:: dans
kɛ:: faire
la:: dans
labɛn:: apprêter
le:: c’est
lon:: jour
ma:: nég. passé
mangalanin:: biche
marfa:: fusil
min:: où
mɔgɔ:: personne
na:: dans
na:: postposition
nan:: venir
ni:: avec, si
ni…ye:: avec
ŋanyi:: très bien, solidement
ɲɔgɔn:: ensemble
a|
o:: cela
o:: connectif
olugu:: eux
sajuguw:: méchants serpents
se:: pouvoir
siran:: avoir peur
sogo:: viande, animal
sɔ:: cob de buffon : espèce de biche
sɛmɛn:: pioche
ta:: prendre
taga:: partir, aller
tele fla o tele fla:: chaque deux jours
tele fla:: deux jours
tuman:: temps, moment
tun:: marque imparfait
tɔɲɔgɔnw:: camarades
tɛ:: nég. Présent
tɛgɛ:: main
waraninkalanw:: panthères
waraw:: fauve
wele:: appeler
wulaba:: grande forêt
wulu:: chien
ye:: être
ye:: postposition
yiriw:: arbres
yɛrɛ:: même
|====

=== REVISION POINT DE GRAMMAIRE : L’emploi du « Ka »

. Pour relier ou connecter des propositions.

  * Ex : N bɛ taga kongo la
    * N bɛ taga donsoya kɛ.
    * N bɛ taga kongo la, ka donsoya kɛ, ka sogo faga
    * N bɛ sogo faga

. Pour auxilliaire devant l’adjectif ka / man

  * Ex : Donsoya ka gbɛlɛ.
  * …ma nɔgɔ
  * Wara sogo ka di wa ?

. Pour accompli du verbe ka / ma

  * Ex : Donsocɛ ka wara sogo domun
  * Den man ɲinan sogo domun

==== EXERCICE : Produire 5 exemples pour chaque emploi de « ka »

.Réponses de propositions
[%collapsible]
====
. N bɛ taga maki la ka domnuni kɛ.
. N bɛ taga San Pédro ka boli poussieri ɲa.
. A bɛ taga ka nan sisan sisan.
. N bɛ taga kɔgɔjida la ka tulon kɛ
. Mɔgɔ be taga Abidjan ka wari sɔrɔ.
====

.Réponses de auxiliaires devant l'adjectif
[%collapsible]
====
. Wari ka ca ferekelaw bolo.
. Mobili bori man nɔgɔ Abidjan.
. Ali ka jan.
. Nɛgɛso fiman cɛ ka ɲi.
. I ya baara ka fisa wa ?
====

.Réponses accompli du verbe
[%collapsible]
====
. N ma faamu.
. I ka dɔ fɔ wa ?
. An ka julakan kalan kunu.
. N ka sisefan domu bi sɔgɔma.
. Muso nin ka wari domu.
====

==== NYININGALI NINUGU JAABI

. Bamori ye jɔn ye ?
. Donsonya ka nɔgɔ wa?
. Muna sinabagatɔ (mɔgɔjusuntan) tɛ se ka donsoya kɛ?
. Bamori tun bɛ nan a tɔɲɔgɔnw wele wagati jumɛn?
. Mun b’a yira ko donsocɛ Bamori tɛgɛ tun ka di?
. Ile dege la ka sogo faga tu kɔnɔ wa ?
. Donsoya ye suya (subagaya) ye wa ?
. Gundo bɛ donsoya la wa?
. I be siran kogonsogo jumɛn ɲɛ katimi kongoso tɔw kan ?
. Muna kongosow tɔw bɛ siran jaara ɲɛ?
. i be se ka sa mara i ya so kɔnɔ wa?
. Mɔgɔ caman bɛ kongosogo domun farafina yan. Ile b’a domun fana wa?
. Sa kinnɔ bɛ se ka mɔgɔ faga wa?
. Adamadenw ni kongosow bɛ se ka sigi so kelen kɔnɔ wa ?
. Ile hakili la, donsocɛ kelen bɛ se ka jaara faga wa?
. Ile ɲana, muso ka kan ni donsoya ye wa?

.Jaabiliw 
[%collapsible.test]
====
. Bamori ye donso farin ye.
. Donsonya ka gɛlɛ.
. Sinabagatɔ tɛ se ka donsoya kɛ sabu kongosogow be se ka mɔgɔ faga.
. A tun bɛ nan a tɔnɔgɔnw wele n'a tun ka dagbɛ faga.
. Sabu tuma bɛe a be nan ka bɔ kongo la ni sɔgɔ dɔ ye.
. Ɔn-ɔn. n ma dege ka sogo faga. nka dege la kongosogo domu
. A bɛ se, ani a tɛ se....
. Gundo gɛ donsoya la.
. A ka gwɛlɛ. N be siran saa ɲɛ katimi mangalanin kan.
. Jaara be se ka kongosogow bɛɛ faga.
. Ɔn-ɔn.  Mɔgɔ te se ka sa mara a ya so kɔnɔ.
. N dege la ka sa domu. N dege la ka toto domu siyen fɔlɔ.  
. Ɔh-hɔn. Farati b'a la. Sa kinnɔ bɛ se ka mɔgɔ faga.  
. Ɔn-ɔn.  O tɛ se.
. Ni marfa b'a bɔlɔ, a be se k'a faga. A ka ɲɔgɔ.
. N ma faamu...
====
