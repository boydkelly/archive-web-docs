= xref:fr@docs:ROOT:lexique-dyu.adoc#_ádamaden[hadamaden] josiraw dantigɛkan
:date: 2021-09-20
:copyright: The Office of the High Commissioner for Human Rights 1996–2009
:description: Déclaration universelle des droits de l’homme
:author: The Office of the High Commissioner for Human Rights
:keywords: Droits de l'homme, Côte d'Ivoire, Mali, Nations unies
:sectnumlevels: 0
:lang: bam
:page-lang: {lang}
:page-image: udhr-p.webp

{copyright}

[abstract]
== ɲebila

image:udhr.webp[Hadamaden josiraw dantigɛkan,356,480,role="left"]

K’a d’a xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ko dine hɔrɔnɲa ni tilennenɲa ni xref:fr@docs:ROOT:lexique-dyu.adoc#_láfiya[lafiya] sintin ye hadamaden
bɛɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_dànbe[danbe] dɔnni n’u josiraw danmakɛɲneni ye,

K’a d’a xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ko xref:fr@docs:ROOT:lexique-dyu.adoc#_ádamaden[hadamaden] josiraw n’u kɔnni kɛra dannajuguya xref:fr@docs:ROOT:lexique-dyu.adoc#_cáman[caman] sababu
ye minnu bɛ maaya ɲugun, ani ko xref:fr@docs:ROOT:lexique-dyu.adoc#_ádamaden[hadamaden] haju bɛɛ la gɛlɛnman ye de ka
diɲe xref:fr@docs:ROOT:lexique-dyu.adoc#_kúra[kura] dabali min xref:fr@docs:ROOT:lexique-dyu.adoc#_dén[den] si tɛ bali k’a hakilila xref:fr@docs:ROOT:lexique-dyu.adoc#_fó[fɔ] nink’a dannasira
batu, min xref:fr@docs:ROOT:lexique-dyu.adoc#_dén[den] bɛɛ tangalen bɛ bagabagali ni ɲani ma,

K’a d’a xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ko a tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲá[ɲɛ] xref:fr@docs:ROOT:lexique-dyu.adoc#_fò[fo] xref:fr@docs:ROOT:lexique-dyu.adoc#_ádamaden[hadamaden] josiraw ka lakana sariyabatufanga de
fe yaasa xref:fr@docs:ROOT:lexique-dyu.adoc#_múruti[muruti] xref:fr@docs:ROOT:lexique-dyu.adoc#_kána[kana] kɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] jagoya ye tɔɲɔn ni degun kanma,

K’a d’a xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ko xref:fr@docs:ROOT:lexique-dyu.adoc#_kànu[kanu] jiidili ka kan ka kɔkɔrɔdon kabilaw ni xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲɔ́gɔn[ɲɔgɔn] cɛ,

K’a d’a xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ko Diɲe kabilatɔn y’a ɲaniya dantigɛ kokura, kabilaw
Bɛnkansɛbɛn kɔnɔ, ko ale sirilen bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_ádamaden[hadamaden] josira bɛrɛw la, hadamaden
danbe n’a kiimɛ na, xref:fr@docs:ROOT:lexique-dyu.adoc#_cɛ́[cɛ] ni xref:fr@docs:ROOT:lexique-dyu.adoc#_mùso[muso] josiraw kɛɲɛni na, ani ko a b’a cɛsire
maaya ka xref:fr@docs:ROOT:lexique-dyu.adoc#_tága[taa] ɲe, diɲɛnatigɛ ka xref:fr@docs:ROOT:lexique-dyu.adoc#_nɔ̀gɔya[nɔgɔya] hɔrɔnɲa jiidilen kɔnɔ,

K’a d’a xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ko tɔndenjamana bɛɛ y’a kandi ko a bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_fára[fara] Kabilatɔn kan ka
hadamaden josiraw n’a ka hɔrɔnɲa xref:fr@docs:ROOT:lexique-dyu.adoc#_bɛ̀rɛ[bɛrɛ] lakana ani k’u waleya diɲe bɛɛ
kɔnɔ,

K’a d’a xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ko fɔlen tɛ se ka tiimɛ a xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲá[ɲɛ] xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] ni bɛn ma kɛ josiraw ni
hɔrɔnɲa faamucogo kan,

Tɔn ɲɛjama y’a kanbɔ

Hadamaden josiraw dantigɛkan in ka kɛ diɲɛ fasojama n’a kabila bɛɛ
kuntilenna ye, yaasa a ka basigi xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] kelen-kelen bɛɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_hákiri[hakili] la, ani
maaya labɛnbolo bɛɛ kɔnɔ, xref:fr@docs:ROOT:lexique-dyu.adoc#_ù[u] k’u xref:fr@docs:ROOT:lexique-dyu.adoc#_cɛsiri[cɛsiri] xref:fr@docs:ROOT:lexique-dyu.adoc#_kàlan[kalan] ni ladamuni xref:fr@docs:ROOT:lexique-dyu.adoc#_fɛ̀[fɛ] ka josira ni
hɔrɔɲasira ninnu batuli jiidi, ka xref:fr@docs:ROOT:lexique-dyu.adoc#_fɛ́ɛrɛ[fɛɛrɛ] xref:fr@docs:ROOT:lexique-dyu.adoc#_tìgɛ[tigɛ] xref:fr@docs:ROOT:lexique-dyu.adoc#_fàso[faso] kɔnɔ n’a xref:fr@docs:ROOT:lexique-dyu.adoc#_kɔ̀n[kɔn] xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ka
sira ninnu jateli n’u waleyali nɛmɛnɛmɛ tiimɛ diɲɛ bɛɛ, tɔndenjamanaw
yɛrɛ kɔnɔmɔgɔw fɛ, ani xref:fr@docs:ROOT:lexique-dyu.adoc#_ù[u] ka marajamanw kɔnɔmɔgɔw fɛ.

== Sariyasen xref:fr@docs:ROOT:lexique-dyu.adoc#_fɔ́lɔ[fɔlɔ] :

Hadamaden bɛɛ danmakɛɲɛnen bɛ bange, xref:fr@docs:ROOT:lexique-dyu.adoc#_dànbe[danbe] ni josira la. xref:fr@docs:ROOT:lexique-dyu.adoc#_hákiri[hakili] ni taasi
b’u bɛɛ la, xref:fr@docs:ROOT:lexique-dyu.adoc#_wà[wa] xref:fr@docs:ROOT:lexique-dyu.adoc#_ù[u] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ka badenɲasira de waleya u ni xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲɔ́gɔn[ɲɔgɔn] cɛ.

== Sariyasen 2n :

Bɛɛ ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] josira ni hɔrɔnɲasira dantigɛlen ninnu bɛɛ la, wolomali fan
si t’a la, xref:fr@docs:ROOT:lexique-dyu.adoc#_jàngo[janko] siyawoloma, xref:fr@docs:ROOT:lexique-dyu.adoc#_fàri[fari] jɛya n’a finɲa, xref:fr@docs:ROOT:lexique-dyu.adoc#_cɛya[cɛya] ni musoya, kan,
diinɛ, politikisira ni miirisira xref:fr@docs:ROOT:lexique-dyu.adoc#_ján[jan] o jan, fasowoloma ni sɛrɛwoloma,
sɔrɔ ni bonda ni jɔyɔrɔwoloma xref:fr@docs:ROOT:lexique-dyu.adoc#_fán[fan] o fan.

Wa xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_mìna[minɛ] i xref:fr@docs:ROOT:lexique-dyu.adoc#_bɔ́yɔrɔ[bɔyɔrɔ] ma, o dabolo xref:fr@docs:ROOT:lexique-dyu.adoc#_mána[mana] kɛ fɛn o fɛn ye politiki ni
sariya la ani diɲɛ kɔnɔ: o ka kɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_jàmana[jamana] wali xref:fr@docs:ROOT:lexique-dyu.adoc#_dugukolo[dugukolo] yɛrɛmahɔrɔn ye,
wali kalifafen, wali maralen, wali dankari bɛ min ka setigiya la,

== Sariyasen 3n :

Hadamaden bɛɛ ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ni xref:fr@docs:ROOT:lexique-dyu.adoc#_kísi[kisi] ni xref:fr@docs:ROOT:lexique-dyu.adoc#_tànga[tanga] ni hɔrɔɲa ye.

== Sariyasen 4n :

Mɔgɔ si tɛ kɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_jɔ́n[jɔn] ye, wali bolokɔnɔmɔgɔ. Jɔnɲa ni jɔnjago xref:fr@docs:ROOT:lexique-dyu.adoc#_ján[jan] o jan
kɔnnen don.

== Sariyasen 5n :

Mɔgɔ si ni xref:fr@docs:ROOT:lexique-dyu.adoc#_mán[man] xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ka ɲangata ; dannajuguya ɲangini wali danbelakari
ɲagagini xref:fr@docs:ROOT:lexique-dyu.adoc#_mán[man] xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_dá[da] i kan.

== Sariyasen 6n :

Mɔgɔ bɛɛ ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ni jateli ye xref:fr@docs:ROOT:lexique-dyu.adoc#_sàriya[sariya] xref:fr@docs:ROOT:lexique-dyu.adoc#_fɛ̀[fɛ] xref:fr@docs:ROOT:lexique-dyu.adoc#_yɔ́rɔ[yɔrɔ] bɛɛ.

== Sariyasen 7n :

Bɛɛ ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] xref:fr@docs:ROOT:lexique-dyu.adoc#_sàriya[sariya] la, bɛɛ ka kan ni lakanani ye sariya xref:fr@docs:ROOT:lexique-dyu.adoc#_fɛ̀[fɛ] Bɛɛ ka kan ka
kisi fisamanciyawalew xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] minnu bɛ dankari kɛ Dantigɛkan in na, ani
mɔgɔlaɲiniwale fɛn o fɛn bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_ná[na] n’o xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲɔ́gɔn[ɲɔgɔn] fisamanciya ye.

== Sariyasen 8n :

Bɛɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_jó[jo] xref:fr@docs:ROOT:lexique-dyu.adoc#_dòn[don] k’i xref:fr@docs:ROOT:lexique-dyu.adoc#_síri[siri] xref:fr@docs:ROOT:lexique-dyu.adoc#_jàmana[jamana] sariyabolo ɲumanw xref:fr@docs:ROOT:lexique-dyu.adoc#_ná[na] k’i josira bɛrɛw
dankariwlew kɛlɛ, i josira minnu sɛmɛntiyalen bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_jàmana[jamana] Sariyabaju fɛ.

== Sariyasen 9n :

Bin xref:fr@docs:ROOT:lexique-dyu.adoc#_mán[man] xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ka kɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] si kan k’i minɛ, k’i datugu, wali k’i gɛn dugu
la.

== Sariyasen 10n :

Bɛɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_jó[jo] bɛ k’i fɛla xref:fr@docs:ROOT:lexique-dyu.adoc#_dá[da] xref:fr@docs:ROOT:lexique-dyu.adoc#_kɛ́nɛ[kɛnɛ] xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] kiirida tilennen bagabagabali kɔrɔ, a ka
jo ni jalaki bɔ xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲɔ́gɔn[ɲɔgɔn] xref:fr@docs:ROOT:lexique-dyu.adoc#_ná[na] n’a xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] kɛ ni mɔgɔlawoloma ye.

== Sariyasen 11n :

. Ko xref:fr@docs:ROOT:lexique-dyu.adoc#_mána[mana] xref:fr@docs:ROOT:lexique-dyu.adoc#_dá[da] xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] la, i tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_mìna[minɛ] sɔntigi ye xref:fr@docs:ROOT:lexique-dyu.adoc#_fò[fo] kiiri k’i jalaki
bange xref:fr@docs:ROOT:lexique-dyu.adoc#_kɛ́nɛ[kɛnɛ] xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] i lapasalisira bɛɛ tiimɛnen kɔ.

. xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] te ɲangi k’a xref:fr@docs:ROOT:lexique-dyu.adoc#_dá[da] xref:fr@docs:ROOT:lexique-dyu.adoc#_kɛwale[kɛwale] wali xref:fr@docs:ROOT:lexique-dyu.adoc#_fìri[fili] xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] kɛwaati y’a sɔrɔ si
tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_jàmana[jamana] kɔnɔ wali jamanaw ni xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲɔ́gɔn[ɲɔgɔn] xref:fr@docs:ROOT:lexique-dyu.adoc#_cɛ́[cɛ] min b’o xref:fr@docs:ROOT:lexique-dyu.adoc#_jàte[jate] ko xref:fr@docs:ROOT:lexique-dyu.adoc#_júgu[jugu] ye. Wa
ɲangili xref:fr@docs:ROOT:lexique-dyu.adoc#_fána[fana] te xref:fr@docs:ROOT:lexique-dyu.adoc#_dá[da] i xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ka tɛmɛn min tun bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_sèn[sen] xref:fr@docs:ROOT:lexique-dyu.adoc#_ná[na] kan ko xref:fr@docs:ROOT:lexique-dyu.adoc#_júgu[jugu] kɛtuma
na.

== Sariyasen 12n :

Bin tɛ se ka kɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_dòn[don] i xref:fr@docs:ROOT:lexique-dyu.adoc#_gundo[gundo] la, i ka dukɔnɔkow la, i siso
kɔnɔ an’i ka gundosɛbɛnw na, wali ka dankari kɛ i xref:fr@docs:ROOT:lexique-dyu.adoc#_tɔ́gɔ[tɔgɔ] ni xref:fr@docs:ROOT:lexique-dyu.adoc#_dànbe[danbe] la.
Sariya ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ka bɛɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_tànga[tanga] o binkanni ni dankari xref:fr@docs:ROOT:lexique-dyu.adoc#_sugu[sugu] ma.

== Sariyasen 13n :

. Bɛɛ ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ni taa-ninka-segin ye i xref:fr@docs:ROOT:lexique-dyu.adoc#_fɛ́rɛ[fɛrɛ] ma, ani i sigiyɔrɔ
sugandili xref:fr@docs:ROOT:lexique-dyu.adoc#_jàmana[jamana] kɔnɔ.

. xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] tɛ bali ka bɔ i ka xref:fr@docs:ROOT:lexique-dyu.adoc#_jàmana[jamana] wali jamana xref:fr@docs:ROOT:lexique-dyu.adoc#_wɛ̀rɛ[wɛrɛ] kono, ka tila
ka segin i ka xref:fr@docs:ROOT:lexique-dyu.adoc#_jàmana[jamana] kɔnɔ.

== Sariyasen 14n :

. Bɛɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_jó[jo] xref:fr@docs:ROOT:lexique-dyu.adoc#_dòn[don] ka bɔ xref:fr@docs:ROOT:lexique-dyu.adoc#_máfiyɛɲa[mafiɲɛya] xref:fr@docs:ROOT:lexique-dyu.adoc#_kɔ́rɔ[kɔrɔ] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_tága[taga] i xref:fr@docs:ROOT:lexique-dyu.adoc#_kùn[kun] kalifa xref:fr@docs:ROOT:lexique-dyu.adoc#_yɔ́rɔ[yɔrɔ] wɛrɛ,
wa o xref:fr@docs:ROOT:lexique-dyu.adoc#_jàmana[jamana] xref:fr@docs:ROOT:lexique-dyu.adoc#_wɛ̀rɛ[wɛrɛ] xref:fr@docs:ROOT:lexique-dyu.adoc#_mán[man] xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] k’i xref:fr@docs:ROOT:lexique-dyu.adoc#_jɛ̀[jɛ] kalifayɔrɔ la.

. I xref:fr@docs:ROOT:lexique-dyu.adoc#_mán[man] xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] n’o josira ye n’i nɔminɛkun ye ko xref:fr@docs:ROOT:lexique-dyu.adoc#_júgu[jugu] kɛli de ye
jaati forobasariya la xref:fr@docs:ROOT:lexique-dyu.adoc#_wála[walima] xref:fr@docs:ROOT:lexique-dyu.adoc#_kɛwale[kɛwale] minnu bɛ Kabilatɔn kuntilenna n’a
daliluw sɔsɔ.

== Sariyasen 15n :

. xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] bɛɛ ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ni kabilaya ye.

. xref:fr@docs:ROOT:lexique-dyu.adoc#_bin[bin] tɛ kɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] si xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] k’i xref:fr@docs:ROOT:lexique-dyu.adoc#_jɛ̀[jɛ] kabilaya la, wali k’i bali ka
kabilaya falen.

== Sariyasen 16n :

. xref:fr@docs:ROOT:lexique-dyu.adoc#_cɛ́[cɛ] ni xref:fr@docs:ROOT:lexique-dyu.adoc#_mùso[muso] si xref:fr@docs:ROOT:lexique-dyu.adoc#_mána[mana] se xref:fr@docs:ROOT:lexique-dyu.adoc#_fúru[furu] ye, siyako t’o la, kabilako t’o la,
diinɛko t’o la, xref:fr@docs:ROOT:lexique-dyu.adoc#_ù[u] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ni xref:fr@docs:ROOT:lexique-dyu.adoc#_fúru[furu] ni xref:fr@docs:ROOT:lexique-dyu.adoc#_denbaya[denbaya] ye. U danma ka kan josira la
furu dontuma n’a kuntagala kɔnɔ ani a sali xref:fr@docs:ROOT:lexique-dyu.adoc#_kɛ́nɛ[kɛnɛ] kan.

. xref:fr@docs:ROOT:lexique-dyu.adoc#_fúru[furu] tɛ se ka xref:fr@docs:ROOT:lexique-dyu.adoc#_síri[siri] ni xref:fr@docs:ROOT:lexique-dyu.adoc#_cɛ́[cɛ] ni xref:fr@docs:ROOT:lexique-dyu.adoc#_mùso[muso] xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] diɲɛ n’a ye faasi.

. [missing]

== Sariyasen 17n :

. I xref:fr@docs:ROOT:lexique-dyu.adoc#_kélen[kelen] xref:fr@docs:ROOT:lexique-dyu.adoc#_ná[na] o, xref:fr@docs:ROOT:lexique-dyu.adoc#_jɛ̀[jɛ] kɔnɔ o, xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] bɛɛ ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ni tɔgɔlafɛntigiya
ye.

. xref:fr@docs:ROOT:lexique-dyu.adoc#_bin[bin] tɛ se ka kɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] k’i tɔgɔlafɛn xref:fr@docs:ROOT:lexique-dyu.adoc#_mìna[minɛ] i la.

== Sariyasen 18n :

Bɛɛ b’i xref:fr@docs:ROOT:lexique-dyu.adoc#_yɛ̀rɛ[yɛrɛ] ye miirisira ni taasi ni diinɛko la; o xref:fr@docs:ROOT:lexique-dyu.adoc#_jó[jo] b’i yamaruya ka
diinɛ n’i dannako falen, ani k’u waleya i xref:fr@docs:ROOT:lexique-dyu.adoc#_kélen[kelen] xref:fr@docs:ROOT:lexique-dyu.adoc#_ná[na] wali i n’i jenɔgɔnw,
kɛnɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] wali dunduguma na, xref:fr@docs:ROOT:lexique-dyu.adoc#_kàlan[kalan] ni matarafali ni batu xref:fr@docs:ROOT:lexique-dyu.adoc#_sèn[sen] fɛ.

== Sariyasen 19n :

Bɛɛ sago bɛ i xref:fr@docs:ROOT:lexique-dyu.adoc#_miiriya[miiriya] la, ani i kakilila jirali; o la i tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_bagabaga[bagabaga] k’a
da i xref:fr@docs:ROOT:lexique-dyu.adoc#_miiriya[miiriya] kan, xref:fr@docs:ROOT:lexique-dyu.adoc#_wà[wa] i xref:fr@docs:ROOT:lexique-dyu.adoc#_jó[jo] bɛ ka miiriya ni kibaruyaw xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲíni[ɲini] ani k’u gansi
fo xref:fr@docs:ROOT:lexique-dyu.adoc#_jàmana[jamana] xref:fr@docs:ROOT:lexique-dyu.adoc#_fò[fo] jamana, jɛnsɛnnifɛɛrɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_sugu[sugu] bɛɛ la.

== Sariyasen 20n :

. Bɛɛ sago bɛ ka xref:fr@docs:ROOT:lexique-dyu.adoc#_jɛ̀[jɛ] dilan, ka ɲɔgɔɲew kɛ.

. xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] tɛ jagoya ka xref:fr@docs:ROOT:lexique-dyu.adoc#_dòn[don] xref:fr@docs:ROOT:lexique-dyu.adoc#_jɛ̀[jɛ] la.

== Sariyasen 21n :

. Bɛɛ ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] in jɔyɔrɔ ye i xref:fr@docs:ROOT:lexique-dyu.adoc#_fàso[faso] koɲɛw ɲɛkun na, i yɛrɛkun wali
i dungɔlamɔgɔ sugandilen fɛ.

. Bɛɛ ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ni jɔyɔrɔ ye, ɲɛnawoloma t’a la, i ka jamana
forobabaaraw la.

. xref:fr@docs:ROOT:lexique-dyu.adoc#_jàmana[jamana] xref:fr@docs:ROOT:lexique-dyu.adoc#_dùngɔ[dungɔ] de ye forobafanga daga; a dungɔ ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ka jira
kalada jɛlenw xref:fr@docs:ROOT:lexique-dyu.adoc#_sèn[sen] fɛ, bɛɛ ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] forobakalada minnu na, xref:fr@docs:ROOT:lexique-dyu.adoc#_gundo[gundo] la wali o
ɲɔgɔnna siraw la minnu bɛ sagolasugandili tiimɛ.

== Sariyasen 22n :

Bɛɛ ye maaya xref:fr@docs:ROOT:lexique-dyu.adoc#_dén[den] ye minkɛ, bɛɛ ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ni ɲɛsuma ye maaya kɔnɔ ; jamana
ka magan ni jamanaw xref:fr@docs:ROOT:lexique-dyu.adoc#_cɛ́[cɛ] ɲɔgɔndɛmɛn ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ka kɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_sábabu[sababu] ye, xref:fr@docs:ROOT:lexique-dyu.adoc#_jàmana[jamana] sariya
n’a xref:fr@docs:ROOT:lexique-dyu.adoc#_séko[seko] hukumu kɔnɔ, ka bɛɛ josira tiimɛ sɔrɔ ni diɲɛnatigɛ ni faamuya
siratigɛ la, xref:fr@docs:ROOT:lexique-dyu.adoc#_dànbe[danbe] ni xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] bɛrɛya yiriwali tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_tága[taa] o minnu kɔ.

== Sariyasen 23n :

. xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] bɛɛ ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ni xref:fr@docs:ROOT:lexique-dyu.adoc#_báara[baara] ye, baaracogo xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲúman[ɲuman] in baara nafaman;
bɛɛ ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_tànga[tanga] baarantaɲa ma.

. Baaraw xref:fr@docs:ROOT:lexique-dyu.adoc#_mána[mana] kɛɲɛ, saraw ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kɛ́ɲɛ[kɛɲɛ] ; xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] bɔ mɔgɔ la,
bɛɛ ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] o la.

. xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] o mɔgɔ n’i bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_báara[baara] la, i ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ni xref:fr@docs:ROOT:lexique-dyu.adoc#_sára[sara] xref:fr@docs:ROOT:lexique-dyu.adoc#_sɛ́bɛ[sɛbɛ] ye min bɛ i
n’i ka xref:fr@docs:ROOT:lexique-dyu.adoc#_denbaya[denbaya] xref:fr@docs:ROOT:lexique-dyu.adoc#_sùtura[sutura] hadamadeɲa na, ani maaya lakanasira xref:fr@docs:ROOT:lexique-dyu.adoc#_tɔ̀[tɔ] fɛn o fɛn
n’a xref:fr@docs:ROOT:lexique-dyu.adoc#_kùn[kun] bɛ.

. Bɛɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_jó[jo] bɛ ka xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] wɛrɛw xref:fr@docs:ROOT:lexique-dyu.adoc#_fára[fara] i xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ka kaaratɔnw dilan, wali ka
don baaratɔnw na, yaasa k’i nafasiraw makara.

== Sariyasen 24n:

Bɛɛ ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] i lafiɲɛ ni ɲɛnajɛ ye, ani baarawaati dannen xref:fr@docs:ROOT:lexique-dyu.adoc#_hàkɛ[hakɛ] la, ani
waati ni xref:fr@docs:ROOT:lexique-dyu.adoc#_wagáti[waati] sɛgɛnnabɔ min tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_sára[sara] la.

== Sariyasen 25n :

. xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] bɛɛ ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ni sɔrɔ ye diɲɛnatigɛ la min bɛ kɛnɛya ni
hɛɛrɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_sábati[sabati] i n’i ka xref:fr@docs:ROOT:lexique-dyu.adoc#_denbaya[denbaya] ye, i ko balo, fɛɛrɛbɔ, siso, furakɛli,
ani maaya dɛmɛnsira gɛlɛnw; i ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ni dɛmɛn ye baarantaɲa ni xref:fr@docs:ROOT:lexique-dyu.adoc#_bàna[bana] ni
dɛsɛ ni firiya ni xref:fr@docs:ROOT:lexique-dyu.adoc#_kɔ́rɔ[kɔrɔ] kɔnɔ, o n’a ɲɔgɔnna baanmako xref:fr@docs:ROOT:lexique-dyu.adoc#_mána[mana] kɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_sábabu[sababu] ye
k’i ka diɲɛnatigɛ sɔrɔsiraw tiɲe.

. xref:fr@docs:ROOT:lexique-dyu.adoc#_bànge[bange] ni deɲa ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ni dɛmɛn ni ladonni kɛrɛnkɛrɛnnen ye. Den
o den, a kɛra xref:fr@docs:ROOT:lexique-dyu.adoc#_fúru[furu] xref:fr@docs:ROOT:lexique-dyu.adoc#_dén[den] ye o, a xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] kɛ furu den ye o, bɛɛ ka kan
lakanacogo xref:fr@docs:ROOT:lexique-dyu.adoc#_kélen[kelen] xref:fr@docs:ROOT:lexique-dyu.adoc#_ná[na] maaya kɔnɔ.

== Sariyasen 26n :

. xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] bɛɛ ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ni xref:fr@docs:ROOT:lexique-dyu.adoc#_kàlan[kalan] ye. xref:fr@docs:ROOT:lexique-dyu.adoc#_sára[sara] xref:fr@docs:ROOT:lexique-dyu.adoc#_mán[man] kan ka bɔ kalan na,
kalanbaju xref:fr@docs:ROOT:lexique-dyu.adoc#_kɔ̀ni[kɔni] xref:fr@docs:ROOT:lexique-dyu.adoc#_ná[na] fiyew. Kalanbaju ye bɛɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] xref:fr@docs:ROOT:lexique-dyu.adoc#_wájibi[wajibi] ye. Fɛɛrɛkalan ni
baarakalan ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ka forobaya; kalandakunba ka kan ka labila bɛɛ ye,
fisamanciya t’a la, xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] o mɔgɔ n’i y’a sarati dafa.

. Ladamuni ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ka ɲesin xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] bɛrɛya yiriwali ma, hadamaden
josiraw matarafali ni hɔrɔɲa bajuw sinsinni . A ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ka faamuya ni
sabali ni xref:fr@docs:ROOT:lexique-dyu.adoc#_teriya[teriya] ɲɔgɔya kabilaw ni sibolow ni diinɛkulu bɛɛ ni xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲɔ́gɔn[ɲɔgɔn] cɛ,
ani ka Diɲɛ kabilatɔn ka xref:fr@docs:ROOT:lexique-dyu.adoc#_láfiya[lafiya] basigiwalew yiriwa.

. Bangebagaw xref:fr@docs:ROOT:lexique-dyu.adoc#_fɔ́lɔ[fɔlɔ] de sago bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_ù[u] denw ladamucogo sugandili la.

== Sariyasen 27n :

. A bɛ bɛɛ sago la k’i nindon xref:fr@docs:ROOT:lexique-dyu.adoc#_jàma[jama] ka faamuyasiraw la, ka bɔ
masiriw nunma, k’i nindon dɔnniya yiriwawalew la ani ka bɔ a hɛɛrɛ
nunma.

. Bɛɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_tɔ́gɔ[tɔgɔ] n’i ka xref:fr@docs:ROOT:lexique-dyu.adoc#_nàfa[nafa] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ka lakana i ka dɔnko ni sɛbɛnko ni
masiriko xref:fr@docs:ROOT:lexique-dyu.adoc#_báara[baara] kɛlenw na.

== Sariyasen 28n :

Bɛɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_jó[jo] bɛ k’a xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲíni[ɲini] xref:fr@docs:ROOT:lexique-dyu.adoc#_lábɛn[labɛn] ka kɛ maaya la ani jamanaw xref:fr@docs:ROOT:lexique-dyu.adoc#_cɛ́[cɛ] la, min b’a to
josira ni hɔrɔnɲa baju minnu dantigɛlen xref:fr@docs:ROOT:lexique-dyu.adoc#_filɛ́[filɛ] Dantigɛkan in kɔnɔ, olu ka
sira sɔrɔ a xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲá[ɲɛ] ma.

== Sariyasen 29n :

. I ka hadamadenɲa yiriwali sirilen be maaya jɛkulu min na, o ka
ci b’i kan.

. xref:fr@docs:ROOT:lexique-dyu.adoc#_dán[dan] si tɛ josira xref:fr@docs:ROOT:lexique-dyu.adoc#_tágama[taama] ni hɔrɔnɲa tɔnɔbɔ la, xref:fr@docs:ROOT:lexique-dyu.adoc#_fò[fo] xref:fr@docs:ROOT:lexique-dyu.adoc#_sàriya[sariya] ka dan
sigilen minnu xref:fr@docs:ROOT:lexique-dyu.adoc#_kùn[kun] ye ka tɔw josiraw ni hɔrɔɲasiraw gasi sigi, ani
tilennenɲa ni basigi ni xref:fr@docs:ROOT:lexique-dyu.adoc#_jàma[jama] ka hɛɛrɛ tɛe xref:fr@docs:ROOT:lexique-dyu.adoc#_tága[taga] xref:fr@docs:ROOT:lexique-dyu.adoc#_wájibi[wajibi] minnu kɔ
sariyafanga kɔnɔ ka olu tiimɛ.

. O jow ni hɔrɔnɲasira ninnu tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_bòri[boli] xref:fr@docs:ROOT:lexique-dyu.adoc#_cógo[cogo] si la min be Kabilatɔn
kuntilennaw n’a daliluw kota.

== Sariyasen 30n :

Jɔyɔrɔ fɛn o fɛn dantigɛlen ye xref:fr@docs:ROOT:lexique-dyu.adoc#_nín[nin] Dantigɛkan in kɔnɔ, xref:fr@docs:ROOT:lexique-dyu.adoc#_fànga[fanga] wali jɛkulu
wali xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] xref:fr@docs:ROOT:lexique-dyu.adoc#_kélen[kelen] sijo tɛ k’o xref:fr@docs:ROOT:lexique-dyu.adoc#_mìna[minɛ] yamaruya ye ka xref:fr@docs:ROOT:lexique-dyu.adoc#_síra[sira] taama, wali ka
walekɛ, min xref:fr@docs:ROOT:lexique-dyu.adoc#_kùn[kun] ye xref:fr@docs:ROOT:lexique-dyu.adoc#_ádamaden[hadamaden] josira ni hɔrɔnɲasira jiralen ninnu tiɲɛni
ye.

