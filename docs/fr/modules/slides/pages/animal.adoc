---
author: 'Boyd Kelly'
date: 2021-12-14
layout: slides
title: Animaux
---

= Animaux en Jula de Côte d'Ivoire
:description: Apprenez les noms des animaux en Jula de C¸ôte d'ivoire
:page-author: 'Boyd Kelly'
:page-revdate: 2021-12-14
:page-layout: slides
:page-lang: fr 
:page-pagination: false
:page-tags: jula, afrique, dyu, leçons, présentation, animaux
:keywords: jula, afrique, dyu, leçons, présentation, animaux
:category: slides

++++
<iframe src="https://slides-dyu.coastsystems.net/animal.dyu.html" width="100%" frameborder="0" allowfullscreen="allowfullscreen" allow="geolocation *; microphone *; camera *; midi *; encrypted-media *"></iframe>
++++
