---
author: 'Boyd Kelly'
date: 2021-11-01
layout: slides
title: Proverbes
---

= Proverbe en Jula de Côte d'Ivoire
:description: Proverbe quotidien en Jula de Côte d'Ivoire
:page-author: 'Boyd Kelly'
:page-revdate: 2021-11-01 
:page-layout: slides
:page-lang: fr 
:page-pagination: false
:page-tags: jula, afrique ,dyu, leçons, présentation, proverbe
:keywords: jula, afrique, dyu, leçons, présentation, proverbe 
:category: slides

++++
<iframe src="https://slides-dyu.coastsystems.net/proverbs.html" width="100%" frameborder="0" allowfullscreen="allowfullscreen" allow="geolocation *; microphone *; camera *; midi *; encrypted-media *"></iframe>
++++
