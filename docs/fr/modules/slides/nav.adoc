---
draft: 'true'
---

* Quiz
** xref:docs:slides:animal.adoc[Animaux]
** xref:docs:slides:5words.adoc[5 phrases]
** xref:docs:slides:proverbs.adoc[Proverbe]
** xref:docs:slides:colors.adoc[Couleurs]
** xref:docs:slides:money.adoc[Argent]
** xref:docs:slides:slides.adoc[Slides]
