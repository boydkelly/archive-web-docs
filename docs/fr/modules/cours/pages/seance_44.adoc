---
title: SÉANCE 44 COURS DE DIOULA
author: Youssouf DIARRASSOUBA
date: 2021-01-02
type: doc
draft: false
categories: []
tags: []
---

= SÉANCE 44 COURS DE DIOULA
:author: Youssouf DIARRASSOUBA
:date: 2021-01-02
:type: post
:toc: true
:experimental:
:sectnums:
:sectnumlevels: 2
:lang: fr
:page-lang: {lang}
include::ROOT:partial$locale/attributes.adoc[]

== LÉÇON 44 : xref:fr@docs:ROOT:lexique-dyu.adoc#_kàlansen[kalansen] xref:fr@docs:ROOT:lexique-dyu.adoc#_bí[bi] xref:fr@docs:ROOT:lexique-dyu.adoc#_náani[naani] NI SABA

=== xref:fr@docs:ROOT:lexique-dyu.adoc#_bàro[baro] : GWƐLƐA

Gbɛlɛya min tun xref:fr@docs:ROOT:lexique-dyu.adoc#_kófɔ[kofɔ] la o sera bɛɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲa[ɲa] na.
Nin tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_yɔ́rɔ[yɔrɔ] xref:fr@docs:ROOT:lexique-dyu.adoc#_kélen[kelen] xref:fr@docs:ROOT:lexique-dyu.adoc#_koo[koo] ye, yɔrɔ bɛɛ lo.
bɛɛ bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_kúle[kule] la, k’a xref:fr@docs:ROOT:lexique-dyu.adoc#_màsɔrɔ[masɔrɔ] bɛɛ sɛgɛn na, bɛɛ ko wari.
Wari tɛ sɔrɔ la ka xref:fr@docs:ROOT:lexique-dyu.adoc#_caya[caya] ka makow ɲɛ.
(pour satisfaire les besoins)
Wari banna, gbɛlɛya bɛ bɛɛ kan.
Kunkow ka ca, bɛɛ n’a ya kunko.
Baarakɛbaliw ka ca.
(les sans emplois)
Sabu xref:fr@docs:ROOT:lexique-dyu.adoc#_báara[baara] tɛ sɔrɔla.
N’i xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] xref:fr@docs:ROOT:lexique-dyu.adoc#_báara[baara] kɛ i tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_wári[wari] sɔrɔ i tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_fèn[fen] sɔrɔ.
Gbɛlɛya donna yɔrɔw la.
fɛnw xref:fr@docs:ROOT:lexique-dyu.adoc#_sɔ̀ngɔ[sɔngɔ] ka gbɛlɛ, xref:fr@docs:ROOT:lexique-dyu.adoc#_lɔgɔfiyɛ[lɔgɔfiyɛ] ka gbɛlɛ, can lo, nka xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] dɔw bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_yèn[yen] xref:fr@docs:ROOT:lexique-dyu.adoc#_fòyi[foyi] xref:fr@docs:ROOT:lexique-dyu.adoc#_mán[man] xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] olugu ye gbɛlɛya kɔ.
(pour certaines personnes n'aime pas autre chose que les difficultés)
O bɛ dɔ xref:fr@docs:ROOT:lexique-dyu.adoc#_fára[fara] fɛnw xref:fr@docs:ROOT:lexique-dyu.adoc#_sɔ̀ngɔ[sɔngɔ] kan.
N’i k’o ɲininga, o b’a xref:fr@docs:ROOT:lexique-dyu.adoc#_fó[fɔ] ko olugu nɔ tɛ.
(si xref:fr@docs:ROOT:lexique-dyu.adoc#_tú[tu] leur demande ils disent ce n'est pas leur faute)
O b’a xref:fr@docs:ROOT:lexique-dyu.adoc#_fó[fɔ] ko minanw xref:fr@docs:ROOT:lexique-dyu.adoc#_sɔ̀ngɔ[sɔngɔ] xref:fr@docs:ROOT:lexique-dyu.adoc#_yɛ̀lɛ[yɛlɛn] na.
jigi=décendre yɛlɛn=augmenter
O b’a xref:fr@docs:ROOT:lexique-dyu.adoc#_fó[fɔ] ko dɔ xref:fr@docs:ROOT:lexique-dyu.adoc#_fára[fara] la mɔbilisara kan.
fara=ajouter
(fɛnw xref:fr@docs:ROOT:lexique-dyu.adoc#_sɔ̀ngɔ[sɔngɔ] yɛlɛna; dɔ xref:fr@docs:ROOT:lexique-dyu.adoc#_fára[fara] la fɛn sɔngɔ kan)
An bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_wele[wele] xref:fr@docs:ROOT:lexique-dyu.adoc#_blà[bla] ɲɛmɔgɔw xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] o ye xref:fr@docs:ROOT:lexique-dyu.adoc#_janto[janto] xref:fr@docs:ROOT:lexique-dyu.adoc#_jàmana[jamana] na.
(lance un appel)
O ye xref:fr@docs:ROOT:lexique-dyu.adoc#_hínɛ[hinɛ] tɔw la: fangantanw, banabagatɔw ani sentanw. (pas de pieds)
O ye xref:fr@docs:ROOT:lexique-dyu.adoc#_sábari[sabali] ka baarada xref:fr@docs:ROOT:lexique-dyu.adoc#_cáman[caman] kɛ fasodenw ye xref:fr@docs:ROOT:lexique-dyu.adoc#_jàngo[jango] baarakɛbaliw ye dɔgɔya.
O ye xref:fr@docs:ROOT:lexique-dyu.adoc#_jìja[jija] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_fàso[faso] xref:fr@docs:ROOT:lexique-dyu.adoc#_wári[wari] lakanda, k’a lamara ka xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲa[ɲa] k’a kɛ ka xref:fr@docs:ROOT:lexique-dyu.adoc#_hɛ́rɛ[hɛrɛ] xref:fr@docs:ROOT:lexique-dyu.adoc#_láse[lase] jamanadenw ma.

foyi xref:fr@docs:ROOT:lexique-dyu.adoc#_mán[man] xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] adamadenw dɔw ye xref:fr@docs:ROOT:lexique-dyu.adoc#_wári[wari] kɔ. 
foyi xref:fr@docs:ROOT:lexique-dyu.adoc#_mán[man] xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] ne ye xref:fr@docs:ROOT:lexique-dyu.adoc#_mìsi[misi] xref:fr@docs:ROOT:lexique-dyu.adoc#_sɔ̀gɔ[sɔgɔ] kɔ

.Vocabulaire
[width="100%",cols="3",frame="none",options="none",stripes="even"]
|====
a|
amana:: pays
baara:: travail
baarada:: travail, emploi
baarakɛbaliw:: ceux qui ne trvaillent pas
banabagatɔw:: malades
banna:: finir+acc
bɛ:: aux.
bɛ:: être
bɛɛ:: tous
ca:: nombreux
caman:: beaucoup
can:: vérité
caya:: multiplier, être beaucoup
di:: bon, agréable
donna:: entrer+acc
dɔ:: quelque chose, en
dɔgɔya:: dimunier
dɔw:: certains
entanw:: ceux qui sont sans pouvoir
fangatanw:: pauvres
fara:: ajouter
farala:: ajouter+acc
faso:: patrie
fasodenw:: peuple, patriotes
foyi:: rien
fɔ:: dire
fɛn:: chose
fɛnw:: choses
gbɛlɛ:: dur, cher
gbɛlɛya:: diffuculté, cherté
gbɛlɛyaba:: grande cherté
hɛrɛ:: paix, bonheur
i ta:: le tien
a|
i:: toi
in:: relatif, qui
inɛ:: avoir pitié
jamanadenw:: peuple, population
jango:: afin que
janto:: veiller
jija:: s’efforcer
ka:: pour
kan:: sur
kelen:: un, une
ko:: affaire
ko:: dire
ko:: que
kofɔra:: annoncer+acc
kulela:: crier+acc
kunkow:: problèmes
kɔ:: que
kɛ:: faire, utiliser
k’a masɔrɔ:: parce que
la:: dans
la:: laisser
lakanda:: protéger
lamara:: garder
lase:: adresser, faire parvenir
lo:: c’est
lɔgɔfyɛ:: marché
ma:: à
makow:: besoins
man:: nég. Devant adjectif
minanw:: marchandises
mɔbilisara:: transport
mɔgɔ:: personne
a|
na:: dans, devant
na:: dans, sur
ni:: et
nin:: ceci, ça
nka:: mais
nɔ:: trace, fute
ɲa:: bien
ɲa:: œil, devant
ɲininka:: demander
ɲɛ:: satisfaire
ɲɛmɔgɔ:: gens de devant, dirigeants
o:: ça
olugu:: eux
sabali:: pardonner
sabu:: parce que
sera:: arriver+acc.
sɔngɔ:: prix
sɔrɔla:: trouver+forme progressive
sɛgɛnna:: fatiguer+acc
ta:: pour
tun:: marque imparfait
tɔw:: les autres
tɛ:: neg., ce n’est pas
u:: ils, leur, elles
wari:: argent
wele:: appel
ye:: postposition
ye:: pour
yen:: là
yɔrɔ:: endroit, lieu
yɛlɛnna:: monter+acc
|====
                                                        
=== POINT DE GRAMMAIRE : LA POSSESSION, LES COMPLEMENTS DU NOM

. …ye…ta ye » = Appartenir
** xref:fr@docs:ROOT:lexique-dyu.adoc#_gafe[gafe] xref:fr@docs:ROOT:lexique-dyu.adoc#_nín[nin] ye ne ta ye:: ce livre m’appartient.
** « …tun ye…ta ye » = appartenait.
** « …tɛ…ta ye » =
** « …tun tɛ…ta ye »
** « …kɛ…ta ye »
** « …kɛla…ta ye »

. « kɛ... ye » = devenir
** xref:fr@docs:ROOT:lexique-dyu.adoc#_nín[nin] bɛ kɛ fɛn ye
** xref:fr@docs:ROOT:lexique-dyu.adoc#_nín[nin] kɔnin tɛ kɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_fòyi[foyi] ye.
** « kɛla... ye »
** « xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] kɛ... ye »

. « ka xref:fr@docs:ROOT:lexique-dyu.adoc#_bán[ban] »
*  A ka xref:fr@docs:ROOT:lexique-dyu.adoc#_báara[baara] kɛ ka ban
*  …domuni kɛ….
*  …kuman fɔ…
*  A xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] mɔgɔw xref:fr@docs:ROOT:lexique-dyu.adoc#_sára[sara] ka ban

=== EXERCICE : Donnez cinq (05) exemples pour chaque structure

=== NYININGALI xref:fr@docs:ROOT:lexique-dyu.adoc#_ninugu[ninugu] JAABI

. Muna an xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] se ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kàlan[kalan] kɛ taratalon?
. I ka mun kɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_kúnu[kunu] sufɛ ?
. I bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_domuni[domuni] xref:fr@docs:ROOT:lexique-dyu.adoc#_júman[jumɛn] kɛ fɛtilonw la?
. I be se ka xref:fr@docs:ROOT:lexique-dyu.adoc#_dùgawu[dugawu] kɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_julakan[julakan] xref:fr@docs:ROOT:lexique-dyu.adoc#_ná[na] wa?
. xref:fr@docs:ROOT:lexique-dyu.adoc#_bí[bi] ye xref:fr@docs:ROOT:lexique-dyu.adoc#_sàan[san] kulan xref:fr@docs:ROOT:lexique-dyu.adoc#_tère[tere] filanan ye. I ya xref:fr@docs:ROOT:lexique-dyu.adoc#_làɲini[lanyini] ye mun ye san kula xref:fr@docs:ROOT:lexique-dyu.adoc#_nín[nin] kɔnɔ?

.Réponses
[%collapsible]
====
. xref:fr@docs:ROOT:lexique-dyu.adoc#_lɔ́n[lɔn] o lɔn, n be xref:fr@docs:ROOT:lexique-dyu.adoc#_báara[baara] kɛ su fɛ.  N tun sɛgɛla taratalon tɛmɛni.
. N tun ka xref:fr@docs:ROOT:lexique-dyu.adoc#_báara[baara] kɛ ni n ya site web ye.
. N ka xref:fr@docs:ROOT:lexique-dyu.adoc#_sìsɛ[sisɛ] domu. 
. Dɔɔni.  Ala ya xref:fr@docs:ROOT:lexique-dyu.adoc#_wúla[wula] nɔgɔya.
. 



====

