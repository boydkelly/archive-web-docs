---
title:  SÉANCE 31 COURS DE DIOULA 
author: Youssouf DIARRASSOUBA
date:  2020-10-10
type: doc
---

= SÉANCE 31 COURS DE DIOULA 
:author: Youssouf DIARRASSOUBA
:date: 2020-10-10
:type: post
:experimental:
:sectnums:
:sectnumlevels: 2
:lang: fr
:page-lang: {lang}
include::ROOT:partial$locale/attributes.adoc[]

== LÉCON 31 : xref:fr@docs:ROOT:lexique-dyu.adoc#_kàlansen[kalansen] xref:fr@docs:ROOT:lexique-dyu.adoc#_bí[bi] SABANAN NI KELENNAN
EXPLICATIONS DE QUELQUES NOTIONS GRAMMATICALES

== EMPLOI DU SUFFIXE « xref:fr@docs:ROOT:lexique-dyu.adoc#_nín[nin] » DANS LE CONTEXTE DE LA DESCRIPTION DES ETATS

L’emploi de la structure grammaticale «Sujet+ verbe-(suffixe)nin+bɛ/tɛ » exprime généralement l’état de quelque chose ou de quelqu’un à l’instant présent.
Avec cette construction, on obtient des formes adjectivales en dioula.

* Kelly siginin bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_sigilan[sigilan] kan
* Youssouf xref:fr@docs:ROOT:lexique-dyu.adoc#_lɔnin[lɔnin] be Ali ya butiki kɔfɛ
* Barikon xref:fr@docs:ROOT:lexique-dyu.adoc#_nín[nin] xref:fr@docs:ROOT:lexique-dyu.adoc#_fanin[fanin] bɛ
* I xref:fr@docs:ROOT:lexique-dyu.adoc#_mùso[muso] sawanin be bi, mun ko bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_yèn[yi] ?
* N xref:fr@docs:ROOT:lexique-dyu.adoc#_sɛgɛnin[sɛgɛnin] bɛ kojugu
* xref:fr@docs:ROOT:lexique-dyu.adoc#_cɛ́[cɛ] xref:fr@docs:ROOT:lexique-dyu.adoc#_nín[nin] xref:fr@docs:ROOT:lexique-dyu.adoc#_nisɔdiyanin[nisɔdiyanin] (sawanin) tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_kábini[kabini] kunu
* Bakari xref:fr@docs:ROOT:lexique-dyu.adoc#_diminin[diminin] be à xref:fr@docs:ROOT:lexique-dyu.adoc#_mùso[muso] kɔrɔ
* A ya masirifenw nɔrɔnin bɛ danan na
* xref:fr@docs:ROOT:lexique-dyu.adoc#_jí[ji] gbanin bɛ kojugu
* I dencɛ nyagbannin bɛ
* xref:fr@docs:ROOT:lexique-dyu.adoc#_mùso[muso] xref:fr@docs:ROOT:lexique-dyu.adoc#_nín[nin] xref:fr@docs:ROOT:lexique-dyu.adoc#_jamanin[jamanin] bɛ

Des spécialistes parlent de suffixe « resultatif ». Vous pouvez egalement entendre des locuteurs dioulaphones dire:

* Kelly siginin lo/le xref:fr@docs:ROOT:lexique-dyu.adoc#_sigilan[sigilan] kan
* Youssouf xref:fr@docs:ROOT:lexique-dyu.adoc#_lɔnin[lɔnin] lo/le Ali ya butiki kɔfɛ
* I xref:fr@docs:ROOT:lexique-dyu.adoc#_mùso[muso] sawanin lo/le bi

== QUELQUES EXPLICATIONS CONCERNANT LES SUFFIXES « xref:fr@docs:ROOT:lexique-dyu.adoc#_bága[baga] » ET « LA »

a. « xref:fr@docs:ROOT:lexique-dyu.adoc#_bága[baga] » (Celui qui…. : agent temporaire, agent de circonstance)
b. « la » (Celui qui…. : agent dont c’est le metier)

== LE SUFFIXE « xref:fr@docs:ROOT:lexique-dyu.adoc#_tɔ̀[tɔ] » ET SES EMPLOIS

=== CONTEXTE 1

Il peut avoir pour equivalent le suffixe « xref:fr@docs:ROOT:lexique-dyu.adoc#_tɔ̀[tɔ] » dans le cas ou « le fait d’etre pourvu ou de contenir queluqe chose» n’est pas volontaire ou provoqué

* xref:fr@docs:ROOT:lexique-dyu.adoc#_banabagatɔ[banabagatɔ] (malade: quelqu’un qui porte une maladie)
* xref:fr@docs:ROOT:lexique-dyu.adoc#_fiyentɔ[fiyentɔ] (quelqu’un qui est porteur de la cessité)
* xref:fr@docs:ROOT:lexique-dyu.adoc#_dɛ́sɛbagatɔ[dɛsɛbagatɔ] (quelqu’un qui est «pouvu»/victime du manque)
* Kunantɔ (lepreux, victime de la lepre ou qui porte la lepre)
* xref:fr@docs:ROOT:lexique-dyu.adoc#_dangatɔ[dangatɔ] (maudit, qui porte la malediction)
* xref:fr@docs:ROOT:lexique-dyu.adoc#_fàtɔ[fatɔ] (fou; qui porte la folie/ Victime de folie)

=== CONTEXTE 2

Participe présent introduisant une forme de simultaneité, de concomitance

* Kelly bɔtɔ bɛn xref:fr@docs:ROOT:lexique-dyu.adoc#_ná[na] ni Moussa ye (En sortant, Coulibaly a rencontré Moussa)
* An nantɔ ka xref:fr@docs:ROOT:lexique-dyu.adoc#_mángoro[mangoro] xref:fr@docs:ROOT:lexique-dyu.adoc#_sàan[san] xref:fr@docs:ROOT:lexique-dyu.adoc#_síra[sira] la (Nous avons acheté des mangues en venant)
* Moussa kumantɔ ka mɔgɔw xref:fr@docs:ROOT:lexique-dyu.adoc#_nɛ̀ni[nɛnin] (En parlant, Moussa a insulté les gens)
* Youssouf bena i ya telefɔni san, a dontɔ so kɔnɔ (Youssouf va acheter ton téléphone en rentrant à la maison)

== EXERCICE DE TRADUCTION

[cols="2*"]
|===
a|
Francais/Dioula

. En revenant de l’école, il a rencontré xref:fr@docs:ROOT:lexique-dyu.adoc#_sòn[son] frère
. Certains fous ne mangent pas la nourriture avariée
. Le professeur de  est couché sur le lit
. Les personnes malades n’aiment pas la nourriture sucré
. Les demunis ne mangent pas trois fois par jour
. Cet enfant est maudit, il ne respecte personne

a|
.Traduction:
[%collapsible]
====
. a kɔsigitɔ ka bɔ lakoliso la, a bɛn xref:fr@docs:ROOT:lexique-dyu.adoc#_ná[na] ni a ya badɛncɛ ye
. xref:fr@docs:ROOT:lexique-dyu.adoc#_fàtɔ[fatɔ] dow tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_domuni[domuni] torinin domu
. Kelly ya xref:fr@docs:ROOT:lexique-dyu.adoc#_kàramɔgɔ[karamɔgɔ] lanin bɛ lafɛn kan
. xref:fr@docs:ROOT:lexique-dyu.adoc#_domuni[domuni] xref:fr@docs:ROOT:lexique-dyu.adoc#_sukarontan[sukarontan] xref:fr@docs:ROOT:lexique-dyu.adoc#_mán[man] xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] banabagatɔw ye
. dɛsɛbagatɔw tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_domuni[domuni] kɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_sìɲɛ[siɲɛ] saba xref:fr@docs:ROOT:lexique-dyu.adoc#_tère[tere] kɔnɔ
. xref:fr@docs:ROOT:lexique-dyu.adoc#_dén[den] xref:fr@docs:ROOT:lexique-dyu.adoc#_nín[nin] ye xref:fr@docs:ROOT:lexique-dyu.adoc#_dangatɔ[dangatɔ] ye, a tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] si bonya
====
|===

[cols="2*"]
|===
a|
Dioula/Francais

. A nantɔ ka nanfenw xref:fr@docs:ROOT:lexique-dyu.adoc#_sàan[san] xref:fr@docs:ROOT:lexique-dyu.adoc#_lɔgɔfiyɛ[lɔgɔfɛ] la
. Youssouf bɛ yeli kɛ, xref:fr@docs:ROOT:lexique-dyu.adoc#_fiyentɔ[fiyentɔ] tɛ
. xref:fr@docs:ROOT:lexique-dyu.adoc#_dangatɔ[dangatɔ] tɛ mɔgɔkɔrɔbaw bonya
. xref:fr@docs:ROOT:lexique-dyu.adoc#_mùso[muso] xref:fr@docs:ROOT:lexique-dyu.adoc#_nín[nin] bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_tìga[tiga] xref:fr@docs:ROOT:lexique-dyu.adoc#_kɔgɔman[kɔgɔman] xref:fr@docs:ROOT:lexique-dyu.adoc#_fèere[feere] MADOU ya butiki kɛrɛfɛ
. xref:fr@docs:ROOT:lexique-dyu.adoc#_dén[den] xref:fr@docs:ROOT:lexique-dyu.adoc#_nín[nin] ye mɔgo xref:fr@docs:ROOT:lexique-dyu.adoc#_hakiliman[hakiliman] ye, a bɛ a kumancogo kɔrɔsi

a|
.Traduction:
[%collapsible]
====
. En venant du marché il a acheté des condiments
. Youssouf est voyant, il n'est pas aveugle
. Les maudits ne respectent pas les vieux
. Cette femme vend des arachides salées à côté de la boutique de Madou
. Cet enfant est intelligent, il pèse ses paroles
====
|===

