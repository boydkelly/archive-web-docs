---
title:  SÉANCE 29 COURS DE DIOULA
author: Youssouf DIARRASSOUBA
date:  2020-09-29
type: doc
---

= SÉANCE 29 COURS DE DIOULA
:author: Youssouf DIARRASSOUBA
:date: 2020-09-29
:description: Explications de quelques notions grammaticales
:sectnums:
:sectnumlevels: 2
:lang: fr
include::ROOT:partial$locale/attributes.adoc[]

[discreet]
== LECON 29 : xref:fr@docs:ROOT:lexique-dyu.adoc#_kàlansen[kalansen] xref:fr@docs:ROOT:lexique-dyu.adoc#_múgan[mugan] NI KƆNƆNTƆNAN

[abstract]
Explications de quelques notions grammaticales

== Emploi de « xref:fr@docs:ROOT:lexique-dyu.adoc#_mán[man] » comme suffixe

=== Pour traduire «qui contient une chose, qui est pourvu de »

sukaroman:: sucré
tuluman:: huileux
kɔgɔman:: salé 
joliman:: ensanglanté
nɔnɔman:: lacté
fangaman:: qui a de la force/autorité

* Kafe xref:fr@docs:ROOT:lexique-dyu.adoc#_sukaroman[sukaroman] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] Coulibaly ye kojugu
* xref:fr@docs:ROOT:lexique-dyu.adoc#_bága[baga] xref:fr@docs:ROOT:lexique-dyu.adoc#_nɔnɔman[nɔnɔman] bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] xref:fr@docs:ROOT:lexique-dyu.adoc#_fá[fa] joonan
* xref:fr@docs:ROOT:lexique-dyu.adoc#_tìga[tiga] xref:fr@docs:ROOT:lexique-dyu.adoc#_kɔgɔman[kɔgɔman] bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_fèere[feere] Madu ya butiki la

NOTE: La forme negative de ces mots se contsruits avec le suffixe « xref:fr@docs:ROOT:lexique-dyu.adoc#_-ntán[ntan] »

sukarontan:: (non sucré)
tuluntan:: (sans huile); 
kɔgɔntan:: (sans sel)
jolintan:: (non ensanglanté); 
nɔnɔntan:: (non lacté)
fangantan:: (qui n’a pas la force= pauvre, necessiteux)

* Ne tɛ nan kɔgɔntan xref:fr@docs:ROOT:lexique-dyu.adoc#_dómu[domun] abada

Il peut avoir pour equivalent le suffixe « tɔ» dans le cas ou « le fait d’etre pourvu ou de
contenir queluqe chose» n’est pas volontaire ou provoqué

banabagatɔ:: malade: quelqu’un qui porte une maladie
fiyentɔ:: quelqu’un qui est porteur de la cessité
dɛsɛbagatɔ:: quelqu’un qui est «pouvu»/victime du manque
kunantɔ:: lepreux, victime de la lepre ou qui porte la lepre
kangatɔ:: maudit, qui porte la malediction
fatɔ:: fou; qui porte la folie/ Victime de folie

=== Pour former des épithètes ou des adverbes

caman:: beaucoup
kalaman:: chaud 
gbiliman:: lourd
hakiliman:: intelligent
nalonman:: idiot, bête

[width="100%",cols="2",frame="none",options="none",stripes="even"]
|===
|A xref:fr@docs:ROOT:lexique-dyu.adoc#_hákiri[hakili] ka di|mɔgɔ xref:fr@docs:ROOT:lexique-dyu.adoc#_hakiliman[hakiliman] le
|Malo ka ca xref:fr@docs:ROOT:lexique-dyu.adoc#_bón[bon] kɔnɔ|Malo xref:fr@docs:ROOT:lexique-dyu.adoc#_cáman[caman] bɛ bon kɔnɔ
|Domuni ka kala|Domuni kalaman ka xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] Coulibaly ye
|Kanu le ka n nalo|Cɛ nalonman dɔ nan xref:fr@docs:ROOT:lexique-dyu.adoc#_ná[na] xref:fr@docs:ROOT:lexique-dyu.adoc#_yàn[yan] bi
|Doni xref:fr@docs:ROOT:lexique-dyu.adoc#_nín[nin] ka gbili|Doni gbiliman bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] banan
|===

== Emploi de «nin » comme suffixe

=== Dimunitif

basinin:: le petit margouillat
ɲinanin:: La petite souris 
daganin:: la petite marmite
wulunin:: le chiot, le petiot chien
wotoronin:: charrette, petite brouette
denin:: petit enfant
mobilinin:: petite voiture
bonin:: petite maison maisonnet

Dans certains cas, cette forme peut marquer une certaine affection pour l’objet qu’on
mentionne.

=== Epithète

Ce sont généralement des verbes transformés en épithète à travers l’ajout du suffixe «nin».

nɔgɔnin:: sale
anuyanin:: propre
sumanin::  froid
ɲiginin:: mouillé

==== Exercise

* Propose des mots dun même type
* Construire des phrases avec les mots proposés
