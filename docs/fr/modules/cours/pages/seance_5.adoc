---
title:  SÉANCE 5 COURS DE DIOULA
author: Youssouf DIARRASSOUBA
date:  2020-05-15
type: doc
---

= SÉANCE 5 COURS DE DIOULA
:author: Youssouf DIARRASSOUBA
:date: 2020-05-15
:type: post
:experimental:
:sectnums:
:sectnumlevels: 2
:lang: fr
:page-lang: {lang}
include::ROOT:partial$locale/attributes.adoc[]

[abstract]
== LECON 5 : xref:fr@docs:ROOT:lexique-dyu.adoc#_kàlansen[kalansen] LOORUNAN

== REVISION DE LA SÉANCE 4

A travers cette révision, l’apprenant va approfondir ses connaissances concernant les contextes d’emploi du verbe « avoir » en dioula de Côte d’Ivoire.

=== Exercice de traduction

. Coulibaly a beaucoup des moutons
. Je n’ai pas de chien à la maison
. L’être humain a combien de pieds ?
. Il y a combien de semaine dans le mois ?
. Mahamane a combien d’enfants ?
. Youssouf n’a pas de voiture
. Sa voiture n’a pas de phare
. Le fils de mon amie a faim
. Est-ce que ton fils a chaud ?
. De quoi as-tu envie/Tu as envie de quoi ?
. Est-ce que vous avez froid ?
. Combien de personnes ont faim ici ?
. Coulibaly a sommeil ce matin
. Certaines personnes n’ont pas de cheveux
. Est-ce que le fils de ton professeur à des jouets ?
. Est-ce que le frère de Moussa à un cuisinier ?
. Je n’ai pas d’employé de maison
. La femme de mon ami à un gros chat
. Le fils du cuisinier n’a pas d’ami
. xref:fr@docs:ROOT:lexique-dyu.adoc#_sòn[son] enfant à une maladie



[abstract]
== COURS DU JOUR : LES CHIFFRES ET LES NOMBRES
--
Objectif : Maîtriser le système de calcul en Dioula
--

== xref:fr@docs:ROOT:lexique-dyu.adoc#_kàlan[kalan] lanyini

=== Dialogue introductif

[horizontal]
Coulibaly:: xref:fr@docs:ROOT:lexique-dyu.adoc#_sisɛfan[sisɛfan] xref:fr@docs:ROOT:lexique-dyu.adoc#_dén[den] xref:fr@docs:ROOT:lexique-dyu.adoc#_jóli[joli] bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_sègi[segi] kɔnɔ?
Youssouf:: xref:fr@docs:ROOT:lexique-dyu.adoc#_sisɛfan[sisɛfan] xref:fr@docs:ROOT:lexique-dyu.adoc#_dén[den] saba
Coulibaly:: xref:fr@docs:ROOT:lexique-dyu.adoc#_jóli[joli] bɛ sisɛkuru kɔnɔ?
Youssouf:: sisɛfanw ka ca, n xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] xref:fr@docs:ROOT:lexique-dyu.adoc#_dá[da] lɔn
Coulibaly:: xref:fr@docs:ROOT:lexique-dyu.adoc#_sisɛfan[sisɛfan] xref:fr@docs:ROOT:lexique-dyu.adoc#_dén[den] tan
Yusuf:: N tɛ se ka xref:fr@docs:ROOT:lexique-dyu.adoc#_jatebɔ[jatebɔ] xref:fr@docs:ROOT:lexique-dyu.adoc#_fó[fɔ] ka se tan
Coulibaly:: N bena i dege

==== Traduction du dialogue

[horizontal]
Coulibaly:: Combien y a-t-il d’œufs dans la corbeille ?
Youssouf:: Trois œufs
Coulibaly:: combien y en a-t-il dans le poulailler ?
Youssouf:: Il y en a beaucoup, je ne connais pas le nombre
Coulibaly:: Dix œufs
Youssouf:: Je ne sais pas compter jusqu’à dix
Coulibaly:: Je vais t’apprendre

[width="100%",cols="4",frame="none",options="none",stripes="even"]
|===
|1. un|kelen|6. six|wɔɔrɔ
|2. deux|fila|7. sept|2oronfila
|3. trois|saba|8. huit|seegi
|4. quatre|naani|9. neuf|kɔnɔntɔ
|5. cinq|looru|10. dix|tan
|===

== Les nombres (grands chiffres et sommes d’argent)

Le tableau ci-après permet de générer de façon automatique les chiffres et les nombres même les plus longs pour mieux comprendre le fonctionnement du système de calcul.

.Système de calcul 
[width="100%",cols="12",frame="topbot",options="header",stripes="even"]
|====
3+^|miliyari
3+^|miliyɔn 
3+^|waga 
3+^|kɛmɛ

|kɛmɛ
|Bi 
|ni
|kɛmɛ
|Bi
|ni
|kɛmɛ
|bi
|ni
|kɛmɛ
|bi
|ni 

| | | | | | | | | | | |6
| | | | | | | | | | |4|6
| | | | | | | | | |2|4|6
| | | | | | | | |3|2|4|6
| | | | | | | |5|3|2|4|6
| | | | | | |7|5|3|2|4|6
| | | | | |9|7|5|3|2|4|6
| | | | |8|9|7|5|3|2|4|6
| | | |1|8|9|7|5|3|2|4|6
|7|3|5|1|8|9|7|5|3|2|4|6
|====

.Définitions des termes du tableau
[width="100%",cols="3",frame="topbot",options="none",stripes="even"]
|====
a|
miliyari:: milliard
kɛmɛ:: centaine
miliyɔn:: million 

a|
bi:: dizaine
waga:: mille 
ni:: unité
kɛmɛ:: cent
 
a|
10.:: xref:fr@docs:ROOT:lexique-dyu.adoc#_tán[tan] 
20.:: mugan
30.:: xref:fr@docs:ROOT:lexique-dyu.adoc#_bí[bi] saba
40.:: xref:fr@docs:ROOT:lexique-dyu.adoc#_bí[bi] xref:fr@docs:ROOT:lexique-dyu.adoc#_náani[naani] 
|====

[CAUTION]
====
Ne pas traduire deux dizaines par *bi fila** ou encore une dizaine par *bi kelen* ; par contre à partir de 30, le nombre de dizaine qui compose le chiffre est pris en compte. 
====

=== Lisons maintenant les chiffres qui sont dans le tableau :

[width="100%",cols="2",frame="none",options="none",stripes="even"]
|====
|6
|wɔrɔ

|46
|bi xref:fr@docs:ROOT:lexique-dyu.adoc#_náani[naani] ni wɔɔrɔ

|246
|kɛmɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_filà[fila] ni xref:fr@docs:ROOT:lexique-dyu.adoc#_bí[bi] xref:fr@docs:ROOT:lexique-dyu.adoc#_náani[naani] ni wɔɔrɔ

|3246
|waga saba ani xref:fr@docs:ROOT:lexique-dyu.adoc#_kɛ̀mɛ[kɛmɛ] xref:fr@docs:ROOT:lexique-dyu.adoc#_filà[fila] ni xref:fr@docs:ROOT:lexique-dyu.adoc#_bí[bi] xref:fr@docs:ROOT:lexique-dyu.adoc#_náani[naani] ni wɔɔrɔ

|53246
|waga xref:fr@docs:ROOT:lexique-dyu.adoc#_bí[bi] xref:fr@docs:ROOT:lexique-dyu.adoc#_lóoru[looru] ni saba ani xref:fr@docs:ROOT:lexique-dyu.adoc#_kɛ̀mɛ[kɛmɛ] xref:fr@docs:ROOT:lexique-dyu.adoc#_filà[fila] ni bi xref:fr@docs:ROOT:lexique-dyu.adoc#_náani[naani] ni wɔɔrɔ

|753246
|waga xref:fr@docs:ROOT:lexique-dyu.adoc#_kɛ̀mɛ[kɛmɛ] woronfila ni xref:fr@docs:ROOT:lexique-dyu.adoc#_bí[bi] xref:fr@docs:ROOT:lexique-dyu.adoc#_lóoru[looru] ni saba ani kɛmɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_filà[fila] ni bi xref:fr@docs:ROOT:lexique-dyu.adoc#_náani[naani] ni wɔɔrɔ

|9753246
|miliyɔn xref:fr@docs:ROOT:lexique-dyu.adoc#_kɔ̀nɔntɔ[kɔnɔntɔ] ani xref:fr@docs:ROOT:lexique-dyu.adoc#_wága[waga] xref:fr@docs:ROOT:lexique-dyu.adoc#_kɛ̀mɛ[kɛmɛ] woronfila ni xref:fr@docs:ROOT:lexique-dyu.adoc#_bí[bi] xref:fr@docs:ROOT:lexique-dyu.adoc#_lóoru[looru] ni saba ani kɛmɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_filà[fila] ni bi xref:fr@docs:ROOT:lexique-dyu.adoc#_náani[naani] ni wɔɔrɔ

|89753246
|miliyɔn xref:fr@docs:ROOT:lexique-dyu.adoc#_bí[bi] xref:fr@docs:ROOT:lexique-dyu.adoc#_sègi[segi] ni xref:fr@docs:ROOT:lexique-dyu.adoc#_kɔ̀nɔntɔ[kɔnɔntɔ] ani xref:fr@docs:ROOT:lexique-dyu.adoc#_wága[waga] xref:fr@docs:ROOT:lexique-dyu.adoc#_kɛ̀mɛ[kɛmɛ] woronfila ni bi xref:fr@docs:ROOT:lexique-dyu.adoc#_lóoru[looru] ni saba ani kɛmɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_filà[fila] ni bi xref:fr@docs:ROOT:lexique-dyu.adoc#_náani[naani] ni wɔɔrɔ

|189753246
|miliyɔn xref:fr@docs:ROOT:lexique-dyu.adoc#_kɛ̀mɛ[kɛmɛ] ni xref:fr@docs:ROOT:lexique-dyu.adoc#_bí[bi] xref:fr@docs:ROOT:lexique-dyu.adoc#_sègi[segi] ni xref:fr@docs:ROOT:lexique-dyu.adoc#_kɔ̀nɔntɔ[kɔnɔntɔ] ani xref:fr@docs:ROOT:lexique-dyu.adoc#_wága[waga] kɛmɛ woronfila ni bi xref:fr@docs:ROOT:lexique-dyu.adoc#_lóoru[looru] ni saba ani kɛmɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_filà[fila] ni bi xref:fr@docs:ROOT:lexique-dyu.adoc#_náani[naani] ni wɔɔrɔ

|735189753246
|miliyari xref:fr@docs:ROOT:lexique-dyu.adoc#_kɛ̀mɛ[kɛmɛ] woronfila ni xref:fr@docs:ROOT:lexique-dyu.adoc#_bí[bi] saba ni xref:fr@docs:ROOT:lexique-dyu.adoc#_lóoru[looru] ani xref:fr@docs:ROOT:lexique-dyu.adoc#_miliyɔn[miliyɔn] kɛmɛ ni bi xref:fr@docs:ROOT:lexique-dyu.adoc#_sègi[segi] ni xref:fr@docs:ROOT:lexique-dyu.adoc#_kɔ̀nɔntɔ[kɔnɔntɔ] ani xref:fr@docs:ROOT:lexique-dyu.adoc#_wága[waga] kɛmɛ woronfila ni bi looru ni saba ani kɛmɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_filà[fila] ni bi xref:fr@docs:ROOT:lexique-dyu.adoc#_náani[naani] ni wɔɔrɔ
|====

[NOTE]
====
*« ani »* permet de passer d’une grande classe à une autre,les grandes classes étant miliyari,
miliyɔn, xref:fr@docs:ROOT:lexique-dyu.adoc#_wága[waga] et kɛmɛ.Pour passer d’un élément à un autre dans la même classe on se sert de *« ni »*.
+
[horizontal]
11:: xref:fr@docs:ROOT:lexique-dyu.adoc#_tán[tan] *ni* kelen
12:: xref:fr@docs:ROOT:lexique-dyu.adoc#_tán[tan] *ni* fila
105:: xref:fr@docs:ROOT:lexique-dyu.adoc#_kɛ̀mɛ[kɛmɛ] *ni* looru
====

Tous ces chiffres ne concernent que les superficies, les distances... Mais pas les sommes d'argent(en CFA) puisque la base du calcul dans ce cas est 5.
On dira par exemple *tan* pour 50 francs CFA, *mugan* pour 100 francs, *bi saba* pour 150 etc...

=== EXERCICE 1 : Traduire ces chiffres en lettres (d’abord en somme d’argent, ensuite en termes de distance ou de superficie, etc.)

. 2500
. 3500
. 3450
. 4320
. 200 
. 175

.En termes d'argent
[%collapsible]
====
. 2500: xref:fr@docs:ROOT:lexique-dyu.adoc#_kɛ̀mɛ[kɛmɛ] looru
. 3500: xref:fr@docs:ROOT:lexique-dyu.adoc#_kɛ̀mɛ[kɛmɛ] woronfila
. 3450: xref:fr@docs:ROOT:lexique-dyu.adoc#_kɛ̀mɛ[kɛmɛ] wɔɔrɔ ni xref:fr@docs:ROOT:lexique-dyu.adoc#_bí[bi] kɔnɔntɔ
. 4320: xref:fr@docs:ROOT:lexique-dyu.adoc#_kɛ̀mɛ[kɛmɛ] xref:fr@docs:ROOT:lexique-dyu.adoc#_sègi[segi] ni xref:fr@docs:ROOT:lexique-dyu.adoc#_bí[bi] wɔɔrɔ ni naani
. 200: xref:fr@docs:ROOT:lexique-dyu.adoc#_bí[bi] naani
. 175: xref:fr@docs:ROOT:lexique-dyu.adoc#_bí[bi] saba ni xref:fr@docs:ROOT:lexique-dyu.adoc#_lóoru[looru] 1. 2500 xref:fr@docs:ROOT:lexique-dyu.adoc#_kɛ̀mɛ[kɛmɛ] looru
====

.En termes de distance ou de superficie...
[%collapsible]
====
. 2500 xref:fr@docs:ROOT:lexique-dyu.adoc#_wága[waga] xref:fr@docs:ROOT:lexique-dyu.adoc#_filà[fila] ni xref:fr@docs:ROOT:lexique-dyu.adoc#_kɛ̀mɛ[kɛmɛ] looru
. 3500 xref:fr@docs:ROOT:lexique-dyu.adoc#_wága[waga] saba ni xref:fr@docs:ROOT:lexique-dyu.adoc#_kɛ̀mɛ[kɛmɛ] looru
. 3450 xref:fr@docs:ROOT:lexique-dyu.adoc#_wága[waga] saba ni xref:fr@docs:ROOT:lexique-dyu.adoc#_kɛ̀mɛ[kɛmɛ] xref:fr@docs:ROOT:lexique-dyu.adoc#_náani[naani] ni xref:fr@docs:ROOT:lexique-dyu.adoc#_bí[bi] looru
. 4320 xref:fr@docs:ROOT:lexique-dyu.adoc#_wága[waga] xref:fr@docs:ROOT:lexique-dyu.adoc#_náani[naani] ni xref:fr@docs:ROOT:lexique-dyu.adoc#_kɛ̀mɛ[kɛmɛ] saba ni mugan
. 200 xref:fr@docs:ROOT:lexique-dyu.adoc#_kɛ̀mɛ[kɛmɛ] fila
. 175 xref:fr@docs:ROOT:lexique-dyu.adoc#_kɛ̀mɛ[kɛmɛ] ni xref:fr@docs:ROOT:lexique-dyu.adoc#_bí[bi] woronfila ni xref:fr@docs:ROOT:lexique-dyu.adoc#_lóoru[looru] 
====

=== EXERCICE : Traduire ces phrases et tenter de proposer des réponses

. xref:fr@docs:ROOT:lexique-dyu.adoc#_lòmuru[lemuru] xref:fr@docs:ROOT:lexique-dyu.adoc#_layɔrɔ[layɔrɔ] xref:fr@docs:ROOT:lexique-dyu.adoc#_kélen[kelen] ye xref:fr@docs:ROOT:lexique-dyu.adoc#_jóli[joli] ye ?
. xref:fr@docs:ROOT:lexique-dyu.adoc#_baranda[baranda] kilo xref:fr@docs:ROOT:lexique-dyu.adoc#_kélen[kelen] ye xref:fr@docs:ROOT:lexique-dyu.adoc#_jóli[joli] ye ?
. Dɔrɔmɛn xref:fr@docs:ROOT:lexique-dyu.adoc#_jóli[joli] bɛ i xref:fr@docs:ROOT:lexique-dyu.adoc#_bólo[bolo] ?
. xref:fr@docs:ROOT:lexique-dyu.adoc#_jóli[joli] bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_mùso[muso] xref:fr@docs:ROOT:lexique-dyu.adoc#_nín[nin] ya saki kɔnɔ ?
. Kafe tasi xref:fr@docs:ROOT:lexique-dyu.adoc#_kélen[kelen] ye xref:fr@docs:ROOT:lexique-dyu.adoc#_jóli[joli] ye?
. Malobɔrɔ kilo xref:fr@docs:ROOT:lexique-dyu.adoc#_múgan[mugan] ni xref:fr@docs:ROOT:lexique-dyu.adoc#_lóoru[looru] bɛ Amadu ya butiki kɔnɔ wa?
. I bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_baarakɛyɔrɔ[baarakɛyɔrɔ] la ni xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] xref:fr@docs:ROOT:lexique-dyu.adoc#_jóli[joli] ye ?
. Tele xref:fr@docs:ROOT:lexique-dyu.adoc#_jóli[joli] bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_lɔ́gɔkun[lɔgɔkun] kɔnɔ ?
. xref:fr@docs:ROOT:lexique-dyu.adoc#_kàlo[kalo] xref:fr@docs:ROOT:lexique-dyu.adoc#_jóli[joli] bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_sàan[san] kɔnɔ
. xref:fr@docs:ROOT:lexique-dyu.adoc#_sèn[sen] xref:fr@docs:ROOT:lexique-dyu.adoc#_jóli[joli] bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_ádamaden[adamaden] na?

