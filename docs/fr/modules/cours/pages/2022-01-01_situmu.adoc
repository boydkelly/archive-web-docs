---
title: Situmu
---

= Situmu
:date: 2022-01-01
:lang: fr
include::ROOT:partial$locale/attributes.adoc[]

.Situmu
[width="100%",cols="a,a,a,a",frame="none",options="header",stripes="even"]
|====
|Contexte|Français|Jula|Alternative


| (Tu es étranger)

|
[unordered]
As-tu jamais mangé des chenilles?::
oui j'ai déjà mangé des chenilles
+
non je n'ai jamais mangé des cheniiles

|
[unordered]
i delila ka xref:fr@docs:ROOT:lexique-dyu.adoc#_citumu[situmu] xref:fr@docs:ROOT:lexique-dyu.adoc#_dómu[domu] wa?::
* ɔh xref:fr@docs:ROOT:lexique-dyu.adoc#_hàn[hɔn] n delila ka xref:fr@docs:ROOT:lexique-dyu.adoc#_citumu[situmu] domu
+
* ɔn ɔn n xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] xref:fr@docs:ROOT:lexique-dyu.adoc#_dáari[deli] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_citumu[situmu] domu
+
* ɔn ɔn n xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] xref:fr@docs:ROOT:lexique-dyu.adoc#_dáari[deli] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_citumu[situmu] xref:fr@docs:ROOT:lexique-dyu.adoc#_dómu[domu] fɔlɔ

|
|
|
|
[unordered]
i ka xref:fr@docs:ROOT:lexique-dyu.adoc#_citumu[situmu] xref:fr@docs:ROOT:lexique-dyu.adoc#_dómu[domu] k'a ye wa?::
* ɔn xref:fr@docs:ROOT:lexique-dyu.adoc#_hàn[hɔn] n ka xref:fr@docs:ROOT:lexique-dyu.adoc#_citumu[situmu] xref:fr@docs:ROOT:lexique-dyu.adoc#_dómu[domu] k'a ye
+
* ɔn ɔn n xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] xref:fr@docs:ROOT:lexique-dyu.adoc#_citumu[situmu] xref:fr@docs:ROOT:lexique-dyu.adoc#_dómu[domu] k'a ye


|
|
|
|
[unordered]
i xref:fr@docs:ROOT:lexique-dyu.adoc#_dáari[deli] la ka xref:fr@docs:ROOT:lexique-dyu.adoc#_citumu[situmu] xref:fr@docs:ROOT:lexique-dyu.adoc#_dómu[domu] wa?::
* ɔh xref:fr@docs:ROOT:lexique-dyu.adoc#_hàn[hɔn] n be xref:fr@docs:ROOT:lexique-dyu.adoc#_dáari[deli] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_citumu[situmu] domu.
+
* ɔn ɔn n tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_dáari[deli] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_citumu[situmu] domu.

|
|
|
[unordered]
Es-tu jamais allé à Tingrela?::
* Oui je suis déjà allé à Tingrela
+
* Non je ne suis jamais allé à Tingrela


|
[unordered]
i delila ka xref:fr@docs:ROOT:lexique-dyu.adoc#_tága[taga] Tingrela wa?::
* ɔn hɔn, n tagala Tingrela siyɛn kelen
+
* ɔn ɔn n xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] ta Tingrela fɔlɔ

|
|(les chenilles sont dans la cuisine)

|
[unordered]
As-tu déjà mangé les chenilles ?:: 
* Non je n'ai pas encore mangé les chenilles
+
* Oui j'ai déjà mangé les chenilles


|
[unordered]
i ka xref:fr@docs:ROOT:lexique-dyu.adoc#_citumu[situmu] xref:fr@docs:ROOT:lexique-dyu.adoc#_dómu[domu] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_bán[ban] wa?::
* ɔn ɔn n xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] xref:fr@docs:ROOT:lexique-dyu.adoc#_citumu[situmu] xref:fr@docs:ROOT:lexique-dyu.adoc#_dómu[domu] ka ban.
+ 
* ɔh hon n ka xref:fr@docs:ROOT:lexique-dyu.adoc#_citumu[situmu] xref:fr@docs:ROOT:lexique-dyu.adoc#_dómu[domu] fɔlɔ. 

|
|(Il faut manger tes chenilles chaque jour.) 

|
[unordered]
As-tu fini de manger les chenilles?::
* Oui j'ai fini de manger les chenilles.
+
* Non je n'ai pas fini de manger les chenilles.

|
[unordered]
i ka xref:fr@docs:ROOT:lexique-dyu.adoc#_citumu[situmu] xref:fr@docs:ROOT:lexique-dyu.adoc#_dómu[domu] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_bán[ban] wa?::
* ɔh  hɔn, n ka xref:fr@docs:ROOT:lexique-dyu.adoc#_citumu[situmu] xref:fr@docs:ROOT:lexique-dyu.adoc#_dómu[domu] ka ban.
+
* ɔn ɔn n xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] xref:fr@docs:ROOT:lexique-dyu.adoc#_citumu[situmu] xref:fr@docs:ROOT:lexique-dyu.adoc#_dómu[domu] fɔlɔ.
|


| Le vendredi soir

|
[unordered]
As-tu l'habitude de manger des chenilles?::
* Oui J'ai l'habitude de manger des chenilles.
+
* Non Je n'ai pas l'habitude de manger des chenilles.


|
[unordered]
i be to ka xref:fr@docs:ROOT:lexique-dyu.adoc#_citumu[situmu] xref:fr@docs:ROOT:lexique-dyu.adoc#_dómu[domu] wa?::
* ɔh hon ne be to ka xref:fr@docs:ROOT:lexique-dyu.adoc#_citumu[situmu] domu.
+
* n be xref:fr@docs:ROOT:lexique-dyu.adoc#_citumu[situmu] xref:fr@docs:ROOT:lexique-dyu.adoc#_dómu[domun] xref:fr@docs:ROOT:lexique-dyu.adoc#_sìɲɛ[sinyɛn] xref:fr@docs:ROOT:lexique-dyu.adoc#_kélen[kelen] kelen.
+
* ɔn ɔn, n tɛ to ka xref:fr@docs:ROOT:lexique-dyu.adoc#_citumu[situmu] domu.
+
* ɔn ɔn, n tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_citumu[situmu] xref:fr@docs:ROOT:lexique-dyu.adoc#_dómu[domu] siyɛn xref:fr@docs:ROOT:lexique-dyu.adoc#_cáman[caman] 

|
[unordered]
i be xref:fr@docs:ROOT:lexique-dyu.adoc#_dáari[deli] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_citumu[situmu] xref:fr@docs:ROOT:lexique-dyu.adoc#_dómu[domu] wa?::
* ɔh xref:fr@docs:ROOT:lexique-dyu.adoc#_hàn[hɔn] n be xref:fr@docs:ROOT:lexique-dyu.adoc#_dáari[deli] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_citumu[situmu] domu.
+
* ɔn ɔn n tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_dáari[deli] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_citumu[situmu] domu.


| Avec les amis 

|
[unordered]
N'as-tu pas l'habitude de manger des chenilles?::
* Oui je n'ai pas l'habitude de manger des chenilles.
+
* Non J'ai l'habitude de manger des chenilles.


|
[unordered]
i tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_dáari[deli] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_citumu[situmu] xref:fr@docs:ROOT:lexique-dyu.adoc#_dómu[domu] wa?::
* ɔh xref:fr@docs:ROOT:lexique-dyu.adoc#_hàn[hɔn] n degila ka xref:fr@docs:ROOT:lexique-dyu.adoc#_citumu[situmu] domu.
+
* ɔn ɔn n xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] xref:fr@docs:ROOT:lexique-dyu.adoc#_dáari[deli] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_citumu[situmu] doumu.
|

|
[unordered]
Est-il jamais allé à Abidjan?::
* Oui il est déjà allé à Abidjan.
+
* Non il n'est jamais allé à Abidjan.

|
|
[unordered]
a degila ka xref:fr@docs:ROOT:lexique-dyu.adoc#_tága[taga] Abidan wa?::
* ɔh hɔn, a tagala Abidjan xref:fr@docs:ROOT:lexique-dyu.adoc#_tùma[tuma] caman.
+
* ɔh ɔn, a xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] xref:fr@docs:ROOT:lexique-dyu.adoc#_tága[taga] Abidjan fɔlɔ.

|====


