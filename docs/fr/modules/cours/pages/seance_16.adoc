---
title:  SÉANCE 16 COURS DE DIOULA
author: Youssouf DIARRASSOUBA
date:  2020-06-30
type: doc
---

= SÉANCE 16 COURS DE DIOULA
:author: Youssouf DIARRASSOUBA
:date: 2020-06-30
:type: post
:experimental:
:sectnums:
:sectnumlevels: 2
:lang: fr
:page-lang: {lang}
include::ROOT:partial$locale/attributes.adoc[]

== LÉÇON 16 : xref:fr@docs:ROOT:lexique-dyu.adoc#_kàlansen[kalansen] xref:fr@docs:ROOT:lexique-dyu.adoc#_tán[tan] NI WƆƆRƆNAN
=== xref:fr@docs:ROOT:lexique-dyu.adoc#_bàro[baro] SABANAN/THEME 3 :  et xref:fr@docs:ROOT:lexique-dyu.adoc#_sòn[son] ami JAMES au restaurant

.Contexte de communication
[abstract]
Kelly bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_terela[terela] kɛ Aïcha ya domunifeereyɔɔrɔ xref:fr@docs:ROOT:lexique-dyu.adoc#_lɔ́gɔkun[lɔgɔkun] xref:fr@docs:ROOT:lexique-dyu.adoc#_lában[laban] xref:fr@docs:ROOT:lexique-dyu.adoc#_cáman[caman] na. xref:fr@docs:ROOT:lexique-dyu.adoc#_bí[bi] sibiri,
a xref:fr@docs:ROOT:lexique-dyu.adoc#_tága[taga] la xref:fr@docs:ROOT:lexique-dyu.adoc#_domuni[domuni] kɛ ni a lɔnbagaw dɔ ye Aïcha fɛ.
Traduction : Kelly prend xref:fr@docs:ROOT:lexique-dyu.adoc#_sòn[son] déjeuner dans le restaurant de Aicha très souvent
les week-end. Aujourd’hui samedi, il est allé manger chez Aïcha avec une de ses connaissances (proches).

****
* I ni xref:fr@docs:ROOT:lexique-dyu.adoc#_tère[tere] AÏCHA
* Nse, i ni tere. Somɔgɔw do ?
* O ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kɛ́nɛ[kɛnɛ] kosɔbɛ
* I bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_nín[nin] xref:fr@docs:ROOT:lexique-dyu.adoc#_jɔ́n[jɔn] ye xref:fr@docs:ROOT:lexique-dyu.adoc#_bí[bi] ?
* N bɛ ni n xref:fr@docs:ROOT:lexique-dyu.adoc#_téri[tericɛ] James ye bi. A b’a xref:fr@docs:ROOT:lexique-dyu.adoc#_fɛ̀[fɛ] ka i ya xref:fr@docs:ROOT:lexique-dyu.adoc#_gwà[gba] xref:fr@docs:ROOT:lexique-dyu.adoc#_nɛ́nɛ[nɛnɛ] ka filɛ
* Ayiwa, a danse. A bena mun xref:fr@docs:ROOT:lexique-dyu.adoc#_dómu[domun] xref:fr@docs:ROOT:lexique-dyu.adoc#_bí[bi] ?
* Ne bena xref:fr@docs:ROOT:lexique-dyu.adoc#_maro[maro] ni tigadɛgɛnan domun
* I xref:fr@docs:ROOT:lexique-dyu.adoc#_téri[tericɛ] do ? Ale bɛ mun xref:fr@docs:ROOT:lexique-dyu.adoc#_fɛ̀[fɛ] ?
* Ne ni JAMES bena xref:fr@docs:ROOT:lexique-dyu.adoc#_domuni[domuni] xref:fr@docs:ROOT:lexique-dyu.adoc#_kélen[kelen] domun
* xref:fr@docs:ROOT:lexique-dyu.adoc#_fòronto[foronto] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] i xref:fr@docs:ROOT:lexique-dyu.adoc#_téri[tericɛ] ye xref:fr@docs:ROOT:lexique-dyu.adoc#_wà[wa] ?
* xref:fr@docs:ROOT:lexique-dyu.adoc#_fòronto[foronto] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] James ye xref:fr@docs:ROOT:lexique-dyu.adoc#_katimi[katimi] ne xref:fr@docs:ROOT:lexique-dyu.adoc#_yɛ̀rɛ[yɛrɛ] kan
* Ola, n bena xref:fr@docs:ROOT:lexique-dyu.adoc#_fòronto[foronto] xref:fr@docs:ROOT:lexique-dyu.adoc#_caya[caya] a ya zagamɛ la. A bena a xref:fr@docs:ROOT:lexique-dyu.adoc#_dómu[domun] ni xref:fr@docs:ROOT:lexique-dyu.adoc#_sògo[sogo] ye xref:fr@docs:ROOT:lexique-dyu.adoc#_wála[walima] xref:fr@docs:ROOT:lexique-dyu.adoc#_jɛ́gɛ[jɛgɛ] ?
* Ne bena n ta xref:fr@docs:ROOT:lexique-dyu.adoc#_dómu[domun] ni xref:fr@docs:ROOT:lexique-dyu.adoc#_sògo[sogo] ye nka n xref:fr@docs:ROOT:lexique-dyu.adoc#_téri[tericɛ] ta kɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_jɛ́gɛ[jɛgɛ] ye.
* N k’a mɛn. Ala ye a suman a kɔnɔ (Ala ye a ni xref:fr@docs:ROOT:lexique-dyu.adoc#_hɛ́rɛ[hɛrɛ] bɛn) !
* Amina. N bɛ nan kɔfɛ
****

Quelques minutes plus tard (wagati/miniti dɔɔni xref:fr@docs:ROOT:lexique-dyu.adoc#_timinin[timinin] kɔfɛ)

****
* , xref:fr@docs:ROOT:lexique-dyu.adoc#_domuni[domuni] xref:fr@docs:ROOT:lexique-dyu.adoc#_díya[diya] la xref:fr@docs:ROOT:lexique-dyu.adoc#_wà[wa] ?
* A xref:fr@docs:ROOT:lexique-dyu.adoc#_díya[diya] la kojugu. N xref:fr@docs:ROOT:lexique-dyu.adoc#_téri[tericɛ] xref:fr@docs:ROOT:lexique-dyu.adoc#_sawa[sawa] la (kɔntan na) katimi.
* A bena mun min ka xref:fr@docs:ROOT:lexique-dyu.adoc#_domuni[domuni] xref:fr@docs:ROOT:lexique-dyu.adoc#_làjigi[lajigi] kanyan ?
* An bena lemuruji min
* Lemuruji xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] gilase. Jabibiji gilasenin xref:fr@docs:ROOT:lexique-dyu.adoc#_fána[fana] be xref:fr@docs:ROOT:lexique-dyu.adoc#_yèn[yi] dɛ ?
* xref:fr@docs:ROOT:lexique-dyu.adoc#_bási[basi] tɛ, an bena lemuruji gilasentan min ten
* Lemuruji xref:fr@docs:ROOT:lexique-dyu.adoc#_filɛ́[filɛ] xref:fr@docs:ROOT:lexique-dyu.adoc#_nín[nin] ye. A xref:fr@docs:ROOT:lexique-dyu.adoc#_kána[kana] xref:fr@docs:ROOT:lexique-dyu.adoc#_bí[bi] ta sara
* Muna, i bena xref:fr@docs:ROOT:lexique-dyu.adoc#_bèn[ben] dɛ AÏCHA. xref:fr@docs:ROOT:lexique-dyu.adoc#_jàgo[jago] tɛ kɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_tén[ten] oh !
* N b’o kalaman, nka ile ye n ya xref:fr@docs:ROOT:lexique-dyu.adoc#_sanikɛla[sanikɛla] xref:fr@docs:ROOT:lexique-dyu.adoc#_bɛ̀rɛ[bɛrɛ] (kiliyan bɛrɛ) ye. I bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_yàn[yan] xref:fr@docs:ROOT:lexique-dyu.adoc#_lɔ́gɔkun[lɔgɔkun] xref:fr@docs:ROOT:lexique-dyu.adoc#_lában[laban] bɛɛ. N ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ka i ladiya
* I n ice AÏCHA
* Folikun tɛ. A bena xref:fr@docs:ROOT:lexique-dyu.adoc#_sègi[segi] xref:fr@docs:ROOT:lexique-dyu.adoc#_yàn[yan] xref:fr@docs:ROOT:lexique-dyu.adoc#_lón[lon] xref:fr@docs:ROOT:lexique-dyu.adoc#_júman[jumɛn] ?
* xref:fr@docs:ROOT:lexique-dyu.adoc#_síbiri[sibiri] nanta, ni Ala xref:fr@docs:ROOT:lexique-dyu.adoc#_sɔ̀n[sɔn] na
* Ayiwa, an bɛ (k’an bɛ) xref:fr@docs:ROOT:lexique-dyu.adoc#_síbiri[sibiri] nanta
* An bɛ (k’an bɛ) AÏCHA.
****

=== BAARO NAANINAN/THEME 4 :  au kiosque à café du quartier

[abstract]
===
Contexte de communication:: Bɛ kafe min xref:fr@docs:ROOT:lexique-dyu.adoc#_lón[lon] bɛɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_sɔ̀gɔma[sɔgɔma] Birahima ya kafefeereyɔɔrɔ (cɔsi) la. 
A bɛ to ka baaro kɛ ni cɔsi xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] tɔw ye ninsɔndiya la.

Traduction::  prend xref:fr@docs:ROOT:lexique-dyu.adoc#_sòn[son] café tous les matins au kiosque à café de Birahima.
Il a pour habitude de causer (discuter) avec toutes les autres personnes du kiosque dans la bonne humeur (ambiance).
===

* A ni sɔgɔma
* N’ba, i ni xref:fr@docs:ROOT:lexique-dyu.adoc#_sɔ̀gɔma[sɔgɔma]  ?
* xref:fr@docs:ROOT:lexique-dyu.adoc#_hɛ́rɛ[hɛrɛ] sila ?
* xref:fr@docs:ROOT:lexique-dyu.adoc#_hɛ́rɛ[hɛrɛ] dɔrɔn
* A sawanin (kɔntannin) bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_bí[bi] dɛ? mun ko bɛ yi?
* xref:fr@docs:ROOT:lexique-dyu.adoc#_fòyi[foyi] tɛ yi. Ni Ala ka si ni kɛnɛya d’i ma, i ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] k’a xref:fr@docs:ROOT:lexique-dyu.adoc#_kóɲuman[konyuman] lɔn
* O ye can (tunyan) ye. Can na, a ya ninsɔndiya le bɛ ne xref:fr@docs:ROOT:lexique-dyu.adoc#_láse[lase] xref:fr@docs:ROOT:lexique-dyu.adoc#_yàn[yan] xref:fr@docs:ROOT:lexique-dyu.adoc#_lón[lon] bɛɛ
* O xref:fr@docs:ROOT:lexique-dyu.adoc#_dɔ̀rɔn[dɔrɔn] tɛ dɛ. N ya kafe duman le xref:fr@docs:ROOT:lexique-dyu.adoc#_fána[fana] bɛ i xref:fr@docs:ROOT:lexique-dyu.adoc#_láse[lase] yan
* I xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] xref:fr@docs:ROOT:lexique-dyu.adoc#_wúya[wuya] fɔ. Ayiwa, i ya kafe duman dɔ xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] n ma
* Ka xref:fr@docs:ROOT:lexique-dyu.adoc#_sukaro[sukaro] xref:fr@docs:ROOT:lexique-dyu.adoc#_caya[caya] xref:fr@docs:ROOT:lexique-dyu.adoc#_bí[bi] ta la xref:fr@docs:ROOT:lexique-dyu.adoc#_wà[wa] ?
* Ɔn-ɔn, n tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_sukaro[sukaro] xref:fr@docs:ROOT:lexique-dyu.adoc#_cáman[caman] xref:fr@docs:ROOT:lexique-dyu.adoc#_fɛ̀[fɛ] tugun. N ya dɔtɔrɔcɛ ko sukaro caman xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲi[nyi] xref:fr@docs:ROOT:lexique-dyu.adoc#_ádamaden[adamaden] ma
* Muna, xref:fr@docs:ROOT:lexique-dyu.adoc#_sukaro[sukaro] bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_bàna[banan] xref:fr@docs:ROOT:lexique-dyu.adoc#_júman[jumɛn] xref:fr@docs:ROOT:lexique-dyu.adoc#_láse[lase] xref:fr@docs:ROOT:lexique-dyu.adoc#_ádamaden[adamaden] ma?
* A bɛ sukarobanan ni xref:fr@docs:ROOT:lexique-dyu.adoc#_bàna[banan] xref:fr@docs:ROOT:lexique-dyu.adoc#_cáman[caman] xref:fr@docs:ROOT:lexique-dyu.adoc#_gwɛ́rɛ[gbɛrɛ] xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] ma
* Ne tun tɛ o kalaman. xref:fr@docs:ROOT:lexique-dyu.adoc#_bí[bi] kɔfɛ, ne bena dɔ bɔ n ya xref:fr@docs:ROOT:lexique-dyu.adoc#_sukaro[sukaro] xref:fr@docs:ROOT:lexique-dyu.adoc#_hakɛya[hakɛya] la
* N’i ko kɛ kɔni, i ya xref:fr@docs:ROOT:lexique-dyu.adoc#_bàna[banan] minsɛninw bena dɔgɔya
* Heee , dɔtɔrɔya dabila, i ya kafe min.
