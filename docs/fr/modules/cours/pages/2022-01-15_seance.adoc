---
title: L'artiste de la semaine : TIKEN JAH FAKOLY 
author: Soumahoro
date: 2022-01-30
type: doc
draft: true
categories: ['Julakan']
tags: ['léçons']
---

= L'artiste de la semaine : TIKEN JAH FAKOLY 
:author: Youssouf DIARRASSOUBA
:date: 2022-01-30
:type: doc
:sectnums:
:sectnumlevels: 2
:lang: fr
include::ROOT:partial$locale/attributes.adoc[]

== COURS DU JOUR : PAROLES DE CHANSON

video::WXKykvbhzf8[youtube]

KALANKƐLON:: 15/01/2022
KALANDEN:: Passokona SYLLA
KALANFA:: Youssouf DIARRASSOUBA

[verse]
____
DƆNKILILALA : TIKEN JAH FAKOLY
DƆNKILI xref:fr@docs:ROOT:lexique-dyu.adoc#_tɔ́gɔ[tɔgɔ] : SUNGURUBA
Ni n be xref:fr@docs:ROOT:lexique-dyu.adoc#_tága[taga] la xref:fr@docs:ROOT:lexique-dyu.adoc#_lɔgɔfiyɛ[lɔgɔfɛ] la,
boro be su la n xref:fr@docs:ROOT:lexique-dyu.adoc#_ná[na] xref:fr@docs:ROOT:lexique-dyu.adoc#_yɔ́rɔ[yɔrɔ] bɛɛ la
N xref:fr@docs:ROOT:lexique-dyu.adoc#_tóro[tolo] xref:fr@docs:ROOT:lexique-dyu.adoc#_fá[fa] la kuman xref:fr@docs:ROOT:lexique-dyu.adoc#_cáman[caman] na
N ka min kɛ, n xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] min kɛ, ko ne le nɔ
Ni Allah le ye xref:fr@docs:ROOT:lexique-dyu.adoc#_dén[den] xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] la
Masa le xref:fr@docs:ROOT:lexique-dyu.adoc#_fána[fana] be xref:fr@docs:ROOT:lexique-dyu.adoc#_nàfolo[nafolo] xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] la an ma
Allah le ye xref:fr@docs:ROOT:lexique-dyu.adoc#_lɔnin[lɔnin] xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] la wo
Ole xref:fr@docs:ROOT:lexique-dyu.adoc#_fána[fana] be xref:fr@docs:ROOT:lexique-dyu.adoc#_fúru[furu] xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] laaaaa

Refrain
O ko ne xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] sunguruba
Allah xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] xref:fr@docs:ROOT:lexique-dyu.adoc#_fúru[furu] xref:fr@docs:ROOT:lexique-dyu.adoc#_díya[diya] ne la
O b’a xref:fr@docs:ROOT:lexique-dyu.adoc#_fó[fɔ] ne xref:fr@docs:ROOT:lexique-dyu.adoc#_mán[man] sunguruba
Allah xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] xref:fr@docs:ROOT:lexique-dyu.adoc#_fúru[furu] xref:fr@docs:ROOT:lexique-dyu.adoc#_díya[diya] ne la (2 fois)

Ni n be xref:fr@docs:ROOT:lexique-dyu.adoc#_tága[taga] la xref:fr@docs:ROOT:lexique-dyu.adoc#_lɔgɔfiyɛ[lɔgɔfɛ] la,
boro be su la n xref:fr@docs:ROOT:lexique-dyu.adoc#_ná[na] xref:fr@docs:ROOT:lexique-dyu.adoc#_yɔ́rɔ[yɔrɔ] bɛɛ la
N xref:fr@docs:ROOT:lexique-dyu.adoc#_tóro[tolo] xref:fr@docs:ROOT:lexique-dyu.adoc#_fá[fa] la kuman xref:fr@docs:ROOT:lexique-dyu.adoc#_cáman[caman] na
N ka min kɛ, n xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] min kɛ, ko ne le nɔ
Ni Allah le ye xref:fr@docs:ROOT:lexique-dyu.adoc#_dén[den] xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] la
Masa le xref:fr@docs:ROOT:lexique-dyu.adoc#_fána[fana] be xref:fr@docs:ROOT:lexique-dyu.adoc#_nàfolo[nafolo] xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] la an ma
Allah le ye xref:fr@docs:ROOT:lexique-dyu.adoc#_lɔnin[lɔnin] xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] la wo
Ole xref:fr@docs:ROOT:lexique-dyu.adoc#_fána[fana] be xref:fr@docs:ROOT:lexique-dyu.adoc#_fúru[furu] xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] laaaaa

Refrain
O ko ne xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] sunguruba
Allah xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] xref:fr@docs:ROOT:lexique-dyu.adoc#_fúru[furu] xref:fr@docs:ROOT:lexique-dyu.adoc#_díya[diya] ne la
O b’a xref:fr@docs:ROOT:lexique-dyu.adoc#_fó[fɔ] ne xref:fr@docs:ROOT:lexique-dyu.adoc#_mán[man] sunguruba
Allah xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] xref:fr@docs:ROOT:lexique-dyu.adoc#_fúru[furu] xref:fr@docs:ROOT:lexique-dyu.adoc#_díya[diya] ne la (2 fois)

Ni Allah ka furulon se
Foyi xref:fr@docs:ROOT:lexique-dyu.adoc#_fòyi[foyi] te a sa, a be kɛ
Bɛɛ ta ko ye Allah bolo
I xref:fr@docs:ROOT:lexique-dyu.adoc#_kána[kana] ye ko xref:fr@docs:ROOT:lexique-dyu.adoc#_fúru[furu] xref:fr@docs:ROOT:lexique-dyu.adoc#_lɔ́gɔ[lɔgɔ] te n na

Ni Allah ka ko min xref:fr@docs:ROOT:lexique-dyu.adoc#_lón[lon] se dununya
Nko xref:fr@docs:ROOT:lexique-dyu.adoc#_fòyi[foyi] te a sa be kɛ
Bɛɛ ta ko ye Allah bolo
I xref:fr@docs:ROOT:lexique-dyu.adoc#_kána[kana] ye ko xref:fr@docs:ROOT:lexique-dyu.adoc#_fúru[furu] xref:fr@docs:ROOT:lexique-dyu.adoc#_lɔ́gɔ[lɔgɔ] te n na

A xref:fr@docs:ROOT:lexique-dyu.adoc#_kána[kana] xref:fr@docs:ROOT:lexique-dyu.adoc#_jìgi[jigi] ne la
Nko i xref:fr@docs:ROOT:lexique-dyu.adoc#_kána[kana] ne xref:fr@docs:ROOT:lexique-dyu.adoc#_mìnan[minan] xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] man
Allah le xref:fr@docs:ROOT:lexique-dyu.adoc#_mán[man] kiti xref:fr@docs:ROOT:lexique-dyu.adoc#_fúru[furu] la
A xref:fr@docs:ROOT:lexique-dyu.adoc#_kána[kana] xref:fr@docs:ROOT:lexique-dyu.adoc#_jìgi[jigi] ne la (2 fois)

Refrain
O ko ne xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] sunguruba
Allah xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] xref:fr@docs:ROOT:lexique-dyu.adoc#_fúru[furu] xref:fr@docs:ROOT:lexique-dyu.adoc#_díya[diya] ne la
O b’a xref:fr@docs:ROOT:lexique-dyu.adoc#_fó[fɔ] ne xref:fr@docs:ROOT:lexique-dyu.adoc#_mán[man] sunguruba
Allah xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] xref:fr@docs:ROOT:lexique-dyu.adoc#_fúru[furu] xref:fr@docs:ROOT:lexique-dyu.adoc#_díya[diya] ne la (2 fois)
____

