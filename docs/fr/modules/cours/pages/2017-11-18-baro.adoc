---
title: Baro
author: Soumahoro
date: 2017-11-18
type: doc
draft: false
categories: ['Julakan']
tags: ['léçons']
---

= Baro
:author: Soumahoro
:date: 2017-11-18
:type: doc
:sectnums:
:sectnumlevels: 2
:lang: fr
:page-lang: {lang}
include::ROOT:partial$locale/attributes.adoc[]

== Exercise

Kuman xref:fr@docs:ROOT:lexique-dyu.adoc#_nún[nu] yɛlɛman xref:fr@docs:ROOT:lexique-dyu.adoc#_julakan[julakan] na.

[horizontal]
Karim:: I ni xref:fr@docs:ROOT:lexique-dyu.adoc#_tère[tere] Bakary. xref:fr@docs:ROOT:lexique-dyu.adoc#_hàkɛ[hakɛ] to, xref:fr@docs:ROOT:lexique-dyu.adoc#_kìbaro[kibaro] xref:fr@docs:ROOT:lexique-dyu.adoc#_júman[juman] bɛ i y so kɔnɔ?
Bakary:: xref:fr@docs:ROOT:lexique-dyu.adoc#_hɛ́rɛ[hɛrɛ] xref:fr@docs:ROOT:lexique-dyu.adoc#_dɔ̀rɔn[dɔrɔn] bɛ so kɔnɔn. Somɔgɔ bɛ ka kɛnɛ.
Karim:: I xref:fr@docs:ROOT:lexique-dyu.adoc#_fà[facɛ] do?
Bakary:: A ka kɛnɛ. Nka, a tagara Abidjan kunu.
Karim:: Ala xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲúman[ɲuman] segi.
Bakary:: Amina. Ala ye xref:fr@docs:ROOT:lexique-dyu.adoc#_tère[tere] nɔgɔya.
Karim:: I xref:fr@docs:ROOT:lexique-dyu.adoc#_dɔgɔmuso[dɔgɔmuso] xref:fr@docs:ROOT:lexique-dyu.adoc#_tɔ́gɔ[tɔgɔ] ye di?
Bakary:: A xref:fr@docs:ROOT:lexique-dyu.adoc#_tɔ́gɔ[tɔgɔ] ye minata. A xref:fr@docs:ROOT:lexique-dyu.adoc#_tára[tara] lakoriso la.
Karim:: Minata ye xref:fr@docs:ROOT:lexique-dyu.adoc#_dén[den] xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲúman[ɲuman] ye. A bɛ mɔgɔw bonya. A bɛ nan kɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_mùso[muso] ɲuman ye.
Bakary: Ala o kɛ!
