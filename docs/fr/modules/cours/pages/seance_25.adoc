---
title:  SÉANCE 25 COURS DE DIOULA 
author: Youssouf DIARRASSOUBA
date:  2020-09-02
type: doc
---

= SÉANCE 25 COURS DE DIOULA 
:author: Youssouf DIARRASSOUBA
:date: 2020-09-02
:type: post
:experimental:
:sectnums:
:sectnumlevels: 2
:lang: fr
:page-lang: {lang}
include::ROOT:partial$locale/attributes.adoc[]

== LÉÇON 25 : xref:fr@docs:ROOT:lexique-dyu.adoc#_kàlansen[kalansen] xref:fr@docs:ROOT:lexique-dyu.adoc#_múgan[mugan] NI LOORUNAN

=== RUBRIQUE DIALOGUE

* xref:fr@docs:ROOT:lexique-dyu.adoc#_bàro[baro] xref:fr@docs:ROOT:lexique-dyu.adoc#_tán[tan] ni naaninan/Thème 14 :  n’a ya xref:fr@docs:ROOT:lexique-dyu.adoc#_denbaya[denbaya] bɛ maki la

Lɔgɔfɛ kɔfɛ, Coulibaly n’a ya xref:fr@docs:ROOT:lexique-dyu.adoc#_denbaya[denbaya] xref:fr@docs:ROOT:lexique-dyu.adoc#_tága[taga] la maki la. Coulibaly, Alexia ni Markus bena xref:fr@docs:ROOT:lexique-dyu.adoc#_domuni[domuni] ni minfen kula nɛnɛn.

(Arrivés au maki : O senin maki la)

* A ni sɔgɔma
* N b’a, a danse. xref:fr@docs:ROOT:lexique-dyu.adoc#_sigiyɔrɔ[sigiyɔrɔ] be yi
* Hakɛto, xref:fr@docs:ROOT:lexique-dyu.adoc#_kɔ́ngɔ[kɔngɔ] bɛ an na. xref:fr@docs:ROOT:lexique-dyu.adoc#_domuni[domuni] bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_fèere[feere] xref:fr@docs:ROOT:lexique-dyu.adoc#_yàn[yan] wa?
* ɔn-hɔn, xref:fr@docs:ROOT:lexique-dyu.adoc#_domuni[domuni] bɛ yan. Nka a b’a xref:fr@docs:ROOT:lexique-dyu.adoc#_fɛ̀[fɛ] ka mun domun?
* xref:fr@docs:ROOT:lexique-dyu.adoc#_màlo[malo] ni tigadɛgɛnan
* Heeee, n balenmancɛ. Ile fana, xref:fr@docs:ROOT:lexique-dyu.adoc#_màlo[malo] ni tigadɛgɛnan tɛ sɔrɔ xref:fr@docs:ROOT:lexique-dyu.adoc#_yàn[yan] dɛh ! Yan ye maki le ye. Nanmanfenw tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_fèere[feere] yan
* O xref:fr@docs:ROOT:lexique-dyu.adoc#_tùma[tuman] na, alugu bɛ domunisugu xref:fr@docs:ROOT:lexique-dyu.adoc#_júman[jumɛn] le xref:fr@docs:ROOT:lexique-dyu.adoc#_fèere[feere] xref:fr@docs:ROOT:lexique-dyu.adoc#_yàn[yan] ?
* xref:fr@docs:ROOT:lexique-dyu.adoc#_yàn[yan] ye maki ye, sisɛsogo jɛninin ni xref:fr@docs:ROOT:lexique-dyu.adoc#_cɛkɛ[cɛkɛ] bɛ sɔrɔ xref:fr@docs:ROOT:lexique-dyu.adoc#_wála[walima] xref:fr@docs:ROOT:lexique-dyu.adoc#_jɛ́gɛ[jɛgɛ] jɛninin
* Ayiwa, an bena o sisɛsogo ni xref:fr@docs:ROOT:lexique-dyu.adoc#_jɛ́gɛ[jɛgɛ] domun
* Ka nan ni xref:fr@docs:ROOT:lexique-dyu.adoc#_jɛ́gɛ[jɛgɛ] xref:fr@docs:ROOT:lexique-dyu.adoc#_jóli[joli] ye?
* Alexia, Markus, alugu bɛ mun xref:fr@docs:ROOT:lexique-dyu.adoc#_fɛ̀[fɛ] ?
* (Alexia) Ne bena sisɛsogo ta nka Markus b’ɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_jɛ́gɛ[jɛgɛ] le fɛ
* N tericɛ, nan ni sisɛsogo yiranni ni xref:fr@docs:ROOT:lexique-dyu.adoc#_jɛ́gɛ[jɛgɛ] xref:fr@docs:ROOT:lexique-dyu.adoc#_kélen[kelen] ye
* xref:fr@docs:ROOT:lexique-dyu.adoc#_sìsɛ[sisɛ] xref:fr@docs:ROOT:lexique-dyu.adoc#_kélen[kelen] ye xref:fr@docs:ROOT:lexique-dyu.adoc#_kɛ̀mɛ[kɛmɛ] xref:fr@docs:ROOT:lexique-dyu.adoc#_séegi[seegi] nka xref:fr@docs:ROOT:lexique-dyu.adoc#_jɛ́gɛ[jɛgɛ] ye kɛmɛ naani
* A ka nyi, i be se ka nan n’o ye
* xref:fr@docs:ROOT:lexique-dyu.adoc#_cɛkɛ[cɛkɛ] do, a bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_síri[siri] xref:fr@docs:ROOT:lexique-dyu.adoc#_jóli[joli] fɛ?
* xref:fr@docs:ROOT:lexique-dyu.adoc#_cɛkɛ[cɛkɛ] ye mun ye?
* A kuman tɛ fɔ, a ye an nɛnɛn ka a xref:fr@docs:ROOT:lexique-dyu.adoc#_filɛ́[filɛ] dɔrɔn. A ka xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲi[nyi] ni sogosugu bɛɛ ye
* xref:fr@docs:ROOT:lexique-dyu.adoc#_síri[siri] saba bena an bɔ
* A ya xref:fr@docs:ROOT:lexique-dyu.adoc#_domuni[domuni] filɛ. Ala ye a suman a kɔnɔ
* Amina

(Après avoir pris le repas : o tlanin domunin na)

* N balenmancɛ, xref:fr@docs:ROOT:lexique-dyu.adoc#_cɛkɛ[cɛkɛ] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] dɛh. An tun xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] xref:fr@docs:ROOT:lexique-dyu.adoc#_dègi[degi] (deli) ka a xref:fr@docs:ROOT:lexique-dyu.adoc#_dómu[domun] fɔlɔ. An bena dɔ xref:fr@docs:ROOT:lexique-dyu.adoc#_sàan[san] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_tága[taga] ni a ye Canada
* N’i b’a xref:fr@docs:ROOT:lexique-dyu.adoc#_fɛ̀[fɛ] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_cɛkɛ[cɛkɛ] san, n bena cɛkɛ feereyɔɔrɔ xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲúman[nyuman] dɔ xref:fr@docs:ROOT:lexique-dyu.adoc#_yìra[yira] i la
* O bena xref:fr@docs:ROOT:lexique-dyu.adoc#_díya[diya] n ye kosɔbɛ. Dɔrɔ xref:fr@docs:ROOT:lexique-dyu.adoc#_júman[jumɛn] bɛ sɔrɔ yan?
* Ginɛsi, bofor, dorogba…Dɔrɔ xref:fr@docs:ROOT:lexique-dyu.adoc#_sugu[sugu] xref:fr@docs:ROOT:lexique-dyu.adoc#_cáman[caman] bɛ yan
* Dorogba wa? Balɔntanna dorogba ya dɔrɔ xref:fr@docs:ROOT:lexique-dyu.adoc#_fána[fana] bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_fèere[feere] xref:fr@docs:ROOT:lexique-dyu.adoc#_wà[wa] ?
* A ya dɔrɔ tɛ dɛh. Kɔdiwarikaw le ka a xref:fr@docs:ROOT:lexique-dyu.adoc#_tɔ́gɔ[tɔgɔ] xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] dɔɔrɔ tɔgɔ xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] xref:fr@docs:ROOT:lexique-dyu.adoc#_sábu[sabu] a k’a pibilisite kɛ telebizɔn na
* N ka a xref:fr@docs:ROOT:lexique-dyu.adoc#_fàamu[famu] sisan. xref:fr@docs:ROOT:lexique-dyu.adoc#_àyiwa[ayiwa] dorogba saba xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] an man
* Dorogba saba bɛ bɛn xref:fr@docs:ROOT:lexique-dyu.adoc#_kɛ̀mɛ[kɛmɛ] xref:fr@docs:ROOT:lexique-dyu.adoc#_náani[naani] ma

Après avoir bu: o tlanin dɔrɔmin na

* A bɛɛ kɛla xref:fr@docs:ROOT:lexique-dyu.adoc#_jóli[joli] ?
* xref:fr@docs:ROOT:lexique-dyu.adoc#_wága[waga] xref:fr@docs:ROOT:lexique-dyu.adoc#_kélen[kelen] ni xref:fr@docs:ROOT:lexique-dyu.adoc#_kɛ̀mɛ[kɛmɛ] wɔɔrɔ
* xref:fr@docs:ROOT:lexique-dyu.adoc#_wága[waga] xref:fr@docs:ROOT:lexique-dyu.adoc#_filà[fila] minan. Wariminsɛn ye i ta ye
* A ni ce. An bena xref:fr@docs:ROOT:lexique-dyu.adoc#_sègi[segi] xref:fr@docs:ROOT:lexique-dyu.adoc#_yàn[yan] xref:fr@docs:ROOT:lexique-dyu.adoc#_lón[lon] jumɛn?
* N xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] la a la ni an bena xref:fr@docs:ROOT:lexique-dyu.adoc#_sègi[segi] yan. An tagayɔrɔ ka ca. Nka bena i ya telefɔni nimero ta
* I bɛ se ka n xref:fr@docs:ROOT:lexique-dyu.adoc#_wele[wele] 08892439 kan. An bɛ ni Ala xref:fr@docs:ROOT:lexique-dyu.adoc#_sɔ̀n[sɔn] na
* An bɛ n balenmancɛ

== LA FORMULE GRAMMATICALE DU JOUR

Des phrases construites avec « bɛ/tɛ » qui expriment un constat, une remarque ou une interdiction.
En espérant que ces quelques exemples permettront de mieux appréhender les éventuelles nuances.

* xref:fr@docs:ROOT:lexique-dyu.adoc#_jí[ji] bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_fèere[feere] xref:fr@docs:ROOT:lexique-dyu.adoc#_yàn[yan] ne
* xref:fr@docs:ROOT:lexique-dyu.adoc#_jabibi[jabibi] bɛ sɔrɔ Bouake lɔgɔfɛba la
* Nanmanfen tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_fèere[feere] maki la
* Dɔrɔ bɛ min xref:fr@docs:ROOT:lexique-dyu.adoc#_lú[lu] xref:fr@docs:ROOT:lexique-dyu.adoc#_nín[nin] kɔnɔ xref:fr@docs:ROOT:lexique-dyu.adoc#_lón[lon] bɛɛ
* xref:fr@docs:ROOT:lexique-dyu.adoc#_móbili[mobili] tɛ timin xref:fr@docs:ROOT:lexique-dyu.adoc#_síra[sira] xref:fr@docs:ROOT:lexique-dyu.adoc#_nín[nin] kan
* xref:fr@docs:ROOT:lexique-dyu.adoc#_samara[samara] tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_dòn[don] xref:fr@docs:ROOT:lexique-dyu.adoc#_mìsiri[misiri] kɔnɔ abada
* xref:fr@docs:ROOT:lexique-dyu.adoc#_ba[ba] xref:fr@docs:ROOT:lexique-dyu.adoc#_nín[nin] xref:fr@docs:ROOT:lexique-dyu.adoc#_jɛ́gɛ[jɛgɛ] tɛ domun
* xref:fr@docs:ROOT:lexique-dyu.adoc#_cɛkɛ[cɛkɛ] tɛ sɔrɔ Canada
* Sirikɛrɛti tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_fèere[feere] yan
* Nyɛgɛnɛn tɛ kɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_yàn[yan] dɛh

== DIVERS

Discussions autour des éventuelles préoccupations de l’apprenant à propos des précédents cours ou de situations de communication qu’il souhaite mieux comprendre.
Toutes les questions sont les bienvenues.

