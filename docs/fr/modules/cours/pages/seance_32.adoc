---
title:  SÉANCE 32 COURS DE DIOULA 
author: Youssouf DIARRASSOUBA
date:  2020-10-11
type: doc
---

= SÉANCE 32 COURS DE DIOULA 
:author: Youssouf DIARRASSOUBA
:date: 2020-10-11
:type: post
:experimental:
:sectnums:
:sectnumlevels: 2
:lang: fr
:page-lang: {lang}
include::ROOT:partial$locale/attributes.adoc[]

[abstract]
== LECON 32 : xref:fr@docs:ROOT:lexique-dyu.adoc#_kàlansen[kalansen] xref:fr@docs:ROOT:lexique-dyu.adoc#_bí[bi] SABA NI FILANAN

== RAPPEL POINTS DE GRAMMAIRE

[loweralpha]
. Correction de l’exercice de la leçon précédente
+
Toutes les réponses sont correctes.
Cependant, la traduction de la première phrase de l’exercice 1 n’est pas correcte.
On va revoir ce point ensemble.
. Petite révision des expressions avec le suffixe « xref:fr@docs:ROOT:lexique-dyu.adoc#_tɔ̀[tɔ] »

* Sunɔgɔtɔ/ xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] xref:fr@docs:ROOT:lexique-dyu.adoc#_cáman[caman] sunɔgɔtɔ bɛ gɔrɔndɔ
* Bolitɔ/ xref:fr@docs:ROOT:lexique-dyu.adoc#_dén[den] bolitɔ xref:fr@docs:ROOT:lexique-dyu.adoc#_dòn[don] xref:fr@docs:ROOT:lexique-dyu.adoc#_ná[na] an xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ni xref:fr@docs:ROOT:lexique-dyu.adoc#_kúle[kule] ye
* xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] ɲumankɛtɔ le bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_ná[na] ni juguman ye xref:fr@docs:ROOT:lexique-dyu.adoc#_tùma[tuma] dɔw la
* Domunikɛtɔ = en mangeant.
* Kumakɛtɔ/Kumantɔ = en parlant.
* Baarakɛtɔ= en travaillant
* Yirantɔ = en faisant frire
* Barokɛtɔ = en causant/en echangeant

=== LES SPECIFICITES ET CONTEXTES D’EMPLOI DE « xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] » EN DIOULA

==== Le mot « xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] » pour evoquer le goût ou le caractère de ce qui est plaisant à la vue ou au toucher

* xref:fr@docs:ROOT:lexique-dyu.adoc#_baranda[baranda] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] kojugu
* xref:fr@docs:ROOT:lexique-dyu.adoc#_zagamɛn[zagamɛn] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] ni xref:fr@docs:ROOT:lexique-dyu.adoc#_jɛ́gɛ[jɛgɛ] ye
* Filimu xref:fr@docs:ROOT:lexique-dyu.adoc#_nín[nin] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] n xref:fr@docs:ROOT:lexique-dyu.adoc#_mùso[muso] ye/filimu ni ka di
* Lemuruji ka xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] xref:fr@docs:ROOT:lexique-dyu.adoc#_katimi[katimi] nyamankuji kan
* Sisɛsogo ka xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] ni salati ye

=== Exercice: Proposes 5 phrases du même type

==== Le mot « xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] » pour exprimer la simplicité ou la facilité à accomplir quelque chose

[qanda]
A xref:fr@docs:ROOT:lexique-dyu.adoc#_sùnɔgɔ[sunɔgɔ] ka di:: (il a une certaine facilité de s’endormir)
Cɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_nín[nin] xref:fr@docs:ROOT:lexique-dyu.adoc#_kàsi[kasi] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] kojugu:: (ce monsieur à une facilité de pleurer )
Coulibaly xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲínan[nyinan] ka di:: (Coulibaly oublie facilement)
N ya xref:fr@docs:ROOT:lexique-dyu.adoc#_lakɔlifa[lakɔlifa] xref:fr@docs:ROOT:lexique-dyu.adoc#_nɛ̀gɛ[nɛgɛ] xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] di:: (Il n’est pas facile de blaguer mon professeur)
Adama xref:fr@docs:ROOT:lexique-dyu.adoc#_kúnun[kunun] xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] di:: (il n’est pas facile de reveiller Adama)

[%colapsible]
. xref:fr@docs:ROOT:lexique-dyu.adoc#_kábini[kabini] a xref:fr@docs:ROOT:lexique-dyu.adoc#_mùso[muso] dencɛ wolola, a xref:fr@docs:ROOT:lexique-dyu.adoc#_sùnɔgɔ[sunɔgɔ] xref:fr@docs:ROOT:lexique-dyu.adoc#_mán[man] di
. i xref:fr@docs:ROOT:lexique-dyu.adoc#_jó[jo] le kelly xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲínan[nyinan] ka ki kɔsɔbe dɛ !
. xref:fr@docs:ROOT:lexique-dyu.adoc#_wári[wari] sɔrɔ xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] bouaké kɔnɔ

=== Exercice: Proposes 5 phrases du même type

Dans des contextes imagés (sens figurés)

[qanda]
A xref:fr@docs:ROOT:lexique-dyu.adoc#_nísɔ[nisɔ] ka di:: (il est de bonne humeur)
I xref:fr@docs:ROOT:lexique-dyu.adoc#_sèn[sen] ka di:: (Tu as la chance dans tes pieds. xref:fr@docs:ROOT:lexique-dyu.adoc#_tú[tu] tombes toujours au xref:fr@docs:ROOT:lexique-dyu.adoc#_bón[bon] moment)
I xref:fr@docs:ROOT:lexique-dyu.adoc#_kùn[kun] ka di:: (Tu as la chance)
I xref:fr@docs:ROOT:lexique-dyu.adoc#_bólo[bolo] ka di:: (tu es adroit)
I xref:fr@docs:ROOT:lexique-dyu.adoc#_dá[da] ka di:: (Tu sais parler ou xref:fr@docs:ROOT:lexique-dyu.adoc#_tú[tu] as l’appétit)

[%colapsible]
====
. xref:fr@docs:ROOT:lexique-dyu.adoc#_kábini[kabini] a ka xref:fr@docs:ROOT:lexique-dyu.adoc#_báara[baara] sɔrɔ, a xref:fr@docs:ROOT:lexique-dyu.adoc#_nísɔ[nisɔ] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] dɛ !
. an bena xref:fr@docs:ROOT:lexique-dyu.adoc#_láfiri[lafiri] ni xref:fr@docs:ROOT:lexique-dyu.adoc#_sìsɛ[sisɛ] domu.  i xref:fr@docs:ROOT:lexique-dyu.adoc#_sèn[sen] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] (i bɛn xref:fr@docs:ROOT:lexique-dyu.adoc#_ná[na] i sen ma.)
. i ya xref:fr@docs:ROOT:lexique-dyu.adoc#_bére[bere] sela xref:fr@docs:ROOT:lexique-dyu.adoc#_kɔ̀nɔnin[kɔnɔnin] ni ma, i xref:fr@docs:ROOT:lexique-dyu.adoc#_bólo[bolo] ka di.
. n be xref:fr@docs:ROOT:lexique-dyu.adoc#_fɛ̀[fɛ] ka baaro kɛ n'i ye.   i xref:fr@docs:ROOT:lexique-dyu.adoc#_dá[da] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] kɔsɔbe.
. ...i xref:fr@docs:ROOT:lexique-dyu.adoc#_kùn[kun] ka di. 
====
