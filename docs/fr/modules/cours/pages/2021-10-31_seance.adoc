---
title: SEANCE 2021-10-31
author: Youssouf DIARRASSOUBA
date: 2021-10-31
draft: false 
categories: []
tags: []
---

= SEANCE 2021-10-31
:author: Youssouf DIARRASSOUBA
:date: 2021-10-31
:experimental:
:sectnums:
:sectnumlevels: 2
:lang: fr
:page-lang: {lang}
include::ROOT:partial$locale/attributes.adoc[]


== REVISION DE POINT DE GRAMMAIRE

CONSIGNES : Après lecture des exemples, construire pour chaque point évoqué dix (10) autres exemples

=== POINT 1 : Le suffixe "man" signifie : un état ou qui a la quaité de...

cɛman:: le mâle, (qui a la qualité de mâle)
musoman:: la femelle
nɛgɛman:: qui est en métal
nɔgɔman:: qui est facile
nɔnɔman:: qui contient du lait
juguman:: qui est méchant
fɛngɛman:: qui est léger
gbiliman:: qui est lourd

=== POINT 2 : "Ala ma" est une expression utilisée pour introduire un vœu, suivi de la forme conjuguée au passé du verbe transitif ou intransitif

"Ala xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] an kodiya la ŋɔgɔn ye".:: Que Dieu fasse que nous nous aimons.
"Ala m’an xref:fr@docs:ROOT:lexique-dyu.adoc#_kísi[kisi] la"::  Que Dieu nous sauve

=== POINT 3 : "min " est un pronom relatif. On l’utilise comme en français.

"N bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_mùso[muso] min xref:fr@docs:ROOT:lexique-dyu.adoc#_lɔ́n[lɔn] kɔsɔbɛ, o ye xref:fr@docs:ROOT:lexique-dyu.adoc#_kàramɔgɔ[karamɔgɔ] ye kalansoba la".:: La femme que je connais très bien, elle est enseignante à l’université.
" xref:fr@docs:ROOT:lexique-dyu.adoc#_tága[taga] a fɛ, xref:fr@docs:ROOT:lexique-dyu.adoc#_kìbaro[kibaroya] min b’a xref:fr@docs:ROOT:lexique-dyu.adoc#_fɛ̀[fɛ] a xref:fr@docs:ROOT:lexique-dyu.adoc#_bɛna[bɛna] o xref:fr@docs:ROOT:lexique-dyu.adoc#_fó[fɔ] i ye".:: Va chez elle, elle te dira la nouvelle qu’elle a.

== SUJET DE DEBAT (AN YA xref:fr@docs:ROOT:lexique-dyu.adoc#_bí[bi] BARO)

=== Dɔrɔgimin (La consommation de la drogue)

. Dɔrɔgi ye mun ye ?
. Muna xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] dɔw bɛ dɔrɔgi min ?
. Ile xref:fr@docs:ROOT:lexique-dyu.adoc#_dègi[degi] la ka dɔrɔgi nɛnɛn xref:fr@docs:ROOT:lexique-dyu.adoc#_wà[wa] ?
. Dɔrɔgimin bɛ mun xref:fr@docs:ROOT:lexique-dyu.adoc#_láse[lase] xref:fr@docs:ROOT:lexique-dyu.adoc#_ádamaden[adamaden] ma?
. xref:fr@docs:ROOT:lexique-dyu.adoc#_nàfa[nafa] bɛ dɔrɔgimin xref:fr@docs:ROOT:lexique-dyu.adoc#_ná[na] wa?
. Ile be se ka ladilikan xref:fr@docs:ROOT:lexique-dyu.adoc#_júman[jumɛn] xref:fr@docs:ROOT:lexique-dyu.adoc#_fó[fɔ] dɔrɔgiminaw ye?
. xref:fr@docs:ROOT:lexique-dyu.adoc#_jàmana[jamana] bɛ sɔrɔ xref:fr@docs:ROOT:lexique-dyu.adoc#_dúniɲa[dununya] kɔnɔ dɔrɔgimin xref:fr@docs:ROOT:lexique-dyu.adoc#_daganin[daganin] bɛ min kɔnɔ wa?
. Ile xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲìnɛ[nyana] kafe farimanw ye dɔrɔgi xref:fr@docs:ROOT:lexique-dyu.adoc#_sugu[sugu] dɔ ye wa?
. Ile bɛ se ka jɛn ni dɔrɔgiminaw ye wa? (muna?)
. A ka xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲi[nyi] jɛkulu dɔw bɛ dɔrɔgiminaw xref:fr@docs:ROOT:lexique-dyu.adoc#_dɛ̀mɛ[dɛmɛ] xref:fr@docs:ROOT:lexique-dyu.adoc#_wà[wa] ?
. Dɔrɔgifeerelaw ka ca dugubaw la xref:fr@docs:ROOT:lexique-dyu.adoc#_wála[walima] dugudeninw?
. I dege (deli) la ka dɔrɔgi dɔ xref:fr@docs:ROOT:lexique-dyu.adoc#_tɔ́gɔ[tɔgɔ] xref:fr@docs:ROOT:lexique-dyu.adoc#_mɛ́n[mɛn] wa?
. Ile xref:fr@docs:ROOT:lexique-dyu.adoc#_hákiri[hakili] la, dɔrɔgiminaw ka ca jamankulu xref:fr@docs:ROOT:lexique-dyu.adoc#_júman[jumɛn] na?
. Dɔw ko ko dɔrɔgi xref:fr@docs:ROOT:lexique-dyu.adoc#_hakɛya[hakɛya] fitinin bɛ sɔrɔ xref:fr@docs:ROOT:lexique-dyu.adoc#_flá[fura] dɔw kɔnɔ (la). xref:fr@docs:ROOT:lexique-dyu.adoc#_tunyan[tunyan] xref:fr@docs:ROOT:lexique-dyu.adoc#_wà[wa] xref:fr@docs:ROOT:lexique-dyu.adoc#_wúya[wuya] ?
