---
title: Jabari ni baara
author: Soumahoro 
date: 2020-12-04
type: doc 
draft: false
tags: ["dialogue", "leçons" ]
categories: ["Julakan"]
---

= Jabari ni xref:fr@docs:ROOT:lexique-dyu.adoc#_báara[baara] 
:author: Boyd Kelly
:date: 2020-12-04
:description: "Cours de dioula Jabari ni baara" 
:sectnums:
:sectnumlevels: 2
:keywords: jula, dyula, dioula, kodiwari, cours, langue, afrique, côte d'ivoire
:category: ["Julakan"]
:lang: fr 
include::ROOT:partial$locale/attributes.adoc[]

== Baro

.Jabari ni baara

[horizontal]
Salif::
I ni su, n tericɛ!

Dau::
N ba! Ala su hɛrɛ! 
So xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] do?

Salif::
O be ka kɛnɛ

Dau::
Dɔ xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] sufɛ.

Salif::
Juguman tɛ ye.
Ɲ mako b'i la.

Dau::
I sigi, bisimila.

Salif::
N xref:fr@docs:ROOT:lexique-dyu.adoc#_jàrabi[jarabi] n ya patɔrɔn denmuso la.
N xref:fr@docs:ROOT:lexique-dyu.adoc#_ba[ba] xref:fr@docs:ROOT:lexique-dyu.adoc#_fɛ̀[fɛ] ka furu.

Dau:: 
Kabako! 
I ko di?
I jarabila i ya patɔrɔn denmuso la?

Salif::
ɔn-hɔn!
Patron denmuso Binta

Dau::
ɔn n tericɛ!
Kuman xref:fr@docs:ROOT:lexique-dyu.adoc#_nín[nin] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_gwɛ̀lɛ[gwɛlɛ] kosɔbɛ.

Salif::
N xref:fr@docs:ROOT:lexique-dyu.adoc#_séko[seko] tɛ tuguni.
Jarabi

Dau:: 
ɔn n'tericɛ!
Kutubu! 
Jarabi yɛlɛmana xref:fr@docs:ROOT:lexique-dyu.adoc#_bàna[bana] ye wa?
I bena i ya xref:fr@docs:ROOT:lexique-dyu.adoc#_báara[baara] xref:fr@docs:ROOT:lexique-dyu.adoc#_cí[ci] wa?

Salif::
Ɔn-ɔn xref:fr@docs:ROOT:lexique-dyu.adoc#_jàrabi[jarabi] le dɔrɔn.
N xref:fr@docs:ROOT:lexique-dyu.adoc#_sɔ̀n[sɔn] xref:fr@docs:ROOT:lexique-dyu.adoc#_hákiri[hakili] xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲúman[ɲuman] la.

Dau::
Binta ka a xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] i xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] wa?

Salif:: 
Ɔn-hɔn, Binta ko ko a be n'fɛ.

Dau::
O ka nɔgɔ!
I be xref:fr@docs:ROOT:lexique-dyu.adoc#_kélen[kelen] ta i ya xref:fr@docs:ROOT:lexique-dyu.adoc#_báara[baara] ni xref:fr@docs:ROOT:lexique-dyu.adoc#_jàrabi[jarabi] lo.

Salif::
N'bɛna ow xref:fr@docs:ROOT:lexique-dyu.adoc#_filà[fila] ta.

Dau::
Cɛ, i xref:fr@docs:ROOT:lexique-dyu.adoc#_kùn[kun] ka gwelɛ!

== Exercises

* Ɲiningali
. Salif xref:fr@docs:ROOT:lexique-dyu.adoc#_téri[tericɛ] xref:fr@docs:ROOT:lexique-dyu.adoc#_tɔ́gɔ[tɔgɔ] ye di?
. Salif xref:fr@docs:ROOT:lexique-dyu.adoc#_hàminanko[haminako] ye mu ye?
. Salif ya patɔrɔn denmuso xref:fr@docs:ROOT:lexique-dyu.adoc#_tɔ́gɔ[tɔgɔ] ye di?
. I ya xref:fr@docs:ROOT:lexique-dyu.adoc#_hákiri[hakili] la, Salif bɛ se ka xref:fr@docs:ROOT:lexique-dyu.adoc#_súnguru[sunguru] ni xref:fr@docs:ROOT:lexique-dyu.adoc#_fúru[furu] wa?

.Jabili
[%collapsible]
====
. Salif xref:fr@docs:ROOT:lexique-dyu.adoc#_téri[tericɛ] xref:fr@docs:ROOT:lexique-dyu.adoc#_tɔ́gɔ[tɔgɔ] ye dau.
. Salif jarabila a ya patɔrɔn denmuso la
. Salif ya patɔrɔn denmuso xref:fr@docs:ROOT:lexique-dyu.adoc#_tɔ́gɔ[tɔgɔ] ye Binta.
. N ya hakiliu la, Salif bena xref:fr@docs:ROOT:lexique-dyu.adoc#_súnguru[sunguru] ni furu.
====
