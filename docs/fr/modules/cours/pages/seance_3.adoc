---
title:  SÉANCE 3 COURS DE DIOULA 
author: Youssouf DIARRASSOUBA
date:  2020-05-05
type: doc
---

= SÉANCE 3 COURS DE DIOULA 
:author: Youssouf DIARRASSOUBA
:date: 2020-05-05
:type: post
:experimental:
:sectnums:
:sectnumlevels: 2
:lang: fr
:page-lang: {lang}
include::ROOT:partial$locale/attributes.adoc[]

[abstract]
== LECON 3 : xref:fr@docs:ROOT:lexique-dyu.adoc#_kàlansen[kalansen] SABANAN
--
L’OBJECTIF DE CETTE LECON EST DE PARVENIR A MAITRISER LES EMPLOIS CONTEXTUELS DU VERBE "ÊTRE" EN DIOULA VEHICULAIRE DE COTE D’IVOIRE
--

Au cours de cette leçon, nous allons étudier le verbe « être » dans ces trois modes d’emploi en dioula de Côte d’Ivoire.
Il faut tout de suite souligner que le contexte détermine les usages de ce verbe dans les langues mandingues en général et dans le dioula véhiculaire de Côte d’Ivoire en particulier.

Veuillez trouver ci-dessous les différents contextes d’emploi :

== LOCALISATION

=== Dialogue introductif

[horizontal]
Youssouf:: I ni xref:fr@docs:ROOT:lexique-dyu.adoc#_sɔ̀gɔma[sɔgɔma] Coulibaly
Coulibaly:: N’ba, i ni sɔgɔma
Youssouf:: xref:fr@docs:ROOT:lexique-dyu.adoc#_hɛ́rɛ[hɛrɛ] bɛ?
Coulibaly:: Hɛrɛ, i bɛ mini ?
Youssouf:: N bɛ so kɔnɔ
Coulibaly:: I bɛ so kɔnɔ ni xref:fr@docs:ROOT:lexique-dyu.adoc#_jɔ́n[jɔn] ye?
Youssouf:: N bɛ ni n ya sofɛricɛ ye so kɔnɔ. Ile do?
Coulibaly:: Ne bɛ ni n ya xref:fr@docs:ROOT:lexique-dyu.adoc#_wùlu[wulu] ye
Youssouf:: Ayiwa, k’an bɛ
Coulibaly:: K’an bɛ Youssouf
 
« bɛ » dans un contexte de localisation traduit le verbe être en dioula de Côte d’Ivoire.
Sa forme negative est « tɛ »

=== Exemples

* N bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_baarakɛyɔrɔ[baarakɛyɔrɔ] la
* Ali bɛ so kɔnɔ
* Moussa bɛ a ya butiki kɔnɔ
* Coulibaly bɛ Bouaké
* Youssouf bɛ Abidjan
* A bɛ ni a xref:fr@docs:ROOT:lexique-dyu.adoc#_mùso[muso] ye Canada
* Daresalam xref:fr@docs:ROOT:lexique-dyu.adoc#_limamu[limamu] bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_mìsiri[misiri] kɔnɔ
* Sogofeerelaw bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_lɔgɔfiyɛ[lɔgɔfɛ] la
* N bangebagaw bɛ Ameriki
* Youssouf ya xref:fr@docs:ROOT:lexique-dyu.adoc#_baarakɛyɔrɔ[baarakɛyɔrɔ] bɛ Cocody

Lire les formes négatives de ces phrases

Pour les travaux pratiques, on aura besoin de connaitre quelques circonstants de lieu.
Il s’agit des différentes unités de la langue qui permettent de situer les éléments qui entourent le locuteur.

[width="100%",cols="2",frame="none",options="none",stripes="even"]
|===
|san fɛ|en haut
|dugu man|en bas, par terre
|kun na|au dessus, sur
|nya fɛ|devant, a l’avant
|kεrε fɛ|a coté de
|Kan|sur,
|la, na|a, en, dans, au
|===

=== Exercice de traduction

. Je suis chez mon professeur
. Où est le jouet de mon fils ?
. Qu’est qui est dans le sac ?
. Il n’y a rien dans xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] poche
. Où se situe le marché du Plateau ?
. Nous sommes chez xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] mère Aminata
. Ils sont tous chez le voisin de mon amie
. Yousouf et xref:fr@docs:ROOT:lexique-dyu.adoc#_sòn[son] ami Moussa sont en face de la boutique
. Je ne suis pas chez moi, je suis à l’hôpital
. Il est ici avec mon fils.
. Le chauffeur de mon père est dans la voiture
. Le repas est sur la table.


== LA PRESENTATION

=== Dialogue introductif

{zwsp}:: I ni sɔgɔma, n balimancɛ
{zwsp}:: N’ba i ni sɔgɔma. xref:fr@docs:ROOT:lexique-dyu.adoc#_hɛ́rɛ[hɛrɛ] bɛ ?
{zwsp}:: Hɛrɛ, hakɛto I xref:fr@docs:ROOT:lexique-dyu.adoc#_tɔ́gɔ[tɔgɔ] ye di?
{zwsp}:: N xref:fr@docs:ROOT:lexique-dyu.adoc#_tɔ́gɔ[tɔgɔ] ye Youssouf, ile do?
{zwsp}:: Ne xref:fr@docs:ROOT:lexique-dyu.adoc#_tɔ́gɔ[tɔgɔ] ye Coulibaly. Ile ye xref:fr@docs:ROOT:lexique-dyu.adoc#_jɔ́n[jɔn] xref:fr@docs:ROOT:lexique-dyu.adoc#_téri[tericɛ] ye?
{zwsp}:: Ne ye Moussa xref:fr@docs:ROOT:lexique-dyu.adoc#_téri[tericɛ] ye. N bɛ bɔ Faransi, ile do?
{zwsp}:: Ne bɛ bɔ kɔdiwari. I baarakɛnyɔgɔnw bɛ Faransi xref:fr@docs:ROOT:lexique-dyu.adoc#_wála[walima] Kɔdiwari ?
{zwsp}:: O bɛɛ bɛ Kɔdiwari yan


Dans le contexte de la présentation, le verbe être est matérialisé par la forme discontinue « ye….ye ». La forme négative est « tɛ….ye »

=== Exemples

. Ne ye Moussa xref:fr@docs:ROOT:lexique-dyu.adoc#_badencɛ[badencɛ] ye
. N ye Amidu dɔgɔcɛ ye
. Ali ye xref:fr@docs:ROOT:lexique-dyu.adoc#_dɔgɔtɔrɔ[dɔtɔrɔ] ye
. Youssouf ye xref:fr@docs:ROOT:lexique-dyu.adoc#_kalanfa[kalanfa] ye
. Drissa dencɛ ye xref:fr@docs:ROOT:lexique-dyu.adoc#_sɛnɛkɛla[sɛnɛkɛla] ye
. Aminata ye gbakɛla ye
. xref:fr@docs:ROOT:lexique-dyu.adoc#_cɛ́[cɛ] xref:fr@docs:ROOT:lexique-dyu.adoc#_nín[nin] ye Assetou xref:fr@docs:ROOT:lexique-dyu.adoc#_fà[facɛ] ye
. Barakissa ye Alidou xref:fr@docs:ROOT:lexique-dyu.adoc#_mùso[muso] ye
. Coulibaly ye Youssouf ya xref:fr@docs:ROOT:lexique-dyu.adoc#_kàlanden[kalanden] ye
. Birahima ye Minisiri ya sofɛricɛ ye

Lire les formes négatives de ces phrases

=== Exercice de traduction

Phrases à traduire

. Qui est ce monsieur?
. Qui est ton père?
. Nous sommes les amis de ton fils Noam
. Je suis le professeur de Madou
. Ali n’est pas cultivateur, il est médecin
. Qui est derrière la voiture de Mariam ?
. Aujourd’hui est mardi
. Le fils de l’ami de Moussa s’appelle Ibrahim
. La fille de la tante de mon chauffeur est élève
. Le professeur de xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] fille est français.

.Traductions
[%collapsible]
====
. xref:fr@docs:ROOT:lexique-dyu.adoc#_cɛ́[cɛ] xref:fr@docs:ROOT:lexique-dyu.adoc#_nín[nin] ye xref:fr@docs:ROOT:lexique-dyu.adoc#_jɔ́n[jɔn] ye?
. I xref:fr@docs:ROOT:lexique-dyu.adoc#_fà[facɛ] ye xref:fr@docs:ROOT:lexique-dyu.adoc#_jɔ́n[jɔn] ye?
. An ye i dencɛ Noam xref:fr@docs:ROOT:lexique-dyu.adoc#_téri[teri] cɛw ye.
. N ye Madu ya xref:fr@docs:ROOT:lexique-dyu.adoc#_lakɔlifa[lakɔlifa] ye.
. Ali tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_sɛnɛkɛla[sɛnɛkɛla] ye, a ye dɔgɔtoɔrɔ ye
. xref:fr@docs:ROOT:lexique-dyu.adoc#_jɔ́n[jɔn] bɛ Mariam ya xref:fr@docs:ROOT:lexique-dyu.adoc#_móbili[mobili] kɔfe ?
. xref:fr@docs:ROOT:lexique-dyu.adoc#_bí[bi] ye taratalon ye.
. Musa xref:fr@docs:ROOT:lexique-dyu.adoc#_téri[tericɛ] dencɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_tɔ́gɔ[tɔgɔ] ye Ibrahim.
. N ya sofericɛ tɛnɛmuso denmuso ye kalenden ye.
. N denmuso ya xref:fr@docs:ROOT:lexique-dyu.adoc#_lakɔlifa[lakɔlifa] ye tubabumuso ye.
====

== QUALIFICATIF

=== Dialogue introductif

[horizontal]
{zwsp}:: I ni sɔgɔma, n balenmancɛ
{zwsp}:: N’ba i ni sɔgɔma. xref:fr@docs:ROOT:lexique-dyu.adoc#_hɛ́rɛ[hɛrɛ] bɛ ?
{zwsp}:: Hɛrɛ, hakɛto I xref:fr@docs:ROOT:lexique-dyu.adoc#_tɔ́gɔ[tɔgɔ] ye di?
{zwsp}:: N xref:fr@docs:ROOT:lexique-dyu.adoc#_tɔ́gɔ[tɔgɔ] ye Youssouf, ile do?
{zwsp}:: Ne xref:fr@docs:ROOT:lexique-dyu.adoc#_tɔ́gɔ[tɔgɔ] ye Coulibaly. Ile ye xref:fr@docs:ROOT:lexique-dyu.adoc#_jɔ́n[jɔn] xref:fr@docs:ROOT:lexique-dyu.adoc#_téri[tericɛ] ye?
{zwsp}:: Ne ye Moussa xref:fr@docs:ROOT:lexique-dyu.adoc#_téri[tericɛ] ye. N bɛ bɔ Faransi, ile do?
{zwsp}:: Ne bɛ bɔ kɔdiwari. I baarakɛnyɔgɔnw bɛ Faransi xref:fr@docs:ROOT:lexique-dyu.adoc#_wála[walima] Kɔdiwari ?


Dans le cadre de la description d’un objet, d’une personne ou de valeurs abstraites, le verbe être est marqué par « ka ».
Sa forme négative est « xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] »

=== Exemples

. Ali xref:fr@docs:ROOT:lexique-dyu.adoc#_mùso[muso] dɔgɔcɛ ka bon
. N ya xref:fr@docs:ROOT:lexique-dyu.adoc#_lakɔlifa[lakɔlifa] ka jan
. Coulibaly ka xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲi[ɲi] kosɔbɛ
. Fanta xref:fr@docs:ROOT:lexique-dyu.adoc#_cɛ́[cɛ] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_júgu[jugu] kojugu
. Alimata ya xref:fr@docs:ROOT:lexique-dyu.adoc#_gwà[gba] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] dêh
. Legilisi ka xref:fr@docs:ROOT:lexique-dyu.adoc#_sùrun[surun] a ya so la
. xref:fr@docs:ROOT:lexique-dyu.adoc#_cɛ́[cɛ] xref:fr@docs:ROOT:lexique-dyu.adoc#_nín[nin] ya xref:fr@docs:ROOT:lexique-dyu.adoc#_bón[bon] ka bon
. xref:fr@docs:ROOT:lexique-dyu.adoc#_móbili[mobili] xref:fr@docs:ROOT:lexique-dyu.adoc#_nín[nin] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_téli[teli] (teri)
. Moussa ya butiki ka dɔgɔ
. Lemurukumun ka kuna

Lire les versions négatives de ces phrases

Exercice de traduction

. Mon frère n’est pas grand
. Le fils de xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] sœur est beau
. La cuisine de xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] femme est bonne
. L’amie de mon frère est très gentille
. Cette mangue est amère

.Traduction
[%collapsible]
====
. N xref:fr@docs:ROOT:lexique-dyu.adoc#_badencɛ[badencɛ] xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] bon
. N xref:fr@docs:ROOT:lexique-dyu.adoc#_badenmuso[badenmuso] dencɛ ka ɲi
. N xref:fr@docs:ROOT:lexique-dyu.adoc#_mùso[muso] xref:fr@docs:ROOT:lexique-dyu.adoc#_gwà[gba] ka di
. N xref:fr@docs:ROOT:lexique-dyu.adoc#_badencɛ[badencɛ] xref:fr@docs:ROOT:lexique-dyu.adoc#_téri[terimuso] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] kosɔbɛ
. xref:fr@docs:ROOT:lexique-dyu.adoc#_mángoro[mangoro] xref:fr@docs:ROOT:lexique-dyu.adoc#_nín[nin] ka kumu??
====

Je me présente :

. Bonjour.
. Je m’appelle .
. Je viens du Canada.
. Je suis le père de…
. Mon père est à Abidjan.
. Mon lieu de travail est situé à Bouaké.
. Je suis beau et grand (de taille).
. L’Anglais est xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] langue maternelle.

.Traduction
[%collapsible, none]
====
. A ni sɔgɔma
. N xref:fr@docs:ROOT:lexique-dyu.adoc#_tɔ́gɔ[tɔgɔ] ye Kelly
. N be bɔ Canada.
. N ye ali xref:fr@docs:ROOT:lexique-dyu.adoc#_fà[facɛ] ye.
. N xref:fr@docs:ROOT:lexique-dyu.adoc#_fà[facɛ] be Abidjan
. N ya xref:fr@docs:ROOT:lexique-dyu.adoc#_baarakɛyɔrɔ[baarakɛyɔrɔ] be buaké
. N ka xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲi[ɲi] ani n ka jan.
. Anglekan ye n ya xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ye
====

