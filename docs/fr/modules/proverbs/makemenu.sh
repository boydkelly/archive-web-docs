#!/bin/bash
export component="julakan"
export module=${PWD##*/}
export menutitle="$(tr '[:lower:]' '[:upper:]' <<< ${module:0:1})${module:1}"

echo $module
export prefix="** xref:$component:$module:"
export basedir=./pages
export menu=../nav.adoc

pushd $basedir > /dev/null

runFind(){
  find . -maxdepth 1 -name "*.adoc" -exec bash -c 'orderByDate "$@"' bash {} \;
}

orderByDate(){
  local file="$1"
  unset postdate
  postdate=`awk '$1 == ":date:" {print $2}' "$file"`
  [[ -n $postdate ]] &&  /bin/touch -a --date=$postdate "$file" 
}

makeMenu(){
  cat <<EOF > $menu
* $menutitle 
EOF

SAVEIFS=$IFS
IFS=$(echo -en "\n\b")
for file in `ls -ut *.adoc`; do
  type=`awk '$1 == ":type:" {print $2}' "$file"`
  if [[ $type == "post" ]]; then
    #description=`awk '$1 == ":description:" {print $2" "$3" "$4" "$5}' $file`
    title=`sed -n /^\=\ .*/p "$file"|sed 's/^= //g'` 
    echo "${prefix}${file}"["${title}"]"" >> $menu
  fi
done
IFS=$SAVEIFS
}

export -f makeMenu
export -f orderByDate

runFind
makeMenu

#remove unwanted stuff
sed -i '/latest/d' $menu
sed -i '/index.adoc/d' $menu
git commit --quiet $menu -m "update menu $(date -Im)"
