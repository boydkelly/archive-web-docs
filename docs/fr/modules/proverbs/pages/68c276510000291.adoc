---
title: Mande zanaw
author: Moussa Diakité
date: 2020-12-27
type: doc
draft: false
categories: []
tags: []
---
= Mande zanaw
:author: Inconnu
:date: 2020-12-27
:type: article
:sectnums:
:sectnumlevels: 2
:lang: fr
include:julakan:ROOT:partial$locale/attributes.adoc[]

== Proverbes africains en xref:fr@docs:ROOT:lexique-dyu.adoc#_jula[jula] de Cote d’ivoire

[verse]
____
Zana xref:fr@docs:ROOT:lexique-dyu.adoc#_kɔ́rɔ[kɔrɔ] tɛ fɔ
A xref:fr@docs:ROOT:lexique-dyu.adoc#_kɔ́rɔ[kɔrɔ] bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲíni[ɲini] le, k'a sɔrɔ
Tiɲɛ dɔ xref:fr@docs:ROOT:lexique-dyu.adoc#_lè[lo ?] xref:fr@docs:ROOT:lexique-dyu.adoc#_tíɲɛ[tiɲɛ] xref:fr@docs:ROOT:lexique-dyu.adoc#_wɛ̀rɛ[wɛrɛ] tɛ min kɔ
Tiɲɛ dɔ xref:fr@docs:ROOT:lexique-dyu.adoc#_lè[lo ?] min tɛ sɔsɔ
Tiɲɛ dɔ xref:fr@docs:ROOT:lexique-dyu.adoc#_lè[lo ?] xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] tɛ min fɔwagati lɔn
Tiɲɛ dɔ xref:fr@docs:ROOT:lexique-dyu.adoc#_lè[lo ?] min xref:fr@docs:ROOT:lexique-dyu.adoc#_jó[jo] ni xref:fr@docs:ROOT:lexique-dyu.adoc#_jaraki[jaraki] yira
Tiɲɛ dɔ xref:fr@docs:ROOT:lexique-dyu.adoc#_lè[lo ?] bɛɛ bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_sɔ̀n[sɔn] min ma
Tiɲɛ bɛɛ ta xref:fr@docs:ROOT:lexique-dyu.adoc#_tíɲɛ[tiɲɛ] lo
Tiɲɛ dɔ xref:fr@docs:ROOT:lexique-dyu.adoc#_lè[lo ?] xref:fr@docs:ROOT:lexique-dyu.adoc#_ádamaden[adamaden] bɛɛ bɛnna min kan
Minw ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kúma[kuma] xref:fr@docs:ROOT:lexique-dyu.adoc#_gundo[gundo] bɛɛ lɔn, olugu xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] lo
Minw bɛ hakiritigiya xref:fr@docs:ROOT:lexique-dyu.adoc#_fɛ̀[fɛ] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_tɛ̀mɛ[tɛmɛ] sanin ni xref:fr@docs:ROOT:lexique-dyu.adoc#_wári[wari] kan
Zana bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_jàabi[jaabi] ni zana le ye
Mɔgɔ kɔrɔbaw ta xref:fr@docs:ROOT:lexique-dyu.adoc#_kúma[kuma] lo
Min b'a xref:fr@docs:ROOT:lexique-dyu.adoc#_fó[fɔ] a ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kɔ́rɔ[kɔrɔ] ni o xref:fr@docs:ROOT:lexique-dyu.adoc#_tìgi[tigi] xref:fr@docs:ROOT:lexique-dyu.adoc#_yɛ̀rɛ[yɛrɛ] ye
Kuma sɔsɔbari lo, xref:fr@docs:ROOT:lexique-dyu.adoc#_kúma[kuma] gbɛnin lo.
A dɔw ye tagamasiyɛnw le ye
Dɔw ye xref:fr@docs:ROOT:lexique-dyu.adoc#_kúma[kuma] kɔrɔmanw ye
Dɔw ye dɔnkiri ye, dɔw ye dɔnsen ye.
Ala ta xref:fr@docs:ROOT:lexique-dyu.adoc#_dáfen[danfɛn] bɛɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] lo.
Manden ko:
Kuma kɔrɔman bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_fó[fɔ] a xref:fr@docs:ROOT:lexique-dyu.adoc#_kɔ́rɔ[kɔrɔ] lɔnbaga le ye.
____

== Hakiritigikuma / Paroles de sage

[width="80%",cols="2",frame="none",options="none",stripes="even"]
|====
|Hakiritigi xref:fr@docs:ROOT:lexique-dyu.adoc#_filà[fla] le bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_sisɛfan[sisɛfan] xref:fr@docs:ROOT:lexique-dyu.adoc#_fìri[firi] xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲɔ́gɔn[ɲɔgɔn] fɛ.|C'est deux hommes sages qui peuvent se lancer un oeuf.
|Dɔ ta kasiko ye dɔ ta yɛrɛko ko ye.|Ce qui fait pleurer l'un, fait rire l'autre.
|Mɔgɔw k'i xref:fr@docs:ROOT:lexique-dyu.adoc#_lɔ́n[lɔn] ni turunin min ye n'i ka o li, mɔgɔw bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_fìri[firi] i ma.|La coupe de cheveux avec laquelle on te connaît, si xref:fr@docs:ROOT:lexique-dyu.adoc#_tú[tu] l'enlèves, les gens ne te reconnaîtront plus.
|Sɔn tɛ pan, a xref:fr@docs:ROOT:lexique-dyu.adoc#_dén[den] ye ŋunuman.|L'antilope saute, xref:fr@docs:ROOT:lexique-dyu.adoc#_sòn[son] petit ne va pas ramper.
|Boro xref:fr@docs:ROOT:lexique-dyu.adoc#_filà[fla] le bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲɔ́gɔn[ɲɔgɔn] ko.|Les deux mains se lavent mutuellement
|N’tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_blà[bla] ɲa, n’tɛ bla kɔ, aw xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] xref:fr@docs:ROOT:lexique-dyu.adoc#_filà[fla] lo.|Il faut que vous soyez trois en chemin pour dire: "Je ne veux ni être devant, ni être derrière!"
|Yɛrɛɲini ka xref:fr@docs:ROOT:lexique-dyu.adoc#_fisa[fisa] ni tɔgɔɲini ye.|Chercher sa vie est mieux que de chercher un nom.
|Sufɛkunli lawurira torotigɛ le ma.|La vraie raison pour raser la tête la nuit c'est de couper une oreille.
|Ni Ala ka xref:fr@docs:ROOT:lexique-dyu.adoc#_sèn[sen] xref:fr@docs:ROOT:lexique-dyu.adoc#_kari[kari] a b’a tagamacogo yira.|Si Dieu casse le pied, il montre comment marcher avec.
|Den bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_bèn[ben] a minabaga le boro.|L'enfant ne tombe que de la main de celui qui le prend.
|Mɔgɔ ɲumankɛtɔ bɛ juguman kɛ.|En faisant le bien, parfois il arrive du mal.
|N’i ka paraw ni bɔrɔkɔnɔbla bɛɛ kɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_kélen[kelen] ye, i xref:fr@docs:ROOT:lexique-dyu.adoc#_bɛna[bɛna] fɛn xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲánaman[ɲanaman] dɔ xref:fr@docs:ROOT:lexique-dyu.adoc#_blà[bla] i xref:fr@docs:ROOT:lexique-dyu.adoc#_yɛ̀rɛ[yɛrɛ] kun.|Si dès le premier coup de bâton xref:fr@docs:ROOT:lexique-dyu.adoc#_tú[tu] mets l'animal dans le sac, tu finiras par mettre dans ton sac un animal encore vivant.
|Sin tɛ kɔnɔ fɛ, nka Ala b’a xref:fr@docs:ROOT:lexique-dyu.adoc#_dén[den] baro.|L'oiseau n'a pas de mamelles, mais Dieu nourrit xref:fr@docs:ROOT:lexique-dyu.adoc#_sòn[son] petit.
|Kɔnɔ xref:fr@docs:ROOT:lexique-dyu.adoc#_dùgu[dugu] tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_sàan[san] ye.|Le village de l'oiseau n'est pas dans le ciel.
|A bɛ fɛntigiw xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲana[ɲana] ko fangantanw bɛ tugura le.|Le riches pensent que les pauvres font exprès.
|Yirinin min bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_kuru[kuru] kan, a bɛ ale xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲana[ɲana] ko a ka xref:fr@docs:ROOT:lexique-dyu.adoc#_ján[jan] ni yiri tɔw bɛɛ ye.|L'arbre qui est sur la montagne pense qu'il est le plus grand de tous les arbres.
|« A xref:fr@docs:ROOT:lexique-dyu.adoc#_blà[bla] ! » « N’ta bla ! » xref:fr@docs:ROOT:lexique-dyu.adoc#_tásuma[tasuma] tɛ.|« Laisse! » « Je ne laisse pas! » Ce n'est pas du feu!
|A flɛ, a flɛ, n’i xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] ye, n’a turura i xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲa[ɲa] ra i xref:fr@docs:ROOT:lexique-dyu.adoc#_bɛna[bɛna] a ye cɔ.|Regarde! Regarde! Si xref:fr@docs:ROOT:lexique-dyu.adoc#_tú[tu] ne vois pas, quand l'objet va te piquer dans l'oeil tu le verras.
|Fɛn min ka xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] xref:fr@docs:ROOT:lexique-dyu.adoc#_kogoro[kogoro] ye, kaganan tɛ o tana.|Ce que le varan terrestre aime, ne peut pas être le totem du varan d'eau.
|Ni fɛn ka xref:fr@docs:ROOT:lexique-dyu.adoc#_dɔ̀gɔya[dɔgɔya] xref:fr@docs:ROOT:lexique-dyu.adoc#_yɔ́rɔ[yɔrɔ] min a bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_kari[kari] o yɔrɔ le ra. |Là où c'est mince, c'est là que ça se casse.
|Ni jeliya diyara xref:fr@docs:ROOT:lexique-dyu.adoc#_jèli[jeli] min na, o b’ a xref:fr@docs:ROOT:lexique-dyu.adoc#_yɛ̀rɛ[yɛrɛ] kɛ Kuyate ye.|Le Griot qui a bien reussi, se fait appeler Kouyaté.
|Ji bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_jàba[jaba] ɲa, a xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] xref:fr@docs:ROOT:lexique-dyu.adoc#_fó[fɔ] ko i ye i tunun k’a xref:fr@docs:ROOT:lexique-dyu.adoc#_túru[turu] dɛ.|L'oignon aime l'eau, mais on ne dit pas de plonger sous l'eau pour le cultiver.
|Ko tɛ ko sa, nka fiyentɔya bɛ donsoya sa.|On dit que rien ne peut rien empêcher, mais la cecité empêche d'être chasseur.
|A to ! A to ! N’i xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] to, n’a tonna i kɔrɔ, i xref:fr@docs:ROOT:lexique-dyu.adoc#_kélen[kelen] b’a cɛ.|Arrête! Arrête! Si xref:fr@docs:ROOT:lexique-dyu.adoc#_tú[tu] n'arrêtes pas, Quand tu en auras une montagne devant toi, tu le ramasseras seul.
|« A’ y’a mina, a b’a xref:fr@docs:ROOT:lexique-dyu.adoc#_dá[da] ra », o ye xref:fr@docs:ROOT:lexique-dyu.adoc#_tíɲɛ[tiɲɛ] ye, nka « a’ y’a xref:fr@docs:ROOT:lexique-dyu.adoc#_mìna[mina] a b’a kɔnɔ », o ye faninya ye.|« Saisissez le, il a ça dans sa bouche! » Cela peut être vrai. Mais « Saisissez-le, il a ça dans xref:fr@docs:ROOT:lexique-dyu.adoc#_sòn[son] ventre! » C'est pas fondé.
|Aw ye Julaw xref:fr@docs:ROOT:lexique-dyu.adoc#_mìna[mina] ! aw ye Julaw xref:fr@docs:ROOT:lexique-dyu.adoc#_blà[bla] ! xref:fr@docs:ROOT:lexique-dyu.adoc#_nín[nin] bɛɛ bɛ nankɔgɔ le ɲinina.| « Saisissez les comerçants! Laissez les commerçants! » Tous ceux là cherchent du sel à acheter.
|Ala le bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_jí[ji] xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] xref:fr@docs:ROOT:lexique-dyu.adoc#_bagabaga[bagabaga] ma, k’a ta so lɔ.|C'est Dieu qui donne l'eau aux termites pour construire leur maison.
|Ala xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] fɛn xref:fr@docs:ROOT:lexique-dyu.adoc#_dán[dan] min bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_kanuya[kanuya] bɔ.|Dans tout ce que Dieu a créé rien ne vaut l'amour.
|Ala ka xref:fr@docs:ROOT:lexique-dyu.adoc#_sùla[sula] xref:fr@docs:ROOT:lexique-dyu.adoc#_lɔ́n[lɔn] kojugukɛbaga ye, k’a to a xref:fr@docs:ROOT:lexique-dyu.adoc#_kána[kana] yeri kɛ su fɛ.|Dieu sait que le singe est mauvais, c'est pourquoi il n'a pas permis de voir la nuit.
|Ala ka xref:fr@docs:ROOT:lexique-dyu.adoc#_gwòlo[gbolo] o gbolo dan, a gbanlon bɛ yi, a sumalon bɛ yi.|Chaque peau que Dieu a créée a xref:fr@docs:ROOT:lexique-dyu.adoc#_sòn[son] jour où il est froid, et son jour où il est chaud.
|« Alahu Akbar, » bɛɛ tɛ moriya ye.|Tous ceux qui disent « Alahu Akbar, » ne sont pas de grands religieux.
|Ala xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] borokandenw xref:fr@docs:ROOT:lexique-dyu.adoc#_dán[dan] ka o kaɲa.|Dieu n'a pas créé les doigts d'égale longueur.
|An ye taga, an ye taga, a xref:fr@docs:ROOT:lexique-dyu.adoc#_lában[laban] ye xref:fr@docs:ROOT:lexique-dyu.adoc#_bòri[bori] ye.|Marchons vite! Marchons vite! C'est qu'on finira par courir.
|====
