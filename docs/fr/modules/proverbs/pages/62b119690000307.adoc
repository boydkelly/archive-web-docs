---
title: Dugutigikuma
author: Inconnu
date: 2020-12-27
type: doc
draft: false
categories: []
tags: []
---
= Dugutigikuma
:author: Inconnu
:date: 2020-12-27
:description: Proverbes du chef de village
:sectnums:
:sectnumlevels: 2
:lang: fr
include:julakan:ROOT:partial$locale/attributes.adoc[]

== Dugutigikuma / Parole de chef de village

[width="80%",cols="2",frame="none",options="none",stripes="even"]
|====
|Dugu o xref:fr@docs:ROOT:lexique-dyu.adoc#_dùgu[dugu] xref:fr@docs:ROOT:lexique-dyu.adoc#_tɔ́gɔ[tɔgɔ] ye ko bɛndugu, xref:fr@docs:ROOT:lexique-dyu.adoc#_kɛ̀lɛ[kɛlɛ] tɛ o dugu ci.|Des querelles ne peuvent pas casser un village qui s'appelle " village entente"
|Dugutigi ta ye a ta xref:fr@docs:ROOT:lexique-dyu.adoc#_dùgu[dugu] ye ; "n’tɛ si" ta ye a xref:fr@docs:ROOT:lexique-dyu.adoc#_nín[nin] ye.|Le chef de village est responsable de xref:fr@docs:ROOT:lexique-dyu.adoc#_sòn[son] village, celui qui refuse de dormir au village est responsable de son âme.
|Mɔgɔ si t'a ta dugusira xref:fr@docs:ROOT:lexique-dyu.adoc#_yìra[yira] n'a numanboro ye|Personne ne montre le chemin de xref:fr@docs:ROOT:lexique-dyu.adoc#_sòn[son] village avec la main gauche.
|Dugu min mɔgɔw bɛ surugukasi kɛ, ni ele ka bakasi kɛ yi, o b’i domu.|Un village où les habitants imitent le cri de l'hyène, si toi xref:fr@docs:ROOT:lexique-dyu.adoc#_tú[tu] imites le cri d'un cabri dans ce village, tu seras mangé.
|Dugumɛnɛnin sɔnkɔrɔ ye xref:fr@docs:ROOT:lexique-dyu.adoc#_kíni[kinin] ye.|Piquer, est une vieille habitude de la fourmi.
|Dugutigi xref:fr@docs:ROOT:lexique-dyu.adoc#_dén[den] ta xref:fr@docs:ROOT:lexique-dyu.adoc#_síra[sira] tɛ kogokɔfɛkuma ra.|Le fils du chef de village n'a rien à voir dans ce qu'on dit derrière le mur.
|Dugutaraman xref:fr@docs:ROOT:lexique-dyu.adoc#_kàsi[kasi] bɛnna dondokɔrɔnin ma. |Chanter tard la nuit convient bien au vieux coq.
|Derege min tɛ i kanna,  i boro bɛ mun xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲíni[ɲini] o xref:fr@docs:ROOT:lexique-dyu.adoc#_júfa[jufa] ra?|Qu'est ce que ta main cherche dans la poche du boubou de quelqu'un d'autre?
|Lonan jatigibugɔ, i tagamatuma sera dɛ!|Etranger qui frappe autochtone, ton départ est proche!
|Dugu mɔgɔkɔrɔbaw ye yiribaw le ye, ni o ka ben, o xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲɔ́gɔn[ɲɔgɔn] sɔrɔ xref:fr@docs:ROOT:lexique-dyu.adoc#_mán[man] di.|Les vieux du village sont des grands arbres, Quand ils tombent, on les remplace difficilement.
|Dunuɲa ye xref:fr@docs:ROOT:lexique-dyu.adoc#_jìgijigi[jigijigi] caaman, ani yɛlɛyɛlɛ caaman ye.|La vie c'est beaucoup de montées et beaucoup de descentes
|Dunuɲa ye sɔgɔmada caaman ye.|La vie est faite de beaucoup de matins
|Dudunkan bɛnna, balankan bɛnna, filenkan bɛnna, xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔgɔya[mɔgɔya] bɛnnin ka xref:fr@docs:ROOT:lexique-dyu.adoc#_fisa[fisa] ni xref:fr@docs:ROOT:lexique-dyu.adoc#_nín[nin] bɛɛ ye.|Les tam-tam s'accordent bien, les balafonds s'accordent bien, les flûtes s'accordent bien,  mais il est encore mieux quand les hommes s'accordent bien.
|Dudunkan o, balankan o, susurikan ka xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] xref:fr@docs:ROOT:lexique-dyu.adoc#_lónan[lonancɛ] xref:fr@docs:ROOT:lexique-dyu.adoc#_tóro[toro] ra ka xref:fr@docs:ROOT:lexique-dyu.adoc#_tɛ̀mɛ[tɛmɛ] xref:fr@docs:ROOT:lexique-dyu.adoc#_nín[nin] bɛɛ kan.|Le xref:fr@docs:ROOT:lexique-dyu.adoc#_sòn[son] des tam-tam c'est bien, le son des balafonds c'est bien, mais le meilleur son dans l'oreille de l'étranger c'est le bruit des pilons.
|Ni xref:fr@docs:ROOT:lexique-dyu.adoc#_dùndun[dundun] ka faran, a bɛ kɛ duduntigi xref:fr@docs:ROOT:lexique-dyu.adoc#_kélen[kelen] ta ye.|Quand le tam-tam se déchire, ça devient l'affaire du batteur seul.
|Jusu le bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_kɛ̀lɛ[kɛlɛ] kɛ.|C'est avec le coeur qu'on se bat.
|Mɔgɔ kɔrɔtɔnin tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_fá[fa] to gbannin na, xref:fr@docs:ROOT:lexique-dyu.adoc#_sáni[sani] to ye xref:fr@docs:ROOT:lexique-dyu.adoc#_suma[suma] o b’a sɔrɔ a wurira.|L'homme pressé ne mange pas à sa faim quand le plat est chaud, avant que le plat ne se refroidisse il se lève.
|Fagama ta kumalɔn tɛ kumalɔn ye, fangantanw ta bɛ lakari a ye, mɔgɔbaw ta bɛ lakari a ye.|Le chef sait parler, mais lui même ne sait pas parler, on lui rapporte les paroles des pauvres on lui rapporte les paroles des grands, (et il les répète).
|Fagantan ta karafe ye a kanaderege ye.|Le mors du pauvre c'est la chemise qu'il porte.
|Fɛn min firibaga tɛ nimisa, o tɔmɔbaga tɛ ɲagari.|Ce que quelqu'un a jeté sans regret, celui qui le ramasse n'aura pas de joie.
|Fiyentɔ ta xref:fr@docs:ROOT:lexique-dyu.adoc#_kɔ̀gɔ[kɔgɔ] bɔnna bɛrɛkisɛ ra, takanɛnɛ dabɔra.|Le sel de l'aveugle est versé dans le gravier, Il va goûter un à un tout ce que sa main va trouver.
|Mugu min bɛ fagama faga, jamanaden bɛɛ bɛ o xref:fr@docs:ROOT:lexique-dyu.adoc#_cíkan[cikan] mɛn.|Le coup de fusil qui tue le chef, tout le peuple va l'entendre tirer.
|Fagamadenbugɔ ni sigi tɛ bɛn.|Frapper le fils du chef et vouloir rester dans le village ne sont pas choses compatibles.
|Masacɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_filà[fla] tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_kùn[kun] xref:fr@docs:ROOT:lexique-dyu.adoc#_gwòlo[gbolo] xref:fr@docs:ROOT:lexique-dyu.adoc#_kélen[kelen] kan.|Deux rois ne peuvent pas s'asseoir sur la même peau.
|Farata ɲininkari xref:fr@docs:ROOT:lexique-dyu.adoc#_kójugu[kojugu] b’a lakasi.|Poser trop de questions à l'orphelin finit par le faire pleurer.
|K’a xref:fr@docs:ROOT:lexique-dyu.adoc#_fó[fɔ] fagantan xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] ko: “I ni xref:fr@docs:ROOT:lexique-dyu.adoc#_sɔ̀gɔma[sɔgɔma] ”, o bɛ bɛn, nka k’a fɔ a ma ko “ hɛra xref:fr@docs:ROOT:lexique-dyu.adoc#_síra[sira] xref:fr@docs:ROOT:lexique-dyu.adoc#_wà[wa] ”, o ye kɛlɛtigɛkan ye.|Dire à un pauvre "Bonjour", c'est bien, mais lui dire: "tu as bien dormi?" c'est une provocation.
|Fagantan nana dunuɲa xref:fr@docs:ROOT:lexique-dyu.adoc#_sèn[sen] xref:fr@docs:ROOT:lexique-dyu.adoc#_mìna[mina] a bosobagaw ye le.|Le pauvre est né pour saisir les pieds du monde, et aider les riches à le dépecer.
|Fatɔ bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_yáala[yaala] n’a xref:fr@docs:ROOT:lexique-dyu.adoc#_dén[den] fiyereta ye xref:fr@docs:ROOT:lexique-dyu.adoc#_tùma[tuma] min na, o le y'a santuma ye.|Lorsque le fou promène xref:fr@docs:ROOT:lexique-dyu.adoc#_sòn[son] fils pour le vendre c'est à ce moment qu'il faut l'acheter.
|Fɛnbanbari xref:fr@docs:ROOT:lexique-dyu.adoc#_díya[diya] tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_lɔ́n[lɔn] tuun.|Une affaire sans fin, finit par ne plus avoir de goût.
|Fɛn bɛɛ bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_tága[taga] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_sègi[segi] xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] munyuribaga ma.|Tout part et reviens vers l'homme patient.
|Fɛn bɛɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_kúra[kura] xref:fr@docs:ROOT:lexique-dyu.adoc#_cɛ́[cɛ] ka ɲi, xref:fr@docs:ROOT:lexique-dyu.adoc#_fó[fɔ] kaburu kura.|Tout nouveau tout beau, sauf la tombe.
|====
