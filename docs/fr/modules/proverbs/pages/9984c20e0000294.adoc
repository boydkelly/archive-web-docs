---
title: xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] ani mɔgɔw
author: Inconnu
date: 2020-12-27
type: doc
draft: false
categories: []
tags: []
---

= xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] ani mɔgɔw
:author: Inconnu
:date: 2020-12-27
:description: Proberbes sur l'homme et les hommes
:sectnums:
:sectnumlevels: 2
:lang: fr
include:julakan:ROOT:partial$locale/attributes.adoc[]

== xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] ani mɔgɔw / L'homme et les hommes

[width="80%",cols="2",frame="none",options="none",stripes="even"]
|====
|Mɔgɔ bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_tágama[tagama] bi, xref:fr@docs:ROOT:lexique-dyu.adoc#_síni[sini] i bɛ ŋunuma, i bɛ ŋunuma bi, sini i bɛ tagama.|L'homme marche aujourd'hui sur deux pieds, demain à quatre pieds ; il va à quatre pattes aujourd'hui, demain il marche sur deux pieds.
|Mɔgɔ bɛ i diyanyako lɔn, nka Ala le bɛ i nafako lɔn.|L'homme sait ce qui lui plaît, mais c'est Dieu qui sait ce qui est pour xref:fr@docs:ROOT:lexique-dyu.adoc#_sòn[son] bien.
|Mɔgɔ bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_fàsa[fasa] k’a sɔrɔ xref:fr@docs:ROOT:lexique-dyu.adoc#_bàna[bana] tɛ i ra.|L'homme peut maigrir sans être malade.
|Mɔgɔ bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_bán[ban] ko dɔ ma, k’a sɔrɔ i m’a xref:fr@docs:ROOT:lexique-dyu.adoc#_fó[fɔ] “n’tɛ”.|On peut refuser une chose, sans dire "non!"
|Mɔgɔ bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_túgun[tugu] ka ŋuna, nka i tɛ se tugu ka wɔsi.|On peut gémir par exprès, mais on ne peut pas transpirer par exprès.
|Mɔgɔ bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_kɔ́[kɔ] min lɔn, i bɛ to xref:fr@docs:ROOT:lexique-dyu.adoc#_jí[ji] le ra.|La rivière qu'on connait, c'est dans xref:fr@docs:ROOT:lexique-dyu.adoc#_sòn[son] eau qu'on se noie.
|Dɔw tɛ gbɛɲɛ bɔrɔlaman lɔn, n’a xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] bɔ k’a xref:fr@docs:ROOT:lexique-dyu.adoc#_yìra[yira] o ra.|Certains ne reconnaissent pas un fouet dans un sac, il faut l'enlever pour le leur montrer.
|Mɔgɔ boritɔ bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_pán[pan] ka xref:fr@docs:ROOT:lexique-dyu.adoc#_filà[fla] min tigɛ, dagasigi tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_màsɔrɔ[masɔrɔ] o ra tuun.|Un médicament qu'on coupe en courant, (s'agissant des feuilles) on n'a pas le temps de le préparer dans un canari.
|Mɔgɔdarabɔsanji tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_fìn[fin] terebɔ fɛ.|Pluie de provocation ne commence pas à l'est.
|Mɔgɔ dɔ bɛ faninya xref:fr@docs:ROOT:lexique-dyu.adoc#_tìgɛ[tigɛ] i ye, xref:fr@docs:ROOT:lexique-dyu.adoc#_sábu[sabu] a bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_màlo[malo] i ma, walama a bɛ siran.|Quelqu'un te ment, simplement parce qu'il a honte de toi, ou qu'il a peur.
|I xref:fr@docs:ROOT:lexique-dyu.adoc#_fá[fa] yɛlɛnna yiri min na, xref:fr@docs:ROOT:lexique-dyu.adoc#_háli[hali] n’i xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] se ka xref:fr@docs:ROOT:lexique-dyu.adoc#_yɛ́lɛ[yɛlɛ] o yiri ra, i ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ka se ka i boro la a jura.|L'arbre dans lequel ton père montait, même si xref:fr@docs:ROOT:lexique-dyu.adoc#_tú[tu] ne peux pas monter à cet arbre, tu dois pouvoir au moins poser ta main sur xref:fr@docs:ROOT:lexique-dyu.adoc#_sòn[son] tronc.
|Mɔgɔ, i bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_ná[na] mɔgɔw le boro, i bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_tága[taga] mɔgɔw le boro.|Homme, xref:fr@docs:ROOT:lexique-dyu.adoc#_tú[tu] viens (au monde) entre les mains des hommes, tu repars entre les mains des hommes.
|Jɛgɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_jìgi[jigi] je xref:fr@docs:ROOT:lexique-dyu.adoc#_jí[ji] ye, xref:fr@docs:ROOT:lexique-dyu.adoc#_sògo[sogo] jigi ye xref:fr@docs:ROOT:lexique-dyu.adoc#_kóngo[kongo] ye, xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] jigi ye mɔgɔ ye.|L'espoir du poisson c'est l'eau, l'espoir du gibier c'est la forêt, l'espoir de l'homme, c'est l'homme.
|Mɔgɔ lɔnnin xref:fr@docs:ROOT:lexique-dyu.adoc#_cí[ci] ka di.|Il est plus facile d'envoyer en mission un homme qui est débout.
|Mɔgɔ tɛ kɛ i xref:fr@docs:ROOT:lexique-dyu.adoc#_yɛ̀rɛ[yɛrɛ] xref:fr@docs:ROOT:lexique-dyu.adoc#_júgu[jugu] ye.|On ne peut pas être xref:fr@docs:ROOT:lexique-dyu.adoc#_sòn[son] propre ennemi.
|Mɔgɔ ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ka i xref:fr@docs:ROOT:lexique-dyu.adoc#_pán[pan] ko dɔw kunna, xref:fr@docs:ROOT:lexique-dyu.adoc#_jàngo[janko] i ye dunuɲa diyabɔ.|On doit sauter par dessus certaines choses afin de pouvoir jouir de la vie.
|Mɔgɔ ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ka i senkɔrɔyɔrɔ xref:fr@docs:ROOT:lexique-dyu.adoc#_filɛ́[flɛ] xref:fr@docs:ROOT:lexique-dyu.adoc#_sáni[sani] i ye cun.|On doit regarder par terre avant de sauter en bas.
|Mɔgɔ xref:fr@docs:ROOT:lexique-dyu.adoc#_kélen[kelen] xref:fr@docs:ROOT:lexique-dyu.adoc#_sàya[saya] tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_dùgu[dugu] diman ci, nka a b’a suma.|La mort d'un seul homme ne détruit pas le village, mais elle refroidit le village.
|Mɔgɔ xref:fr@docs:ROOT:lexique-dyu.adoc#_kélen[kelen] xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲa[ɲa] wɔɔrɔ, o xref:fr@docs:ROOT:lexique-dyu.adoc#_tùma[tuma] fiyen tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_dùgu[dugu] ra wa?|Toi seul xref:fr@docs:ROOT:lexique-dyu.adoc#_tú[tu] as six yeux! Est ce qu'il n'y a pas d'aveugle dans le village?
|Mɔgɔ bɛ se ka kɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_kólɔnbaga[kolɔnbaga] ye, nka i tɛ se ka kɛ ko bɛɛ lɔnbaga ye.|On peut être un savant, mais on ne peut pas tout savoir.
|Mɔgɔ kumabari ta xref:fr@docs:ROOT:lexique-dyu.adoc#_kúma[kuma] b’a kɔnɔ.|L'homme qui ne parle pas, a sa parole dans xref:fr@docs:ROOT:lexique-dyu.adoc#_sòn[son] coeur.
|Jɛn ta xref:fr@docs:ROOT:lexique-dyu.adoc#_nɔ́gɔ[nɔgɔ] tɛ nɔgɔjugu ye.|Les taches de l'union se sont pas de mauvaises taches.
|Mɔgɔ min b’a xref:fr@docs:ROOT:lexique-dyu.adoc#_fó[fɔ] i xref:fr@docs:ROOT:lexique-dyu.adoc#_diminin[diminin] xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] ko i ye sabari, ni o ma kɛ i ɲin ye, i xref:fr@docs:ROOT:lexique-dyu.adoc#_júgu[jugu] tɛ.|Celui qui te dit de te calmer quand xref:fr@docs:ROOT:lexique-dyu.adoc#_tú[tu] es fâché, même s'il n'est pas ton ami, il n'est pas ton ennemi.
|Mɔgɔ min bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_ná[na] i ta so o ka xref:fr@docs:ROOT:lexique-dyu.adoc#_fisa[fisa] n’i ye.|Celui qui vient chez toi est mieux que toi.
|Mɔgɔsaba teriya, xref:fr@docs:ROOT:lexique-dyu.adoc#_kélen[kelen] b’a xref:fr@docs:ROOT:lexique-dyu.adoc#_kùn[kun] fɛ.|Amitié de 3 personnes, l'un restera seul.
|Mɔgɔ sɔnkɔrɔ bɛ kiti xref:fr@docs:ROOT:lexique-dyu.adoc#_gwòya[gboya] i ra.|Ton ancien caractère peut te faire perdre le procès.
|Mɔgɔ tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_bakɔrɔnin[bakɔrɔnin] karifa xref:fr@docs:ROOT:lexique-dyu.adoc#_súrugu[surugu] ma.|On ne confie pas le bouc à l'hyène.
|Mɔgɔ tɛ bɔ dundunfɔyɔrɔ ra ka xref:fr@docs:ROOT:lexique-dyu.adoc#_ná[na] i kɔnɔbara xref:fr@docs:ROOT:lexique-dyu.adoc#_fó[fɔ] so kɔnɔ.|On ne quitte pas auprès du tam-tam, pour aller taper xref:fr@docs:ROOT:lexique-dyu.adoc#_sòn[son] ventre à la maison
|Mɔgɔ tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_dén[den] dɔn, k’a kɔsin i xref:fr@docs:ROOT:lexique-dyu.adoc#_yɛ̀rɛ[yɛrɛ] ma.|On ne fait pas danser un bébé (sur les genoux) en ayant xref:fr@docs:ROOT:lexique-dyu.adoc#_sòn[son] dos vers soi même.
|Mɔgɔ tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_dòn[don] sulaw ta xref:fr@docs:ROOT:lexique-dyu.adoc#_tóron[toron] na, ko dɔ kukala xref:fr@docs:ROOT:lexique-dyu.adoc#_kána[kana] xref:fr@docs:ROOT:lexique-dyu.adoc#_màga[maga] i ra.|On ne peut pas entrer dans le jeu des singes, sans que la queue de l'un ne te touche.
|====
