---
title: xref:fr@docs:ROOT:lexique-dyu.adoc#_ntàlen[ntalen] wɛrɛw 
author: Inconnu
date: 2020-12-27
draft: false
categories: []
tags: []
---

= xref:fr@docs:ROOT:lexique-dyu.adoc#_ntàlen[ntalen] wɛrɛw 
:author: Inconnu
:date: 2020-12-27
:description: Autres proverbes dioula 
:sectnums:
:sectnumlevels: 2
:lang: fr
include:julakan:ROOT:partial$locale/attributes.adoc[]

==  xref:fr@docs:ROOT:lexique-dyu.adoc#_ntàlen[ntalen] wɛrew

[width="80%",cols="2",frame="none",options="none",stripes="even"]
|====
|Jenbe tɛ a xref:fr@docs:ROOT:lexique-dyu.adoc#_yɛ̀rɛ[yɛrɛ] fɔ, xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] le bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_jenbe[jenbe] fɔ|Ce n'est pas le tam-tam qui parle mais celui qui joue le tam-tam.
|Mɔgɔ be xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] kɛ mɔgɔ ye.|C'est l'homme qui fait l'homme.
|Dimi le be xref:fr@docs:ROOT:lexique-dyu.adoc#_dìmi[dimi] sa|C'est le mal qui guérit le mal.
|====
