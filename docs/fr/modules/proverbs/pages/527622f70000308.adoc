---
title: Fɛn kelen
author: Inconnu
date: 2020-12-27
type: doc
draft: false
categories: []
tags: []
---
= Fɛn kelen
:author: Inconnu
:date: 2020-12-27
:description: Une chose
:sectnums:
:sectnumlevels: 2
:lang: fr
include:julakan:ROOT:partial$locale/attributes.adoc[]

== Fɛn xref:fr@docs:ROOT:lexique-dyu.adoc#_kélen[kelen] / Une chose

[width="80%",cols="2",frame="none",options="none",stripes="even"]
|====
|Fɛn xref:fr@docs:ROOT:lexique-dyu.adoc#_kélen[kelen] xref:fr@docs:ROOT:lexique-dyu.adoc#_tìgi[tigi] caaman xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] ɲi, fɛn kelen tigintan ma ɲi.|Une chose avec beaucoup de propriétaires, c'est pas bien, une chose sans propriétaire, c'est pas bien.
|Fɛn min bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_banabagatɔ[banabagatɔ] kɛnɛya, o bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_fá[fa] xref:fr@docs:ROOT:lexique-dyu.adoc#_blà[bla] kɛnɛbagatɔ ra.|Ce qui guérit un malade, rend fou un bien portant.
|Fɛn min bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] mina, i tɛ o senkan mɛn.|Ce qui va t'attraper, xref:fr@docs:ROOT:lexique-dyu.adoc#_tú[tu] ne l'entends pas venir.
|Fɛn min bɛ to xref:fr@docs:ROOT:lexique-dyu.adoc#_misɛnya[misɛnya] ra, o xref:fr@docs:ROOT:lexique-dyu.adoc#_dán[dan] ye foni ye.|Il n'y a que le fonio qui reste petit.
|Fɛn min ka xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] ye i ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ka o xref:fr@docs:ROOT:lexique-dyu.adoc#_blà[bla] i xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲa[ɲa] fɛ, n’o tɛ a kɔflɛ bɛ caya.|Ce que xref:fr@docs:ROOT:lexique-dyu.adoc#_tú[tu] aimes, mets le devant toi, sinon les coups d'oeil par derrière seront nombreux.
|Fɛn min ka xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲi[ɲi] xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲɔ[ɲɔ] xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] o ye xref:fr@docs:ROOT:lexique-dyu.adoc#_bɔ̀rɔ[bɔrɔ] ye.|Ce qui est mieux pour le mil, c'est le sac.
|Fɛn min nana i xref:fr@docs:ROOT:lexique-dyu.adoc#_káma[kama] o tɛ i jɛn.|Ce qui est venu rien que pour toi, ne te manquera jamais.
|Fɛn min ka xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔ̀gɔ[mɔgɔ] sennateriya, o ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] ka i darateriya.|Ce qui t'a fait courir vite, doit te faire parler vite.
|Fɛn ɲanin, xref:fr@docs:ROOT:lexique-dyu.adoc#_tìgi[tigi] caaman.|Chose réussie, (revendiquée par) plusieurs propriétaires!
|Ni fɛn xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲúman[ɲuman] tɔmɔbaga ko a tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲa[ɲa] a kɔ, o burunna min xref:fr@docs:ROOT:lexique-dyu.adoc#_fɛ̀[fɛ] o xref:fr@docs:ROOT:lexique-dyu.adoc#_bɛna[bɛna] a xref:fr@docs:ROOT:lexique-dyu.adoc#_yɛ̀rɛ[yɛrɛ] faga.|Si celui qui a ramassé l'objet ne veut plus s'en passer, et que dire de celui qui l'a égaré? il va se tuer!
|Fɛnw fincogo ye xref:fr@docs:ROOT:lexique-dyu.adoc#_kélen[kelen] ye, nka o manamanacogo tɛ kelen ye.|Les choses noircissent de la même manière, mais ils ne brillent pas de la même manière.
|Fɔnɲɔba bɛ yiribaw ben, nka a bɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_tɛ̀mɛ[tɛmɛ] yirimisɛnw ni binw kunna.|Le grand vent fait tomber les gros arbres, mais il passe au dessus des arbustes et des herbes.
|Fɔɲɔ bɛ ɲɔfiyɛbaga boro kɔrɔtanin le sɔrɔ.|Le vent vient trouver les mains de la vanneuse en haut.
|Fiyentɔ jusuba, i xref:fr@docs:ROOT:lexique-dyu.adoc#_ɲa[ɲa] yɛlɛlon le fɔra i ye cɔ.|Un aveugle qui a un gros coeur? on t'a certainement dit le jour où tes yeux s'ouvriront.
|Fiyentɔ bɛ dɔnkɛ n’a ta dɔnkɛsara ye a boro.|L'aveugle danse avec xref:fr@docs:ROOT:lexique-dyu.adoc#_sòn[son] salaire de danse en main.
|Fiyentɔya xref:fr@docs:ROOT:lexique-dyu.adoc#_ladegi[ladegi] xref:fr@docs:ROOT:lexique-dyu.adoc#_mán[man] gbo.|Faire l'aveugle n'est pas difficile.
|Fiyentɔya o, nanbaraya o, wurudennin ka xref:fr@docs:ROOT:lexique-dyu.adoc#_nín[nin] bɛɛ kɛ ka tɛmɛn.|Vivre sans voir, sans marcher, le petit chiot a passé par toutes ces étapes.
|Ni folo bɛ i xref:fr@docs:ROOT:lexique-dyu.adoc#_kán[kan] na, n’i bɛ ko, i b’a ko, n’i bɛ mun i b’a mun, nka a xref:fr@docs:ROOT:lexique-dyu.adoc#_mán[man] xref:fr@docs:ROOT:lexique-dyu.adoc#_dí[di] i ye.|Si xref:fr@docs:ROOT:lexique-dyu.adoc#_tú[tu] as le goitre, quand tu te laves, tu le laves, quand tu t'embaumes, tu l'embaumes, mais tu ne l'aimes pas.
|Foro xref:fr@docs:ROOT:lexique-dyu.adoc#_dámina[damina] ye wagabɔn ye.|Pour faire un champ, on commence par enlever les herbes.
|Forokonin xref:fr@docs:ROOT:lexique-dyu.adoc#_tóra[tora] a logisara ra.|Le sac a été vendu à xref:fr@docs:ROOT:lexique-dyu.adoc#_sòn[son] prix de revient, (sans bénéfice).
|Ni forotobɔrɔ ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kɔ́rɔ[kɔrɔ] xref:fr@docs:ROOT:lexique-dyu.adoc#_cógo[cogo] o cogo, tisota bɛ sɔrɔ a ra.|Le sac de piment a beau être vieux, il y en aura suffisamment pour faire éternuer.
|Foyi tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_yèn[yi] ni xref:fr@docs:ROOT:lexique-dyu.adoc#_kɔ́rɔ[kɔrɔ] tɛ min na.|Rien ne se fait sans cause.
|Foyi tɛ to xref:fr@docs:ROOT:lexique-dyu.adoc#_kuraya[kuraya] ra.|Rien ne reste toujours à l'état neuf.
|“Flamuso, i kɔnɔman xref:fr@docs:ROOT:lexique-dyu.adoc#_lè[lo ?] xref:fr@docs:ROOT:lexique-dyu.adoc#_wà[wa] ?” “Ɔn hɔn.” “Den ye xref:fr@docs:ROOT:lexique-dyu.adoc#_cɛ́[cɛ] le ye wa ?” “N’ xref:fr@docs:ROOT:lexique-dyu.adoc#_má[ma] o xref:fr@docs:ROOT:lexique-dyu.adoc#_lɔ́n[lɔn] !”|« Femme peulh, xref:fr@docs:ROOT:lexique-dyu.adoc#_tú[tu] es enceinte? » «  Oui! » « Ton enfant est-il un garçon? » « Ça, je ne sais pas ! »
|Gbabugu ka xref:fr@docs:ROOT:lexique-dyu.adoc#_kɔ́rɔ[kɔrɔ] ni xref:fr@docs:ROOT:lexique-dyu.adoc#_mìsiri[misiri] ye.|La cuisine est plus âgée que la mosquée.
|Gbɛrɛgbɛrɛko sɔrɔ xref:fr@docs:ROOT:lexique-dyu.adoc#_mán[man] gbo.|Un malheur arrive facilement.
|Girindi tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_fá[fa] ye, nka kɔngɔtɔ t’a kɛ.|Roter ne veut pas dire qu'on est rassasié, mais celui qui a faim ne le fait pas.
|Gundojugu bɛ i ko xref:fr@docs:ROOT:lexique-dyu.adoc#_sògo[sogo] kɛnɛ, n’a torira, a xref:fr@docs:ROOT:lexique-dyu.adoc#_kása[kasa] bɛ bɔ.|Le mauvais secret c'est comme de la viande fraîche, Quand il pourrit, il sent mauvais.
|Gban tɛ se ka xref:fr@docs:ROOT:lexique-dyu.adoc#_janya[janya] a karibaga ma.|Le pied de gombo ne peut pas être trop long pour celui qui coupe le gombo.
|Hakɛ bɛ dunuɲa ko bɛɛ ra. |Il y a une offense en toutes choses dans ce monde.
|Hakɛ bɛɛ tɛ xref:fr@docs:ROOT:lexique-dyu.adoc#_blà[bla] ka lahara kɔnɔ.|Toutes les offenses n'attendent pas le dernier jugement.
|« xref:fr@docs:ROOT:lexique-dyu.adoc#_hàkɛ[hakɛ] to ! » O tɛ wurukinda suma.|Dire: « Pardon! » ne guérit pas une morsure de chien.
|Hakiri le ye xref:fr@docs:ROOT:lexique-dyu.adoc#_mɔgɔya[mɔgɔya] ye.|C'est la réflexion qui fait l'homme.
|==== 
