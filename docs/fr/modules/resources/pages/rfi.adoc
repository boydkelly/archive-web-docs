---
categories: [ "Julakan"]
date: 2020-05-08
hyperlink: https://www.rfi.fr/ma
image: /images/rfi_mandenkan.webp
tags: [  "jula", "langue"]
title: 'Radio France International Mandenkan'
type: notification
---

= Radio France International Mandenkan
:author: Boyd Kelly
:email:
:date: 2020-05-08
:filename: resources.adoc
:description: Radio France International Mandenkan
:keywords: Côte d'Ivoire, jula, julakan, dioula 
:lang:  fr
include::ROOT:partial$locale/attributes.adoc[]

ifdef::site-gen-antora[]
image::rfi_mandenkan.webp["RFI", 300, role=left]
endif::[]


[width="100%", cols="2", frame="topbot", options="none", stripes="even"]
|====
2+|Ecoutez l’actualité internationale, africaine, française et régionale (Afrique de l’Ouest) en mandenkan, ainsi que le sport, une revue de presse régionale.  Basé à Bambako, mais diffusé aux locuteurs mandenkan en Afrique de l'ouest. Mise-à-jour quotidiennement.  Les infos en 10 minutes, suivi par une émission d'actualité de 20 minutes du lundi au vendredi à 8h00 et 12h00.
|Site web: link:http://rfi.fr/ma[rfi]

|====
