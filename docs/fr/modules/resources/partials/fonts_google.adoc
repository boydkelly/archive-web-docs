---
categories: [ "Julakan"]
date: 2020-05-08
hyperlink: https://fonts.google.com/?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom
image: /images/polices.webp
tags: [  "jula", "langue"]
title: 'Polices web chez Google adaptées aux langues mandingues'
type: notification
---

= Polices web chez Google adaptées aux langues mandingues 
:author: Boyd Kelly
:email:
:date: 2020-10-21
:filename: polices.adoc
:description: Polices de caractère web pour Bambara, Jula (et d'autres langues mandenkan)
:type: notification
:keywords: Côte d'Ivoire, jula, julakan, dioula, polices, fonts, i18n 
:lang: fr 
include::ROOT:partial$locale/attributes.adoc[]

ifdef::site-gen-antora[]
image::polices.webp["web fonts", 300, role=left]
endif::[]

Si vous êtes développeur allez chez link:https://fonts.google.com/?preview.text=M%C9%94g%C9%94k%C9%94r%C9%94w%20ka%C9%B2i%20ka%20k%C9%9B%20ni%20%C5%8Bania%20%C9%B2uman%20ye&preview.text_type=custom[Google Fonts] ` pour déterminer si votre police préférée contienne les codes unicodes pour gérer votre langue.
