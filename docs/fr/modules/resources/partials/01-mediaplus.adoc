---
categories: ["Julakan"]
color: is-success
date: 2020-07-07-08T14:07:30
draft: 'true'
image: /images/radiomediaplus.webp
tags: [ "jula", "langue"]
title: 'Radio Media+ au cœur de Bouaké (FM 103) +'
type: notification
---

= Radio Media+ au cœur de Bouaké (FM 103) +
:author: Boyd Kelly
:email:
:date: 2020-05-08
:filename: resources.adoc
:description: Ressources Jula 
:type: notification
:keywords: Côte d'Ivoire, Ivory Coast, jula, julakan, dioula 
:lang: fr
include::ROOT:partial$locale/attributes.adoc[]

ifdef::site-gen-antora[]
image::radiomediaplus.webp["radiomedia+", 300, role=left]
endif::[]

[width="100%", cols="4", frame="topbot", options="footer", stripes="even"]
|====
4+|Très bonne ressource pour acquérir l'accent local, augmenter la compréhension et vocabulaire: +
Diffusion en langue Jula aux heures suivantes:
|jour|Heure|Animateur|Émission
|mardi|9h-10h30||Faits de société
|mercredi|9h-10h30||Faits de société
|dimanche|9h-10h30||Faits de société
|https://www.facebook.com/RADIOMEDIAPLUSCIBOUAKE[Facebook]
|Phone: +225 31 63 12 69
2+|Email: mediaplusci@yahoo.fr


|====
