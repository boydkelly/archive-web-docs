#!/bin/bash
export prefix="** xref:fr@julakan:resources:"
export basedir=./pages
export menu=../nav.adoc

pushd $basedir > /dev/null

runFind(){
  find . -maxdepth 1 -name "*.adoc" -exec bash -c 'orderByDate "$@"' bash {} \;
}

orderByDate(){
  local file="$1"
  unset postdate
  postdate=`awk '$1 == ":date:" {print $2}' "$file"`
  [[ -n $postdate ]] &&  /bin/touch -a --date=$postdate "$file" 
}

makeMenu(){
  cat <<EOF > $menu
* Ressources pour apprendre le Jula
EOF
SAVEIFS=$IFS
IFS=$(echo -en "\n\b")
for file in `ls -ut *.adoc`; do
  type=`awk '$1 == "type:" {print $2}' "$file"`
  if [[ $type == "notification" ]]; then
    #description=`awk '$1 == ":description:" {print $2" "$3" "$4" "$5}' $file`
    title=`sed -r -n 's/(^= )(.*$)/\2/gp' $file`
    description=`awk '$1 == ":description:" {$1=""; print $0}' "$file"|sed 's/^ //g'`
    echo "${prefix}${file}"["${title}"]"" >> $menu
  fi
done
IFS=$SAVEIFS
}

export -f makeMenu
export -f orderByDate

runFind
makeMenu

sed -i '/latest/d' $menu
sed -i '/index.adoc/d' $menu
git commit $menu -m "update menu"  && git push 
