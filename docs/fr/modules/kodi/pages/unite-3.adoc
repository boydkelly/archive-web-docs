---
draft: true
---
[#III]
= {nbsp}
:sectnums:
:partnums:
:lang: fr

:part: III
:!sect:
:!exercise:
:!chapter-number:

==  {nbsp}

===  {nbsp}

Wéle, san, mέn sont des transitifs. En dioula, l’objet précède toujours le verbe, exemples :

Musa bé Amadu wéle:: Moussa appelle Amadou
à kà kù sàn:: il a acheté de l’igname

le schéma est donc le  suivant :

sujet  +  copule  + objet  +  verbe

ce que nous avons appelé syntagme ,nominal ( cf. 11-0-3) peut constituer le sujet et l’objet ; exemple :

[width="100%",cols="4",frame="topbot",opts="header",stripes="even"]
|====
|Mùso fla
|bé
|Jέgε ní sògo
|sàn

|Synt. Nom. sujet
|copule
|Synt. Nom. objet
|Verbe
|====

===  {nbsp}

Sàn , verbe transitif, se conjugue de la manière suivante :

A bé màlo sàn:: il achète du riz
A tέ màlo sàn:: il n’achète pas de riz
A má màlo sàn:: il n’ a pas acheté du riz

D’où le tableau – provisoire  -  suivant :

[width="100%",cols="3",frame="topbot",opts="header",stripes="even"]
|====
|
|+
|_

|présent
|bé
|tέ

|passé
|kà
|má
|====

[NOTE]
====
[loweralpha]
. Ce tableau est assez peu différent du tableau de conjugaison des verbes intransitifs ; à une exception près : la marque kà du passé affirmatif est particulière aux verbes transitifs.

. Ces quatre copules, lorsqu’elles sont employées avec un objet et un verbe transitif, donnent à l’enoncé un sens actif ;
. On pourra entendre comme équivalent de kà les copules bára et yé.
====

===  {nbsp}

Les verbes transitifs et les verbes intransitifs connaissent un deuxième type de présent, qu’on peut appeler présent progressif ; exemples :

[width="50%",cols="10,45,45",frame="none",opts="none",stripes="even"]
|====
|VI
|à bé táala
|il est en train de partir

|VT
|à bé màlo tóbila
|il est en train de faire cuire le riz
|====

La différence entre les deux présents n’est pas toujours très claire ; de plus, il peut exister, suivant les locuteurs, d’autres formes exprimant l’idée de présent progressif.

Seku be mun kεra ?:: a be a la ka + activité
A be nεgεso bolila:: il est dedans pour dire qu’il est plongé dans cette activité.

===  {nbsp}

L’impératif et le prohibitif (impératif négatif) s’expriment de la manière suivante :

[width="50%",cols="10,45,45",frame="none",opts="none",stripes="even"]
|====
|
|VI
|táa
|pars !

|
|Kánà táa
|ne pars pas !

|VT 
|sògo dómu 
|mange la viande !

|
|Kánà sògo sàan 
|n’achète pas la viande !
|====

Les verbes transitifs employés à l’impératif ou au prohibitif prennent  nécessairement l’objet.
Un énoncé comme : prends ! sera rendu en dioula par à tà (prends-le), soit objet  + verbe.

===  {nbsp}

La forme yé…yé (au négatif t …yé) exprime une identité par exemples : 

Nìn yé mùru yé:: ceci est un couteau
Né yé Musa dén yé:: je suis l’enfant de Moussa
nìn tέbèse yé:: ceci n’est pas une machette

NOTE: on entend également la forme bé…yé.

===  {nbsp}

nìn  (ces, ce, cet) est un démonstratif ; comme tout déterminant, il est placé derrière le nom ; exemples :

mùso nìn…:: cette femme…
wùlu nìn…:: ce chien…

il porte la marque w du pluriel, et se prononce alors nìnw ou nùnunw ; nous écrirons toujours nìnw ; nìn peut s’employer sans déterminer : voir exemples en 111-0-5.

===  {nbsp}

dέ, comme kέ ou wà, est une marque phrastique, c’est-à-dire qui s’applique à l’ensemble de l’enoncé ; sa valeur est emphatique ; exemples :

kánà à kέ dέ:: ne le fais pas "surtout" !
ń má à dómu dέ:: je ne l’ai pas mangé, "je te le jure" !

dέ, comme kέ, est un élément fréquemment employé ; dέ et kέ n’ont pas d’équivalent véritable en français.

===  {nbsp}

kósͻn fait partie des propositions, classe d’éléments qui sera étudiée en IV-0 ; on peut néanmoins retenir comme formules courantes :

à kέ ála kósͻn:: fais-le pour Dieu (au nom de Dieu) !
à nàna Musa kósͻn:: il est venu grâce à Moussa
í bé táa mùn kósͻn ?:: pourquoi pars-tu ?

===  {nbsp}

Sìgilan est un nominal formé par siffixation de –lan au verbe sìgi (asseoir) ; le suffixe –lan / 
-nan réfère à un ‘’instrument’’ ; on trouvera, formé selon le même procédé :

dátúgulan:: (couvercle) 
+
de 

dátúgu:::   (fermer)


flánnan:: (balai)
+
de

flán::: (balayer)


[glossary]
== Vocabulaire 

.Vocabulaire {part} 1 
[glossary.anki]
kà:: (marque d'énoncé)
dɛ́:: (particule d'emphase)
wéle:: appeler
sàn:: acheter
mɛ́n:: entendre
fèere:: vendre

.Vocabulaire {part} 2 
[glossary.anki]
kɛ́:: faire
tóbi:: préparer (de la nourriture)
màlo:: riz
sògo:: viande, animal
kú:: igname
bàranda:: banane
sìsɛ:: poulet
sísàn:: maintenant
lɔ́n:: savoir, connaître
bé... la:: (marque d'énoncé)
tɛ́... la:: (marque d'énoncé)
ɛ́ɛ:: (interjection)

.Vocabulaire {part} 3 
[glossary.anki]
nìn:: ce, cette...
kóson:: grâce à, à cause de
yé... yé:: (marque d'énoncé)
kánà:: (marque d'énoncé)
dómu:: manger
tà:: prendre
nà:: venir
sɔ̀n:: accepter, oser
tàbali:: table
sìgilan:: chaise

