---
draft: true
---
[#IX]
= {nbsp}
:sectnums:
:partnums:
:lang: fr 

:part: IX
:!sect:
:!exercise:
:!chapter-number:

==  {nbsp}

===  {nbsp}

Les formes ká kàn kà, mán kán kà, très fréquentes en Jula, sont des expressions figées formées à partir d'un énoncé adjectival.
Elles correspondent à l'obligation; exemples :

à ká kán kà bɔ́:: il doit sortir (il faut qu'il sorte)

à mán kán kà táa::
il ne faut pas qu'il parte

[NOTE]
====
la forme affirmative est parfois contractée en: kán kà; exemples: 

à kán kà nà:: il faut qu'il vienne
====

===  {nbsp}

Les ordinaux se forment en Jula par l'adjonction du suffixe -nan: sába (trois) -- sàbanan (troisième). 
A kélen (un) correspond fɔ́lɔ (premier), mais dans les nombres supérieurs à 10, on retrouve le suffixe -nan: tán ní kélenan (onzième). 
Le schéma tonal nom + ordinal est le même que le schéma nom + adjectival actif, et donc différent du schéma tonal nom + numéral.

===  {nbsp}

pé est un élément qui se place derrière un numéral son sens est limitatif exemples :

dén kélen pé b'a fɛ:: il n'a qu'un seul enfant
à kà dàrsi flà pé d'à mà:: il ne lui a donné que dix francs

===  {nbsp}

kɔ́nɔma, nɛgɛma, yírilama, hakilima sont des qualifiants formées par suffixation de -ma ou -lama à des éléments nominaux : kɔ́nɔ (ventre), nɛgɛ (fer), yíri (bois), hákili (esprit).
Ces éléments complexes fonctionnent, 1) comme qualifiants dans un syntagme qualificatif du même schéma tonal que le syntagme nom + numéral, 2) comme attributs, de la même manière que les participes (cf 7-1-0) exemples :

mùso kɔ́nomaw nàna kùnù::
les femmes enceintes sont venues hier
 
à mùso kɔ́nɔma lo::
sa femme est enceinte
 
mɔ̀go hàkilima lo::
c'est un homme réfléchi

[NOTE]
====
. dans certains cas, le suffixe peut être -ma ou -lama;
. tous les noms n'acceptent pas cette suffixation; parmi ceux qui l'acceptent, on peut citer mùsoma (femelle), cèma, (mâle), jima (liquide).
====

===  {nbsp}

Un verbe transitif, employé sans objet, garde son sens actif lorsqu'il se nominalise par suffixation de -li/-ni;
il est alors lui-même employé comme objet etc dans un énoncé dont le verge est le plus souvent kɛ (faire) exemples:

à bé dómuni kɛ́::
il mange (il fait le "manger")

a là tigɛli kɛ::
il coupe ( il fait le "couper")

===  {nbsp}

kò (affaire) est un nom qui peut se placer derrière n'importe quel nom ou verbe pour former un nom composé exemples:

à nàko tún gwɛ̀ lɛ::
sa venue (le faire venir, qu'il vienne) était difficile

wáriko ká gwɛ̀lɛ dɛ́::
ce qui concerne l'argent (les affaires d'argent) c'est délicat vraiment!

mùso -- mùsoko::
affaires de femmes, histoires de femmes...

fúru -- fúruko::
affaires concernant le mariage

kó peut également former un composé à partir de : objet + verbe, objet + adjectif + verbe… ;

wárisͻrͻko:: ce qui concerne l’obtention de l’argent
dénnyumansͻrͻko:: ce qui concerne l’obtention de bons enfants

////
IX-0-7
////
===  {nbsp}
cὲ et mùso servent à former des composés sexués : térimuso (amie), téricε (ami), déncε (fils), dénmuso (fille) , wùlumuso (chienne), sàgacε (mouton mâle), sàgamuso (brebis), etc…

////
IX-0-8
////
===  {nbsp}
-ya est un suffixe êxtremement fréquent en  dioula, qui peut apparaître après des nominaux, des verbaux, des adjectivaux ; son sens général est ‘’ le concept d’abstrait’’ ; exemples :
        
dén misεn:: enfant
dénmisεnnya:::  enfance
kέrε:: sain
kέnεya::: santé, être sain
kiran:: avoir peur
síranya::: peur

Tous les adjectifs peuvent se suffixer –ya :dí – díya ( être agréable, plaire), kùmu – kùmuya (être acide, devenir acide ) jàn – jàanya (grandir, être grand)…

////
IX-0-9
////
===  {nbsp}

Le comparatif se forme en dioula par la forme ní… yé  placée après l’énoncé adjectival ; ex. :

Musa ká jàn ní Seku yé:: Moussa est  plus grand que Sékou
sògo ká dí ní jὲgε yé:: la viande est meilleure que le poisson
sògo mán dí ní màlo yé:: la viande est moins bonne que le riz
 
////
IX-0-10
////

===  {nbsp}

Sí est un élément postposé au nominal ou au syntagme nominal ; il s’emploie, dans le sens de ‘’ aucun’’, uniquement dans des phrases négatives ; exemples :

A má mùso sí yé:: il n’a vu aucune femme
mͻgͻ sí má nà:: personne n’est venu
à má à fͻ mͻgͻ sí yé:: il ne l’a dit à personne
à má móbili sí yé:: il n’a vu aucune voiture

////
IX-0-11
////

===  {nbsp}

Avoir en rapport avec sentiment, désir, émotion, sensation la forme bé… lá (copule + [nom ou pronom] + postp.) est très courante en dioula ; ex :

hákili b’ à lá (l’esprit est en lui):: il est réfléchi
a hàkili b’a la::: il se souvient ; quand le sujet est précisé c’est se souvenir
kͻngͻ b’à lá:: il a faim
nέnε b’à lá:: il a froid
bàna b’ à lá:: il est malade
múra b’ à lá:: il a le rhume
sèn flà b’ à lá:: il a deux pieds
sen flà bé tabali lá:: la table a deux pieds
fóyi t’ à lá:: il n’a rien
mùn be i lá ?:: tu as quoi ?
fóyi tε n’na:: j’ai rien

////
IX-0-12
////

===  {nbsp}

nkà (mais) est un élément qui sert à coordonner deux éléments de phrases ; exemples :

A b’à fὲ kà nà, nkà wári t’ à fὲ:: il veut venir, mais il n’a pas d’argent.

Très souvent, nkà est remplacé par mὲ (emprunt en français ‘’mais’’ ), tout comme _sabu la_ est remplacé par _pàsike_ (parce que) ; 
certains locuteurs dioula emploient les deux termes en concurrence, d’autres utilisent uniquement le mot emprunté ; 
dans certains cas, le mot ou la tournure proprement dioula a complètement disparu chez la plupart des locuteurs :
c’est pratiquement le cas de _kͻmi/kͻm_ (comme). 

////
IX-0-13
////

===  {nbsp}

La forme à + (tlè…) + numéral + yé nìn yé se traduit par : ‘’ il y a … jours’’. Elle est utilisée en concurrence avec la forme dèpi + (tlè…) + numéral, empruntée en français ; exemples :	

ń táara, à tlè sàba yé nì yé:: je suis parti il y a trois jours
ń táara, à kalo flà yé nìn yé:: je suis parti il y a deux mois

////
IX-0-14
////

===  {nbsp}

le verbe dími ( faire mal) est employé sous les formes suivantes :

ń sèn bé ń dími:: mon pied me fait mal
à sèn b’ à dími:: son pied lui fait mal
í sèn b’ í dími:: ton pied te fait mal
Musa sèn b’ à dími:: Moussa a mal au pied ou le pied de Moussa lui fait mal
n’ kùn bé n’ dími:: ma tête me fait mal ou j’ai mal à la tête


[glossary]
== Vocabulaire 

.Vocabulaire {part} 
[glossary.anki]
kɔ́nɔ:: ventre
nɛ̀gɛ:: fer, métal
hákili:: esprit
yÍri:: bois
fͻlͻ:: premier  
fͻlͻfͻlͻ::: au commencement
ká kán kà:: il faut que
-nan:: (suffixe des ordinaux)
-ma, -lama:: (suffixe qualifiant)

.Vocabulaire {part} 
[glossary.anki]
tlè:: jour/ soleil
fύru:: épouser
kό:: affaire
kálo:: mois/ lune
lͻgͻkun:: semaine
sà:: mourir
gbὲlε:: difficile
sÍ:: aucun
pé:: un seul (numéral)
-li/ -ni:: (suffixe nominalisateur des verbaux)

.Vocabulaire {part} 
[glossary.anki]
bàna:: maladie
dÍmi:: faire mal
kùn:: tête
bÌ:: aujourd'hui
nͻgͻ:: facile
kέnε:: sain ou frais
jàn:: grand, haut, loin
tlό:: oreille
nύn:: nez
sèn:: pied, patte,jambe
bόlo:: bras, main
dá:: bouche
kͻ:: dos
nyá:: œil
nkà:: mais
-ya:: (suffixe abstrait)

////
IX 1 6 
lesson 1
////

==  {nbsp}

===  {nbsp}

==== Exercise {counter:exercise} icon:question-circle[]  icon:arrow-right[] icon:plus-circle[]

====
icon:headphones[] nìn lámɛ́n

Awa kɔ́nɔma lò wa ?::
Ɔ̀n-hɔn Awa kɔ́nɔma lò.

icon:comment[] sègi à kàn

Awa kɔ́nɔma ló wa ?:: Ɔ̀n-hɔn Awa kɔ́nɔma lò.

[]
. à mùso kɔ̀nɔma lò wa ?
. à dénmuso hákilima lò 
. à kà mùso kɔ̀nɔma yé wa ? 
. mùso kɔnɔma saba nana wa ?
====

==== Exercise {counter:exercise} icon:question-circle[]  icon:arrow-right[] icon:plus[] pé

====
icon:headphones[] nìn lámɛ́n

mùso kélen nàna::
mùso kélen pé nàna 

icon:comment[] sègi à kàn 

mùso kélen nàna::
mùso kélen pé nàna 
====

. mɔ̀gɔ náani táara yèn
. à kɔ̀rɔcɛ sàba nàna à fò
. mùso kélen sɔnna
. à kà múru nɛ̀gɛma kélen sàn
. yíri kélen tìgɛla
. mɔ̀gɔ kélen bɔ́la wa?
. mùso kélen b'à fɛ̀
. dɔ́gɔcɛ sàba bé musa fɛ̀
. à bé dàrsi lóoru fɛ̀
. à bé sàga kélen fɛ̀
. dén mísɛn lóoru bé lú kɔ́nɔ
. dàrsi tán dí n mà 

[%collapsible]
====
. mɔ̀gɔ náani pé táara yèn
. à kɔ̀rɔcɛ sàba pé nàna à fò
. mùso kélen pé sɔnna
. à kà múru nɛ̀gɛma kélen pé sàn
. yíri kélen pé tìgɛla
. mɔ̀gɔ kélen pé bɔ́la wa?
. mùso kélen pé b'à fɛ̀
. dɔ́gɔcɛ sàba pé bé musa fɛ̀
. à bé dàrsi lóoru pé fɛ̀
. à bé sàga kélen pé fɛ̀
. dén mísɛn lóoru pé bé lú kɔ́nɔ
. dàrsi tán pé dí n mà 
====

===  {nbsp}

==== Exercise {counter:exercise}

Répondre à l'aide des nombres encadrés

====
icon:headphones[] nìn lámɛ́n

à táara à tlè yé nìn yé? icon:arrow-right[] +++ <span class="conum" data-value="3"></span>+++::
à táara à tlè sàba yé nìn yé

icon:comment[] sègi à kàn

à táara à tlè yé nìn yé? icon:arrow-right[] +++ <span class="conum" data-value="3"></span>+++::
à táara à tlè sàba yé nìn yé
====

. à mùso nàna à tlè jòli yé nin yé? icon:arrow-right[] +++<span class="conum" data-value="4"></span>+++ 
. à kè mùso fùru à kálo jòli yé nin yé? icon:arrow-right[] +++<span class="conum" data-value="5"></span>+++ 
. à fàcɛ sàla, à à tlè jòli nin yé? icon:arrow-right[] +++<span class="conum" data-value="9"></span>+++ 
. cɛ kɔ̀rɔba sàla à sánji jòli yé nin yé? icon:arrow-right[] +++<span class="conum" data-value="3"></span>+++ 
. à táara Abidjan, à sánji jòli yé nin yé? icon:arrow-right[] +++<span class="conum" data-value="12"></span>+++ 
. à dénmuso sàla, à lɔ́gɔkun jòli yé ni yé? icon:arrow-right[] +++<span class="conum" data-value="5"></span>+++ 
. à má na, à kálo jòli yé ni yé? icon:arrow-right[] +++<span class="conum" data-value="11"></span>+++ 
. à séla yàn, à kálo jòli yé ni yé? icon:arrow-right[] +++<span class="conum" data-value="13"></span>+++ 
. ò táara à tlè jòli yé ni yé? icon:arrow-right[] +++<span class="conum" data-value="6"></span>+++ 
. mùso kɔ̀nɔma táara, à tlè jòli yé ni yé? icon:arrow-right[] +++<span class="conum" data-value="4"></span>+++ 
. à dén sàbanan sàla, à tlè jòli yé ni yé? icon:arrow-right[] +++<span class="conum" data-value="8"></span>+++ 
. báara bánna, à tlè jòli yé ni yé? icon:arrow-right[] +++<span class="conum" data-value="16"></span>+++ 

[%collapsible]
====
. à mùso nàna à tlè náani yé nin yé?
. à kè mùso fùru à kálo lɔ́ɔru yé nin yé?
. à fàcɛ sàla, à à tlè kɔ́nɔtɔ nin yé?
. cɛ kɔ̀rɔba sàla à sánji sàba yé nin yé?
. à táara Abidjan, à sánji tán ní flà yé nin yé?
. à dénmuso sàla, à lɔ́gɔkun lɔ́ɔru yé ni yé?
. à má na, à kálo tán ní kélen yé ni yé?
. à séla yàn, à kálo tán ní sàba yé ni yé?
. ò táara à tlè wɔ́ɔrɔ yé ni yé?
. mùso kɔ̀nɔma táara, à tlè náani yé ni yé?
. à dén sàbanan sàla, à tlè ségi yé ni yé?
. báara bánna, à tlè tán ní wɔ́ɔrɔ yé ni yé?
====

==== Exercise {counter:exercise} icon:arrow-right[] icon:plus[] pí

====
icon:headphones[] *nìn lámɛ́n*

mùso nàna:: mùso sí má na

icon:comment[] *sègi à kàn* 

mùso nàna:: mùso sí má na
====

. cɛ̀ má nà kúnu
. à má mɔ̀gɔ fò
. mɔ̀gɔ táara à fɔ̀
. à ká à di mɔ́gɔ mà
. à mán kán kà sògo dómu
. yíri tɛ̀ yèn
. a tún má mùso fúru
. dén sàla

[%collapsible]
====
. cɛ̀ sí má nà kúnu
. à má mɔ̀gɔ sí fò
. mɔ̀gɔ sí táara à fɔ̀
. à ká à di mɔ́gɔ sí mà
. à mán kán kà sògo sí dómu
. yíri  sí tɛ̀ yèn
. a tún má mùso sí fúru
. dén sí sàla
====

===  {nbsp}

==== Exercise {counter:exercise} objet + V icon:arrow-right[] V + -li/-ni + kɛ́

====
icon:headphones[]
nìn lámɛ́n::
à má jɛ́gɛ dómu +
à má dómuni kɛ +
à bé jɛ́gɛ dómu +
à bé dómuni kɛ +

icon:comment[] 
sègi à kàn::
à bé jɛ́gɛ dómu +
à bé dómuni kɛ
====

. à má màlo dómu
. à tùn bé màlo dómu
. à tùn bé màlo dómula
. à bé sògo tìgɛ
. à bé sògo tìgɛtìgɛ
. à bámuso bé kú tóbi
. à bé fàni sànna
. à n' à mùso bé bàranda fèere

[%collapsible]
====
. à má dómuni kɛ
. à tùn bé dómuni kɛ
. à tùn bé dómuni kɛla
. à bé tìgɛli kɛ
. à bé tìgɛtìgɛli kɛ
. à bámuso bé tóbili kɛ
. à bé sànni kɛla
. à n' à mùso bé fèere kɛ
====

===  {nbsp}

==== Exercise {counter:exercise} | icon:plus-circle[]  icon:arrows-h[] icon:minus-circle[]
====
icon:headphones[] **nìn lámɛ́n**

[none]
* à táako mán dí ń ye.
* à táako ka dí ń ye.

icon:comment[] **sègi à kàn** 

[none]
* à táako mán dí ń ye.
* à táako ka dí ń ye.
====

. à nàko mán dí ń yé
. mùsofuruko ká gwɛ̀lɛ dɛ́
. ń má Musa séko mɛ́n dɛ́
. à táara Bouake à fúruko kósɔn
. màlosɔrɔko ká gwɛ̀lɛ yèn dɛ́
. Musa kó mùsoko yé ko gwɛ̀lɛmán yé
. báarakɛko mán dí Musa yé
. kúmacaman mán dí mɔ̀gɔ ɲùmán yé
. à má nà à dénko kóson
. à nàko tùn ká gwɛ̀lɛ kósobɛ

[%collapsible]
====
. à nàko ká dí ń yé
. mùsofuruko mán gwɛ̀lɛ dɛ́
. ń ká Musa séko mɛ́n dɛ́
. à má táa Bouake à fúruko kósɔn
. màlosɔrɔko mán gwɛ̀lɛ yèn dɛ́
. Musa kó mùsoko tɛ́ ko gwɛ̀lɛmán yé
. báarakɛko ká dí Musa yé
. kúmacaman ká dí mɔ̀gɔ ɲùmán yé
. à nàna à dénko kóson
. à nàko tùn mán gwɛ̀lɛ kósobɛ
====

==== Exercise {counter:exercise} | icon:question-circle[]  icon:arrow-right[] icon:plus-circle[]

====
icon:headphones[] **nìn lámɛ́n**

[none]
* à táara à tlè flà yé nin yé
* à n'à dén táara à tlè flà yé nin yé

icon:comment[] **sègi à kàn** 

[none]
* à táara à tlè flà yé nin yé
* à n'à dén táara à tlè flà yé nin yé
====

. à sàla, à kálo flà yé ní yé
. Musa má sɔ̀n kà mɔ̀gɔ sí fò
. à má dómuni kɛ́, à tlè sába yé nin yé
. Seku mùso táara Bouake, Bakari sàkó kóson
. Adama kà aàrsí lóoru pé dí Musa mà
. a tùn má Bakari sàkó mɛ́n
. ɔ́nhɔ̀n, Seku tɛ́ sànni kɛ́ yàn
. à má fén sí sɔ̀rɔ báara yɔrɔ lá

[%collapsible]
====
. à n'à dén sàla, à kálo flà yé ní yé
. Musa n'à dén má sɔ̀n kà mɔ̀gɔ sí fò
. à n'à dén má dómuni kɛ́, à tlè sába yé nin yé
. Seku mùso n'à dén táara Bouake, Bakari sàkó kóson
. Adama n'a dén kà dàrsí lóoru pé dí Musa mà
. a n'a dén tùn má Bakari sàkó mɛ́n
. ɔ́nhɔ̀n, Seku n'a dén tɛ́ sànni kɛ́ yàn
. à n'a dén má fén sí sɔ̀rɔ báara yɔrɔ lá
====

==== Exercise {counter:exercise} 

====
icon:headphones[] **nìn lámɛ́n**

Musa::
Kúnu, ń tùn bànala +
Hier, j'étais málade

Awa::
mùn lò tùn b'í dími? +
Qu'est-ce qui te faisait mal?

Musa::
ń kùn lò kɛ̀, nkà bi, à nɔ̀gɔyala +
Ma tête, máis aujourd'hui ça va mieux

Awa:: 
áa kɛ́nɛya ká dí ní bàna yé dɛ́ +
Ah, mieux vaut être bien portant que málade!

icon:comment[] **sègi à kàn** (sínyɛ sàba)

[none]
* kúnu, ń tùn bànala
* mùn lò tùn b'í dími?
* ń kùn lò kɛ̀, nkà bi, à nɔ̀gɔyala
* áa kɛ́nɛya ká dí ní bàna yé dɛ́
====

==== Exercise {counter:exercise} | icon:question-circle[]  icon:arrow-right[] icon:plus-circle[]

====
[none]
** icon:headphones[] **nìn lámɛ́n**
[none]
*** à kùn b'à dimi
*** à kùn t'a dimi

[none]
** icon:comment[] **sègi à kàn** 
[none]
*** à kùn b'à dimi
*** à kùn t'a dimi
====

[width="100%",cols="60, 40",frame="topbot",opts="none",stripes="none"]
|===
a|
[none]
* {nbsp} +
. à sèn b'à dími
. ń kùn tùn bé ń dími
. à kán b'à dimi
. Musa kɔ́ b'a dími
. ń bólo bé ń dími
. dén nin ḱɔ́nɔ b'à dími
. ń ɲá bé ń dími
. à dá b'à à dími
. à tló b'à dími
. à nún b'à dimi kósobɛ
a|
.{nbsp}
[%collapsible]
====
. à sèn t'à dími
. ń kùn tùn tɛ́ ń dími
. à kán t'à dimi
. Musa kɔ́ t'a dími
. ń bólo tɛ́ ń dími
. dén nin ḱɔ́nɔ t'à dími
. ń ɲá tɛ̀̀ ń dími
. à dá t'à à dími
. à tló t'à dími
. à nún t'à dimi kósobɛ
====
|===

==== Exercise {counter:exercise} 

Répondre à l'aide des desseins: 

[none]
** icon:headphones[] **nìn lámɛ́n**

[none]
*** question
*** answer

[none]
** icon:comment[] **sègi à kàn** 

*Questions* 

. à sèn b'à dími
. ń kùn tùn bé ń dími
. à kán b'à dimi
. Musa kɔ́ b'a dími
. ń bólo bé ń dími
. dén ni ḱɔ́nɔ b'à dími
. ń ɲá bé ń dími
. à dá b'à à dími
. à tló b'à dími
. à nún b'à dimi kósobɛ

.Réponses
[%collapsible]
====
. à sèn t'à dími
. ń kùn tùn tɛ́ ń dími
. à kán t'à dimi
. Musa kɔ́ t'a dími
. ń bólo tɛ́ ń dími
. dén nin ḱɔ́nɔ t'à dími
. ń ɲá tɛ̀̀ ń dími
. à dá t'à à dími
. à tló t'à dími
. à nún t'à dimi kósobɛ
====

