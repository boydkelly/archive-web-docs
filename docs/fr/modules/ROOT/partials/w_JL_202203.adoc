.Kɔrɔsili Sangaso, saan 2022, marisikalo
[width="100%",cols="3",frame="none",opts="header",stripes="none"]
|====
|Statistiques|Les mots les plus communs (après les 100 1ers)|Mots les plus longs
a|image:w_JL_202203.svg[w_JL_202203,300]

* Nombre total de mots 17769



* Mots uniques 1061



* Mots pluriels 894



* Mots à confirmer 10
a|

. si
. dusu
. b’an
. kɔrɔ
. yala
. kɔ
. balimaw
. te
. bibulu
. aw
a|
* musoɲɔgɔnɲinilaw
* tilenbaliyakow
* pereperelatigɛ
* ɔriganisasiyɔn
* cɛɲɔgɔnɲinilaw
* tilenninyakow
* ɔkutɔburukalo
* ninsɔndiyanin
* mɔgɔtilenninw
* kerecɛnɲɔgɔnw
|====
