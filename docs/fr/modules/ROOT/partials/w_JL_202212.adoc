.Kɔrɔsili Sangaso, saan 2022, desanburukalo
[width="100%",cols="3",frame="none",opts="header",stripes="none"]
|====
|Statistiques|Les mots les plus communs (après les 100 1ers)|Mots les plus longs
a|image:w_JL_202212.svg[w_JL_202212,300]

* Nombre total de mots 26896



* Mots uniques 1419



* Mots pluriels 1208



* Mots à confirmer 62
a|

. kɔ
. suu
. kɔrɔ
. hɛɛrɛ
. ton
. don
. delili
. tiɲɛn
. tɛna
. sisan
a|
* ninsɔndiyaninba
* dusukasibagatɔw
* alaɲasiranbagaw
* timinadiyaninw
* sarakalasebaga
* sagakulufitini
* pereperelatigɛ
* ɔriganisasiyɔn
* mɔgɔmurutininw
* weleweledalaw
|====
