.Sangaso, saan 2022, ɔkutɔburukalo
[width="100%",cols="3",frame="none",opts="header",stripes="none"]
|====
|Statistiques|Les mots les plus communs (après les 100 1ers)|Mots les plus longs
a|image:w_JL_202210.svg[w_JL_202210,300]

* Nombre total de mots 15943



* Mots uniques 1176



* Mots pluriels 987



* Mots à confirmer 48
a|

. b’o
. y’o
. wagati
. sariyaw
. ɲuman
. masaya
. kɛɲɛ
. fɔlɔ
. bɔ
. wa
a|
* ninsɔndiyaninba
* mɔgɔjagabɔbagaw
* tilenbaliyakow
* ɔriganisasiyɔn
* kunnafonidilaw
* jamalajɛnyɔrɔw
* warigwɛlamanw
* ɔkutɔburukalo
* mɔgɔtilenninw
* ladilikɛbagaw
|====
