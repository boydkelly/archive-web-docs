.Kɔrɔsili Sangaso, saan 2021, feburukalo
[width="100%",cols="3",frame="none",opts="header",stripes="none"]
|====
|Statistiques|Les mots les plus communs (après les 100 1ers)|Mots les plus longs
a|image:w_JL_202102.svg[w_JL_202102,300]

* Nombre total de mots 18975



* Mots uniques 1111



* Mots pluriels 939



* Mots à confirmer 2
a|

. kolo
. n’an
. k’an
. b’o
. yɔrɔ
. sabu
. kosɔbɛ
. mɔgɔkɔrɔw
. balimamuso
. wala
a|
* ɔriganisasiyɔn
* mɔgɔdagamuninw
* kɛrɛnkɛrɛnnin
* kerecɛnɲɔgɔnw
* bolokunnanden
* baarakɛɲɔgɔnw
* weleweledala
* waajulibaara
* sigiyɔrɔsoba
* ɲumanlɔnbali
|====
