.Kɔrɔsili Sangaso, saan 2022, zuwɛnkalo
[width="100%",cols="3",frame="none",opts="header",stripes="none"]
|====
|Statistiques|Les mots les plus communs (après les 100 1ers)|Mots les plus longs
a|image:w_JL_202206.svg[w_JL_202206,300]

* Nombre total de mots 16046



* Mots uniques 1046



* Mots pluriels 891



* Mots à confirmer 7
a|

. sɔbɛ
. mɔgɔw
. minɛ
. don
. nin
. faamu
. yen
. siranya
. see
. kanu
a|
* sɛgɛsɛgɛrikɛlaw
* ninsɔndiyaninba
* tilenbaliyakow
* ɔriganisasiyɔn
* tilenbaliyako
* sɛtanburukalo
* ɔkutɔburukalo
* kɛrɛnkɛrɛnnin
* kerecɛnɲɔgɔnw
* dususalobagaw
|====
