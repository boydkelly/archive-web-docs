.Kɔrɔsili Sangaso, saan 2022, awirilikalo
[width="100%",cols="3",frame="none",opts="header",stripes="none"]
|====
|Statistiques|Les mots les plus communs (après les 100 1ers)|Mots les plus longs
a|image:w_JL_202204.svg[w_JL_202204,300]

* Nombre total de mots 19441



* Mots uniques 1080



* Mots pluriels 917



* Mots à confirmer 7
a|

. sabu
. n’o
. labɛn
. waajuli
. t’a
. ɲɔgɔn
. seko
. nin
. kɔrɔ
. si
a|
* ɔriganisasiyɔn
* kunkanbaarabaw
* kɛrɛnkɛrɛnninw
* hakilitigiyako
* tangakabakuru
* ninsɔndiyanin
* lajɛnsɛbɛyɔrɔ
* kɛrɛnkɛrɛnnin
* kerecɛnɲɔgɔnw
* kalankɛminanw
|====
