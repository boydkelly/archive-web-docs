= Asciidoctor Tabset Extension
:date: 2021-12-10

[tabset]
====

French::
+
--
[discrete]
== A section

With an explanation!
--

Tab B::
+
--
[discrete]
== A section

With no explanation?
--

Tab C::
+
--
* A multilevel list
** level 2
*** level 3
** level 2
* level 1
--

Tab D::
+
--
A multilevel description list::
It's all about descriptions!
2nd Level:::
With lots of details!
3rd Level::::
And nit-picky micro-details!
--
====

