#!/usr/bin/bash
[[ -z "$1" ]] && { echo "need a file" ; exit 1 ; }
sed -i -E 's,([0-9]{2})/([0-9]{2})/([0-9]{4}),\3-\2-\1,g' "$1"
sed -i '1,20{s/^\s*COURS DE DIOULA :/:date:/g}' "$1"
sed -i -E '1,20{s/Enseignant :|Kalenfa :/:author:/g}' "$1"
sed -i -E '1,20{/^Apprenant|^Kalenden/d;}' "$1"
sed -i 's/^[ \t]*LECON/=== LÉÇON/' "$1"
sed -i 's/kelly/Coulibaly/gi' "$1"
#remove all leading space
sed -i 's/^[ \t]*//' "$1"
#remove page numbers; could be useful for anyting converted with pdf2text
sed -i '/^\s\d*$/d' "$1"
sed -i '/^[[:space:]]*[0-9]$/d' "$1"
#deletes multiple blank lines replaces with a single blank line
sed -i 'N;/^\n$/D;P;D;' "$1"
#remove space between locale setting
#sed -i '10,19{/^[[:space:]]*$/d}' "$1" 

#sed -i '/^:author:.*/d' "$1" &&  sed -i "/^=\s/a :author: $author" "$1"

git commit --quiet $1 -m "$0"
