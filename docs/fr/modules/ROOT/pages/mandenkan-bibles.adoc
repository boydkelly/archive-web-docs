---
title: Bibles mandenkan
author: Boyd Kelly
date: 2022-02-02
draft: false
tags: ["jula", "android", "afrique"]
categories: ["Julakan"]
---

= Bibles mandenkan
:author: Boyd Kelly
:page-author: Boyd Kelly
:page-revdate: 2022-02-02
:page-image: bible-android.webp
:description: Bibles mandenkan pour android à télécharger
:keywords: jula, bible, kodiwari,burkina faso, mandenkan
:tags: ["tech", "android", "afrique"]
:category: Julakan 
:page-tags: julakan
:lang: fr
include::ROOT:partial$locale/attributes.adoc[]


[abstract]
Il est avantageux de lire les versets de la Bible dans la version adaptée pour notre interlocuteur.
Mais parfois il est difficile de retrouver toutes les Bibles mandenkan sur Google Play.

En cas de besoin voici les versions de la Bible en Jula de Burkina Faso, Jula de Côte d'Ivoire, et Bambara.

* xref:attachment$Bam.Bible_1.0.apk[ABM, (Bible en bambara, Alliance Biblique du Mali)]
* xref:attachment$Dioula_Bible-1.0.apk[ABBF, (Bible en dioula, Alliance Biblique de Burkina Faso)]
* xref:attachment$Bible_Dioula.apk[WYI, (Bible en dioula de Côte d'Ivoire)]
* xref:attachment$Dioula_Dictionary-1.0.apk[Dictionnaire Dioula]

== Les 5 expressions du jours

** xref:docs:slides:5words.adoc[5 phrases]
