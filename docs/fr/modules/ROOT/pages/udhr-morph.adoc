= Morphologie de la Déclaration Universelle des droits de l'homme en anglais, bambara, dioula et français
:author: Boyd Kelly
:page-author: Boyd Kelly
:page-revdate: 2022-09-07
:page-image: udhr-p.webp
:description: Morphologie de la Déclaration Universelle des droits de l'homme en anglais, bambara, dioula et français
:keywords: 'tech', 'dioula', 'jula', 'dyula', 'bambara', 'bambanakan' 'anglais', 'français', 'langue', 'nations unies', 'droits homme, 'humanité', 'morphologie'
:tags: 'tech', 'jula', 'langue'
:category: Language
:lang: fr

image:{page-image}[UNHDR,300]

[NOTE]
.Voir aussi:
====
* xref:bam@docs:ROOT:udhr.adoc[Hadamaden josiraw dantigɛkan]
* xref:dyu@docs:ROOT:udhr.adoc[Dunya’’ mumɛ mɔgɔya’’ hakɛyaw dantigɛlikan]
* xref:en@docs:ROOT:udhr.adoc[Universal Declaration of Human Rights]
* xref:fr@docs:ROOT:udhr.adoc[Déclaration Universelle des droits de l'homme]
====

[width="100%",cols="25, 25, 25, 25",frame="none",opts="header",stripes="none"]
|====
|anglais
|bambara
|dioula
|français
a|
include::ROOT:partial$udhr-stats.en.adoc[]
a|
include::ROOT:partial$udhr-stats.bam.adoc[]
a|
include::ROOT:partial$udhr-stats.dyu.adoc[]
a|
include::ROOT:partial$udhr-stats.fr.adoc[]
|====
