#/usr/bin/bash
#project=proverbs
dest="$HOME/Documents/Jula/Obsidian"

#for x in tmp tsv md; do export "${x}=${project}.$x"; done

while IFS= read -r -d '' file
do

asciidoctor -s -e -a skip-front-matter -b docbook $file -o - \
  | pandoc -f docbook -t markdown -o $dest/${file%.*}.md

done <   <(find * -name "*.adoc" -maxdepth 1 -print0)


