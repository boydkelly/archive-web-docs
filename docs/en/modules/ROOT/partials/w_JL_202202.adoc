.Kɔrɔsili Sangaso, saan 2022,feburukalo
[width="100%",cols="3",frame="none",opts="header",stripes="none"]
|====
|Statistics|The 10 most common words (after the 1st 100)|Longest words
a|image:w_JL_202202.svg[w_JL_202202,300]

* Total words 20256



* Unique words 1219



* Plural words 1029



* Words to be confirmed 0
a|

. hakili
. ton
. ɲɔgɔn
. miiri
. t’a
. nafa
. kuntigiya
. janto
. tiɲɛn
. sabu
a|
* ninsɔndiyaninba
* ɔriganisasiyɔn
* mɔgɔsabalininw
* hakilitigiyako
* hakilitigiyaba
* bolokunnandenw
* tilenninyakow
* tangakabakuru
* sɛtanburukalo
* politikimɔgɔw
|====
