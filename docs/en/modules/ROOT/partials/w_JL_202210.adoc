.Sangaso, saan 2022, ɔkutɔburukalo
[width="100%",cols="3",frame="none",opts="header",stripes="none"]
|====
|Statistics|The 10 most common words (after the 1st 100)|Longest words
a|image:w_JL_202210.svg[w_JL_202210,300]

* Total words 15943



* Unique words 1176



* Plural words 987



* Words to be confirmed 48
a|

. b’o
. y’o
. wagati
. sariyaw
. ɲuman
. masaya
. kɛɲɛ
. fɔlɔ
. bɔ
. wa
a|
* ninsɔndiyaninba
* mɔgɔjagabɔbagaw
* tilenbaliyakow
* ɔriganisasiyɔn
* kunnafonidilaw
* jamalajɛnyɔrɔw
* warigwɛlamanw
* ɔkutɔburukalo
* mɔgɔtilenninw
* ladilikɛbagaw
|====
