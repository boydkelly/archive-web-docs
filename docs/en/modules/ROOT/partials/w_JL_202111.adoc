.Kɔrɔsili Sangaso, saan 2021, novanburukalo
[width="100%",cols="3",frame="none",opts="header",stripes="none"]
|====
|Statistics|The 10 most common words (after the 1st 100)|Longest words
a|image:w_JL_202111.svg[w_JL_202111,300]

* Total words 19949



* Unique words 1229



* Plural words 1047



* Words to be confirmed 22
a|

. ɲumanya
. ɲɔgɔn
. yen
. kelen
. daminɛ
. dusu
. y’o
. olu
. muɲu
. nan
a|
* ninsɔndiyaninba
* tilenbaliyakow
* ɔriganisasiyɔn
* mambetsadykova
* dɔnkililabagaw
* weleweledalaw
* tilenbaliyako
* novanburukalo
* maseduwanikaw
* marayɔrɔbabaw
|====
