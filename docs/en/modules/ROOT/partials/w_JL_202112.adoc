.Kɔrɔsili Sangaso, saan 2021, desanburukalo
[width="100%",cols="3",frame="none",opts="header",stripes="none"]
|====
|Statistics|The 10 most common words (after the 1st 100)|Longest words
a|image:w_JL_202112.svg[w_JL_202112,300]

* Total words 29564



* Unique words 1533



* Plural words 1270



* Words to be confirmed 5
a|

. miiri
. b’o
. bibulu
. taga
. jija
. faamu
. yala
. t’a
. ɲini
. don
a|
* fanibugudilanbagaw
* farisogobanatigiw
* israɛldenɲɔgɔnw
* tilenbaliyakow
* sɛgɛsɛgɛrikɛla
* sarakalasebaga
* sagakulufitini
* ninsɔndiyatigi
* mɔgɔsabalininw
* kunkanbaarabaw
|====
