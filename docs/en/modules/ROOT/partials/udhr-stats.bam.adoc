image::udhr-morph.bam.svg[UDHR,310]
[horizontal]
Total words:: 3967



[horizontal]
Unique words:: 559




//- 

[]
*  Sample vocabulary



. lakanacogo
. jɔ
. jɛ
. mána
. jo
. júgu
. fɛla
. diɲɛ
. tiɲe
. mán


//- 

[]
*  Longest words



** fisamanciyawalew
** tɔgɔlafɛntigiya
** sariyabatufanga
** sagolasugandili
** mɔgɔlaɲiniwale
** tɔndenjamanaw
** kɛrɛnkɛrɛnnen
** jɛnsɛnnifɛɛrɛ
** tɔndenjamana
** sɛmɛntiyalen
