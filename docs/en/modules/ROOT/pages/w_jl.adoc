---
title: Morphology of the Jula language in the Watchtower 
author: Boyd Kelly
date: 2022-01-14
draft: false
tags: ["watchtower", "jula", "language"]
categories: ["Language"]
---

= Morphology of the Jula language in the Watchtower 
:author: Boyd Kelly
:page-author: Boyd Kelly
:page-revdate: 2022-01-14
:page-image: w_JL_w100.svg
:description: Morphology study of the Jula language in the Watchtower magazine 
:keywords: watchtower, jula, language
:page-tags: watchtower, jula, language 
:category: Language
:lang: en 

image:{page-image}[Alt text,300]

[NOTE]
.See also:
====
* xref:en@docs:ROOT:w_jl_w100.adoc[Compiliation of the 100 most common Jula words in the Watchtower]
* xref:en@blog:ROOT:2020-12-06-w100.adoc[The 100 most common words in Ivory Coast Jula (Kodi)]
====

[WARNING]
.These figures may change for several reasons:
====
. New editions are added to the inventory of words 
. Errors may occur with French words that must be extracted from the Jula text. 
* Proper names in French and Jula present difficulies. 
. The consideration of the plural may change 
====

include::ROOT:partial$w_JL_include.adoc[]

