---
author: 'Boyd Kelly'
date: 2021-11-01
layout: slides
title: Couleurs
---

= Colours in Ivory Coast Jula
:description: Learn colours in Ivory Coast Jula
:page-author: 'Boyd Kelly'
:page-revdate: 2021-11-01
:page-layout: slides
:page-lang: fr 
:page-pagination: false
:page-tags: jula, africa, dyu, lessons, presentation
:keywords: jula, africa, dyu, lessons, presentation
:category: slides

++++
<iframe src="https://slides-dyu.coastsystems.net/colors.html" width="100%" frameborder="0" allowfullscreen="allowfullscreen" allow="geolocation *; microphone *; camera *; midi *; encrypted-media *"></iframe>
++++
