---
title: Radio France International Mandenkan
date:  2020-05-08
type: notification 
tags: [ "jula", "language"]
categories: ["Julakan" ]
image: /images/rfi_mandenkan.webp
hyperlink: https://www.rfi.fr/ma
---

= Radio France International Mandenkan
:author: Boyd Kelly
:email:
:date: 2020-05-08
:filename: resources.adoc
:description: Radio France International Mandenkan
:keywords: Côte d'Ivoire, Ivory Coast, jula, julakan, dioula 
:lang:  en
include::ROOT:partial$locale/attributes.adoc[]

ifdef::site-gen-antora[]
image::rfi_mandenkan.webp[RFI,300,role=left]
endif::[]


[width="100%",cols="2",frame="topbot",options="none",stripes="even"]
|====
2+|Listen to International, African, French and regional (West Africa) news in Mandenkan, as well as sports, and a regional press review.
Based in Bambako, but broadcast to Mandenkan listers across West Africa.  Updated daily.
10 min news cast is followed by 20 min of interviews and discussion from Monday to Friday and 8:00 AM and 12:00 PM.
|Web site: link:http://rfi.fr/ma[rfi]
|
|====
