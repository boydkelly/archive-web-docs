---
title: Bebi Philip - La vraie force
date:  2020-05-09
type: notification
tags: [ "jula", "language"]
categories: ["Julakan"]
video: GlwS8DAFAUk
hyperlink: https://www.youtube.com/embed/GlwS8DAFAUk
---

= Bebi Philip - La vraie force
:author: Boyd Kelly
:email:
:date: 2020-05-08
:filename: resources.adoc
:description: Ressources pour apprendre le Jula
:type: notification
:tags: [ "jula", "language"]
:categories: ["Julakan"]
:keywords: Côte d'Ivoire, Ivory Coast, jula, julakan, dioula, musique, bebi philip
:lang: en 
:page-lang: {lang}
include::ROOT:partial$locale/attributes.adoc[]

ifdef::site-gen-antora[]
video::GlwS8DAFAUk[youtube]
endif::[]

[width="100%",cols="2",frame="topbot",options="none",stripes="even"]
|====
2+|The lyrics need some phonetic love...
|'Official lyrics'...|An bena lalaga yan
a|Djon bênan Lolo léyé
Ikanita nanfigui nouman
Alé mannan mannan kouman léfè
Andjiguié Alla'h léyé
Nika Démissin nia mangorossou bonan
|
|====

