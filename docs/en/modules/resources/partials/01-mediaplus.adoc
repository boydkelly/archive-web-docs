---
title: Radio Media+ in the heart of Bouaké (FM 103)
date:  2020-07-07-08T14:07:30
type: notification
tags: ["jula", "language"]
categories: ["Julakan" ]
color: "is-success"
image: /images/radiomediaplus.webp
draft: true
---

= Radio Media+ in the heart of Bouaké (FM 103)
:author: Boyd Kelly
:email:
:date: 2020-05-08
:filename: resources.adoc
:description: Jula learners ressources 
:type: notification
:keywords: Côte d'Ivoire, Ivory Coast, jula, julakan, dioula 
:lang: en
include::ROOT:partial$locale/attributes.adoc[]

ifdef::site-gen-antora[]
image::radiomediaplus.webp[radiomedia+,300,role=left]
endif::[]

[width="100%",cols="4",frame="topbot",options="footer",stripes="even"]
|====
4+|Great resource for picking up the local accent, improving oral comprehension, and augmenting vocabulary. +
FM 103 broadcasting in the Jula language at the following times:
|Day|Time|Host|Program
|Tuesday|9h-10h30||Social issues
|Wednesday|9h-10h30||Social issues
|Sunday|9h-10h30||Social issues
|https://www.facebook.com/RADIOMEDIAPLUSCIBOUAKE[Facebook]
|Phone: +225 31 63 12 69
2+|Email: mediaplusci@yahoo.fr
|====
