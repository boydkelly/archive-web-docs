---
title: "Mandenkan"
date:  2020-05-08
type: notification 
tags: [ "jula", "language"]
categories: ["Julakan" ]
image: /images/logo-mandenka.webp
hyperlink: https://www.mandenkan.com
---

= http://www.mandenkan.com[Mandenkan]
:author: Boyd Kelly
:email:
:date: 2020-07-06
:filename: resources.adoc
:description: Mandenkan.com 
:type: notification
:keywords: Côte d'Ivoire, Ivory Coast, jula, julakan, dioula,
:lang: en 
include::ROOT:partial$locale/attributes.adoc[]

ifdef::site-gen-antora[]
image::logo-mandenka.webp[mandenkan.com,300,role=left]
endif::[]

[width="100%",cols="2",frame="topbot",options"header",stripes="even"]
|====
2+|This web site is produced by a true linuguistic expert, and native speaker, who also offers personal tutoring in Abidjan and distance learning. 
He has an excellent command of grammar, and knows the challenges of a learner faced with the Mandenkan thought schema.
|Web site: link:https://www.mandenkan.com[Mandenkan]
|Contact: contact@mankenkan.com
|====

