---
categories: [ "Julakan"]
date: 2020-05-09
hyperlink: https://www.youtube.com/embed/GlwS8DAFAUk
tags: [  "jula", "langue"]
title: 'Bebi Philip - La vraie force'
type: notification
video: GlwS8DAFAUk
---

= Bebi Philip - La vraie force
:author: Boyd Kelly
:email:
:date: 2020-05-08
:filename: resources.adoc
:description: Ressources pour apprendre le Jula
:imagesdir: /images/
:tags: [ "jula", "langue"]
:categories: ["Julakan"]
:keywords: Côte d'Ivoire, Ivory Coast, jula, julakan, dioula, musique, bebi philip
:lang: fr 
include::ROOT:partial$locale/attributes.adoc[]

[width="100%", cols="2", frame="topbot", options="none", stripes="even"]
|====
2+|Les paroles on besoin d'un peu d'amour phonétique...
|Paroles 'officielles'..|An bena lalaga yan
a|Djon bênan Lolo léyé
Ikanita nanfigui nouman
Alé mannan mannan kouman léfè
Andjiguié Alla'h léyé
Nika Démissin nia mangorossou bonan
|
|====

