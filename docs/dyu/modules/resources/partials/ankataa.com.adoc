---
categories: [ "Julakan"]
date: 2020-05-08
hyperlink: https://www.ankataa.com
image: /images/ankataa.webp
tags: [  "jula", "langue"]
title: Ankataa
type: notification
---

= Le site web https://www.ankataa.com[Ankataa]
:author: Boyd Kelly
:email:
:date: 2020-05-08
:filename: resources.adoc
:description: Ressources pour apprendre le Jula
:imagesdir: /images/
:type: notification
[ "jula", "langue"]
:keywords: Côte d'Ivoire, Ivory Coast, jula, julakan, dioula,
:lang: fr 

[width="100%", cols="3,4,3", frame="topbot", options="none", stripes="even"]
|====
3+|Vous trouverez ici, des cours en ligne, des recherches approfondies, ainsi qu'une chaine Youtube pour vous aider à apprendre le Jula et let Bambara. 
Formidable pour les apprenants de Jula de Côte d'Ivoire.  (Seulement remplacer le 'ye' par 'ka', le 'don' par 'le' ou 'lo' et vous êtes prêts... eh bien peut-être queques autres éléments, mais d'après tout c'est le Mandenkan!
|Site web: :link:https://www.ankataa.com[Ankataa]
|Chaine Youtube: link:https://www.youtube.com/channel/UCEQgnXDXNHaAjKA8GJZ3zHw[Na baro kɛ]
|====

