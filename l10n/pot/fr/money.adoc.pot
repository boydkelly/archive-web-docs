# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Boyd
# This file is distributed under the same license as the Blog package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Blog 1.0\n"
"POT-Creation-Date: 2022-09-08 15:52+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Attribute :page-lang:
#: docs/fr/modules/slides/pages/animal.adoc:13
#: docs/fr/modules/slides/pages/money.adoc:13
#: docs/fr/modules/slides/pages/colors.adoc:13
#: docs/fr/modules/slides/pages/proverbs.adoc:13
#, no-wrap
msgid "fr "
msgstr ""

#. type: Attribute :category:
#: docs/fr/modules/slides/pages/animal.adoc:17
#: docs/fr/modules/slides/pages/money.adoc:17
#: docs/fr/modules/slides/pages/5words.adoc:17
#: docs/fr/modules/slides/pages/colors.adoc:17
#: docs/fr/modules/slides/pages/proverbs.adoc:17
#, no-wrap
msgid "slides"
msgstr ""

#. type: YAML Front Matter: title
#: docs/fr/modules/slides/pages/money.adoc:1
#, no-wrap
msgid "Argent"
msgstr ""

#. type: Title =
#: docs/fr/modules/slides/pages/money.adoc:8
#, no-wrap
msgid "L'argent en Jula de Côte d'Ivoire"
msgstr ""

#. type: Attribute :description:
#: docs/fr/modules/slides/pages/money.adoc:9
#, no-wrap
msgid "Apprenez à manipuler l'argent en Jula de Côte d'Ivoire"
msgstr ""

#. type: Attribute :page-tags:
#: docs/fr/modules/slides/pages/money.adoc:15
#, no-wrap
msgid "jula, afrique ,dyu, leçons, présentation, argent, FCFA"
msgstr ""

#. type: Attribute :keywords:
#: docs/fr/modules/slides/pages/money.adoc:16
#, no-wrap
msgid "jula, afrique, dyu, leçons, présentation, argent, FCFA "
msgstr ""

#. type: delimited block +
#: docs/fr/modules/slides/pages/money.adoc:21
#, no-wrap
msgid "<iframe src=\"https://slides-dyu.coastsystems.net/money.html\" width=\"100%\" frameborder=\"0\" allowfullscreen=\"allowfullscreen\" allow=\"geolocation *; microphone *; camera *; midi *; encrypted-media *\"></iframe>\n"
msgstr ""
