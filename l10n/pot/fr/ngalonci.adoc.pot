# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Boyd
# This file is distributed under the same license as the Blog package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Blog 1.0\n"
"POT-Creation-Date: 2022-06-07 21:56-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: frn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: YAML Front Matter: catégories
#: content/fr/modules/blog/pages/mogo.adoc:1
#: content/fr/modules/blog/pages/a-sen-b-a-la.adoc:1
#: content/fr/modules/blog/pages/ngalonci.adoc:1
#: content/fr/modules/blog/pages/2018-05-18-anki.adoc:1
#: content/fr/modules/blog/pages/2020-12-06-w100.adoc:1
#: content/fr/modules/blog/pages/2021-04-23-animal.adoc:1
#: content/fr/modules/blog/pages/2018-07-30-alpha-jula.adoc:1
#, no-wrap
msgid "[\"Julakan\"]"
msgstr ""

#. type: Attribute :warning-caption:
#: content/fr/modules/blog/pages/mogo.adoc:15
#: content/fr/modules/blog/pages/2020-10-21-fonts.adoc:20
#: content/fr/modules/blog/pages/a-sen-b-a-la.adoc:14
#: content/fr/modules/blog/pages/ngalonci.adoc:17
#: content/fr/modules/blog/pages/2020-12-06-w100.adoc:20
#: content/fr/modules/blog/pages/2021-04-23-animal.adoc:19
#: content/fr/modules/blog/pages/2018-07-30-alpha-jula.adoc:25
#: content/fr/modules/blog/pages/2020-11-24-money.adoc:26
#: content/fr/modules/blog/pages/technology.adoc:9
#: content/fr/modules/blog/pages/anglais-francais.adoc:9
#: content/fr/modules/blog/pages/julakan.adoc:5
#: content/fr/modules/blog/pages/julakan.adoc:9
#, no-wrap
msgid "Julakan"
msgstr ""

#. type: Attribute :lang:
#: content/fr/modules/blog/pages/2020-10-21-fonts.adoc:21
#: content/fr/modules/blog/pages/nvim-plugins.adoc:20
#: content/fr/modules/blog/pages/proverb.adoc:1
#: content/fr/modules/blog/pages/clavier-android.adoc:13
#: content/fr/modules/blog/pages/ngalonci.adoc:1
#: content/fr/modules/blog/pages/ngalonci.adoc:16
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:19
#: content/fr/modules/blog/pages/ysa.adoc:13
#: content/fr/modules/blog/pages/2018-05-18-anki.adoc:13
#: content/fr/modules/blog/pages/2016-04-21-justdoit.adoc:15
#: content/fr/modules/blog/pages/2020-04-23-website.adoc:1
#: content/fr/modules/blog/pages/2020-04-23-website.adoc:13
#: content/fr/modules/blog/pages/font-test.adoc:1
#: content/fr/modules/blog/pages/sb-bashrc.adoc:1
#: content/fr/modules/blog/pages/sb-bashrc.adoc:14
#: content/fr/modules/blog/pages/2021-01-02-adoc-vim.adoc:19
#: content/fr/modules/blog/pages/2018-07-30-alpha-jula.adoc:21
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:23
#: content/fr/modules/blog/pages/2021-07-14-anki.adoc:1
#: content/fr/modules/blog/pages/2021-07-14-anki.adoc:12
#, no-wrap
msgid "fr"
msgstr ""

#. type: YAML Front Matter: image
#: content/fr/modules/blog/pages/ngalonci.adoc:1
#: content/fr/modules/blog/pages/font-test.adoc:1
#, no-wrap
msgid "/images/DSC00932.JPG"
msgstr ""

#. type: YAML Front Matter: tags
#: content/fr/modules/blog/pages/ngalonci.adoc:1
#, no-wrap
msgid "[\"julakan\", \"comptes\"]"
msgstr ""

#. type: Title =
#: content/fr/modules/blog/pages/ngalonci.adoc:1
#: content/fr/modules/blog/pages/ngalonci.adoc:13
#, no-wrap
msgid "Menteur"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/ngalonci.adoc:22
msgid "image:DSC00932.JPG[role=\"left\"]"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/ngalonci.adoc:28
msgid ""
"Il était une fois un village où vivait un monsieur très rusé et dont la "
"malhonnêteté était jugée excessive.  Un jour, ce monsieur eu vent de la "
"richesse d’un roi et décida de lui arracher son pouvoir et tous les "
"avantages liés à son statut.  Pour monter son coup, il demanda à son épouse "
"de lui remettre ses boucles en or.  Son père lui avait aussi laissé un très "
"beau cheval.  Ces deux éléments devraient suffire à réussir son entreprise."
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/ngalonci.adoc:32
msgid ""
"Le malhonnête mit du mil et une pépite d’or dans un petit panier "
"qu’utilisent les cavaliers pour nourrir leurs chevaux, panier généralement "
"attaché à la gueule du cheval.  L’animal ayant avalé le mil et l’or, il alla "
"voir le Roi accompagné de son cheval savamment préparé pour la funeste "
"mission.  Une fois chez le Roi, Ngalonci lui annonça que son cheval produit "
"des déchets en or."
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/ngalonci.adoc:38
msgid ""
"Le Roi surpris lui demanda des preuves pour étayer ses propos.  Le Roi eu "
"très vite la preuve puisque les premiers déchets recueillis du cheval "
"contenaient de l’or.  Le Roi décida alors d’acheter ce cheval peu ordinaire "
"à un prix inégalé.  Il enferma son \"prestigieux cheval\" dans un enclot et "
"toutes les fois que l’animal déféquait, il recueillait les déchets afin d’en "
"extraire de l’or.  Après que le cheval ait produit suffisamment de déchets, "
"le Roi demanda à ses femmes esclaves de fouiller les déchets (en lavant les "
"lavant)."
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/ngalonci.adoc:44
msgid ""
"Pendant ce temps, Ngalonci était déjà très loin.  Les femmes vinrent avec "
"des calebasses pour accomplir la tâche qui leur avait été confiée.  Après "
"avoir lavé tous les déchets, elles ne trouvèrent qu’une pépite.  Le roi "
"entra alors dans une colère indescriptible.  Il ordonna à ces soldats de lui "
"ramener Ngalonci."
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/ngalonci.adoc:50
msgid ""
"Mais Ngalonci avait déjà planifié quelque chose au cas où sa femme et lui "
"venaient à être arrêtés.  En effet, il avait mis du sang dans le gésier d’un "
"poulet après en avoir extrait les débris.  Il attacha cela au niveau de la "
"gorge de sa femme.  Il avait préparé cette dernière à \"ce plan de "
"secours\".  Ngalonci lui demanda de provoquer une scène de ménage pendant "
"que le Roi et lui seront en train d’échanger."
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/ngalonci.adoc:58
msgid ""
"Ainsi, il entrerait en colère et simulerait une scène d’assassinat.  "
"Lorsqu’ils arrivèrent chez sa majesté, celui-ci dit à Ngalonci : « Le cheval "
"que tu m’as vendu n’a produit qu’une pépite d’or contrairement à ce que tu "
"m’avais annoncé ».  C’est alors que la femme de Ngalonci engagea la dispute "
"préméditée.  Le malhonnête jaillit sur elle, fit semblant d’être en colère, "
"l’assomma puis coupa le gésier de poulet.  Le sang se mit à couler.  "
"L’assistance terrorisée s’écria : « Il a tué sa femme !!! Il a tué sa "
"femme !!! »."
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/ngalonci.adoc:63
msgid ""
"Ngalonci demeura serein puisque lui seul savait réellement que la femme "
"n’était pas morte, il dit au Roi : « Laissez-la.  Revenons à nos moutons.» "
"La femme était étalée.  Face à cette indifférence, le Roi répondit : « On ne "
"peut pas continuer de discuter pendant que toi tu as tué, on ne peut pas. »."
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/ngalonci.adoc:72
msgid ""
"Ngalonci rassura le Roi : « Ce n’est rien, elle se réveillera tout de "
"suite.  Apportez-moi une calebasse, de l’eau de puits et un balai. ».  Il "
"récita des incantations sur l’eau et posa le balai dans la calebasse.  Après "
"quoi, il versa de l’eau sur sa femme à l’aide du balai.  La femme éternua et "
"se leva.  C’est la surprise générale.  Le Roi demanda à l’homme son secret "
"et souhaita avoir ce pouvoir.  Ngalonci trouva là un moyen de lui soutirer "
"de l’argent."
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/ngalonci.adoc:76
msgid ""
"Il dit au Roi : « Il est difficile d’avoir un tel pouvoir ! ».  Il fixa un "
"montant exorbitant.  Le Roi le lui remit."
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/ngalonci.adoc:82
msgid ""
"Une fois le pouvoir fictif acquis, le Roi décida de faire un test.  Il "
"appela sa dernière épouse, sa préférée et lui expliqua ce qu’il allait faire "
"puisque c’est elle qu’il avait choisi pour réaliser son exploit.  Il "
"procédera comme il a (lui-même) vu l’expérience se dérouler.  Un jour, "
"pendant qu’il causait avec des amis, une dispute éclata entre sa femme et "
"lui.  Il l’égorgea."
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/ngalonci.adoc:86
msgid ""
"Tout le monde eu peur.  Le Roi rassura : « ça ce n’est rien », il demanda "
"lui aussi qu’on apporte une calebasse, de l’eau de puits et un balai.  Après "
"quoi, il récita des incantations et versa à son tour de l’eau sur sa femme "
"bien aimée mais en vain, elle ne se réveilla pas."
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/ngalonci.adoc:94
msgid ""
"Le Roi se mit très en colère.  Il ordonna à ses soldats de rattraper ce rusé "
"de Ngalonci.  Il dit: « Ramenez-moi tout de suite ce vaurien afin que je lui "
"tranche la gorge ».  On ramena Ngalonci.  Cette fois-ci, tout le monde était "
"assuré de sa mort.  Le Roi tua un bœuf vieux de sept ans et enleva sa peau.  "
"Le menteur fut emballé dans cette peau et envoyé au bord de la mer pour être "
"jeté."
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/ngalonci.adoc:99
msgid ""
"Une fois au bord de la mer, un bruit assourdissant éclata au village.  Ils "
"posèrent l’infortuné au bord de la mer pour aller voir ce qui se passe au "
"village.  Cette priorité parce qu’ils croyaient à une insurrection car des "
"villages s’attaquaient très souvent.  En tant que soldats, leur priorité est "
"avant tout la défense du village."
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/ngalonci.adoc:103
msgid ""
"Après qu’ils soient partis, Ngalonci attendit des bruits annonçant l’arrivée "
"de quelqu’un, il s’agissait en effet d’un commerçant.  Il se mit à crier en "
"ces termes : « Je ne veux pas de l’or, même si tu m’obliges à le prendre, je "
"n’en veux pas.  Moi, je ne veux pas du tout de l’or. »."
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/ngalonci.adoc:108
msgid ""
"Le passant s’arrêta pour observer de plus près la peau enroulée.  Il "
"s’écria : « Y’a-t-il un être humain dans cette peau de bœuf ? ».  Il "
"attendit une voix parler d’or.  Alors le commerçant demanda : « De quelle "
"affaire d’or parles-tu depuis ? »."
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/ngalonci.adoc:112
msgid ""
"Ngalonci répondit : « Cette peau où je me trouve est pleine d’or et on me "
"demande de rester là-dedans pendant trois jours avant d’en être le "
"bénéficiaire.  J’ai dit que je ne voulais pas de cet or.  Et toi, tu le "
"veux ? »."
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/ngalonci.adoc:119
msgid ""
"Le commerçant répondit « Oui ».  « En tout cas si tu veux l’or, tu peux me "
"faire sortir et prendre ma place ; mais sache que tu passeras trois jours "
"dans la peau » dit le rusé Ngalonci.  Le commerçant accepta le deal et dit : "
"« Tu m’as bien parlé d’or qui se trouverait dans cette peau ? Même s’il me "
"faut passer une semaine, je suis d’accord ».  Ainsi, il mit le commerçant "
"dans la peau et l’enferma.  Il prit ensuite le cheval de celui-ci et tout le "
"reste."
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/ngalonci.adoc:126
msgid ""
"Au retour des soldats, la peau était toujours posée près de la mer.  Ils "
"s’apprêtaient à la jeter dans la mer, lorsqu’une voix se fit entendre : « Ce "
"n’est pas moi.  Celui qui était dans cette peau avant moi est déjà parti, je "
"ne suis qu’un passant ».  « Qui écoutera tes idioties ? Malhonnête », "
"répliquèrent les soldats.  Ils allèrent le jeter en pleine mer."
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/ngalonci.adoc:136
msgid ""
"Quelques jours après, Ngalonci rendit visite au roi.  Une fois chez le roi, "
"il lui dit : « Les habitants de l’au-delà vous saluent, ils vont bien.  Vos "
"ancêtres vous saluent.  Ils ont très envie de vous voir.  L’au-delà̀ est "
"agréable et je vais y retourner.  Le monde d’ici-bas est difficile alors que "
"la- bas, on ne travaille pas, il n’y a rien d’autre que le repos.  Tu es "
"libre de tout.  Rien que l’aisance.  Femmes, enfants, personne ne te "
"dérange. »"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/ngalonci.adoc:139
msgid ""
"Aveuglé par cette résurrection trompeuse, Le Roi décida d’aller visiter l’au-"
"delà puisqu’il lui avait été dit que ses ancêtres voulaient le voir.  L’on "
"tua donc un bœuf vieux de sept ans dans la peau duquel on enroula le roi qui "
"fut à son tour jeté dans la mer."
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/ngalonci.adoc:141
msgid ""
"Depuis ce jour, Ngalonci règne sur le trône parce que tout le monde le "
"craint.  Et depuis, le roi n’est pas revenu."
msgstr ""
