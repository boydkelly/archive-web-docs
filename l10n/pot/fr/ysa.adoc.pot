# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Boyd
# This file is distributed under the same license as the Blog package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Blog 1.0\n"
"POT-Creation-Date: 2022-09-08 15:52+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: frn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Attribute :lang:
#: content/fr/modules/blog/pages/2020-10-21-fonts.adoc:21
#: content/fr/modules/blog/pages/nvim-plugins.adoc:20
#: content/fr/modules/blog/pages/proverb.adoc:1
#: content/fr/modules/blog/pages/clavier-android.adoc:13
#: content/fr/modules/blog/pages/ngalonci.adoc:1
#: content/fr/modules/blog/pages/ngalonci.adoc:16
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:19
#: content/fr/modules/blog/pages/ysa.adoc:13
#: content/fr/modules/blog/pages/2018-05-18-anki.adoc:13
#: content/fr/modules/blog/pages/2016-04-21-justdoit.adoc:15
#: content/fr/modules/blog/pages/2020-04-23-website.adoc:1
#: content/fr/modules/blog/pages/2020-04-23-website.adoc:13
#: content/fr/modules/blog/pages/font-test.adoc:1
#: content/fr/modules/blog/pages/sb-bashrc.adoc:1
#: content/fr/modules/blog/pages/sb-bashrc.adoc:14
#: content/fr/modules/blog/pages/2021-01-02-adoc-vim.adoc:19
#: content/fr/modules/blog/pages/2018-07-30-alpha-jula.adoc:21
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:23
#: content/fr/modules/blog/pages/2021-07-14-anki.adoc:1
#: content/fr/modules/blog/pages/2021-07-14-anki.adoc:12
#: content/fr/modules/blog/pages/2022-07-15-hunspell-fr.adoc:23
#: content/fr/modules/blog/pages/2022-07-18-hunspell-la.adoc:21
#, no-wrap
msgid "fr"
msgstr ""

#. type: YAML Front Matter: catégories
#: content/fr/modules/blog/pages/proverb.adoc:1
#: content/fr/modules/blog/pages/ysa.adoc:1
#: content/fr/modules/blog/pages/2016-04-21-justdoit.adoc:1
#, no-wrap
msgid "[\"Anglais en français\"]"
msgstr ""

#. type: Attribute :category:
#: content/fr/modules/blog/pages/proverb.adoc:17
#: content/fr/modules/blog/pages/ysa.adoc:14
#: content/fr/modules/blog/pages/2016-04-21-justdoit.adoc:17
#, no-wrap
msgid "Anglais-en-français"
msgstr ""

#. type: YAML Front Matter: tags
#: content/fr/modules/blog/pages/ysa.adoc:1
#, no-wrap
msgid "[\"langue\", \"français\", \"anglais\"]"
msgstr ""

#. type: Title =
#: content/fr/modules/blog/pages/ysa.adoc:1
#: content/fr/modules/blog/pages/ysa.adoc:9
#, no-wrap
msgid "Yet Still Again ?"
msgstr ""

#. type: Attribute :description:
#: content/fr/modules/blog/pages/ysa.adoc:12
#, no-wrap
msgid "Yet? Still? Again?"
msgstr ""

#. type: Attribute :keywords:
#: content/fr/modules/blog/pages/ysa.adoc:15
#, no-wrap
msgid "anglais, français, grammaire"
msgstr ""

#. type: Title ==
#: content/fr/modules/blog/pages/ysa.adoc:18
#, no-wrap
msgid "Quelle est la différence entre ces trois termes ?"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/ysa.adoc:23
msgid ""
"Souvent le français est plus précis que l'anglais... Surtout en matière de "
"verbes.  Par contre l'anglais peut aussi être plus précis que le français."
msgstr ""

#. type: Title ==
#: content/fr/modules/blog/pages/ysa.adoc:24
#, no-wrap
msgid "Voici un exemple"
msgstr ""

#. type: Labeled list
#: content/fr/modules/blog/pages/ysa.adoc:26
#, no-wrap
msgid "Yet"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/ysa.adoc:27
#: content/fr/modules/blog/pages/ysa.adoc:28
#: content/fr/modules/blog/pages/ysa.adoc:29
#: content/fr/modules/blog/pages/ysa.adoc:34
msgid "Encore"
msgstr ""

#. type: Labeled list
#: content/fr/modules/blog/pages/ysa.adoc:27
#, no-wrap
msgid "Still"
msgstr ""

#. type: Labeled list
#: content/fr/modules/blog/pages/ysa.adoc:28
#, no-wrap
msgid "Again"
msgstr ""

#. type: Labeled list
#: content/fr/modules/blog/pages/ysa.adoc:29
#, no-wrap
msgid "Even"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/ysa.adoc:30
msgid "Encore (souvent traduit par 'même', mais parfois 'encore')"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/ysa.adoc:32
msgid ""
"et encore.footnote:[Dans link:https://www.coastsystems.net/en/post/ysa[la "
"version anglaise] de ce document j'ai traduit 'encore' par 'aussi'.] :"
msgstr ""

#. type: Labeled list
#: content/fr/modules/blog/pages/ysa.adoc:33
#, no-wrap
msgid "Yet again"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/ysa.adoc:37
msgid ""
"Oui, parfois on trouve en anglais les deux mots ensemble dans une phrase : "
"'*yet again*...' Qu'est-ce que cela signifie ?"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/ysa.adoc:40
msgid ""
"Nous avons non seulement quatre mots en anglais, mais quatre significations "
"différentes avec la même traduction en français.  Connaissez-vous la nuance?"
msgstr ""

#. type: Block title
#: content/fr/modules/blog/pages/ysa.adoc:41
#, no-wrap
msgid "Voici quelques phrases pour vous aider:"
msgstr ""

#. type: Labeled list
#: content/fr/modules/blog/pages/ysa.adoc:43
#, no-wrap
msgid "She hasn't come yet"
msgstr ""

#. type: delimited block *
#: content/fr/modules/blog/pages/ysa.adoc:44
#: content/fr/modules/blog/pages/ysa.adoc:45
msgid "Elle n'est pas encore venue"
msgstr ""

#. type: Labeled list
#: content/fr/modules/blog/pages/ysa.adoc:44
#, no-wrap
msgid "She is yet to come"
msgstr ""

#. type: Labeled list
#: content/fr/modules/blog/pages/ysa.adoc:45
#, no-wrap
msgid "She is still sleeping"
msgstr ""

#. type: delimited block *
#: content/fr/modules/blog/pages/ysa.adoc:46
msgid "Elle dort encore.  (Elle continue à dormir)"
msgstr ""

#. type: Labeled list
#: content/fr/modules/blog/pages/ysa.adoc:46
#, no-wrap
msgid "She is eating again"
msgstr ""

#. type: delimited block *
#: content/fr/modules/blog/pages/ysa.adoc:47
msgid "Elle mange encore.  (Elle mange de nouveau)"
msgstr ""

#. type: Labeled list
#: content/fr/modules/blog/pages/ysa.adoc:47
#, no-wrap
msgid "She is going yet again"
msgstr ""

#. type: delimited block *
#: content/fr/modules/blog/pages/ysa.adoc:48
msgid "Elle va encore.  (Elle continue d'y aller une fois de plus)"
msgstr ""

#. type: Labeled list
#: content/fr/modules/blog/pages/ysa.adoc:48
#, no-wrap
msgid "It's even more difficult than I thought"
msgstr ""

#. type: delimited block *
#: content/fr/modules/blog/pages/ysa.adoc:49
msgid "C'est encore plus difficile que je ne le pensais."
msgstr ""

#. type: Labeled list
#: content/fr/modules/blog/pages/ysa.adoc:49
#, no-wrap
msgid "There's even more to come"
msgstr ""

#. type: delimited block *
#: content/fr/modules/blog/pages/ysa.adoc:50
msgid "Il y en a encore davantage à venir."
msgstr ""

#. type: Table
#: content/fr/modules/blog/pages/ysa.adoc:59
#, no-wrap
msgid ""
"|Yet|Une action qui ne s'est pas produit jusqu'au présent. (encore)\n"
"|Still|Une action qui continue à se produire, qui n'a jamais arrêté.\n"
"|Again|Une action discontinue qui reprend.\n"
"|Even|Nuancé avec 'still'|Une chose qui s'est arrêtée/épuisée, mais il y en aura...|(encore !)\n"
"|Yet again|Une action discontinue qui reprend au moins une fois mais probablement plus.\n"
msgstr ""
