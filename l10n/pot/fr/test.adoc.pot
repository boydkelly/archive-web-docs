# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Boyd
# This file is distributed under the same license as the Blog package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Blog 1.0\n"
"POT-Creation-Date: 2020-11-30 11:17+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#: content/fr/post/mogo.adoc:1 content/fr/post/fonts.adoc:1
#: content/fr/post/nvim-plugins.adoc:1 content/fr/post/vimplug.adoc:1
#: content/fr/post/proverb.adoc:1 content/fr/post/a-sen-b-a-la.adoc:1
#: content/fr/post/clavier-android.adoc:1 content/fr/post/ngalonci.adoc:1
#: content/fr/post/alpha-jula.adoc:1
#: content/fr/post/ponctuation-francaise.adoc:1 content/fr/post/ysa.adoc:1
#: content/fr/post/anki.adoc:1 content/fr/post/justdoit.adoc:1
#: content/fr/post/website.adoc:1 content/fr/post/datally.adoc:1
#: content/fr/post/clavier-ivoirien.adoc:1 content/fr/post/fontlist.adoc:1
#: content/fr/post/sb-bashrc.adoc:1 content/fr/post/money.adoc:1
#: content/fr/post/test.adoc:1
#, no-wrap
msgid "---\n"
msgstr ""

#. type: Target for macro image
#: content/fr/post/fonts.adoc:23 content/fr/post/test.adoc:15
#, no-wrap
msgid "polices.png"
msgstr ""

#. type: Plain text
#: content/fr/post/test.adoc:8
#, no-wrap
msgid ""
"title: \"Test\"\n"
"date: 2020-11-15T16:01:29Z\n"
"draft: true\n"
"lang: fr\n"
"type: post\n"
"image: /images/polices.png \n"
"---\n"
msgstr ""

#. type: Title =
#: content/fr/post/test.adoc:10
#, no-wrap
msgid "\"Test\""
msgstr ""
