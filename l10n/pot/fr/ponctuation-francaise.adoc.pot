# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Boyd
# This file is distributed under the same license as the Blog package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Blog 1.0\n"
"POT-Creation-Date: 2022-09-08 15:52+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: frn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Attribute :lang:
#: content/fr/modules/blog/pages/2020-10-21-fonts.adoc:21
#: content/fr/modules/blog/pages/nvim-plugins.adoc:20
#: content/fr/modules/blog/pages/proverb.adoc:1
#: content/fr/modules/blog/pages/clavier-android.adoc:13
#: content/fr/modules/blog/pages/ngalonci.adoc:1
#: content/fr/modules/blog/pages/ngalonci.adoc:16
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:19
#: content/fr/modules/blog/pages/ysa.adoc:13
#: content/fr/modules/blog/pages/2018-05-18-anki.adoc:13
#: content/fr/modules/blog/pages/2016-04-21-justdoit.adoc:15
#: content/fr/modules/blog/pages/2020-04-23-website.adoc:1
#: content/fr/modules/blog/pages/2020-04-23-website.adoc:13
#: content/fr/modules/blog/pages/font-test.adoc:1
#: content/fr/modules/blog/pages/sb-bashrc.adoc:1
#: content/fr/modules/blog/pages/sb-bashrc.adoc:14
#: content/fr/modules/blog/pages/2021-01-02-adoc-vim.adoc:19
#: content/fr/modules/blog/pages/2018-07-30-alpha-jula.adoc:21
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:23
#: content/fr/modules/blog/pages/2021-07-14-anki.adoc:1
#: content/fr/modules/blog/pages/2021-07-14-anki.adoc:12
#: content/fr/modules/blog/pages/2022-07-15-hunspell-fr.adoc:23
#: content/fr/modules/blog/pages/2022-07-18-hunspell-la.adoc:21
#, no-wrap
msgid "fr"
msgstr ""

#. type: YAML Front Matter: catégories
#: content/fr/modules/blog/pages/nvim-plugins.adoc:1
#: content/fr/modules/blog/pages/vimplug.adoc:1
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:1
#: content/fr/modules/blog/pages/2020-04-23-website.adoc:1
#: content/fr/modules/blog/pages/datally.adoc:1
#: content/fr/modules/blog/pages/clavier-ivoirien.adoc:1
#: content/fr/modules/blog/pages/sb-bashrc.adoc:1
#: content/fr/modules/blog/pages/2021-01-02-adoc-vim.adoc:1
#, no-wrap
msgid "[\"Technologie\"]"
msgstr ""

#. type: Attribute :category:
#: content/fr/modules/blog/pages/nvim-plugins.adoc:19
#: content/fr/modules/blog/pages/vimplug.adoc:16
#: content/fr/modules/blog/pages/clavier-android.adoc:18
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:18
#: content/fr/modules/blog/pages/2020-04-23-website.adoc:18
#: content/fr/modules/blog/pages/datally.adoc:15
#: content/fr/modules/blog/pages/font-test.adoc:18
#: content/fr/modules/blog/pages/clavier-ivoirien.adoc:17
#: content/fr/modules/blog/pages/sb-bashrc.adoc:15
#: content/fr/modules/blog/pages/2021-01-02-adoc-vim.adoc:18
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:20
#: content/fr/modules/blog/pages/2021-07-14-anki.adoc:13
#: content/fr/modules/blog/pages/technology.adoc:5
#: content/fr/modules/blog/pages/technology.adoc:7
#: content/fr/modules/blog/pages/anglais-francais.adoc:7
#: content/fr/modules/blog/pages/julakan.adoc:7
#: content/fr/modules/blog/pages/2022-07-15-hunspell-fr.adoc:20
#: content/fr/modules/blog/pages/2022-07-18-hunspell-la.adoc:19
#, no-wrap
msgid "Technologie"
msgstr ""

#. type: YAML Front Matter: image
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:1
#, no-wrap
msgid "/images/vim.webp"
msgstr ""

#. type: YAML Front Matter: tags
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:1
#, no-wrap
msgid "[\"vim\", \"français\", \"langue\", \"linux\" ]"
msgstr ""

#. type: Title =
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:1
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:11
#, no-wrap
msgid "Ponctuation française facile sous Linux avec nvim/vim"
msgstr ""

#. type: Attribute :description:
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:14
#, no-wrap
msgid "Ponctuation française facile vec neovim et vim sous linux"
msgstr ""

#. type: Attribute :keywords:
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:15
#, no-wrap
msgid "vim, nvim, technolgie, afrique, linux, ponctuation, français "
msgstr ""

#. type: Attribute :page-tags:
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:16
#, no-wrap
msgid "vim, français, langue, linux, ponctuation "
msgstr ""

#. type: Positional ($2) AttributeList argument for style 'quote'
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:22
#, no-wrap
msgid "Victor Hugo"
msgstr ""

#. type: Positional ($3) AttributeList argument for style 'quote'
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:22
#, no-wrap
msgid "La légende des siècles"
msgstr ""

#. type: delimited block _
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:26
msgid ""
"« Les sauterelles envahissent l’Égypte et les virgules envahissent la "
"ponctuation »."
msgstr ""

#. type: Positional ($1) AttributeList argument for macro 'image'
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:28
#, no-wrap
msgid "vim"
msgstr ""

#. type: Target for macro image
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:28
#, no-wrap
msgid "vim.webp"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:31
msgid ""
"Si vous avez l'habitude de taper en anglais et en français, comme moi, "
"comment gérez vous les espaces qui précèdent les points d'interrogation et "
"d'exclamation, les deux-points, le point-virgule, et les guillemets ? En "
"français, selon les normes (?? en tout cas), il faut avoir des espaces avant "
"le point d'interrogation et le point mais les anglais sont étonnés de voir "
"cela."
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:33
msgid "Avec mon éditeur préféré Nvim sous Linux c'est chose facile."
msgstr ""

#. type: Title ==
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:34
#, no-wrap
msgid "Allons !"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:37
msgid "Créer un ficher à l'endroit suivant pour soit vim/gvim ou bien nvim"
msgstr ""

#. type: Table
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:44
#, no-wrap
msgid ""
"|(vim/gvim): \n"
"|*.vim/keymap/fr.vim)*\n"
"|(nvim): \n"
"|*.local/share/nvim/site/keymap/fr.vim*\n"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:47
msgid "Avec le contenu très simple suivant:"
msgstr ""

#. type: delimited block -
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:51
#, no-wrap
msgid "let b:keymap_name=\"French\"\n"
msgstr ""

#. type: delimited block -
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:53
#, no-wrap
msgid "\"highlight lCursor ctermbg=red guibg=red\n"
msgstr ""

#. type: delimited block -
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:55
#, no-wrap
msgid "loadkeymap\n"
msgstr ""

#. type: delimited block -
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:60
#, no-wrap
msgid ""
"?\t  ?\n"
"!   !\n"
":   :\n"
";   ;\n"
msgstr ""

#. type: delimited block =
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:67
msgid ""
"Les espaces qui se trouvent devant les points d'exclamation et "
"d'interrogation sont des espaces insécables.  On peut entrer ces espaces "
"Unicode sous Neovim et vim en tapant kbd:[Ctrl + V + u + 0 + 0 + a + 0 ] et "
"normalement vous pouvez aussi couper-coller le texte ci-haut."
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:70
msgid ""
"Ce fichier va bien s'occuper des marques d'interrogation et d'exclamation, "
"les deux-points et le point-virgule. Quant aux guillemets on va créer un "
"fichier de configuration, spécifique « french.vim » qui se trouvera à "
"l'endroit suivant:"
msgstr ""

#. type: Table
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:77
#, no-wrap
msgid ""
"|(vim/gim):\n"
"|~/.vim/spell/fr.vim\n"
"|(nvim):\n"
"|~/.config/nvim/spell/fr.vim\n"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:80
msgid "Ce fichier contient le suivant:"
msgstr ""

#. type: delimited block -
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:85
#, no-wrap
msgid ""
"setl spell complete+=kspell\n"
"setl keymap=fr\n"
msgstr ""

#. type: delimited block -
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:89
#, no-wrap
msgid ""
":imap <expr> \" getline('.')[col('.')-2]=~'\\S' ?  ' »' : '« '\n"
"set background=dark\n"
"set wordwrap\n"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:92
msgid ""
"Ce fichier va gérer non seulement le chargement du « keymap », mais va aussi "
"remplacer en temps réel les \" anglais avec les « ou » selon la position et "
"y compris l'espace insécable. Le fichier se charge autommatiquement si vous "
"changer la langue de correction d'orthorgraphe en français."
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:94
msgid ""
"lancer :set spelllang=fr pour activer les paramêtres français.  (La "
"correction d'orthographe est indépenente des paramêtres de clavier. Vous "
"pouvez activer ou désactiver la correction avec set spell ou set nospell)"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:96
msgid ""
"Ce fichier va aussi change la palette de couleurs selon que la langue du "
"fichier et détermine aussi une option de retour automatique. Vous pouvez "
"ajouter ici n'importe quelle commande vim pour personaliser l'environnement "
"de travail en français. (Davantage sur ce sujet une prochaine fois)"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:98
msgid ""
"Alors si vous changer votre dictionnaire pour corriger l'orthographe en "
"français, les options de clavier vont suivre automatiquement !"
msgstr ""

#. type: Title ==
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:99
#, no-wrap
msgid "Allez vous essayer ?  Formidable !"
msgstr ""
