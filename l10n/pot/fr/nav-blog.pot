# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Boyd
# This file is distributed under the same license as the Blog package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Blog 1.0\n"
"POT-Creation-Date: 2022-10-04 22:05+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: frn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#: content/fr/modules/ROOT/nav.adoc:2
msgid "Catégories"
msgstr ""

#. type: Plain text
#: content/fr/modules/ROOT/nav.adoc:3
msgid "xref:fr@blog:ROOT:technology.adoc[Technologie]"
msgstr ""

#. type: Plain text
#: content/fr/modules/ROOT/nav.adoc:4
msgid "xref:fr@blog:ROOT:anglais-francais.adoc[Anglais en français]"
msgstr ""

#. type: Plain text
#: content/fr/modules/ROOT/nav.adoc:5
msgid "xref:fr@blog:ROOT:julakan.adoc[Julakan]"
msgstr ""

#. type: Plain text
#: content/fr/modules/ROOT/nav.adoc:7
msgid "Tous les posts..."
msgstr ""

#. type: Plain text
#: content/fr/modules/ROOT/nav.adoc:8
msgid "xref:fr@blog:ROOT:2022-07-18-hunspell-la.adoc[Meilleur hunspell-la]"
msgstr ""

#. type: Plain text
#: content/fr/modules/ROOT/nav.adoc:9
msgid "xref:fr@blog:ROOT:2022-07-15-hunspell-fr.adoc[Meilleur hunspell-fr]"
msgstr ""

#. type: Plain text
#: content/fr/modules/ROOT/nav.adoc:10 content/fr/modules/ROOT/nav.adoc:38
msgid "xref:en@blog:ROOT:2022-01-04-antora.adoc[Multilingual Antora]"
msgstr ""

#. type: Plain text
#: content/fr/modules/ROOT/nav.adoc:11
msgid ""
"xref:fr@blog:ROOT:2021-12-28-protein.adoc[Protéines pour les athlètes "
"ivoiriens]"
msgstr ""

#. type: Plain text
#: content/fr/modules/ROOT/nav.adoc:12
msgid "xref:fr@blog:ROOT:proverb.adoc[Qui se sent morveux se mouche ]"
msgstr ""

#. type: Plain text
#: content/fr/modules/ROOT/nav.adoc:13
msgid ""
"xref:fr@blog:ROOT:2021-07-14-anki.adoc[\"Les écueils d'Anki pour apprendre "
"une langue ?\"]"
msgstr ""

#. type: Plain text
#: content/fr/modules/ROOT/nav.adoc:14
msgid ""
"xref:fr@blog:ROOT:2021-04-23-animal.adoc[Les animaux en Jula de Côte "
"d'Ivoire ]"
msgstr ""

#. type: Plain text
#: content/fr/modules/ROOT/nav.adoc:15
msgid ""
"xref:fr@blog:ROOT:2021-01-02-adoc-vim.adoc[Astuces pour éditer asciidoc avec "
"neovim/Vim (comme un boss !) ]"
msgstr ""

#. type: Plain text
#: content/fr/modules/ROOT/nav.adoc:16
msgid ""
"xref:fr@blog:ROOT:2020-12-06-w100.adoc[Les 100 mots les plus fréquents en "
"Jula de Côte d'Ivoire ]"
msgstr ""

#. type: Plain text
#: content/fr/modules/ROOT/nav.adoc:17
msgid ""
"xref:fr@blog:ROOT:2020-11-09-sb-bashrc.adoc[\"Silverblue Tip - lancez des "
"commandes partout\"]"
msgstr ""

#. type: Plain text
#: content/fr/modules/ROOT/nav.adoc:18
msgid ""
"xref:fr@blog:ROOT:clavier-ivoirien.adoc[Clavier Ivoirien sans gymnastiques]"
msgstr ""

#. type: Plain text
#: content/fr/modules/ROOT/nav.adoc:19
msgid ""
"xref:fr@blog:ROOT:2020-10-21-fonts.adoc[Polices d'écriture adaptées pour le "
"bambara, le jula (et d'autres langues mandingues)]"
msgstr ""

#. type: Plain text
#: content/fr/modules/ROOT/nav.adoc:20
msgid ""
"xref:fr@blog:ROOT:2020-08-26-fontlist.adoc[Polices d'écriture web pour "
"langues mandingues]"
msgstr ""

#. type: Plain text
#: content/fr/modules/ROOT/nav.adoc:21
msgid "xref:fr@blog:ROOT:ngalonci.adoc[Menteur]"
msgstr ""

#. type: Plain text
#: content/fr/modules/ROOT/nav.adoc:22
msgid ""
"xref:fr@blog:ROOT:vimplug.adoc[Gérer les modules vim sans soucis (abandonner "
"le Gestionnaire !) ]"
msgstr ""

#. type: Plain text
#: content/fr/modules/ROOT/nav.adoc:23
msgid "xref:fr@blog:ROOT:2020-11-24-money.adoc[L'argent et chiffres en Jula]"
msgstr ""

#. type: Plain text
#: content/fr/modules/ROOT/nav.adoc:24
msgid "xref:fr@blog:ROOT:nvim-plugins.adoc[Nvim plugins ]"
msgstr ""

#. type: Plain text
#: content/fr/modules/ROOT/nav.adoc:25
msgid ""
"xref:fr@blog:ROOT:clavier-android.adoc[Tapez en Baoulé ou Julakan comme un "
"boss sous Android !]"
msgstr ""

#. type: Plain text
#: content/fr/modules/ROOT/nav.adoc:26
msgid "xref:fr@blog:ROOT:2020-04-23-website.adoc[Mise-à-jour du site web !]"
msgstr ""

#. type: Plain text
#: content/fr/modules/ROOT/nav.adoc:27
msgid ""
"xref:fr@blog:ROOT:ponctuation-francaise.adoc[Ponctuation française facile "
"sous Linux avec nvim/vim]"
msgstr ""

#. type: Plain text
#: content/fr/modules/ROOT/nav.adoc:28
msgid "xref:fr@blog:ROOT:a-sen-b-a-la.adoc[a sen b'a la]"
msgstr ""

#. type: Plain text
#: content/fr/modules/ROOT/nav.adoc:29
msgid ""
"xref:fr@blog:ROOT:2018-07-30-alpha-jula.adoc[Guide de prononciation Jula "
"pour les francophones]"
msgstr ""

#. type: Plain text
#: content/fr/modules/ROOT/nav.adoc:30
msgid ""
"xref:fr@blog:ROOT:2018-05-18-anki.adoc[Cartes Anki pour apprendre le Jula]"
msgstr ""

#. type: Plain text
#: content/fr/modules/ROOT/nav.adoc:31
msgid "xref:fr@blog:ROOT:datally.adoc[Datally]"
msgstr ""

#. type: Plain text
#: content/fr/modules/ROOT/nav.adoc:32
msgid "xref:fr@blog:ROOT:ysa.adoc[Yet Still Again ?]"
msgstr ""

#. type: Plain text
#: content/fr/modules/ROOT/nav.adoc:33
msgid "xref:fr@blog:ROOT:2016-04-21-justdoit.adoc[Just do it !]"
msgstr ""

#. type: Plain text
#: content/fr/modules/ROOT/nav.adoc:35
msgid "Asciidoc(tor) & Antora"
msgstr ""

#. type: Plain text
#: content/fr/modules/ROOT/nav.adoc:36
msgid ""
"xref:en@blog:ROOT:asciidoc-syntax-quick-reference.adoc[Syntax AsciiDoc "
"référence rapide]"
msgstr ""

#. type: Plain text
#: content/fr/modules/ROOT/nav.adoc:37
msgid ""
"xref:en@blog:ROOT:asciidoc-quick-reference.adoc[AsciiDoc référence rapide]"
msgstr ""

#. type: Plain text
#: content/fr/modules/ROOT/nav.adoc:38
msgid "xref:en@blog:ROOT:demo.adoc[Asciidoctor Demo]"
msgstr ""
