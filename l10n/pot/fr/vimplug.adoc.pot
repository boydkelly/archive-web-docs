# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Boyd
# This file is distributed under the same license as the Blog package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Blog 1.0\n"
"POT-Creation-Date: 2022-07-16 22:37-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: frn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Attribute :lang:
#: content/fr/modules/blog/pages/mogo.adoc:12
#: content/fr/modules/blog/pages/2020-08-26-fontlist.adoc:18
#: content/fr/modules/blog/pages/vimplug.adoc:17
#: content/fr/modules/blog/pages/a-sen-b-a-la.adoc:13
#: content/fr/modules/blog/pages/datally.adoc:16
#: content/fr/modules/blog/pages/font-test.adoc:17
#: content/fr/modules/blog/pages/clavier-ivoirien.adoc:15
#: content/fr/modules/blog/pages/2020-12-06-w100.adoc:21
#: content/fr/modules/blog/pages/2021-04-23-animal.adoc:20
#: content/fr/modules/blog/pages/2020-11-24-money.adoc:27
#, no-wrap
msgid "fr "
msgstr ""

#. type: YAML Front Matter: catégories
#: content/fr/modules/blog/pages/nvim-plugins.adoc:1
#: content/fr/modules/blog/pages/vimplug.adoc:1
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:1
#: content/fr/modules/blog/pages/2020-04-23-website.adoc:1
#: content/fr/modules/blog/pages/datally.adoc:1
#: content/fr/modules/blog/pages/clavier-ivoirien.adoc:1
#: content/fr/modules/blog/pages/sb-bashrc.adoc:1
#: content/fr/modules/blog/pages/2021-01-02-adoc-vim.adoc:1
#, no-wrap
msgid "[\"Technologie\"]"
msgstr ""

#. type: Attribute :category:
#: content/fr/modules/blog/pages/nvim-plugins.adoc:19
#: content/fr/modules/blog/pages/vimplug.adoc:16
#: content/fr/modules/blog/pages/clavier-android.adoc:18
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:18
#: content/fr/modules/blog/pages/2020-04-23-website.adoc:18
#: content/fr/modules/blog/pages/datally.adoc:15
#: content/fr/modules/blog/pages/font-test.adoc:18
#: content/fr/modules/blog/pages/clavier-ivoirien.adoc:17
#: content/fr/modules/blog/pages/sb-bashrc.adoc:15
#: content/fr/modules/blog/pages/2021-01-02-adoc-vim.adoc:18
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:20
#: content/fr/modules/blog/pages/2021-07-14-anki.adoc:13
#: content/fr/modules/blog/pages/technology.adoc:5
#: content/fr/modules/blog/pages/technology.adoc:7
#: content/fr/modules/blog/pages/anglais-francais.adoc:7
#: content/fr/modules/blog/pages/julakan.adoc:7
#: content/fr/modules/blog/pages/2022-07-15-hunspell-fr.adoc:20
#, no-wrap
msgid "Technologie"
msgstr ""

#. type: YAML Front Matter: image
#: content/fr/modules/blog/pages/vimplug.adoc:1
#: content/fr/modules/blog/pages/2021-01-02-adoc-vim.adoc:1
#, no-wrap
msgid "/images/nvim_94554.webp"
msgstr ""

#. type: YAML Front Matter: tags
#: content/fr/modules/blog/pages/vimplug.adoc:1
#, no-wrap
msgid "[\"linux\", \"vim\", \"asciidoc\",\"neovim\"]"
msgstr ""

#. type: YAML Front Matter: title
#: content/fr/modules/blog/pages/vimplug.adoc:1
#, no-wrap
msgid "Gérer les modules vim sans soucis (abandonner le Gestionnaire !)"
msgstr ""

#. type: Title =
#: content/fr/modules/blog/pages/vimplug.adoc:10
#, no-wrap
msgid "Gérer les modules vim sans soucis (abandonner le Gestionnaire !) "
msgstr ""

#. type: Attribute :description:
#: content/fr/modules/blog/pages/vimplug.adoc:13
#, no-wrap
msgid "Abandonner le Gestionnaire de modules vim ! "
msgstr ""

#. type: Attribute :keywords:
#: content/fr/modules/blog/pages/vimplug.adoc:14
#: content/fr/modules/blog/pages/vimplug.adoc:15
#, no-wrap
msgid "vim, asciidoc, neovim"
msgstr ""

#. type: Table
#: content/fr/modules/blog/pages/vimplug.adoc:24
#, no-wrap
msgid ""
"a|image::vim_94609.webp[]\n"
"a|image::nvim_94554.webp[]\n"
msgstr ""

#. type: Title ==
#: content/fr/modules/blog/pages/vimplug.adoc:26
#, no-wrap
msgid "Gérer les modules Vim et Nvim facilement avec les gestionnaires inégrés !"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/vimplug.adoc:30
msgid ""
"Les versions récentes de Vim et Nvim sont équipées de gestionnaire de "
"modules, mais ce n'est pas tout le monde qui en profite.  Eh bien voici "
"quelques raisons d'abandonner Pathogen, Vim-Plug, Vundle, et même minpac "
"(qui est quand même basé sur le gestionnaire intégré pour gérer le chemin "
"d'execution)"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/vimplug.adoc:32
msgid ""
"D'abord quels sont les opérations courantes d'un gestionnaire de modules ?"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/vimplug.adoc:34
msgid "Installer un module"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/vimplug.adoc:35
msgid "Désinstller un module"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/vimplug.adoc:36
msgid "Activer un module"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/vimplug.adoc:37
msgid "Désactiver un module"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/vimplug.adoc:38
msgid "Lister les modules installés"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/vimplug.adoc:39
msgid "Mettre à jour les modules"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/vimplug.adoc:40
msgid "Quoi d'autre ?"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/vimplug.adoc:43
msgid ""
"Certains vont affirmer que vimplug ou bien d'autres gestionnaires "
"simplifient ces taches.  En tant que minimaliste, je ne suis pas d'accord."
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/vimplug.adoc:46
msgid ""
"J'ai même installé minpack et puis rapidement ne n'en ai pas vu le besoin. "
"Pourquoi donner une instruction à un élément externe pour charger un module "
"que vim peut faire sans *aucune* configuration ! Et la configuration "
"supplémentaire a fini par encombrer mon fichier init.vim avec les "
"instructions de minpac que vim fait lui-même."
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/vimplug.adoc:54
msgid ""
"Alors si on abandonne son gestionnaires de module préféré, comment on fait ? "
"Eh bien la plupart des gens utilisent déjà git.  Alors remplacer les "
"quelques 4 ou 5 commandes de votre ancien gestionnaire de module avec une "
"commande git correspondante.  Et le bienfait supplémentaire c'est que vous "
"n'aurez *rien* de plus dans votre ficher init.  Vous avez déjà installé Vim "
"ou Nvim ? Laissez le faire le travail ici ! Les commandes git dont vous "
"aurez besoin sont :"
msgstr ""

#. type: delimited block -
#: content/fr/modules/blog/pages/vimplug.adoc:64
#, no-wrap
msgid ""
"pluginstall:: git submodule add [url de github]\n"
"plugremove:: git submodule deinit start/opt/[module]\n"
"pluglist:: git submodule \n"
"plugdisable:: git submodule mv start/[module] /opt\n"
"plugupdate:: git submodule update --remote --recursive\n"
"plugsaveall:: git commit -a -m \"Installé mon nouveau module\" && git push origin master\n"
msgstr ""

#. type: delimited block *
#: content/fr/modules/blog/pages/vimplug.adoc:68
msgid ""
"Si vous désinstallez un module, vous aurez aussi besoin de lancer une "
"deuxièment commande pour vous débarraser de toutes ses traces :"
msgstr ""

#. type: delimited block -
#: content/fr/modules/blog/pages/vimplug.adoc:72
#, no-wrap
msgid "git rm --cached [chemin du module] \n"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/vimplug.adoc:75
msgid ""
"Vous pouvez bien sûr utiliser des alias bash si ces commandes semblent trop "
"longues !"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/vimplug.adoc:77
msgid ""
"Pour démarrer cette nouvelle façon de faire vous n'avez que vous rendre dans "
"votre répertoie de modules et créer les sous répertoires s'ils n'existe pas "
"déjà."
msgstr ""

#. type: delimited block -
#: content/fr/modules/blog/pages/vimplug.adoc:83
#, no-wrap
msgid ""
"vim:  cd ~/.vim/pack/plugins\n"
"nvim: cd /.local/share/nvim/site/pack/plugins/\n"
"mkdir {opt,start}\n"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/vimplug.adoc:86
msgid ""
"Se nécessaire, vous devez créer un dépot git dans ce répertoire « plugins » "
"et faire un push vers github/gitlab."
msgstr ""

#. type: delimited block -
#: content/fr/modules/blog/pages/vimplug.adoc:94
#, no-wrap
msgid ""
"git init\n"
"git add *\n"
"git commit -a -m \"mes modules\"\n"
"git add remote [url du projet de votre compte git]\n"
"git push -u orign master\n"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/vimplug.adoc:97
msgid "Alors ce n'est pas compliqué !"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/vimplug.adoc:101
msgid ""
"Maintenant si vous êtes sur n'importe quelle autre machine, vous pouvez "
"lancer « git clone myplugins » ;  Ensuite « git submodule init --"
"recursive..  Et vous venez d'installer tous vos modules."
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/vimplug.adoc:103
msgid ""
"L'avantage inattendu de cette façon est que vous n'avez plus de tracas de "
"configuration de votre .vimrc ou init.nvim."
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/vimplug.adoc:105
msgid "La preuve ? Voici mon fichier de configuration init.nvim actuel :"
msgstr ""

#. type: delimited block -
#: content/fr/modules/blog/pages/vimplug.adoc:110
#, no-wrap
msgid ""
"filetype plugin indent on\n"
"syntax enable\n"
msgstr ""

#. type: delimited block -
#: content/fr/modules/blog/pages/vimplug.adoc:126
#, no-wrap
msgid ""
"set encoding=utf-8\n"
"set hidden\n"
"set noerrorbells\n"
"set smartindent\n"
"set smartcase\n"
"set nobackup\n"
"set undodir=~/.config/nvim/undodir\n"
"set undofile\n"
"set incsearch\n"
"set path+=**\n"
"set wildmenu\n"
"set ts=2\n"
"set sw=2\n"
"set expandtab\n"
"set nowrap\n"
msgstr ""

#. type: delimited block -
#: content/fr/modules/blog/pages/vimplug.adoc:128
#, no-wrap
msgid "silent! helptags ALL\n"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/vimplug.adoc:131
msgid "C'est tout ! A+"
msgstr ""

#. type: Block title
#: content/fr/modules/blog/pages/vimplug.adoc:134
#: content/fr/modules/blog/pages/2021-01-02-adoc-vim.adoc:162
#, no-wrap
msgid "Pages connexes"
msgstr ""
