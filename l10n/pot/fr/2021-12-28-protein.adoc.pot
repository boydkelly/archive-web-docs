# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Boyd
# This file is distributed under the same license as the Blog package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Blog 1.0\n"
"POT-Creation-Date: 2022-09-20 08:20+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: frn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Attribute :lang:
#: content/fr/modules/blog/pages/2020-10-21-fonts.adoc:21
#: content/fr/modules/blog/pages/nvim-plugins.adoc:20
#: content/fr/modules/blog/pages/proverb.adoc:1
#: content/fr/modules/blog/pages/clavier-android.adoc:13
#: content/fr/modules/blog/pages/ngalonci.adoc:1
#: content/fr/modules/blog/pages/ngalonci.adoc:16
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:19
#: content/fr/modules/blog/pages/ysa.adoc:13
#: content/fr/modules/blog/pages/2018-05-18-anki.adoc:13
#: content/fr/modules/blog/pages/2016-04-21-justdoit.adoc:15
#: content/fr/modules/blog/pages/2020-04-23-website.adoc:1
#: content/fr/modules/blog/pages/2020-04-23-website.adoc:13
#: content/fr/modules/blog/pages/font-test.adoc:1
#: content/fr/modules/blog/pages/2020-11-09-sb-bashrc.adoc:1
#: content/fr/modules/blog/pages/2020-11-09-sb-bashrc.adoc:14
#: content/fr/modules/blog/pages/2021-01-02-adoc-vim.adoc:19
#: content/fr/modules/blog/pages/2018-07-30-alpha-jula.adoc:21
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:23
#: content/fr/modules/blog/pages/2021-07-14-anki.adoc:1
#: content/fr/modules/blog/pages/2021-07-14-anki.adoc:12
#: content/fr/modules/blog/pages/2022-07-15-hunspell-fr.adoc:23
#: content/fr/modules/blog/pages/2022-07-18-hunspell-la.adoc:21
#, no-wrap
msgid "fr"
msgstr ""

#. type: Attribute :category:
#: content/fr/modules/blog/pages/nvim-plugins.adoc:19
#: content/fr/modules/blog/pages/vimplug.adoc:16
#: content/fr/modules/blog/pages/clavier-android.adoc:18
#: content/fr/modules/blog/pages/ponctuation-francaise.adoc:18
#: content/fr/modules/blog/pages/2020-04-23-website.adoc:18
#: content/fr/modules/blog/pages/datally.adoc:15
#: content/fr/modules/blog/pages/font-test.adoc:18
#: content/fr/modules/blog/pages/clavier-ivoirien.adoc:17
#: content/fr/modules/blog/pages/2020-11-09-sb-bashrc.adoc:15
#: content/fr/modules/blog/pages/2021-01-02-adoc-vim.adoc:18
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:20
#: content/fr/modules/blog/pages/2021-07-14-anki.adoc:13
#: content/fr/modules/blog/pages/technology.adoc:5
#: content/fr/modules/blog/pages/technology.adoc:7
#: content/fr/modules/blog/pages/anglais-francais.adoc:7
#: content/fr/modules/blog/pages/julakan.adoc:7
#: content/fr/modules/blog/pages/2022-07-15-hunspell-fr.adoc:20
#: content/fr/modules/blog/pages/2022-07-18-hunspell-la.adoc:19
#, no-wrap
msgid "Technologie"
msgstr ""

#. type: Attribute :page-tags:
#: content/fr/modules/blog/pages/2018-07-30-alpha-jula.adoc:93
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:21
#, no-wrap
msgid "santé"
msgstr ""

#. type: YAML Front Matter: categories
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:1
#: content/fr/modules/blog/pages/2022-07-15-hunspell-fr.adoc:1
#: content/fr/modules/blog/pages/2022-07-18-hunspell-la.adoc:1
#, no-wrap
msgid "[\"Technology\"]"
msgstr ""

#. type: YAML Front Matter: tags
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:1
#: content/fr/modules/blog/pages/2022-07-15-hunspell-fr.adoc:1
#: content/fr/modules/blog/pages/2022-07-18-hunspell-la.adoc:1
#, no-wrap
msgid "[\"tech\", \"android\", \"afrique\"]"
msgstr ""

#. type: YAML Front Matter: title
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:1
#, no-wrap
msgid "Protéine pour les athlètes ivoiriens"
msgstr ""

#. type: Title =
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:11
#, no-wrap
msgid "Protéine pour les athlètes ivoiriens "
msgstr ""

#. type: Attribute :description:
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:17
#, no-wrap
msgid "Quelques suggestions aux les athlètes en Côte d'Ivoire, de sources d'aliments riche en protéines et facilement disponibles."
msgstr ""

#. type: Attribute :keywords:
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:18
#, no-wrap
msgid "protéine, santé, Côte d'Ivoire, athlètes, nourriture"
msgstr ""

#. type: Attribute :tags:
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:19
#, no-wrap
msgid "[\"tech\", \"santé\", \"afrique\"]"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:32
msgid ""
"image:protein.webp[Avocats et oeufs,400,float=left] Tandis que le citoyen "
"moyen inactif doit consommé environ .8g de protéine par jour, les athlètes "
"eux doivent normalement en consommer de 1.2g a 2g de protéines par jour par "
"kilo.  Cette consomation dépend aussi du niveau de l'activité physique et "
"les objectifs en vue.  Donc un athlète qui pèse 80kg et qui veut se "
"développer doit consommer jusqu'à 160g de protéine par jour.  Ce protéine "
"doit parvenir de sources diverses. Voici alors quelques suggestions aux "
"athlètes de source de protéines facilement disponibles en Côte d'Ivoire et "
"riches en protéines."
msgstr ""

#. type: Title ==
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:33
#, no-wrap
msgid "Sources animales"
msgstr ""

#. type: Title ===
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:35
#, no-wrap
msgid "Lait"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:39
msgid ""
"Le lait (non sucré bien sûr ! ! !), est une très bonne source de protéine.  "
"Une tasse de lait entier contient 8g de protéine.  Consommez du yaourt non "
"sucré avec des fruits."
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:43
msgid ""
"Le lait en poudre lui mérite une attention particulière.  Ce lait contient 2 "
"sortes de protéines, 80% caséine, et 20% protéines de petit lait.  Ce 80% de "
"casein digère plus lentement et est donc conseillé de prendre la nuit."
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:47
msgid ""
"Des sportifs payent très cher les 'poudres' de protéine, mais le meilleur "
"poudre se trouve au supermarché à moindre prix.  Assurez vous d'acheter le "
"lait en poudre écrémé avec 0% de matières grasses. On peut aussi faire du "
"très bon yaourt avec ce lait."
msgstr ""

#. type: Title ===
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:48
#, no-wrap
msgid "Poulet"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:52
msgid ""
"Ah oui... mais les poulets africains sont mieux que ceux bourrés d'hormones "
"de croissance.  Privilégiez surtout le blanc du poulet, et rejetez la "
"peau (source de gras non nécessaire) !"
msgstr ""

#. type: Title ===
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:53
#, no-wrap
msgid "Thon et autres poissons"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:57
msgid ""
"Le garba (avec le moins d'huile que possible), accompagné d'une bonne "
"salade, est un repas excellent pour un athlète ! (Pardon, les oignons et "
"accompagnements traditionnelles ne compte pas comme 'salade'.  Il faut "
"plutôt une bonne portion des légumes !)"
msgstr ""

#. type: Title ===
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:58
#, no-wrap
msgid "Boeuf"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:62
msgid ""
"100g de boeuf contient 28g de protéine.  Le choucouya, pourrait être une "
"source façile de protéine, pour atteindre un cible quotidien.  Par contre il "
"faut rejeter le gras. Mieux vaux trouver la viande qui est braisée sans de "
"l'huile ajouté."
msgstr ""

#. type: Title ===
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:63
#, no-wrap
msgid "Oeufs"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:68
msgid ""
"Les oeufs sont une des sources de protéines les moins chers.  Un oeuf "
"contient 12.6 gram de protéine (pour 100F !).  Éviter de manger des "
"omelettes en kiosque préparé dans l'huile.  Préférez les oeufs bouillis sur "
"le bord de la route avec un peu de sel et kan kan kan. (Pas le poudre poison "
"magi.)"
msgstr ""

#. type: Title ==
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:69
#, no-wrap
msgid "Sources végétales"
msgstr ""

#. type: Title ===
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:71
#, no-wrap
msgid "Épinards"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:77
msgid ""
"Les épinards constituent un des meilleurs aliments en Côte d'Ivoire.  Une "
"tasse environ d'épinards cuites contient 5.6g de protéine.  Mais ce n'est "
"pas tout.  Les épinards sont tellement remplis de vitamines et minéraux, si "
"vous les mangez régulièrement vous n'aurez aucun besoin de suppléants ni de "
"remontants de votre pharmacien !"
msgstr ""

#. type: Block title
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:78
#, no-wrap
msgid "Épinards sont incroyables !"
msgstr ""

#. type: Table
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:168
#, no-wrap
msgid ""
"|Calcium, Ca \n"
"|(mg)\n"
"|244.8\n"
" \n"
"|Fer, Fe \n"
"|(mg)\n"
"|6.43\n"
" \n"
"|Magnèse, Mg \n"
"|(mg)\n"
"|156.6\n"
" \n"
"|Phosphore, P \n"
"|(mg)\n"
"|100.8\n"
" \n"
"|Potassium, K \n"
"|(mg)\n"
"|838.8\n"
" \n"
"|Sodium, Na \n"
"|(mg)\n"
"|126\n"
" \n"
"|Zinc, Zn \n"
"|(mg)\n"
"|1.37\n"
" \n"
"|Cuivre, Cu \n"
"|(mg)\n"
"|0.31\n"
" \n"
"|Manganèse, Mn \n"
"|(mg)\n"
"|1.68\n"
" \n"
"|Sélénium, Se \n"
"|(mcg)\n"
"|2.7\n"
" \n"
"|Vitamine A, \n"
"|(IU)\n"
"|18865.8\n"
" \n"
"|Carotène, bêta \n"
"|(mcg)\n"
"|11318.4\n"
" \n"
"|Vitamine E \n"
"|(mg)\n"
"|3.74\n"
" \n"
"|Lutein + zeaxanthin \n"
"|(mcg)\n"
"|20354.4\n"
" \n"
"|Vitamine C\n"
"|(mg)\n"
"|17.64\n"
" \n"
"|Thiamine \n"
"|(mg)\n"
"|0.17\n"
" \n"
"|Riboflavine \n"
"|(mg)\n"
"|0.42\n"
" \n"
"|Niacine \n"
"|(mg)\n"
"|0.88\n"
" \n"
"|Acide pantothénique \n"
"|(mg)\n"
"|0.26\n"
" \n"
"|Vitamine B-6\n"
"|(mg)\n"
"|0.44\n"
" \n"
"|Foliate\n"
"|(mcg)\n"
"|262.8\n"
" \n"
"|Vitamine K\n"
"|(mcg)\n"
"|888.48\n"
msgstr ""

#. type: Title ===
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:170
#, no-wrap
msgid "Arachides"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:175
msgid ""
"Les arachides sont partout ! Au lieu de manger des sucreries et gâteaux, "
"manger des arachides.  Pour 100g d'arachides vous absorbez 25.8g de "
"protéine. C'est très fort !"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:178
msgid ""
"Une sauce feuille épinards avec arachides accompagné de poulet, boeuf ou "
"poisson, c'est une excellente source de protéine pour un athlète."
msgstr ""

#. type: Title ===
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:179
#, no-wrap
msgid "Lentilles, Haricots rouge, Haricots blancs"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:184
msgid ""
"Disponibles séchés au marché, ou encore au supermarché en boite, ces "
"aliments sont très riche en protéines.  Une tasse de ces légumineuses peut "
"contenir de 6 à 15g de protéines.  Remplacez le riz avec des lentilles !"
msgstr ""

#. type: Title ===
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:185
#, no-wrap
msgid "Avocats"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:188
msgid ""
"Non seulement délicieux et source de bon gras essentiels, un avocat peut "
"contenir 4g de protéines. Essentiels pour votre salade."
msgstr ""

#. type: Title ===
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:189
#, no-wrap
msgid "Pois chiches"
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:193
msgid ""
"Disponibles en boîte dans presque tous les supermarchés, et aussi en version "
"séchés, surtout chez les fournisseurs libanais.  Ces 'pois' sont en effets "
"très forts en protéine avec 19g de protéine pour 100g de pois chiches."
msgstr ""

#. type: Plain text
#: content/fr/modules/blog/pages/2021-12-28-protein.adoc:196
msgid ""
"Pour ceux qui n'ont pas le temps....  Ajouter à une boite de pois chiches, "
"une boite de haricots verts, une oignon coupé, tomates, et avocats, et vous "
"avez un repas végétarien chargé de protéine."
msgstr ""
