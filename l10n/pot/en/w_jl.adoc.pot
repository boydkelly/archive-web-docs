# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Boyd
# This file is distributed under the same license as the Blog package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Blog 1.0\n"
"POT-Creation-Date: 2022-10-04 22:25+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Attribute :category:
#: content/en/modules/ROOT/pages/asciidoc-quick-reference.adoc:724
#: content/en/modules/ROOT/pages/asciidoc-quick-reference.adoc:753
#: docs/en/modules/ROOT/pages/w_jl.adoc:18
#: docs/en/modules/ROOT/pages/w_jl_w100.adoc:18
#, no-wrap
msgid "Language"
msgstr ""

#. type: Attribute :lang:
#: docs/en/modules/resources/partials/bebiphilip.adoc:21
#: docs/en/modules/resources/partials/bible.adoc:20
#: docs/en/modules/resources/partials/mandenkan.com.adoc:19
#: docs/en/modules/resources/partials/jw.org.adoc:21
#: docs/en/modules/resources/pages/fonts_google.adoc:19
#: docs/en/modules/resources/pages/fonts_ngalonci.adoc:19
#: docs/en/modules/ROOT/pages/w_jl.adoc:19
#: docs/en/modules/ROOT/pages/w_jl_w100.adoc:19
#: docs/en/modules/ROOT/pages/home.adoc:10
#: docs/en/modules/ROOT/pages/index.adoc:10
#: content/en/modules/ROOT/pages/font-test.adoc:17
#, no-wrap
msgid "en "
msgstr ""

#. type: YAML Front Matter: categories
#: docs/en/modules/ROOT/pages/w_jl.adoc:1
#: docs/en/modules/ROOT/pages/w_jl_w100.adoc:1
#, no-wrap
msgid "[\"Language\"]"
msgstr ""

#. type: YAML Front Matter: tags
#: docs/en/modules/ROOT/pages/w_jl.adoc:1
#: docs/en/modules/ROOT/pages/w_jl_w100.adoc:1
#, no-wrap
msgid "[\"watchtower\", \"jula\", \"language\"]"
msgstr ""

#. type: YAML Front Matter: title
#: docs/en/modules/ROOT/pages/w_jl.adoc:1
#, no-wrap
msgid "Morphology of the Jula language in the Watchtower"
msgstr ""

#. type: Title =
#: docs/en/modules/ROOT/pages/w_jl.adoc:10
#, no-wrap
msgid "Morphology of the Jula language in the Watchtower "
msgstr ""

#. type: Attribute :description:
#: docs/en/modules/ROOT/pages/w_jl.adoc:15
#, no-wrap
msgid "Morphology study of the Jula language in the Watchtower magazine "
msgstr ""

#. type: Attribute :keywords:
#: docs/en/modules/ROOT/pages/w_jl.adoc:16
#: docs/en/modules/ROOT/pages/w_jl_w100.adoc:16
#, no-wrap
msgid "watchtower, jula, language"
msgstr ""

#. type: Attribute :page-tags:
#: docs/en/modules/ROOT/pages/w_jl.adoc:17
#, no-wrap
msgid "watchtower, jula, language "
msgstr ""

#. type: Plain text
#: docs/en/modules/ROOT/pages/w_jl.adoc:22
msgid "image:{page-image}[Alt text,300]"
msgstr ""

#. type: Block title
#: docs/en/modules/ROOT/pages/w_jl.adoc:24
#, no-wrap
msgid "See also:"
msgstr ""

#. type: delimited block =
#: docs/en/modules/ROOT/pages/w_jl.adoc:27
msgid ""
"xref:en@docs:ROOT:w_jl_w100.adoc[Compiliation of the 100 most common Jula "
"words in the Watchtower]"
msgstr ""

#. type: delimited block =
#: docs/en/modules/ROOT/pages/w_jl.adoc:28
msgid ""
"xref:en@blog:ROOT:2020-12-06-w100.adoc[The 100 most common words in Ivory "
"Coast Jula (Kodi)]"
msgstr ""

#. type: Block title
#: docs/en/modules/ROOT/pages/w_jl.adoc:31
#, no-wrap
msgid "These figures may change for several reasons:"
msgstr ""

#. type: delimited block =
#: docs/en/modules/ROOT/pages/w_jl.adoc:34
msgid "New editions are added to the inventory of words"
msgstr ""

#. type: delimited block =
#: docs/en/modules/ROOT/pages/w_jl.adoc:35
msgid ""
"Errors may occur with French words that must be extracted from the Jula text."
msgstr ""

#. type: delimited block =
#: docs/en/modules/ROOT/pages/w_jl.adoc:36
msgid "Proper names in French and Jula present difficulies."
msgstr ""

#. type: delimited block =
#: docs/en/modules/ROOT/pages/w_jl.adoc:37
msgid "The consideration of the plural may change"
msgstr ""
