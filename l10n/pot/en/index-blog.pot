# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Boyd
# This file is distributed under the same license as the Blog package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Blog 1.0\n"
"POT-Creation-Date: 2022-10-05 11:48+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Attribute :lang:
#: docs/en/modules/resources/pages/index.adoc:9
#: docs/en/modules/resources/partials/01-mediaplus.adoc:20
#: docs/en/modules/resources/partials/ankataa.com.adoc:20
#: docs/en/modules/resources/partials/rfi.adoc:18
#: docs/en/modules/ROOT/pages/index.adoc:11
#: content/en/modules/ROOT/pages/about.adoc:16
#: content/en/modules/ROOT/pages/linkedin.adoc:10
#: content/en/modules/ROOT/pages/contact.adoc:10
#: content/en/modules/ROOT/pages/home.adoc:12
#: content/en/modules/ROOT/pages/index.adoc:11
#: content/en/modules/ROOT/partials/contact.adoc:10
#, no-wrap
msgid "en"
msgstr ""

#. type: Attribute :keywords:
#: docs/en/modules/ROOT/pages/home.adoc:8
#: docs/en/modules/ROOT/pages/index.adoc:9
#: content/en/modules/ROOT/pages/home.adoc:10
#: content/en/modules/ROOT/pages/index.adoc:9
#, no-wrap
msgid "index, home, technology, jula, julakan, dioula, dyula, abidjan, côte d'ivoire"
msgstr ""

#. type: Attribute :page-tags:
#: docs/en/modules/ROOT/pages/home.adoc:9
#: docs/en/modules/ROOT/pages/index.adoc:10
#: content/en/modules/ROOT/pages/home.adoc:11
#: content/en/modules/ROOT/pages/index.adoc:10
#, no-wrap
msgid "tech, julakan, africa"
msgstr ""

#. type: Block title
#: docs/en/modules/ROOT/pages/home.adoc:20
#: docs/en/modules/ROOT/pages/index.adoc:18
#: content/en/modules/ROOT/pages/home.adoc:19
#: content/en/modules/ROOT/pages/index.adoc:18
#, no-wrap
msgid "[#msg]#proverb#"
msgstr ""

#. type: delimited block =
#: docs/en/modules/ROOT/pages/home.adoc:24
#: docs/en/modules/ROOT/pages/index.adoc:22
#: content/en/modules/ROOT/pages/home.adoc:23
#: content/en/modules/ROOT/pages/index.adoc:22
msgid "[#french]#proverb# +"
msgstr ""

#. type: Title =
#: docs/en/modules/ROOT/pages/index.adoc:1
#: content/en/modules/ROOT/pages/home.adoc:1
#: content/en/modules/ROOT/pages/index.adoc:1
#, no-wrap
msgid "Côte d'Ivoire Language and Technology Blog "
msgstr ""

#. type: Attribute :description:
#: docs/en/modules/ROOT/pages/index.adoc:4
#: content/en/modules/ROOT/pages/index.adoc:4
#, no-wrap
msgid "\"landing page for blog about technolgy and language in africa\""
msgstr ""

#. type: Block title
#: docs/en/modules/ROOT/pages/index.adoc:15
#: content/en/modules/ROOT/pages/home.adoc:16
#: content/en/modules/ROOT/pages/index.adoc:15
#, no-wrap
msgid "Jula proverb "
msgstr ""

#. type: Target for macro image
#: docs/en/modules/ROOT/pages/index.adoc:24
#: content/en/modules/ROOT/pages/index.adoc:24
#, no-wrap
msgid "neo4j-home.webp"
msgstr ""
