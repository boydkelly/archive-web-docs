# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Boyd
# This file is distributed under the same license as the Blog package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Blog 1.0\n"
"POT-Creation-Date: 2022-09-08 15:49+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Attribute :lang:
#: content/en/modules/blog/pages/smoketest.adoc:1
#: docs/en/modules/resources/pages/index.adoc:9
#: docs/en/modules/resources/partials/01-mediaplus.adoc:20
#: docs/en/modules/resources/partials/ankataa.com.adoc:20
#: docs/en/modules/resources/partials/rfi.adoc:18
#: content/en/modules/ROOT/pages/about.adoc:16
#: content/en/modules/ROOT/pages/linkedin.adoc:10
#: content/en/modules/ROOT/pages/contact.adoc:10
#: content/en/modules/ROOT/pages/home.adoc:17
#: content/en/modules/ROOT/pages/font-test.adoc:1
#: content/en/modules/ROOT/partials/contact.adoc:10
#, no-wrap
msgid "en"
msgstr ""

#. type: YAML Front Matter: categories
#: docs/en/modules/resources/partials/01-mediaplus.adoc:1
#: docs/en/modules/resources/partials/ankataa.com.adoc:1
#: docs/en/modules/resources/partials/bible.adoc:1
#: docs/en/modules/resources/partials/mandenkan.com.adoc:1
#: docs/en/modules/resources/partials/rfi.adoc:1
#: docs/en/modules/resources/partials/jw.org.adoc:1
#: docs/en/modules/resources/pages/fonts_google.adoc:1
#: docs/en/modules/resources/pages/fonts_ngalonci.adoc:1
#, no-wrap
msgid "[\"Julakan\" ]"
msgstr ""

#. type: YAML Front Matter: hyperlink
#: docs/en/modules/resources/partials/ankataa.com.adoc:1
#, no-wrap
msgid "https://www.ankataa.com"
msgstr ""

#. type: YAML Front Matter: image
#: docs/en/modules/resources/partials/ankataa.com.adoc:1
#, no-wrap
msgid "/images/ankataa.webp"
msgstr ""

#. type: Attribute :tags:
#: docs/en/modules/resources/partials/ankataa.com.adoc:1
#: docs/en/modules/resources/partials/ankataa.com.adoc:18
#: docs/en/modules/resources/partials/jw.org.adoc:1
#: docs/en/modules/resources/partials/jw.org.adoc:19
#, no-wrap
msgid "[ \"jula\", \"language\" ]"
msgstr ""

#. type: YAML Front Matter: title
#: docs/en/modules/resources/partials/ankataa.com.adoc:1
#, no-wrap
msgid "Ankataa"
msgstr ""

#. type: Title =
#: docs/en/modules/resources/partials/ankataa.com.adoc:11
#, no-wrap
msgid "Le site web https://www.ankataa.com[Ankataa]"
msgstr ""

#. type: Attribute :description:
#: docs/en/modules/resources/partials/ankataa.com.adoc:16
#: docs/en/modules/resources/partials/bebiphilip.adoc:16
#: docs/en/modules/resources/partials/bible.adoc:17
#, no-wrap
msgid "Ressources pour apprendre le Jula"
msgstr ""

#. type: Attribute :keywords:
#: docs/en/modules/resources/partials/ankataa.com.adoc:19
#: docs/en/modules/resources/partials/mandenkan.com.adoc:18
#, no-wrap
msgid "Côte d'Ivoire, Ivory Coast, jula, julakan, dioula,"
msgstr ""

#. type: Positional ($1) AttributeList argument for macro 'image'
#: docs/en/modules/resources/partials/ankataa.com.adoc:24
#, no-wrap
msgid "ankataa"
msgstr ""

#. type: Target for macro image
#: docs/en/modules/resources/partials/ankataa.com.adoc:24
#, no-wrap
msgid "ankataa.webp"
msgstr ""

#. type: Table
#: docs/en/modules/resources/partials/ankataa.com.adoc:34
#, no-wrap
msgid ""
"3+|An expert linguist providing online courses, in depth research as well an awesome Youtube channel for learning basic Jula and Bambara. \n"
"Great for Jula learniers in Ivory Coast. (Just substitute 'ka' for ye, and 'le' or 'lo' for 'don'...  and a few others but after all this *is* mandenkan!)\n"
"|Web site:link:https://www.ankataa.com[Ankataa]\n"
"|\n"
"|Youtube channel:link:https://www.youtube.com/channel/UCEQgnXDXNHaAjKA8GJZ3zHw[Na baro kɛ]\n"
msgstr ""
