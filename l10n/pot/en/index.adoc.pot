# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Boyd
# This file is distributed under the same license as the Blog package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Blog 1.0\n"
"POT-Creation-Date: 2021-12-23 20:20+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Attribute :lang:
#: content/en/modules/blog/pages/smoketest.adoc:1
#: docs/en/modules/resources/pages/index.adoc:9
#: docs/en/modules/resources/pages/01-mediaplus.adoc:21
#: docs/en/modules/resources/pages/ankataa.com.adoc:21
#: docs/en/modules/resources/pages/rfi.adoc:19
#: content/en/modules/ROOT/pages/linkedin.adoc:10
#: content/en/modules/ROOT/pages/contact.adoc:10
#: content/en/modules/ROOT/pages/home.adoc:17
#: content/en/modules/ROOT/pages/font-test.adoc:1
#: content/en/modules/ROOT/partials/contact.adoc:10
#, no-wrap
msgid "en"
msgstr ""

#. type: Title =
#: docs/en/modules/resources/pages/index.adoc:5
#, no-wrap
msgid "Jula Study Resources"
msgstr ""

#. type: Attribute :description:
#: docs/en/modules/resources/pages/index.adoc:8
#, no-wrap
msgid "Jula Study Resources "
msgstr ""
