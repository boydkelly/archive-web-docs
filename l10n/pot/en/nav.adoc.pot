# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Boyd
# This file is distributed under the same license as the Blog package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Blog 1.0\n"
"POT-Creation-Date: 2021-12-15 17:55+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#: docs/en/modules/slides/pages/slides.adoc:1
#: docs/en/modules/slides/pages/slides.adoc:6 docs/en/modules/slides/nav.adoc:5
#, no-wrap
msgid "Slides"
msgstr ""

#. type: Plain text
#: docs/en/modules/slides/nav.adoc:6
msgid "xref:julakan:slides:animal-dyu.adoc[Animals]"
msgstr ""

#. type: Plain text
#: docs/en/modules/slides/nav.adoc:7
msgid "xref:julakan:slides:5words.adoc[5 words]"
msgstr ""

#. type: Plain text
#: docs/en/modules/slides/nav.adoc:8
msgid "xref:julakan:slides:proverbs.adoc[Proverbs]"
msgstr ""

#. type: Plain text
#: docs/en/modules/slides/nav.adoc:9
msgid "xref:julakan:slides:colors.adoc[Colours]"
msgstr ""

#. type: Plain text
#: docs/en/modules/slides/nav.adoc:9
msgid "xref:julakan:slides:money.adoc[Money]"
msgstr ""
