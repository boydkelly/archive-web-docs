#!/usr/bin/bash
set -o errexit
set -o nounset
set -o pipefail

for x in lynx ; do
type -P $x >/dev/null 2>&1 || { echo >&2 "${x} not installed.  Aborting."; exit 1; }
done

for x in en fr ; do
mkdir -p locale/$x/LC_MESSAGES
msgfmt -o locale/$x/LC_MESSAGES/${0#./}.mo $x.po
done

export TEXTDOMAIN=$0
export TEXTDOMAINDIR=$(pwd)/locale
# export LANGUAGE=fr
# echo $(gettext "title")

function maindoc (){
LANGUAGE=$1
echo $(gettext "title")

date=`date -I`

page="./docs/$1/modules/ROOT/pages/udhr-morph.adoc"

cat << EOF > "$page"
---
title: $(gettext "title")
author: Boyd Kelly
date: $(gettext "date")
draft: false
tags: $(gettext "tags")
categories: ["Language"]
---

= $(gettext "title")
:author: Boyd Kelly
:page-author: Boyd Kelly
:page-revdate: $date
:page-image: udhr-p.webp
:description: $(gettext "description")
:keywords: $(gettext "keywords")
:tags: $(gettext "tags")
:category: Language
:lang: $x

image:{page-image}[UNHDR,300]

[NOTE]
.$(gettext "see")
====
* xref:bam@julakan:ROOT:udhr.adoc[Hadamaden josiraw dantigɛkan]
* xref:dyu@julakan:ROOT:udhr.adoc[Dunya’’ mumɛ mɔgɔya’’ hakɛyaw dantigɛlikan]
* xref:en@julakan:ROOT:udhr.adoc[Universal Declaration of Human Rights]
* xref:fr@julakan:ROOT:udhr.adoc[Déclaration Universelle des droits de l'homme]
====

[width="100%",cols="25, 25, 25, 25",frame="none",opts="header",stripes="none"]
|====
|$(gettext "english")
|$(gettext "bambara")
|$(gettext "dyula")
|$(gettext "french")
a|
include::{includedir}/udhr-stats.en.adoc[]
a|
include::{includedir}/udhr-stats.bam.adoc[]
a|
include::{includedir}/udhr-stats.dyu.adoc[]
a|
include::{includedir}/udhr-stats.fr.adoc[]
|====
EOF

}

function partial (){
#  echo partial x: $x
#  echo partial stats $1
  export LANGUAGE=$1
  partials="./docs/$x/modules/ROOT/partials"
  udhrstats="./docs/$x/modules/ROOT/partials/udhr-stats.${stats}.adoc"
  printf "image::udhr-morph.${stats}.svg[UDHR,310]\n" > $udhrstats
  printf "[horizontal]\n" >> $udhrstats
  echo  $(gettext "totalwords")":: $total_words" >> $udhrstats
  echo -e $udhrstats
  #[[ "$x" == "fr" ]]  && exit
  echo -e "\n\n" >> $udhrstats
  echo  $(gettext "totalwords")":: $total_words" 
  printf "[horizontal]\n" >> $udhrstats
  echo $(gettext "uniqwords"):: $unique_words >> $udhrstats
  echo -e "\n\n" >> $udhrstats
  printf "\n//- \n\n" >> $udhrstats
  printf "[]\n" >> $udhrstats
  echo "* " $(gettext "samplewords") >> $udhrstats 
  echo -e "\n\n" >> $udhrstats
  input="stop.$stats.txt"
#  [[ "$stats" == "fr" ]] && exit
# ok this is working but it is no longer after the first 100 since we are using stopwords.
#  cat ${tmp} | sed -e "$(sed 's:.*:s/^&$//i:' $input)" | sort | uniq -c | sort -rn | awk -v f=$udhrstats 'BEGIN {OFS=","} NR==1, NR==10 {print ". " $2  >> f}'
 # cat ${tmp} | sed -e "$(sed 's:.*:s/^&$//i:' $input)" | sort -u | shuf | awk -v f=$udhrstats 'BEGIN {OFS=","} NR==1, NR==10 {print ". " $1  >> f}'
  cat ${tmp} | sed -e "$(sed 's:.*:s/^&$//i:' $input)" | sort -u | shuf | awk -v f=$udhrstats 'BEGIN {OFS=","} NR==1, NR==10 {print ". " $1  >> f}'
  printf "\n\n//- \n\n" >> $udhrstats
  printf "[]\n" >> $udhrstats
  echo "* " $(gettext "longestwords") >> $udhrstats
  echo -e "\n\n" >> $udhrstats
  cat ${tmp} | sort | uniq | awk '{print length($1), $1}' | sort -rn | awk 'NR==1, NR==10 {print "** " $2}' >> $udhrstats
}

function analyze () {
  images="./docs/$x/modules/ROOT/assets/images"
  #file in path in all 3 langs
  filepath="./docs/$stats/modules/ROOT/pages/udhr.adoc"
  root=${filepath%pages*}
  file=${filepath##*/}
  base=${file%.*}
  #tmmp is file out below
#  for y in tmp svg txt webp ; do export "${y}=${file%.*}.$x.$y"; done

  tmp=${base}.${stats}.tmp
  txt=${base}.${stats}.txt

  asciidoctor -a includedir=../partials -a skip-front-matter $filepath -o - | lynx -stdin -dump \
    | sed -e "s/[^[:alpha:]’]/ /g; s/[[:space:]]/\n/g" | tr [Ɔ,Ɛ,Ɲ,Ŋ] [ɔ,ɛ,ɲ,ŋ] | tr [:upper:] [:lower:] \
    | sed '/^$/d' | tee $base.${stats}.tmp | sort -u > ${base}.${stats}.txt

  total_words=`cat ${tmp} | wc -l`
  unique_words=`cat ${tmp} | sort | uniq | wc -l`
  new_words=`cat ${tmp} | wc -l`
  #echo for lang stats: $1
  python3 ./minimal.py $stats $base
}

function nowhite (){
  sed -i 's/[ \t]*$//' $1
}

for x in en fr ;
do
# LANGUAGE=$x
# echo $TEXTDOMAIN
# echo $TEXTDOMAINDIR
# echo $LANGUAGE
# echo processing main doc for $x
# echo $(gettext "title")

    maindoc $x

    for stats in en bam dyu fr;
    do
      stopfile="stop.$stats.txt"
      nowhite $stopfile
      analyze $x
      partial $x
    done
   cp *.svg $images/ && rm *.svg

  done
rm udhr.*.tmp
rm udhr.*.txt
rm -fr locale
