#!/usr/bin/env python3

import csv
import sys
import random
import genanki
import os

# Filename of the data file
data_filename = sys.argv[1]

# Filename of the Anki deck to generate
deck_filename = os.path.splitext(data_filename)[0] + '.apkg'

# Title of the deck as shown in Anki
anki_deck_title = "Kó dì ?"

# Name of the card model
anki_model_name = "Kodi"

# Create the deck model

model_id = random.randrange(1 << 30, 1 << 31)

style = """
u {
  text-decoration-style: wavy;
  text-underline-position: under;
  color: white;
  padding: 1px;
}

.card {
  font-family: Noto Sans;
  font-size: 30px;
  color: #495057;
  text-align: center;
  padding: 10px;
}

.upper {
  font-family: Noto Sans;
  font-size: 30px;
  padding: 30px 10px 30px;
}

.lower {
  font-size: 30px;
  padding: 25px 10px 25px;
}

.soundFront {
  position: absolute;
  top: 40px;
  right: 40px;
  font-size: 18px;
  content: url("play.png");
  width: 6%;arialarial
}

.fr {
  border-left: 4px solid #4285f4;
   color: #4285f4;
}

.dyu {
  border-left: 4px solid #f4b400;
  border-color: #f4b400;
}
"""

anki_model = genanki.Model(
    model_id,
    anki_model_name,
    fields=[{"name": "dyu"}, {"name": "fr"}, {"name": "Glossaire"}, {"name": "Document"}],
    templates=[
        {
            "name": "Card 1",
            "qfmt": '<div class="upper dyu">{{dyu}}</div>',
            "afmt": '{{FrontSide}}<hr id="answer"><div class="lower fr">{{fr}}</div>',
        },
        {
            "name": "Card 2",
            "qfmt": '<div class="upper fr">{{fr}}</div>',
            "afmt": '{{FrontSide}}<hr id="answer"><div class="lower dyu">{{dyu}}</div>',
        },
    ],
    css=style,
)

# The list of flashcards
anki_notes = []

with open(data_filename, "r") as csv_file:

    csv_reader = csv.reader(csv_file, delimiter="\t")

    for row in csv_reader:
        anki_note = genanki.Note(
            model=anki_model,
            fields=[row[0], row[1], row[2], row[3]],
        )
        anki_notes.append(anki_note)

# Shuffle flashcards
random.shuffle(anki_notes)

anki_deck = genanki.Deck(model_id, anki_deck_title)
anki_package = genanki.Package(anki_deck)

# Add flashcards to the deck
for anki_note in anki_notes:
    anki_deck.add_note(anki_note)

# Save the deck to a file
anki_package.write_to_file(deck_filename)

print("Created deck with {} flashcards".format(len(anki_deck.notes)))
