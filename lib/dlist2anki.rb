require 'csv'

Asciidoctor::Extensions.register do
  Card = Struct.new(:question, :answer, :gloss_ref, :doc_title)
  tree_processor do
    process do |doc|
      filename = File.basename(doc.attr 'docfile', '.adoc') + '.tsv'
      puts filename
      outdir= ".local/"
      doc_title = doc.attr 'doctitle'
      puts doc_title
      cards = []
        (doc.find_by context: :dlist,  role: 'anki').each do |anki_block|
          gloss_ref = anki_block.title
          #puts gloss_ref 
          #puts anki_block.items[2]
          anki_block.items.each do |(questions, answer_block)|
            question_text = questions.map {|q| q.text.rstrip }.join ' '
            puts question_text
            answer_lines = [answer_block.text]
            answer_text = answer_block.find_by do |node|
              node != answer_block && (node.context == :paragraph || node.context == :list_item)
            end.each do |node|
              answer_lines << (node.context == :paragraph ? node.content : %(- #{node.text}))
            end
            answer_text = answer_lines.join '<br>'
            c = Card.new(question_text, answer_text, gloss_ref, doc_title)
            cards << c
          end
        end

        CSV.open(outdir + filename, 'w', col_sep: "\t") do |csv|
          cards.each do |c|
            csv << [c.question, c.answer, c.gloss_ref, c.doc_title]
             #puts "card created: #{c.question}"
          end
           #puts cards.join ?\n
           puts "\n#{cards.length} cards created, written to #{filename}"
        end
    end
  end
end

