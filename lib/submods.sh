#!/bin/bash

git submodule init dyu-xdxf && git submodule update --remote
#pushd ../../../build/ && git submodule update --remote && popd
#pushd ../../../build/ && git clone content/web-docs-*.git web-docs && popd

[[ -d ../../../build/web-docs ]] && git -C ../../../build/web-docs pull origin main
[[ ! -d ../../../build/web-docs ]] && { pushd ../../../build ;  git clone content/web-docs-*.git web-docs; }
sleep 5
#[[ -d ../../../build/web-docs ]] && git -C ../../../build/web-docs pull origin main
#[[ ! -d ../../../build/web-docs ]] && { pushd ../../../build ;  git clone content/web-docs-*.git web-docs; }
