---
author: 'Boyd Kelly'
categories: ["Technology"]
date: 2022-07-15
draft: 'false'
tags: ["tech", "android", "africa"]
title: 'Better hunspell-fr'
type: post
---

=  Better hunspell-fr
:author: Boyd Kelly
:page-author: Boyd Kelly
:page-revdate: 2022-07-15
:page-role: blog
:page-image: orthographe.webp 
:description: hunspell-fr 7.0 and alternatives
:keywords: hunspell, french, alternatives, "reforme 1990", spellcheck, fedora, suse, centos, mageia, mandriva 
:tags: tech
:category: Technology
:page-tags: hunspell, french, alternatives
:page-alias: index.adoc
:lang: en
include::blog:ROOT:partial$locale/attributes-fr.adoc[]

// image:dictionnaire.jpg["Par heurtelions — Travail personnel, CC BY-SA 4.0, https://commons.wikimedia.org/w/index.php?curid=5294387",400, float="left" ]

NOTE: This version of hunspell-fr is available at https://copr.fedorainfracloud.org/coprs/boydkelly/hunspell-fr/[Fedora copr] rpm Linux versions: Fedora, SUSE, EPEL, CENTOS, Mageia, Mandriva.

== description

The hunspell -fr 7.0 source files included with Fedora provides 3 spelling alternatives.

. classique
. réforme1990
. toutesvariantes

However Fedora only actually installs the toutesvariantes variant and creates a symlink to all the French locales: fr_FR, fr_CA, fr_BE etc. You are not given the choice to use exclusively the 'classique' or 'réforme1990' spelling.

This version of hunspell-fr installs all 3 spelling variants and incorporates the altenatives utility allowing the user to not only select between the variants but also select a diffrent spelling variant for each French language locale.

== Installation

. Install the repo and update:

[source, bash]
----
dnf copr install boydkelly/hunspell-fr
dnf update hunspell-fr
----

. Use the alternatives command to select the spelling variants.

[source, bash]
----
allternatives --config hunspell-fr_CA
----

This will present you with a list where you can choose the spelling variant for the specified locale.

[source, bash]
----
There are 3 programs which provide 'hunspell-fr_CA'.

  Selection    Command
-----------------------------------------------
*+ 1           /usr/share/hunspell/fr-toutesvariantes.dic
   2           /usr/share/hunspell/fr-classique.dic
   3           /usr/share/hunspell/fr-reforme1990.dic

Enter to keep the current selection[+], or type selection number:
----

== Nice work !
