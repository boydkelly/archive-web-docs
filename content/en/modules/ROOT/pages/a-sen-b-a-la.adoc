---
catégories: ["Julakan"]
date: 2018-08-07T07:05:01+00:00
tags: ["jula", "language"]
title: "a sen b'a la"
type: blog
---

= a sen b'a la
:author: Boyd Kelly
:page-revdate: 2018-08-07T07:05:01+00:00
:description: A sen b'a la
:lang: en 
:category: Julakan
:page-tags: jula, language
:keywords: jula, dioula, language,proverb
include::blog:ROOT:partial$locale/attributes-fr.adoc[]

== He is involved !

[cols="1"]
|===
|*a sen b’a la*
|He is involved.  /  He's implicated.
|(He has his foot in it.)
|===
