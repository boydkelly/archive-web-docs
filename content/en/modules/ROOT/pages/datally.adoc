---
catégories: ["Technology"]
date: 2018-05-22T15:24:23Z
tags: ["africa", "android"]
title: Datally
type: blog
---

= Datally
:author: Boyd Kelly
:page-revdate: 2018-05-22T15:24:23Z
:description: A new application on Google Play to manage wifi usage
:page-tags: tech, android, africa
:keywords: "Technology", "android", "africa", "wifi"
:category: Technology
:lang: en 
include::blog:ROOT:partial$locale/attributes-fr.adoc[]

== A new application on Google Play to manage wifi usage

This is a new application from Google, Datally, that helps follow and control the applicaitons that chew up bandwith. Free and with no advertising. A real necessity for 3g/4g connections.

link:https://goo.gl/AKm8BZ[Datally sur Google Play]
