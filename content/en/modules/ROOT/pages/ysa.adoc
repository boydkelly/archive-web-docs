---
catégories: ["English in French"]
date: '2018-05-21 22:46:26 +0000'
tags: ["english", "french", "language" ]
title: 'Yet Still Again ?'
type: blog
---

= Yet Still Again ?
:author: Boyd Kelly
:page-revdate: 2018-05-21 22:46:26 +0000
:description: Yet Still Again ?
:lang: en
:category: English-in-French
:keywords: english", "french", "language" 
include::blog:ROOT:partial$locale/attributes-fr.adoc[]

== Quelle est la différence entre ces trois termes ?

[abstract]
French has a reputation of being more precise than English. And it often is especially when it comes to verbs. However sometimes English can be more precise than French.

== Here is an exam;le

Yet:: Encore
Still:: Encore
Again:: Encore
Even:: Encore (souvent traduit par 'même', mais parfois 'encore')

et encore.footnote:[Dans link:https://www.coastsystems.net/en/post/ysa[la version anglaise] de ce document j'ai traduit 'encore' par 'aussi'.] :

Yet again:: Encore

Oui, parfois on trouve en anglais les deux mots ensemble dans une phrase : '*yet again*...' Qu'est-ce que cela signifie ?

Nous avons non seulement quatre mots en anglais, mais quatre significations différentes avec la même traduction en français. Connaissez-vous la nuance?

.Voici quelques phrases pour vous aider:
****
She hasn't come yet:: Elle n'est pas encore venue
She is yet to come:: Elle n'est pas encore venue
She is still sleeping::  Elle dort encore. (Elle continue à dormir)
She is eating again::  Elle mange encore. (Elle mange de nouveau)
She is going yet again::  Elle va encore. (Elle continue d'y aller une fois de plus)
It's even more difficult than I thought:: C'est encore plus difficile que je ne le pensais.
There's even more to come:: Il y en a encore davantage à venir.
****

[width="100%", cols="2,8", frame="topbot", options="none", stripe="even"]
|===
|Yet|An action that has not (yet) occurred.
|Still|An ongoing action that has not stopped.
|Again|An action which stops and restarts.
|Even|Similar to 'still'|Something has stopped or is no longer but it will be again
|Yet again|An action that reoccurs at least once but probably more.
|===

