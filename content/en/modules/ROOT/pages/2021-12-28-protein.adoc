---
author: 'Boyd Kelly'
categories: ["Technology"]
date: 2021-12-28T18:18:42
draft: 'false'
tags: ["tech", "android", "africa"]
title: 'Protein sources for Ivorian Atheletes'
type: post
---

= Protein sources for Ivorian Athletes 
:author: Boyd Kelly
:page-author: Boyd Kelly
:page-revdate: 2021-12-28
:page-role: blog
:page-image: protein.webp
:description: A few suggestions pour  athletes in  Ivory Coast for foods that are rich in proteins and easily obtainable.
:keywords: protein, health, "Côte d'Ivoire", "Ivory Coast", athletes, food
:tags: ["tech", "santé", "africa"]
:category: Technology
:page-tags: health
:page-alias: index.adoc
:lang: en
include::blog:ROOT:partial$locale/attributes-fr.adoc[]

[abstract]
image:protein.webp[Avocats et oeufs,400,float=left] Although the average individual should consume approximately .8g of protein per kilo per day, athletes should normally consume between 1.2g and 2g of protein per day per kilo. This will also depend of course on the level of physical activity and desired objectives. So an athelete weighing 80kg and who wants to build some muscle mass should consume up to 160g of protein per day. This protein should come from various sources. Here are a few suggestions for athletes in Ivory Coast looking for easily obtainable foods rich in proteins.

== Animal sources

=== Milk
Milk (non sweetened of course!!), is a very good source of protein. One cup of whole milk contains 8g of protein. Have unsweetened yogurt with fruits.

Powdered milk merits a special note. This milk contains two types of protein. 80% casein, and 20% whey protein. The 80% casein, digests slower and is recommended to take at night to allow your body to build muscle.

[TIP]
Many athletes overspend on protein powders, but the best powder is low cost right in the supermarket. Make sure to the the skimmed powderd milk with 0% fat content. It is possible to make very good yoghurt with this milk.

=== Chicken

Ah yes... but the African chickens are better than those stuffed with growth hormones. Prefer the white meat and leave the skin (source of unnecessary fat)!

=== Tuna and other fish

Garba, (with as little oil as possible), accompanied with a good salad, est an excellent meal for an athlete! (But sorry the onions and traditional side accompaniments don't count as 'salad'. You need a good serving vegetables!)

=== Beef

100g of beef contains 28g of protein. Le choucouya, could be an easy source of protein to attain a daily goal, However the fat content is not acceptable. Better to find barbecued meat prepared with no added oil.

=== Eggs

Eggs are one of the cheapest sources of protein. One egg (100F) contains 12.6 grams of protein. Avoid eating omelets at the kiosque that are prepared with a lot of oil. Prefer boiled eggs on the roadside with a small amount of salt and kan kan kan. (no magi poison powder.)

== Vegetable Sources

=== Spinach

Spinach constitutes one of the best foods in Ivory Coast. One single cup of cooked spinache contains 5.6g of protein. But that's not all. Spinach contains so many vitamines and minerals, if you eat this regularly you will need no supplements and no energizers from your pharmacy!

.Spinach is incredible!
[width="100%", cols="3", frame="topbot", opts="header", stripes="even"]
|====
|Calcium, Ca 
|(mg)
|244.8
 
|Fer, Fe 
|(mg)
|6.43
 
|Magnèse, Mg 
|(mg)
|156.6
 
|Phosphore, P 
|(mg)
|100.8
 
|Potassium, K 
|(mg)
|838.8
 
|Sodium, Na 
|(mg)
|126
 
|Zinc, Zn 
|(mg)
|1.37
 
|Cuivre, Cu 
|(mg)
|0.31
 
|Manganèse, Mn 
|(mg)
|1.68
 
|Selenium, Se 
|(mcg)
|2.7
 
|Vitamine A, 
|(IU)
|18865.8
 
|Carotène, beta 
|(mcg)
|11318.4
 
|Vitamine E 
|(mg)
|3.74
 
|Lutein + zeaxanthin 
|(mcg)
|20354.4
 
|Vitamine C
|(mg)
|17.64
 
|Thiamine 
|(mg)
|0.17
 
|Riboflavine 
|(mg)
|0.42
 
|Niacine 
|(mg)
|0.88
 
|Acide pantothenique 
|(mg)
|0.26
 
|Vitamine B-6
|(mg)
|0.44
 
|Foliate
|(mcg)
|262.8
 
|Vitamine K
|(mcg)
|888.48
|====

=== Peanuts

Peanuts are everywhere! Instead of eating candies and cakes, eat peanuts. For 100g of peanuts you are taking in 25.8g of protein. Excellent!

[TIP]
A leafy spinach and peanut sauce, accompanied with chicken, beef or fish is an excellent source of protein for an athlete.

=== Lentils, Red beans, White beans

Available dried at the market, and canned at the supermarket, all beans are very rich in protein. One cup of these legumes may contain between 6 and 15g of protein. Replace rice with lentils!

=== Avocadoes

Not only delicious and source of essential oils, an avocado contains 4g of protein. Essential ingrediant for your salad.

=== Chick Peas

These are available in cans in most supermarkets, as well as dried especially in the Lebanese stores. These 'peas' are very high in protein with 19g of protein per 100g of peas.

[TIP]
For those who are short on time... Mix one can of chick peas, one can of green beans, a small chopped onion, tomatoes, and avocados and you have an excellent vegetarian meal full of protein.

