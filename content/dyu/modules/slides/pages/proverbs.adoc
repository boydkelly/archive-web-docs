---
author: 'Boyd Kelly'
date: 2021-11-01
layout: slides
title: Ntalenw
---

= Ntalen julakan na
:description: Kodiwari ntalen lon o lon
:page-author: 'Boyd Kelly'
:page-revdate: 2021-11-01 
:page-layout: slides
:page-lang: dyu 
:page-pagination: false
:page-tags: jula, farafin, dyu, kalan, yirali, ntalen
:keywords: jula, farafin, dyu, kalan, yirali, ntalen 
:category: slides

++++
<iframe src="https://slides-dyu.coastsystems.net/proverbs.html" width="100%" frameborder="0" allowfullscreen="allowfullscreen" allow="geolocation *; microphone *; camera *; midi *; encrypted-media *"></iframe>
++++
