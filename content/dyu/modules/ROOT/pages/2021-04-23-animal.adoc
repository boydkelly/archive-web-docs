---
author: 'Boyd Kelly'
catégories: ["Julakan"]
date: 2021-04-23
draft: 'false'
image: /images/animal.svg
tags: ["référence", "julakan"]
title: 'Kodiwari biganw julakan na'
type: blog
---

= Kodiwari biganw julakan na 
:author: Boyd Kelly
:page-revdate: 2021-04-23
:description: Kodiwari biganw julakan na
:filename: 2021-04-23-animal.adoc
:keywords: baganw,  Kodiwari
:page-tags: reference,jula,dioula,dyula,animals
:category: Julakan
:lang: dyu 
include::blog:ROOT:partial$locale/attributes-fr.adoc[]

image::animal.svg["biganw bɛɛ", width=100%,role="left"]

.Kodiwari biganw julakan na 
[width="100%", cols="3", format=tsv,frame="bottom",options="header",stripes="even"]
|====
include::ROOT:partial$animal.fr.tsv[]
|====
