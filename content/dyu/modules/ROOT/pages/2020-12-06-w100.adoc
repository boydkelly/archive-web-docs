---
author: 'Boyd Kelly'
catégories: ["Julakan"]
date: 2020-12-06
draft: 'false'
image: /images/w100.svg
tags: ["référence", "julakan"]
title: 'Kumaden kɛmɛ minw ka teli ka fɔ kodiwari julakan na'
type: blog
---

= Kumaden kɛmɛ minw ka teli ka fɔ kodiwari julakan na 
:author: Boyd Kelly
:page-author: Boyd Kelly
:page-revdate: 2020-12-06
:description: Kumaden kɛmɛ minw ka teli ka fɔ kodiwari julakan na 
:page-image: w100.svg
:page-tags: référence, julakan
:keywords: jula, julakan, kumadenw
:category: Julakan
:lang: dyu 
include::blog:ROOT:partial$locale/attributes-fr.adoc[]

image::w100.svg["image", width=100%]

.Les mots en ordre de fréquence.footnote:[Kodi, Editions Africaines, 1981]
[width="100%", cols="10,20,20,20,30", format=tsv,frame="bottom",options="header",stripes="even"]
|====
include::ROOT:partial$w100.tsv[]
|====

