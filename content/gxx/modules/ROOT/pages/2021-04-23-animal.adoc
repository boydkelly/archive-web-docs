---
title: Nmian an' 'ɲnɩn 'dhe ghwɛɛ'ghwlu 'dhi 
author: Guéi Richard 
date: 2021-04-23
image: /images/animal.gxx.svg
draft: false
type: blog
tags: ["référence", "gxx", "guéré", "afrique", "Côte d'Ivoire"]
catégories: ["Julakan"]
---

= nmian an' 'ɲnɩn 'dhe ghwɛɛ'ghwlu 'dhi 
:author: Guéi Richard 
:date: 2021-04-23
:draft: false
:description:
:filename: 2021-04-23-animal.adoc
:imagesdir: /assets/images/
:keywords: gxx, guéré,
:tags: ["référence", "gxx", "guéré", "afrique", "Côte d'Ivoire"]
:catégories: ["Julakan"]
:lang: gxx 
include::ROOT:partial$locale/attributes.adoc[]

image::animal.gxx.svg[image,width=100%,role="left"]

.nmian an' 'ɲnɩn 'dhe ghwɛɛ'ghwlu 'dhi
[width="80%",cols="2",format=csv,frame="bottom",options="header",stripes="even"]
|====
include::ROOT:partial$animal.gxx.csv[]
|====

