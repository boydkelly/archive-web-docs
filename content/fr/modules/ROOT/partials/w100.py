#!/usr/bin/env python3

import os
import csv
import pandas as pd
from os import path
from wordcloud import WordCloud

# get data directory (using getcwd() is needed to support running example in generated IPython notebook)
#d = path.dirname(__file__) if "__file__" in locals() else os.getcwd()

# Read the whole text.
#text = open(path.join(d, 'w100.tsv')).read()

#tsv_file = open("w100.tsv")
#reader = csv.reader(tsv_file, delimiter="\t")

#reader = csv.reader(open('w100.tsv', 'r',newline='\n'),delimiter="\t")
#reader = csv.reader(open('wordcount.csv', 'r',newline='\n'))

df = pd.read_csv('w100.tsv', usecols=[0,1], header=1, sep="\t")
df.columns =['freq', 'word']
df=df[['word','freq']]
records = df.to_dict(orient='records')
data = {x['word']: x['freq'] for x in records}

# Generate a word cloud image
#wordcloud = WordCloud().generate(text)
wc = WordCloud(background_color="white", font_path="/usr/share/fonts/google-noto/NotoSans-Medium.ttf", max_words=1000)
wc.generate_from_frequencies(data)

wc_svg = wc.to_svg(embed_font=True)
f = open("output.svg","w+")
f.write(wc_svg )
f.close()

# Display the generated image:
# the matplotlib way:
import matplotlib.pyplot as plt
# lower max_font_size
#wordcloud = WordCloud(max_font_size=40).generate(text)
plt.figure()
plt.imshow(wc, interpolation="bilinear")
plt.axis("off")
plt.show()

# The pil way (if you don't have matplotlib)
# image = wordcloud.to_image()
# image.show()
