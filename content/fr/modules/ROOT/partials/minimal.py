#!/usr/bin/env python
import os

from os import path
from wordcloud import WordCloud

# get data directory (using getcwd() is needed to support running example in generated IPython notebook)
d = path.dirname(__file__) if "__file__" in locals() else os.getcwd()

# Read the whole text.
text = open(path.join(d, 'temp.txt')).read()

# Generate a word cloud image
#wordcloud = WordCloud().generate(text)

wc = WordCloud(background_color="white", stopwords="stopwords.txt", font_path="/usr/share/fonts/google-noto/NotoSans-Medium.ttf", max_words=1000)
wc.generate(text)

wc_svg = wc.to_svg(embed_font=True)
f = open("output.svg","w+")
f.write(wc_svg)
f.close()
# Display the generated image:
# the matplotlib way:
import matplotlib.pyplot as plt

# lower max_font_size
plt.figure()
plt.imshow(wc, interpolation="bilinear")
plt.axis("off")
plt.show()

