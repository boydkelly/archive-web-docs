---
date: 2018-05-21
menu:
  main:
    weight: 10
slug: about
title: 'Qui suis-je ?'
translationkey: about
type: page
---

= Qui suis-je ?
:author: Boyd Kelly
:date: 2018-05-21T21:18:02Z
:description: A propos de Coast Business Technologies
:lang: fr
include::ROOT:partial$locale/attributes.adoc[]

En sommaire, j'ai fait du bénévolat extensif en afrique, tout en travaillant avec la technologie. J'ai supervisé departement de support technique pour le Bureau des Passports du Canada à l'ouest et ensuite travaillé dans le departement informatique d'une grande société de logiciel.

Au cours de route j'ai hébergé des sites web, géré les serveurs mail et DNS et enseigné les cours de Microsoft Exchange à l'Université de la columbie-britanique. Avec beaucoup d’expérience dans les technologies virtuelles sur Linux, Amazon et GCE, je peux travailler à distance avec pleine confiance.

Ce site web static est hébérgé sur Gitlab, créée par moi avec http://www.asciidoctor.org[Asciidoctor], http://hugo.io[Hugo], et http://neovim.io[neovim] . Le DNS est hébergé chez Amazon Route 53.

++++
<iframe src="https://docs.google.com/forms/d/e/1FAIpQLSeSaw5q-3yyLxe8mH5WNYHDJ8TgUFOaQNzvJdXRx4SJdc-QmA/viewform?embedded=true" width="700" height="1000" frameborder="0" marginheight="0" marginwidth="0"></iframe>
++++
