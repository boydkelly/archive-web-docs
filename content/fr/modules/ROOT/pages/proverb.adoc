---
title: "Qui se sent morveux..."
date: 2020-06-06
author: Molière 
draft: false
lang: fr 
type: blog
tags: ["proverbe", "français"]
catégories: ["Anglais en français"]
---

= Qui se sent morveux se mouche 
:page-revdate: 2020-06-06
:description: proverbe
:page-tags: proverbe, français
:keywords: proverbe, français
:category: Anglais-en-français

[quote]
____
Qui se sent morveux se mouche.
____
