---
title: Datally
date:  2018-05-22T15:24:23Z
type: blog
tags: ["afrique", "android"]
catégories: ["Technologie"]
---

= Datally
:author: Boyd Kelly
:page-revdate: 2018-05-22T15:24:23Z
:description: Application pour gérer le wifi
:page-tags: tech, android, afrique
:keywords: technologie, android, afrique, wifi
:category: Technologie
:lang: fr 
include::blog:ROOT:partial$locale/attributes-fr.adoc[]

== Nouvelle application de Google pour gérer l'usage wifi

Voici une nouvelle application Datally, de Google qui permet de suivre et de contrôler les applications qui mangent la volume internet.  Gratuit et sans pub.  Une nécessité pour les connections 3g/4g.

link:https://goo.gl/AKm8BZ[Datally sur Google Play]
