#!/usr/bin/bash
#Before runing this script ensure the doc has a valid asciidoctor title
#then set the type lang and author defaults

export sectnumlevels=2
export toc="true"
export includedir="include"

[[ -z "$1" ]] && { echo "need a file" ; exit 1 ; }
sed -n '/^=\s/q1' "$1" && { echo "this looks like it doesn't have a title" ; exit 1 ; }
export file=$1
#delete blank lines
sed -i '10,19{/^[[:space:]]*$/d}' "$1" 

export title=`sed -n /^\=\ .*/p "$1"` 
export author=`awk /^author:/{'first = $1; $1=""; print $0'} "$1" |sed 's/^ //g'`
export categories=`awk /^categories:/{'first = $1; $1=""; print $0'} "$1" |sed 's/^ //g'`
export tags=`awk /^tags:/{'first = $1; $1=""; print $0'} "$1" |sed 's/^ //g'`
export image=`awk /^image:[[:space:]]/{'first = $1; $1=""; print $0'} "$1" |sed 's/^ //g' | sed 's/\/images\///g'`

[[ -z $author ]] && author="Boyd Kelly"
export date=`awk '/date:/{print $2}' "$1" | uniq` 
[[ -z "$date" ]] && date=`date -Im`
export type="blog"
export lang="fr"
export draft="true"
[[ -z $categories ]] && categories="[]"
[[ -z $tags ]] && tags="[]"

#start the asciidoctor attributes
sed -i '/^:author:.*/d' "$1" 
sleep 1
sed -i "/^=\s/a :author: $author" "$1"
#[[ ! $(awk '/^:date:/{print $2}' "$1") ]] && sed -i "/^:author:/a :date: $date" "$1"
sed -i '/^:page-author:.*/d' "$1"
sleep 1
sed -i "/^:author:/a :page-author: $author" "$1"
sleep 1
sed -i '/^:date:.*/d' "$1"
sleep 1
sed -i "/^:page-author:/a :page-revdate: $date" "$1"
sleep 1
sed -i '/^:page-revdate:.*/d' "$1"
sleep 1
sed -i "/^:page-author:/a :page-revdate: $date" "$1"
sleep 1
sed -i '/^:page-image:.*/d' "$1"
sleep 1
sed -i "/^:page-revdate:/a :page-image: $image" "$1"
sleep 1
sed -i '/^:page-tags:.*/d' "$1"
sleep 1
echo $tags
sed -i "/^:keywords:/a :page-tags: $tags" "$1"
sleep 1

#if [ ! $lang = "en" ]; then
#  sed -n '/^:lang:\s/q1' "$1" && sed -i "/^:sectnumlevels:/a :lang: $lang" "$1" 
#sleep 0.45 
#  sed -i '/include::locale/d' "$1"
#  sed -i '/^:lang:/a include::locale/attributes.adoc[]' "$1" 
#sleep 0.45 
#  #sed -i "/^include::locale/a #;" "$1" && sed -i 's/^#;//g' "$1"
#fi

#sed -i "/^\=\ .*/a #;" "$1"
#sed -i '18 a #;' "$1" && sed -i 's/^#;//g' "$1"
# 

#!/usr/bin/bash
#[[ -z $1 ]] && { echo "need a file" ; exit 1 ; }
#sed -n '/^---\s/q0' $1 || { echo "this looks like it already has a header" ; exit 1 ; }
#this returns 0 when its NOT found.   
#sed -r -n '/^---(\s|$)/q1' "$1" || { echo " This looks like it already has a header" ; exit 1 ;  }
