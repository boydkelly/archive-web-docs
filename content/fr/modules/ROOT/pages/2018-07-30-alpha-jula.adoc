---
title: Guide de prononciation Jula pour les francophones
author: Boyd Kelly
date:  2018-07-30T22:14:31Z
type: blog
keywords: [jula, julakan, dioula, dyula, dyu, international, internationale, keyboard, clavier, android, baoule, "ivory coast", "côte d'Ivoire", bete, senoufo, language, langues afrique, africa, technologie, technology, multilingual, multilingue, français, french, anglais, english, linux]
image: /images/blackboard.webp
tags: ["jula", "langue","référence"]
catégories: ["Julakan"]
hyperlink: https://www.coastsystems.net/fr/page/2018-07-30-alpha-jula/
aliases: [ "fr/page/2018-07-30-alpha-jula/" ]   
menu: 
  links:
    title: Guide de prononciation Jula pour les francophones
---

= Guide de prononciation Jula pour les francophones
:author: Boyd Kelly
:page-revdate: 2018-07-30T22:14:31Z
:description: Guide de prononciation Jula pour les francophones
:lang: fr
:page-image: blackboard.webp
:page-tags: jula, langue
:keywords:  jula, julakan, dioula, dyula, dyu, international, internationale, keyboard, clavier, android, baoule, ivory coast, côte d'Ivoire, bete, senoufo, language, langues afrique, africa, technologie, technology, multilingual, multilingue, français, french, anglais, english, linux 
:category: Julakan
include::blog:ROOT:partial$locale/attributes-fr.adoc[]

.Guide de prononciation Jula
[width="100%",cols="5",frame="topbot",options="header",stripes="even"]
|====
| Lettre | Symbol IPA | Prononciation française | Exemple Jula | Sens français

| a
| a
| papa
| saga
| mouton

| aa
| ɑ 
| pâte
| baara
| travail

| an
| ɑ̃
| sans 
| sanji
| pluie

| b
| b
| bol
| baga
| bouillie

| c
| tʃ 
| match
| cɛ
| homme

| d
| e
| dos
| disi
| poitrine

| e
| e
| école
| bese
| machète

| ee
| e:
| féerie
| feere
| vendre

| en
| brin
| ɛ̃
| sen
| pied

| ɛ
| ɛ
| mère
| kɛnɛya
| santé 

| ɛɛ
| ɛ:
| fête
| mɛɛn
| durer

| ɛn
| ɛ̃
|
| bɛn
| entente

| f
| f
| fer
| foro
| champ

| g
| g
| gant
| mɔgɔ
| personne

| gw ou gb
| gw ou gb
| gw ou gb
| gwɛlɛ ou gbɛlɛ
| hard 

| h
| h
| hello.footnote:[anglais, aspiré]
| hakili
| intelligence

| i
| i
| lit
| misi
| boeuf

| ii
| i:
| i allongé
| miiri
| pensée

| in
| ĩ  
| i nasaliséfootnote:[à ne pas confondre avec 'brin' /ɛ̃/]
| min
| boire

| j
| https://upload.wikimedia.org/wikipedia/commons/1/1d/Voiced_palatal_plosive.ogg[ɟ] où dʒ
| Abidjan
| ji
| eau

| k
| k
| kiosque
| kitabu
| livre

| l
| l
| lame
| lolo
| étoile

| m
| m
| main
| maro
| riz

| n
| n
| nuage
| nɔnɔ
| lait

| ɲ
| ɲ
| oignon
| ɲɔ
| mil

| ŋ
| ŋ 
| parkingfootnote:Approximative[Approximative]
| ŋɔni
| épine

| o
| o
| mot
| bolo
| bras

| oo
| o:
| o allongé
| looru
| cinq

| on
| õ
| nomfootnote:Approximative[]
| bon
| maison

| ɔ
| ɔ
| sort
| sɔrɔ
| trouver

| ɔɔ
| ɔ:
| ɔ allongé
| fɔɔnɔ
| vomir

| ɔn
| ɔ̃
| an/monfootnote:Approximative[]
| dɔn
| danser

| p
| p
| pont
| pan
| sauter

| r
| r
| perofootnote:[espagnol]
| bɔrɔ
| sac

| s
| s
| sauce
| sara
| payer

| t
| t
| table
| toto
| rat

| u
| u
| loup
| kura
| nouveau

| uu
| u:
| footballfootnote:Approximative[]
| suuri
| baisser

| un
| ũ
| approximative
| kunkolo
| tête

| v
| v
| vol
| votekɛfootnote:[emprunt du français] 
| vote 

| w
| w
| doigt
| wari
| argent

| y
| j
| papaye
| yan
| ici 

| z
| z
| zoo
| zɔnzɔn
| crevette
|====
