#!/usr/bin/bash
awk -v infile=$1 '
    BEGIN { outfile = infile".tmp" }
    /^:page-tags:/ {
        gsub( /"/, " ", $0 );
        gsub( /[[:blank:]],/, ",", $0 );
        gsub( /\]/, "", $0 );
        gsub( /\[/, "", $0 );
    }
    { print $0 }
' $1 > $1.tmp
mv $1.tmp $1
sleep 1
awk -v infile=$1 '
    BEGIN { outfile = infile".tmp" }
    /^:keywords:/ {
        gsub( /"/, " ", $0 );
        gsub( /[[:blank:]],/, ",", $0 );
        gsub( /\]/, "", $0 );
        gsub( /\[/, "", $0 );
    }
    { print $0 }
' $1 > $1.tmp
mv $1.tmp $1
