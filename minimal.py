#!/usr/bin/env python3
import sys
import os

from os import path
from wordcloud import WordCloud

# get data directory (using getcwd() is needed to support running example in generated IPython notebook)
d = path.dirname(__file__) if "__file__" in locals() else os.getcwd()

lang = sys.argv[1]
base = sys.argv[2]
text = base + "." + lang + ".tmp" 
stop = "stop." + lang + ".txt" 

#print (stop)
# Read the whole text.
text = open(path.join(d, text)).read()


stopwords=set()
with open(stop, "r") as file:
    for line in file:
        stripped_line = line.strip()
        stopwords.add(stripped_line)

# Generate a word cloud image

wc = WordCloud(background_color="black", stopwords=stopwords,
        collocations=False,
        min_word_length=3,
        repeat=False,
        relative_scaling=.2,
        font_path="/usr/share/fonts/google-noto/NotoSans-Medium.ttf", max_words=20)

wc.generate(text)

output = base + "-morph." + lang + ".svg" 

wc_svg = wc.to_svg(embed_font=True)
f = open(output,"w+")
f.write(wc_svg)
f.close()

# Display the generated image:
# the matplotlib way:
#import matplotlib.pyplot as plt

# lower max_font_size
#plt.figure()
#plt.imshow(wc, interpolation="bilinear")
#plt.axis("off")
#plt.show()

