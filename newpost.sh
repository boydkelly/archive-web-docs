#!/usr/bin/bash
# You need po4a > 0.54, see https://github.com/mquinson/po4a/releases
# There is no need of system-wide installation of po4a
# Usage: PERLLIB=/path/to/po4a/lib make_pot.sh
# you may set following variables
# SRCDIR root of the documentation repository
# POT_DIR place where to create the pot

ME="Boyd Kelly"
PACKAGE="Blog"
VERSION="1.0"
file=$1
[[ -z $file ]] && { echo specify file ; exit 1 ; } ; 
source="fr"

# root of the documentation repository
SRC_DIR="./content"
content="./docs"

# place where to create the pot files
[[ -z "$POT_DIR" ]] && POT_DIR="./l10n/pot"

# place where the po files are
[[ -z "$PO_DIR" ]]	&& PO_DIR="./l10n/po"

if [ ! -d "$SRC_DIR" ] ; then
    echo "Error, please check that SRC_DIR match to the root of the documentation repository"
    echo "You specified modules are in $SRC_DIR"
    exit 1
fi

#all the adocs under the source language
    basename=$(basename -s .adoc "$file")
    dirname=$(dirname "$file")
    path=$(dirname ${file#${content#./}/})
    subdir=${path#$source}
    pot_file="$POT_DIR/$source$subdir/$basename.pot"
    pot_file="$POT_DIR/$source/$basename.adoc.pot"
    echo dirname $dirname
    echo src_dir $SRC_DIR
    echo path $path
    echo subdir $subdir
echo potfile $pot_file
echo master $file
    #create (or replace) pot file from source
    po4a-gettextize \
        --format asciidoc  \
        --master "$file" \
        --master-charset "UTF-8" \
        --copyright-holder "$ME" \
        --package-name "$PACKAGE" \
        --package-version "$VERSION" \
        --verbose \
        --po "$pot_file"

    #update translated po files for each target language
    for target in $(ls "$PO_DIR" ); do
      echo target $target
      echo source $source
      if [ ! $target == $source ] ; then
      #not needed localized for now
    localized_file="$SRC_DIR/$target$subdir/$basename.adoc"
    po_file="$PO_DIR/$target/$basename.adoc.po"
    echo target $target
    echo master $file
    echo pofile $po_file
        # po4a-updatepo would be angry otherwise
        # sed -i 's/Content-Type: text\/plain; charset=CHARSET/Content-Type: text\/plain; charset=UTF-8/g' "$po_file"
        # add the source lang. 
        # echo $target create or update po file for each target language 
        if ! po4a-updatepo \
            --format asciidoc \
            --master "$file" \
            --master-charset "UTF-8" \
            --copyright-holder "$ME" \
            --package-name "$PACKAGE" \
            --package-version "$VERSION" \
            --po "$po_file" ; then
        echo ""
        echo "Error updating $target PO file for: $file"
      else
        #poedit and gtranslate will not let you change the source language; and we already know the target so set that too.
        sed -i '/X-Source-Language:.*/d' $po_file &&  sed -i "/Content-Type:/a \"X-Source-Language: $source $(printf '%q' '\n')\"" $po_file 
        sed -i "s/^\"Language:.*$/\"Language: $target $(printf '%q' '\n')\"/g" $po_file 
        echo Updated $po_file
        fi
      fi
    done

#==== "REMOVE TEMPORARY FILES"
find "$PO_DIR/" -name *.po~ -delete
