#!/usr/bin/bash
destdir=images
workdir=chapters/dyu
imagesdir=../../images
stopwords=../../stopwords.txt
fontfile=../../_resources/fonts/NotoSans-Regular.ttf
pushd $workdir

while IFS= read -r -d '' file
do
  echo $file
  sed -n '/^=\s/q1' "$file" && { echo "this looks like it doesn't have a title" ; exit 1 ; }
  #add footer
  sed -i -e '/^include::footer.*$/d' ${file} && sed -i -e '$ainclude::footer.adoc[]' ${file} 
  sed -i '/^image:chapter.*$/d' "$file" &&  sed -i "/^=\s/a image:${file%.*}.png[image,width=100%]" "$file"
  sed -i '1 a #;' "$file" && sed -i 's/^#;//g' "$file"
  #compress muliple blank lines to one
  sed -i -e '/./b' -e :n -e 'N;s/\n$//;tn' ${file}
  wordcloud_cli --fontfile $fontfile --min_word_length 2 --text $file --stopwords $stopwords --background "white" --mode RGBA --max_words 100 --imagefile "$imagesdir/${file%.*}.png"
done <   <(find * -name "chapter*.adoc" -print0)
popd
