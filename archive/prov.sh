#!/bin/bash
in=./data/proverbs.tsv
out=./public/proverb.txt

for x in shuf ls; do
type -P $x >/dev/null 2>&1 || { echo >&2 "${x} not installed.  Aborting."; exit 1; }
done

shuf -n 1k $in | awk -F"\t" '{ print $0 }' > $out
