#!/usr/bin/bash
#!/bin/bash

#set -x

for x in en fr ; do
mkdir -p locale/$x/LC_MESSAGES
msgfmt -o locale/$x/LC_MESSAGES/${0#./}.mo $x.po
done

./test.sh

export TEXTDOMAIN=$0
export TEXTDOMAINDIR=$(pwd)/locale
#echo $TEXTDOMAIN
#echo $TEXTDOMAINDIR


export LANGUAGE=fr
echo $(gettext "title")

./test.sh

export LANGUAGE=en
./test.sh
rm -fr locale
