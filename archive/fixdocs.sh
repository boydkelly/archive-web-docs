#!/usr/bin/bash
#find . -name "*.adoc" -exec sed -i -e "1,20{s/'\[/\[ /g; s/\]'/\]/g; s/''//g}" {} \;
#find . -name "*.adoc" -exec sed -i -e "1,20{s/catégo/catego/g}" {} \;
X=page-tags

find . -name "*.adoc" -exec sed -i -e '/page-tags/ s/\[//g;/page-tags/ s/\]//g; /page-tags/ s/"//g' {} \;
find . -name "*.adoc" -exec sed -i -e '/:keywords:/ s/\[//g;/:keywords:/ s/\]//g; /:keywords:/ s/"//g' {} \;
