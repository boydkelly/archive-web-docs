#!/usr/bin/bash
for x in content docs ; do
pushd $x 
find . -name "*.adoc" -exec sed -i -e "1,20{s/'\[/\[ /g; s/\]'/\]/g; s/''//g}" {} \;
find . -name "*.md" -exec sed -i -e "1,20{s/'\[/\[ /g; s/\]'/\]/g; s/''//g}" {} \;
popd
done
