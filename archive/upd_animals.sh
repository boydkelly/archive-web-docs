#!/usr/bin/bash
for lang in fr en dyu; do
  #lang symbol in filename for slides
  tsv=~/dev/web-docs/content/$lang/partials/animal.$lang.tsv
  csv=~/dev/web-docs/content/$lang/partials/animal.$lang.csv
  anki_tsv=~/Anki/brainbrew/Jula/src/data/Main.tsv
  case $lang in
    en)
      source="Dyula"
      target="English"
      translate=63
      ;;
    fr)
      source="dioula"
      target="français"
      translate=5
      ;;
    dyu)
      source="Julakan"
      target="Tubabukan"
      translate=5
      ;;
  esac
  printf '%s\t%s\n' $source $target > $tsv
  awk -v field=$translate -F "\t"  'BEGIN{OFS="\t"}($64 ~ /animal/){print $2, $field}' ${anki_tsv} | sort -k1 >> $tsv
  printf '%s,%s\n' $source $target > $csv
  awk -v field=$translate -F "\t"  'BEGIN{OFS=","}($64 ~ /animal/){print $2, $field}' $anki_tsv | sort -k1 >> $csv
  #> $tsv
done
